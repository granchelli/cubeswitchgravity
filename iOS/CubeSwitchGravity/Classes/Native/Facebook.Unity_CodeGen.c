﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Object Facebook.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_mD457D1E5A4D052EFDE5216A6D29E6A5173CFC129 ();
// 0x00000002 System.String Facebook.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m7A8C1B1AE5AD8547DE3B0A5FA67C4DF79A097AE0 ();
// 0x00000003 System.Void Facebook.MiniJSON.Json::.cctor()
extern void Json__cctor_mF29D657C09E73D9144F1AA429A97388241BB039A ();
// 0x00000004 System.Void Facebook.MiniJSON.Json_Parser::.ctor(System.String)
extern void Parser__ctor_m774D5188FD4910AD900F1EA7EA8FCCF1F14B4219 ();
// 0x00000005 System.Char Facebook.MiniJSON.Json_Parser::get_PeekChar()
extern void Parser_get_PeekChar_m318D1925B32A148068BAED86C52BB1E3BE1684A3 ();
// 0x00000006 System.Char Facebook.MiniJSON.Json_Parser::get_NextChar()
extern void Parser_get_NextChar_m3C8D7E32AF4450D24FE19448704394EAEC7757FF ();
// 0x00000007 System.String Facebook.MiniJSON.Json_Parser::get_NextWord()
extern void Parser_get_NextWord_m319391DE7A33D2D492C0F34F3C66B884CED4EC01 ();
// 0x00000008 Facebook.MiniJSON.Json_Parser_TOKEN Facebook.MiniJSON.Json_Parser::get_NextToken()
extern void Parser_get_NextToken_m99B8BB73199B24DE25ADB808FCB6D73E300ABC1E ();
// 0x00000009 System.Object Facebook.MiniJSON.Json_Parser::Parse(System.String)
extern void Parser_Parse_m11269AA6FC5AEC43D48761292A9E3183661C620D ();
// 0x0000000A System.Void Facebook.MiniJSON.Json_Parser::Dispose()
extern void Parser_Dispose_m12A54D23F022479AF8413C7A396198887830F6A6 ();
// 0x0000000B System.Collections.Generic.Dictionary`2<System.String,System.Object> Facebook.MiniJSON.Json_Parser::ParseObject()
extern void Parser_ParseObject_mBDCCE4D00E8715B1CD5BF7EF8481B513C22E5020 ();
// 0x0000000C System.Collections.Generic.List`1<System.Object> Facebook.MiniJSON.Json_Parser::ParseArray()
extern void Parser_ParseArray_m44D0E4652B03ABF3555C88FDE4618A97ED1E0F88 ();
// 0x0000000D System.Object Facebook.MiniJSON.Json_Parser::ParseValue()
extern void Parser_ParseValue_mBF5D02EF66E7837C5A2064C37B01963062079B7D ();
// 0x0000000E System.Object Facebook.MiniJSON.Json_Parser::ParseByToken(Facebook.MiniJSON.Json_Parser_TOKEN)
extern void Parser_ParseByToken_m668183645AF32273B0B973C72B1DE0BD22B4C98B ();
// 0x0000000F System.String Facebook.MiniJSON.Json_Parser::ParseString()
extern void Parser_ParseString_m811F635CE7678BED3ED10B931CDC6792B8090FBE ();
// 0x00000010 System.Object Facebook.MiniJSON.Json_Parser::ParseNumber()
extern void Parser_ParseNumber_m970BA8742514F0F851697D43D6DFFC68FB462A0E ();
// 0x00000011 System.Void Facebook.MiniJSON.Json_Parser::EatWhitespace()
extern void Parser_EatWhitespace_m593814103F5069BC08E4B3BD1C1ABD943DDE4FAB ();
// 0x00000012 System.Void Facebook.MiniJSON.Json_Serializer::.ctor()
extern void Serializer__ctor_m08B6F57E91D667D48003A087D00209947F5DF8A4 ();
// 0x00000013 System.String Facebook.MiniJSON.Json_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mAB4A450BFFA0BA3B6EA4FA1343D2F0FE27CDF0C6 ();
// 0x00000014 System.Void Facebook.MiniJSON.Json_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m44DF1E1239708E84DED26228E0B19FD31F2F2814 ();
// 0x00000015 System.Void Facebook.MiniJSON.Json_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m417DA6002FB1E7CAC4EF032DC8245488E98CA12A ();
// 0x00000016 System.Void Facebook.MiniJSON.Json_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mF863F8896BF00D0F66326B47BF13B7E30FDDEFE1 ();
// 0x00000017 System.Void Facebook.MiniJSON.Json_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mF84FEBC2C6794A547A1831215E545289DB9361EB ();
// 0x00000018 System.Void Facebook.MiniJSON.Json_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mD67D9D2C840DD3A88D5DA92C431FD1CAE2A1AD92 ();
// 0x00000019 System.Void Facebook.Unity.AccessToken::.ctor(System.String,System.String,System.DateTime,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.DateTime>,System.String)
extern void AccessToken__ctor_mB8A77C718530154A525F31F0EBEDD80DB559D1ED ();
// 0x0000001A Facebook.Unity.AccessToken Facebook.Unity.AccessToken::get_CurrentAccessToken()
extern void AccessToken_get_CurrentAccessToken_m13595BFE82BABD89E825951DBD385A777B111456 ();
// 0x0000001B System.Void Facebook.Unity.AccessToken::set_CurrentAccessToken(Facebook.Unity.AccessToken)
extern void AccessToken_set_CurrentAccessToken_m4CC5129C6FEC856E1C80E03A770BB46D789EFD81 ();
// 0x0000001C System.String Facebook.Unity.AccessToken::get_TokenString()
extern void AccessToken_get_TokenString_m5358AABD0A44AA1F5664F93A9543F990DCA46D74 ();
// 0x0000001D System.Void Facebook.Unity.AccessToken::set_TokenString(System.String)
extern void AccessToken_set_TokenString_mF814F1B8E97395B4FDF4CAB88E69A0B6AA5C88C1 ();
// 0x0000001E System.DateTime Facebook.Unity.AccessToken::get_ExpirationTime()
extern void AccessToken_get_ExpirationTime_m68DFE53A1F10E3059BBE2781D5E4C5BFBDE955AE ();
// 0x0000001F System.Void Facebook.Unity.AccessToken::set_ExpirationTime(System.DateTime)
extern void AccessToken_set_ExpirationTime_m07B3B011E8CB81A3D7F703C8168599F2227FDB17 ();
// 0x00000020 System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::get_Permissions()
extern void AccessToken_get_Permissions_m9C12C0140717961695A07170D29997F234086B25 ();
// 0x00000021 System.Void Facebook.Unity.AccessToken::set_Permissions(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AccessToken_set_Permissions_m6D221CB2E6956192F5AC05DC0F8372D842EBA734 ();
// 0x00000022 System.String Facebook.Unity.AccessToken::get_UserId()
extern void AccessToken_get_UserId_m6A292624EB7B830F117057DD79EEB0C5BA2809BC ();
// 0x00000023 System.Void Facebook.Unity.AccessToken::set_UserId(System.String)
extern void AccessToken_set_UserId_mD406272464DA7806C22D3973A1466F2CF2729939 ();
// 0x00000024 System.Nullable`1<System.DateTime> Facebook.Unity.AccessToken::get_LastRefresh()
extern void AccessToken_get_LastRefresh_m0D58E94638FA92C7222CFF32447653B8E5131D72 ();
// 0x00000025 System.Void Facebook.Unity.AccessToken::set_LastRefresh(System.Nullable`1<System.DateTime>)
extern void AccessToken_set_LastRefresh_m614BFAB83B147F84931E19D03EF61F7A4D0D1BE1 ();
// 0x00000026 System.String Facebook.Unity.AccessToken::get_GraphDomain()
extern void AccessToken_get_GraphDomain_m052F2656A15F3A311922AE24A876098151764B66 ();
// 0x00000027 System.Void Facebook.Unity.AccessToken::set_GraphDomain(System.String)
extern void AccessToken_set_GraphDomain_m12C8E2D6DED115896636A316A4E37DA88704E029 ();
// 0x00000028 System.String Facebook.Unity.AccessToken::ToString()
extern void AccessToken_ToString_m76F38B75D62CF33C0E3CA0F628A0A2EFCD221E1A ();
// 0x00000029 System.String Facebook.Unity.AccessToken::ToJson()
extern void AccessToken_ToJson_m636079271D15A9F225A9DE63736EDBF2C412083F ();
// 0x0000002A System.String Facebook.Unity.CallbackManager::AddFacebookDelegate(Facebook.Unity.FacebookDelegate`1<T>)
// 0x0000002B System.Void Facebook.Unity.CallbackManager::OnFacebookResponse(Facebook.Unity.IInternalResult)
extern void CallbackManager_OnFacebookResponse_mD054881D43474F70B5882EC382C2EE40F2800B4C ();
// 0x0000002C System.Void Facebook.Unity.CallbackManager::CallCallback(System.Object,Facebook.Unity.IResult)
extern void CallbackManager_CallCallback_m4503B80223B4FDB720FE56F1889F3CCA9D0E2198 ();
// 0x0000002D System.Boolean Facebook.Unity.CallbackManager::TryCallCallback(System.Object,Facebook.Unity.IResult)
// 0x0000002E System.Void Facebook.Unity.CallbackManager::.ctor()
extern void CallbackManager__ctor_mC0168BDF32B716ADD5CCBDB939AD7DFF90FF742C ();
// 0x0000002F UnityEngine.GameObject Facebook.Unity.ComponentFactory::get_FacebookGameObject()
extern void ComponentFactory_get_FacebookGameObject_m0A194DADC5B8AD0BE03909FC45B8D73127A6615E ();
// 0x00000030 T Facebook.Unity.ComponentFactory::GetComponent(Facebook.Unity.ComponentFactory_IfNotExist)
// 0x00000031 T Facebook.Unity.ComponentFactory::AddComponent()
// 0x00000032 System.Uri Facebook.Unity.Constants::get_GraphUrl()
extern void Constants_get_GraphUrl_m5238E7C321F36003524DE171D8283BB6E69C4685 ();
// 0x00000033 System.String Facebook.Unity.Constants::get_GraphApiUserAgent()
extern void Constants_get_GraphApiUserAgent_mD87358FFD5A372B88DDF28688BBAC2AC7773E3D1 ();
// 0x00000034 System.Boolean Facebook.Unity.Constants::get_IsMobile()
extern void Constants_get_IsMobile_m993267365AA7A5997A8847F7B9AAAD9085FA593E ();
// 0x00000035 System.Boolean Facebook.Unity.Constants::get_IsEditor()
extern void Constants_get_IsEditor_mF25D5FD4D5E6634B2946B23CB5384A482A35FF9D ();
// 0x00000036 System.Boolean Facebook.Unity.Constants::get_IsWeb()
extern void Constants_get_IsWeb_m7355944255950FA4BC21FC474193646B6DF70835 ();
// 0x00000037 System.Boolean Facebook.Unity.Constants::get_IsGameroom()
extern void Constants_get_IsGameroom_m96DD7D4F9C7591EBA8F0785DA042B5AD24FD9B72 ();
// 0x00000038 System.String Facebook.Unity.Constants::get_UnitySDKUserAgentSuffixLegacy()
extern void Constants_get_UnitySDKUserAgentSuffixLegacy_mF4299844782178B55576CEC64191BB8C1C3B07B3 ();
// 0x00000039 System.String Facebook.Unity.Constants::get_UnitySDKUserAgent()
extern void Constants_get_UnitySDKUserAgent_m9B09D7176AC2B77DC563D1C2CB2192BE39E52FC9 ();
// 0x0000003A System.Boolean Facebook.Unity.Constants::get_DebugMode()
extern void Constants_get_DebugMode_mC1BDD7AF06975C5BB0DBD72579236BC00C91005B ();
// 0x0000003B Facebook.Unity.FacebookUnityPlatform Facebook.Unity.Constants::get_CurrentPlatform()
extern void Constants_get_CurrentPlatform_mB0F7FCDB1EC2D4FAEE2B03EF0D7CA0F15BA9F767 ();
// 0x0000003C Facebook.Unity.FacebookUnityPlatform Facebook.Unity.Constants::GetCurrentPlatform()
extern void Constants_GetCurrentPlatform_m7BBC95D5E47A64B81CBAFDDCAB962B52140BD9C2 ();
// 0x0000003D System.String Facebook.Unity.FB::get_AppId()
extern void FB_get_AppId_m1489BBE6274C96BCE43A5DF4AD76EC6A65FD27D5 ();
// 0x0000003E System.Void Facebook.Unity.FB::set_AppId(System.String)
extern void FB_set_AppId_m2412746196389AE1014CA4363C92805BB681E6EA ();
// 0x0000003F System.String Facebook.Unity.FB::get_ClientToken()
extern void FB_get_ClientToken_m8EF152654D7B2172128D2FB5A14219B850DF3B9B ();
// 0x00000040 System.Void Facebook.Unity.FB::set_ClientToken(System.String)
extern void FB_set_ClientToken_mC373D3B586E8333D44C9CB46FFAD766EC9B5FAE5 ();
// 0x00000041 System.String Facebook.Unity.FB::get_GraphApiVersion()
extern void FB_get_GraphApiVersion_m5372280099E5B014E5457F5ABF6AAA40C15047FD ();
// 0x00000042 System.Void Facebook.Unity.FB::set_GraphApiVersion(System.String)
extern void FB_set_GraphApiVersion_mCF555205CB4276F6B4693EC511B977A1BCED6B9A ();
// 0x00000043 System.Boolean Facebook.Unity.FB::get_IsLoggedIn()
extern void FB_get_IsLoggedIn_m1BE9E0F416A9AF5A7B6DEF648C80F616A508A51D ();
// 0x00000044 System.Boolean Facebook.Unity.FB::get_IsInitialized()
extern void FB_get_IsInitialized_m7D638468F8A8CD50CC2C67E32FFF295E79D8D9B4 ();
// 0x00000045 System.Boolean Facebook.Unity.FB::get_LimitAppEventUsage()
extern void FB_get_LimitAppEventUsage_m079FE8AF3219757075749AF63D90970E34348B1E ();
// 0x00000046 System.Void Facebook.Unity.FB::set_LimitAppEventUsage(System.Boolean)
extern void FB_set_LimitAppEventUsage_m23835B5DFA342006D43634738FA4CBA51C10BA55 ();
// 0x00000047 Facebook.Unity.IFacebook Facebook.Unity.FB::get_FacebookImpl()
extern void FB_get_FacebookImpl_m63590A128C20A71E977DB12B4779C22175D28AB0 ();
// 0x00000048 System.Void Facebook.Unity.FB::set_FacebookImpl(Facebook.Unity.IFacebook)
extern void FB_set_FacebookImpl_m3B81A77E08A0304719EACAF972ECA92A7E4286E2 ();
// 0x00000049 System.String Facebook.Unity.FB::get_FacebookDomain()
extern void FB_get_FacebookDomain_m153C3DCD60AB3C50CBEF5BF54354E8ECCE487AF3 ();
// 0x0000004A System.Void Facebook.Unity.FB::set_FacebookDomain(System.String)
extern void FB_set_FacebookDomain_mEC4EDACFA2C197646CCA9AADB8644F01D68B1CC1 ();
// 0x0000004B Facebook.Unity.FB_OnDLLLoaded Facebook.Unity.FB::get_OnDLLLoadedDelegate()
extern void FB_get_OnDLLLoadedDelegate_mDF0CD83548CAE0D591E32D1AF14AFCF2AB0721D5 ();
// 0x0000004C System.Void Facebook.Unity.FB::set_OnDLLLoadedDelegate(Facebook.Unity.FB_OnDLLLoaded)
extern void FB_set_OnDLLLoadedDelegate_mFD5ED2115E5D1CE1CCDF0DF3F310BE46CF3A89EF ();
// 0x0000004D System.Void Facebook.Unity.FB::Init(Facebook.Unity.InitDelegate,Facebook.Unity.HideUnityDelegate,System.String)
extern void FB_Init_mB6B57CA36F605F9F609A7D42AA9CE920B515D268 ();
// 0x0000004E System.Void Facebook.Unity.FB::Init(System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void FB_Init_mFEB9120C2C1B29BFFC4E6E443272F34E8A9F94E3 ();
// 0x0000004F System.Void Facebook.Unity.FB::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void FB_LogInWithPublishPermissions_m7F47751E84794A4E3AE77A674D828146F2718CBE ();
// 0x00000050 System.Void Facebook.Unity.FB::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void FB_LogInWithReadPermissions_m4ECC85AD931B78DEECFA1084FAC2AFB58341710C ();
// 0x00000051 System.Void Facebook.Unity.FB::LogOut()
extern void FB_LogOut_mD584DA2202B57BFB8CAD936BB832EB1681F1F867 ();
// 0x00000052 System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m5FAC156DE18EB4FAF52031E378EB262E7AF094D9 ();
// 0x00000053 System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m62BDFEB81DF3266D19211546675766E3A426C4A2 ();
// 0x00000054 System.Void Facebook.Unity.FB::AppRequest(System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m8AD947D050E3955D25C42B0C2D686DE0B690D9E3 ();
// 0x00000055 System.Void Facebook.Unity.FB::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void FB_ShareLink_m53E9A04825EB1CBD416AFD537F29AA3A8C5FB90E ();
// 0x00000056 System.Void Facebook.Unity.FB::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void FB_FeedShare_m9E00AB534B163AF9BF8A26D24F6D4B99FC407C49 ();
// 0x00000057 System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void FB_API_m9948056643882A44BB7A99A36D4AAB8EAF1823F6 ();
// 0x00000058 System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,UnityEngine.WWWForm)
extern void FB_API_m092EB1628F038A356C081EA93D6A0B7333CE46C6 ();
// 0x00000059 System.Void Facebook.Unity.FB::ActivateApp()
extern void FB_ActivateApp_m29519DDADC61EFC4FBD1597384D767A1AD94B76B ();
// 0x0000005A System.Void Facebook.Unity.FB::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void FB_GetAppLink_mEB6C1A0F0DA93B19230BA1DD9AE4C5A17D24CD90 ();
// 0x0000005B System.Void Facebook.Unity.FB::ClearAppLink()
extern void FB_ClearAppLink_m2DDF420FEB425F2D5A56015BE4207AECA63DE3CB ();
// 0x0000005C System.Void Facebook.Unity.FB::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogAppEvent_mB49D6D0BD4D73D64DA84BAFC96FD8B04C237B89F ();
// 0x0000005D System.Void Facebook.Unity.FB::LogPurchase(System.Decimal,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogPurchase_mFDA037CAD9DB8C180B1408C6DB962FC75C4D1FA2 ();
// 0x0000005E System.Void Facebook.Unity.FB::LogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogPurchase_mCB8CB69EFDE7DAFC2D08139E8DD9999D6BD2E89D ();
// 0x0000005F System.Void Facebook.Unity.FB::LogVersion()
extern void FB_LogVersion_mB06DB28E9661B5C64AB751CE443D10787C32160F ();
// 0x00000060 System.Void Facebook.Unity.FB::.ctor()
extern void FB__ctor_mAD07653B33A22EA448D20821520C53288385827E ();
// 0x00000061 System.Void Facebook.Unity.FB::.cctor()
extern void FB__cctor_m0C93E0230071AD18B70832FC11F6198CBDAB0343 ();
// 0x00000062 System.Void Facebook.Unity.FB_OnDLLLoaded::.ctor(System.Object,System.IntPtr)
extern void OnDLLLoaded__ctor_m4740E133CF9AF20863045F7CDBBC75AC967BA08A ();
// 0x00000063 System.Void Facebook.Unity.FB_OnDLLLoaded::Invoke()
extern void OnDLLLoaded_Invoke_m5A60D20E804F13BF0B065BCA1956E806D542B94F ();
// 0x00000064 System.IAsyncResult Facebook.Unity.FB_OnDLLLoaded::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDLLLoaded_BeginInvoke_m772C09243226136A8FF317DB6B6921BD71493AA3 ();
// 0x00000065 System.Void Facebook.Unity.FB_OnDLLLoaded::EndInvoke(System.IAsyncResult)
extern void OnDLLLoaded_EndInvoke_m8A44591C82E83A1ECF6C933206C7119842DE8354 ();
// 0x00000066 Facebook.Unity.IPayFacebook Facebook.Unity.FB_Canvas::get_FacebookPayImpl()
extern void Canvas_get_FacebookPayImpl_mAB0E2F3BE1D5BBE8BB2805676CA062865F0E62DC ();
// 0x00000067 System.Void Facebook.Unity.FB_Canvas::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void Canvas_Pay_m4D886BF85C9ECD532CF39BD6F1B791098030B173 ();
// 0x00000068 System.Void Facebook.Unity.FB_Mobile::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void Mobile_set_ShareDialogMode_m99A2957C9226B65D18ACBB6DDF83D2C5F25CBB2A ();
// 0x00000069 Facebook.Unity.Mobile.IMobileFacebook Facebook.Unity.FB_Mobile::get_MobileFacebookImpl()
extern void Mobile_get_MobileFacebookImpl_mEB711C6EF15966CAA14F87E19C6C7CD7AAE5CB26 ();
// 0x0000006A System.Void Facebook.Unity.FB_Mobile::FetchDeferredAppLinkData(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void Mobile_FetchDeferredAppLinkData_m7328195B26434CDC789CF26095E751087CD12F00 ();
// 0x0000006B System.Void Facebook.Unity.FB_Mobile::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void Mobile_RefreshCurrentAccessToken_m7AD37E813F087C0AD28BE1119F74A6DB5EE1252C ();
// 0x0000006C System.Boolean Facebook.Unity.FB_Mobile::IsImplicitPurchaseLoggingEnabled()
extern void Mobile_IsImplicitPurchaseLoggingEnabled_m453D5DC8536D809900D1BF4D568ABE16A6FD7520 ();
// 0x0000006D Facebook.Unity.FacebookGameObject Facebook.Unity.FB_CompiledFacebookLoader::get_FBGameObject()
// 0x0000006E System.Void Facebook.Unity.FB_CompiledFacebookLoader::Start()
extern void CompiledFacebookLoader_Start_mD8581407131D0FE65C4DE014F585FEFB654B54C3 ();
// 0x0000006F System.Void Facebook.Unity.FB_CompiledFacebookLoader::.ctor()
extern void CompiledFacebookLoader__ctor_mB320F10ABDDFE441221BA38ADDA0C1A3D760AE26 ();
// 0x00000070 System.Void Facebook.Unity.FB_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mAB6F7C4FA96C02B80E4696A39A92B5932FE4C152 ();
// 0x00000071 System.Void Facebook.Unity.FB_<>c__DisplayClass36_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__0_mFE950F43B02994F7E220985D5218B65447CB8915 ();
// 0x00000072 System.Void Facebook.Unity.FB_<>c__DisplayClass36_0::<Init>b__1()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__1_mB4D317BEF67A7F06A9BB38EA241211448D7A3092 ();
// 0x00000073 System.Void Facebook.Unity.FB_<>c__DisplayClass36_0::<Init>b__2()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__2_m6AA4B7C73842A9B5A74906BEE8057D24C92E0E36 ();
// 0x00000074 System.Void Facebook.Unity.FB_<>c__DisplayClass36_0::<Init>b__3()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__3_mAEA05F41CB6536A57E5DE76FC986ED4BC3F292B9 ();
// 0x00000075 System.Void Facebook.Unity.FB_<>c__DisplayClass36_0::<Init>b__4()
extern void U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__4_m9C9317AC8D3A6E3136F06AC6F1FCE2349C8F0258 ();
// 0x00000076 System.Void Facebook.Unity.FBGamingServices::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void FBGamingServices_OpenFriendFinderDialog_m3EDAC959E0130BF64F402E5A55A8905330EA9DAA ();
// 0x00000077 System.Void Facebook.Unity.FBGamingServices::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void FBGamingServices_UploadImageToMediaLibrary_m807ADEE82483109434CAF8A93734B1430CFB69AD ();
// 0x00000078 System.Void Facebook.Unity.FBGamingServices::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void FBGamingServices_UploadVideoToMediaLibrary_mEC3D77F26B6FF0545CF2ECB6FA17FDE2626817FB ();
// 0x00000079 Facebook.Unity.Mobile.IMobileFacebook Facebook.Unity.FBGamingServices::get_MobileFacebookImpl()
extern void FBGamingServices_get_MobileFacebookImpl_mD23C632BD9D0A9958EC70F0AF90A893FC1099082 ();
// 0x0000007A System.Void Facebook.Unity.FBGamingServices::.ctor()
extern void FBGamingServices__ctor_m72856C111E894E11AC8AD8F11A58C409F164FD04 ();
// 0x0000007B System.Void Facebook.Unity.FacebookBase::.ctor(Facebook.Unity.CallbackManager)
extern void FacebookBase__ctor_m0170BED48D2A6D65D31DA00BCF7C473E3F291F03 ();
// 0x0000007C System.Boolean Facebook.Unity.FacebookBase::get_LimitEventUsage()
// 0x0000007D System.Void Facebook.Unity.FacebookBase::set_LimitEventUsage(System.Boolean)
// 0x0000007E System.String Facebook.Unity.FacebookBase::get_SDKName()
// 0x0000007F System.String Facebook.Unity.FacebookBase::get_SDKVersion()
// 0x00000080 System.String Facebook.Unity.FacebookBase::get_SDKUserAgent()
extern void FacebookBase_get_SDKUserAgent_m898B03A73DF563DB791AC1DF0B6845681586A3D7 ();
// 0x00000081 System.Boolean Facebook.Unity.FacebookBase::get_LoggedIn()
extern void FacebookBase_get_LoggedIn_mB760F006DD10F7F7851224DD04BFFC729603B842 ();
// 0x00000082 System.Boolean Facebook.Unity.FacebookBase::get_Initialized()
extern void FacebookBase_get_Initialized_m64D5DD277FB710ADFC9A7EE2B4B155415F50A275 ();
// 0x00000083 System.Void Facebook.Unity.FacebookBase::set_Initialized(System.Boolean)
extern void FacebookBase_set_Initialized_mFEF325E118282493BD607C2E48018DE5DFE8A9F9 ();
// 0x00000084 Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::get_CallbackManager()
extern void FacebookBase_get_CallbackManager_m6277A4C7F3B2FF628BB3307D0F2CB4426DFC79D2 ();
// 0x00000085 System.Void Facebook.Unity.FacebookBase::set_CallbackManager(Facebook.Unity.CallbackManager)
extern void FacebookBase_set_CallbackManager_mFD31AC262A5348912AD870E1F59371AD32D36096 ();
// 0x00000086 System.Void Facebook.Unity.FacebookBase::Init(Facebook.Unity.InitDelegate)
extern void FacebookBase_Init_m72BB5AEAA6E4A817E1F0AF3E5B5FEEEF5D477B69 ();
// 0x00000087 System.Void Facebook.Unity.FacebookBase::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x00000088 System.Void Facebook.Unity.FacebookBase::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x00000089 System.Void Facebook.Unity.FacebookBase::LogOut()
extern void FacebookBase_LogOut_m472F02624D74CE38AF02F5AEFFE5092D4D51CD27 ();
// 0x0000008A System.Void Facebook.Unity.FacebookBase::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
// 0x0000008B System.Void Facebook.Unity.FacebookBase::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x0000008C System.Void Facebook.Unity.FacebookBase::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x0000008D System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void FacebookBase_API_mA643418E3AC41C4BD02F15C897FF97784E19B151 ();
// 0x0000008E System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void FacebookBase_API_m10A05AF1F4C066CFCE02F41C8E84AD4299524A54 ();
// 0x0000008F System.Void Facebook.Unity.FacebookBase::ActivateApp(System.String)
// 0x00000090 System.Void Facebook.Unity.FacebookBase::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x00000091 System.Void Facebook.Unity.FacebookBase::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x00000092 System.Void Facebook.Unity.FacebookBase::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x00000093 System.Void Facebook.Unity.FacebookBase::OnInitComplete(Facebook.Unity.ResultContainer)
extern void FacebookBase_OnInitComplete_m51DE408F9CFA69B17CEAD2C54C1B85D0E0A9EDA2 ();
// 0x00000094 System.Void Facebook.Unity.FacebookBase::OnLoginComplete(Facebook.Unity.ResultContainer)
// 0x00000095 System.Void Facebook.Unity.FacebookBase::OnLogoutComplete(Facebook.Unity.ResultContainer)
extern void FacebookBase_OnLogoutComplete_mBDA27D0C7E74CAC758E1387243836EF05882A189 ();
// 0x00000096 System.Void Facebook.Unity.FacebookBase::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x00000097 System.Void Facebook.Unity.FacebookBase::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
// 0x00000098 System.Void Facebook.Unity.FacebookBase::OnShareLinkComplete(Facebook.Unity.ResultContainer)
// 0x00000099 System.Void Facebook.Unity.FacebookBase::ValidateAppRequestArgs(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FacebookBase_ValidateAppRequestArgs_m354428BCE63BA4A4199FB742AC9D7EFD1EAB1F99 ();
// 0x0000009A System.Void Facebook.Unity.FacebookBase::OnAuthResponse(Facebook.Unity.LoginResult)
extern void FacebookBase_OnAuthResponse_m198C37E188A9F614197FB7FEF8B0B0BD9080DC09 ();
// 0x0000009B System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.FacebookBase::CopyByValue(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void FacebookBase_CopyByValue_m36AEFA6C6724782E70FBFB0FD91E9B6D9AFCE05F ();
// 0x0000009C System.Uri Facebook.Unity.FacebookBase::GetGraphUrl(System.String)
extern void FacebookBase_GetGraphUrl_m68445D2B00523B04F79669C68DCC1EED4A31403B ();
// 0x0000009D System.Void Facebook.Unity.FacebookBase::<OnInitComplete>b__35_0(Facebook.Unity.ILoginResult)
extern void FacebookBase_U3COnInitCompleteU3Eb__35_0_m2B64B634700D72D1755A23E0537940CD7465DDEF ();
// 0x0000009E System.Void Facebook.Unity.FacebookBase_<>c::.cctor()
extern void U3CU3Ec__cctor_mFEACD9475775AC530C18E40EC62767F9E4DB453A ();
// 0x0000009F System.Void Facebook.Unity.FacebookBase_<>c::.ctor()
extern void U3CU3Ec__ctor_m3937E429B1DAB2A6390579F525D87CD705306528 ();
// 0x000000A0 System.Boolean Facebook.Unity.FacebookBase_<>c::<ValidateAppRequestArgs>b__41_0(System.String)
extern void U3CU3Ec_U3CValidateAppRequestArgsU3Eb__41_0_mB0D69F4F62704D337D1623E77A751A556C1F9DF0 ();
// 0x000000A1 System.Void Facebook.Unity.InitDelegate::.ctor(System.Object,System.IntPtr)
extern void InitDelegate__ctor_m3B50265F79D8DF8D1E8FAD737965D2C7FC57ED50 ();
// 0x000000A2 System.Void Facebook.Unity.InitDelegate::Invoke()
extern void InitDelegate_Invoke_m93BAC2CDD6BAD72CEF92943490142B9E2D5E1311 ();
// 0x000000A3 System.IAsyncResult Facebook.Unity.InitDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void InitDelegate_BeginInvoke_m85E47DCD6AEF2E0F3064879630F7F7709EA9F542 ();
// 0x000000A4 System.Void Facebook.Unity.InitDelegate::EndInvoke(System.IAsyncResult)
extern void InitDelegate_EndInvoke_m19AF528352A49E8B96EF7BAB14A39100537423E7 ();
// 0x000000A5 System.Void Facebook.Unity.FacebookDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x000000A6 System.Void Facebook.Unity.FacebookDelegate`1::Invoke(T)
// 0x000000A7 System.IAsyncResult Facebook.Unity.FacebookDelegate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000000A8 System.Void Facebook.Unity.FacebookDelegate`1::EndInvoke(System.IAsyncResult)
// 0x000000A9 System.Void Facebook.Unity.HideUnityDelegate::.ctor(System.Object,System.IntPtr)
extern void HideUnityDelegate__ctor_mF491837CE67C28E52F8DAAB9DF8B71F195149063 ();
// 0x000000AA System.Void Facebook.Unity.HideUnityDelegate::Invoke(System.Boolean)
extern void HideUnityDelegate_Invoke_mCA0B59C7D14B5C248069F4A9ECC7EE070D4D674C ();
// 0x000000AB System.IAsyncResult Facebook.Unity.HideUnityDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void HideUnityDelegate_BeginInvoke_mB41A5458F30C075A1891B68F43A369D41EE5C055 ();
// 0x000000AC System.Void Facebook.Unity.HideUnityDelegate::EndInvoke(System.IAsyncResult)
extern void HideUnityDelegate_EndInvoke_m610888250BA07306F6100BABDA62B884262338E6 ();
// 0x000000AD Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::get_Facebook()
extern void FacebookGameObject_get_Facebook_m895AC7FFAF0F66F74C9E756494660E26A5920114 ();
// 0x000000AE System.Void Facebook.Unity.FacebookGameObject::set_Facebook(Facebook.Unity.IFacebookImplementation)
extern void FacebookGameObject_set_Facebook_mB57E522C6DB990DDB7F3E9CA9B2E5C769C6A7862 ();
// 0x000000AF System.Void Facebook.Unity.FacebookGameObject::Awake()
extern void FacebookGameObject_Awake_mBA12D0A607908EAE100AF983B1B5EAA75A236EF9 ();
// 0x000000B0 System.Void Facebook.Unity.FacebookGameObject::OnInitComplete(System.String)
extern void FacebookGameObject_OnInitComplete_m00090D77BEE8E0090FF2B8DE7DB78A920743E393 ();
// 0x000000B1 System.Void Facebook.Unity.FacebookGameObject::OnLoginComplete(System.String)
extern void FacebookGameObject_OnLoginComplete_mCE5EDBE1166EF1745BF18018F38D9ED9C5157E7A ();
// 0x000000B2 System.Void Facebook.Unity.FacebookGameObject::OnLogoutComplete(System.String)
extern void FacebookGameObject_OnLogoutComplete_m243E1C0F885F143D6AB799434EBCB94AFBDC3FFA ();
// 0x000000B3 System.Void Facebook.Unity.FacebookGameObject::OnGetAppLinkComplete(System.String)
extern void FacebookGameObject_OnGetAppLinkComplete_m44C0A233E1FA79CAEA9F1985E80FE1DC14BBC8A9 ();
// 0x000000B4 System.Void Facebook.Unity.FacebookGameObject::OnAppRequestsComplete(System.String)
extern void FacebookGameObject_OnAppRequestsComplete_mE2F4B76808EF48E63F4DF51212367A3572B7B3E9 ();
// 0x000000B5 System.Void Facebook.Unity.FacebookGameObject::OnShareLinkComplete(System.String)
extern void FacebookGameObject_OnShareLinkComplete_mC5927F0C0B874B7E0549C5064BB3AF8563F3256B ();
// 0x000000B6 System.Void Facebook.Unity.FacebookGameObject::OnAwake()
extern void FacebookGameObject_OnAwake_m15415D2BC7F8F912199C89C42DC22A4F6DE2BC93 ();
// 0x000000B7 System.Void Facebook.Unity.FacebookGameObject::.ctor()
extern void FacebookGameObject__ctor_mA738180F76F2C190481692B8F4A4C389D7121EC2 ();
// 0x000000B8 System.String Facebook.Unity.FacebookSdkVersion::get_Build()
extern void FacebookSdkVersion_get_Build_mBD35BBA90D719AE98FA66E269A29420E7E4E45AF ();
// 0x000000B9 System.Boolean Facebook.Unity.IFacebook::get_LoggedIn()
// 0x000000BA System.Boolean Facebook.Unity.IFacebook::get_LimitEventUsage()
// 0x000000BB System.Void Facebook.Unity.IFacebook::set_LimitEventUsage(System.Boolean)
// 0x000000BC System.String Facebook.Unity.IFacebook::get_SDKUserAgent()
// 0x000000BD System.Boolean Facebook.Unity.IFacebook::get_Initialized()
// 0x000000BE System.Void Facebook.Unity.IFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x000000BF System.Void Facebook.Unity.IFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x000000C0 System.Void Facebook.Unity.IFacebook::LogOut()
// 0x000000C1 System.Void Facebook.Unity.IFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
// 0x000000C2 System.Void Facebook.Unity.IFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x000000C3 System.Void Facebook.Unity.IFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x000000C4 System.Void Facebook.Unity.IFacebook::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000000C5 System.Void Facebook.Unity.IFacebook::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000000C6 System.Void Facebook.Unity.IFacebook::ActivateApp(System.String)
// 0x000000C7 System.Void Facebook.Unity.IFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x000000C8 System.Void Facebook.Unity.IFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000C9 System.Void Facebook.Unity.IFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000CA System.Void Facebook.Unity.IFacebookCallbackHandler::OnInitComplete(System.String)
// 0x000000CB System.Void Facebook.Unity.IFacebookCallbackHandler::OnLoginComplete(System.String)
// 0x000000CC System.Void Facebook.Unity.IFacebookCallbackHandler::OnAppRequestsComplete(System.String)
// 0x000000CD System.Void Facebook.Unity.IFacebookCallbackHandler::OnShareLinkComplete(System.String)
// 0x000000CE System.Void Facebook.Unity.IFacebookResultHandler::OnInitComplete(Facebook.Unity.ResultContainer)
// 0x000000CF System.Void Facebook.Unity.IFacebookResultHandler::OnLoginComplete(Facebook.Unity.ResultContainer)
// 0x000000D0 System.Void Facebook.Unity.IFacebookResultHandler::OnLogoutComplete(Facebook.Unity.ResultContainer)
// 0x000000D1 System.Void Facebook.Unity.IFacebookResultHandler::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000D2 System.Void Facebook.Unity.IFacebookResultHandler::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
// 0x000000D3 System.Void Facebook.Unity.IFacebookResultHandler::OnShareLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000D4 System.Void Facebook.Unity.IPayFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
// 0x000000D5 System.Void Facebook.Unity.MethodArguments::.ctor()
extern void MethodArguments__ctor_m386044A8B3C685AE0AABB18251E10FDBEA11A03C ();
// 0x000000D6 System.Void Facebook.Unity.MethodArguments::.ctor(Facebook.Unity.MethodArguments)
extern void MethodArguments__ctor_mEFF2A6F8D8D7BE60B2A79C5964382E3BD5B7BE04 ();
// 0x000000D7 System.Void Facebook.Unity.MethodArguments::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments__ctor_mC8ACEA4A4E630AE683DE86979DED99649B6A8168 ();
// 0x000000D8 System.Void Facebook.Unity.MethodArguments::AddPrimative(System.String,T)
// 0x000000D9 System.Void Facebook.Unity.MethodArguments::AddNullablePrimitive(System.String,System.Nullable`1<T>)
// 0x000000DA System.Void Facebook.Unity.MethodArguments::AddString(System.String,System.String)
extern void MethodArguments_AddString_m8EFB3D4292AE489D9EE657588137BD1FDB059923 ();
// 0x000000DB System.Void Facebook.Unity.MethodArguments::AddCommaSeparatedList(System.String,System.Collections.Generic.IEnumerable`1<System.String>)
extern void MethodArguments_AddCommaSeparatedList_mA3D6E35D9F6FF2F973BE3720BF03CEE69E238EAF ();
// 0x000000DC System.Void Facebook.Unity.MethodArguments::AddDictionary(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments_AddDictionary_mC9D7D520A5B3804A6B20211B33DDCA1E9112B80F ();
// 0x000000DD System.Void Facebook.Unity.MethodArguments::AddList(System.String,System.Collections.Generic.IEnumerable`1<T>)
// 0x000000DE System.Void Facebook.Unity.MethodArguments::AddUri(System.String,System.Uri)
extern void MethodArguments_AddUri_mA2AAF7BC3C9E7E8CBF2157D7D0616471C6394DEA ();
// 0x000000DF System.String Facebook.Unity.MethodArguments::ToJsonString()
extern void MethodArguments_ToJsonString_m18DDE098F5D2249E3FAC5C1502981B45C1EE3A24 ();
// 0x000000E0 System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.Unity.MethodArguments::ToStringDict(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments_ToStringDict_mD7AD5E7239868ECCC804CDD2CB3DBF58EA05F4DA ();
// 0x000000E1 System.Void Facebook.Unity.MethodCall`1::.ctor(Facebook.Unity.FacebookBase,System.String)
// 0x000000E2 System.String Facebook.Unity.MethodCall`1::get_MethodName()
// 0x000000E3 System.Void Facebook.Unity.MethodCall`1::set_MethodName(System.String)
// 0x000000E4 Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1::get_Callback()
// 0x000000E5 System.Void Facebook.Unity.MethodCall`1::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
// 0x000000E6 System.Void Facebook.Unity.MethodCall`1::set_FacebookImpl(Facebook.Unity.FacebookBase)
// 0x000000E7 System.Void Facebook.Unity.MethodCall`1::set_Parameters(Facebook.Unity.MethodArguments)
// 0x000000E8 System.Void Facebook.Unity.MethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x000000E9 System.Void Facebook.Unity.AccessTokenRefreshResult::.ctor(Facebook.Unity.ResultContainer)
extern void AccessTokenRefreshResult__ctor_mF2038CFA769B53E2610D59FDD350B740DEB183A4 ();
// 0x000000EA Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::get_AccessToken()
extern void AccessTokenRefreshResult_get_AccessToken_mBE835E6356640BA0B070C533A6F8D22964EC4048 ();
// 0x000000EB System.Void Facebook.Unity.AccessTokenRefreshResult::set_AccessToken(Facebook.Unity.AccessToken)
extern void AccessTokenRefreshResult_set_AccessToken_m15D989B19DDDD38D9DC5375CFEAE6EB3AF5F09A1 ();
// 0x000000EC System.String Facebook.Unity.AccessTokenRefreshResult::ToString()
extern void AccessTokenRefreshResult_ToString_mE3259D4D4AC178A023C79B031D8D5347564A22C1 ();
// 0x000000ED System.Void Facebook.Unity.AppLinkResult::.ctor(Facebook.Unity.ResultContainer)
extern void AppLinkResult__ctor_mAFE640A41C69087201E90CD189054755DB3F354D ();
// 0x000000EE System.String Facebook.Unity.AppLinkResult::get_Url()
extern void AppLinkResult_get_Url_m3901581148CA1CFF2919FAA7D09831798E7AE41C ();
// 0x000000EF System.Void Facebook.Unity.AppLinkResult::set_Url(System.String)
extern void AppLinkResult_set_Url_m8E3BCCAA0072AEDEE537D6DB12442C0E42B4C10F ();
// 0x000000F0 System.String Facebook.Unity.AppLinkResult::get_TargetUrl()
extern void AppLinkResult_get_TargetUrl_m6949D521A7A2FF60E540C0646C33212120A701A6 ();
// 0x000000F1 System.Void Facebook.Unity.AppLinkResult::set_TargetUrl(System.String)
extern void AppLinkResult_set_TargetUrl_m4954599E465E50429F00DE15D4B62CAA7C65432B ();
// 0x000000F2 System.String Facebook.Unity.AppLinkResult::get_Ref()
extern void AppLinkResult_get_Ref_mB08E7ACBE6150EBB39CAB582A5C520E24FBADE4A ();
// 0x000000F3 System.Void Facebook.Unity.AppLinkResult::set_Ref(System.String)
extern void AppLinkResult_set_Ref_m633906A27F0A75BBFDEF6A93C8C1920568EB859D ();
// 0x000000F4 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::get_Extras()
extern void AppLinkResult_get_Extras_mD8677AC2751360664795737C93D50E0FEF23F1B4 ();
// 0x000000F5 System.Void Facebook.Unity.AppLinkResult::set_Extras(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void AppLinkResult_set_Extras_mD78F58CDBBF7CE94B4B24E5984F4421C4A9DAD6D ();
// 0x000000F6 System.String Facebook.Unity.AppLinkResult::ToString()
extern void AppLinkResult_ToString_mA89DD76B5872401C6434CCCD64A5D19FB32C00C3 ();
// 0x000000F7 System.Void Facebook.Unity.AppRequestResult::.ctor(Facebook.Unity.ResultContainer)
extern void AppRequestResult__ctor_m16A2CBD36B61B99A284DD4F414BD45D3D93EB3E7 ();
// 0x000000F8 System.String Facebook.Unity.AppRequestResult::get_RequestID()
extern void AppRequestResult_get_RequestID_mE251958C92C77BB41B529C742B8EE86A9E8BD76D ();
// 0x000000F9 System.Void Facebook.Unity.AppRequestResult::set_RequestID(System.String)
extern void AppRequestResult_set_RequestID_mCBAAB37D93451C70A995F61FD587298C444776E8 ();
// 0x000000FA System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::get_To()
extern void AppRequestResult_get_To_m6B51C1328A39FA434AF54736E6B738BE0A858E73 ();
// 0x000000FB System.Void Facebook.Unity.AppRequestResult::set_To(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AppRequestResult_set_To_m89C22563F28CDC50E60D52EC3DF5BEE284AF33E9 ();
// 0x000000FC System.String Facebook.Unity.AppRequestResult::ToString()
extern void AppRequestResult_ToString_mC7E6B5B358943FA73C478C4E799AEB4ECEC172CB ();
// 0x000000FD System.Void Facebook.Unity.GamingServicesFriendFinderResult::.ctor(Facebook.Unity.ResultContainer)
extern void GamingServicesFriendFinderResult__ctor_mBDA75E41C0FDF72924F51C912F4851429D8760A3 ();
// 0x000000FE System.Void Facebook.Unity.GraphResult::.ctor(UnityEngine.WWW)
extern void GraphResult__ctor_m5754C0FC9077CA7E8288774646AA3E46B478578E ();
// 0x000000FF System.Void Facebook.Unity.GraphResult::set_ResultList(System.Collections.Generic.IList`1<System.Object>)
extern void GraphResult_set_ResultList_mC7DCF98D485AAB5C9413705E03DF5D4A4F59C29C ();
// 0x00000100 UnityEngine.Texture2D Facebook.Unity.GraphResult::get_Texture()
extern void GraphResult_get_Texture_m3B4B5577997E5BB0BB7BCF574A27C22C257F503D ();
// 0x00000101 System.Void Facebook.Unity.GraphResult::set_Texture(UnityEngine.Texture2D)
extern void GraphResult_set_Texture_m76584D43DFE384A562539EF6F3F53A6B9B2A4D4E ();
// 0x00000102 System.Void Facebook.Unity.GraphResult::Init(System.String)
extern void GraphResult_Init_m0FBBBEE51D496DB73F1FFD27BC5CD4B94E016E4C ();
// 0x00000103 UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture()
// 0x00000104 System.String Facebook.Unity.IInternalResult::get_CallbackId()
// 0x00000105 System.String Facebook.Unity.IResult::get_Error()
// 0x00000106 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.IResult::get_ResultDictionary()
// 0x00000107 System.String Facebook.Unity.IResult::get_RawResult()
// 0x00000108 System.Boolean Facebook.Unity.IResult::get_Cancelled()
// 0x00000109 System.Void Facebook.Unity.LoginResult::.ctor(Facebook.Unity.ResultContainer)
extern void LoginResult__ctor_m4E8637B7A3F87227D05C3F18CCB2E8592899E8A1 ();
// 0x0000010A Facebook.Unity.AccessToken Facebook.Unity.LoginResult::get_AccessToken()
extern void LoginResult_get_AccessToken_m47F9E37AC46C9F96DB467E1F382CFD5CB83ECA22 ();
// 0x0000010B System.Void Facebook.Unity.LoginResult::set_AccessToken(Facebook.Unity.AccessToken)
extern void LoginResult_set_AccessToken_m456AF71BAB7E7821EB31D8E7EA6503A69BD98925 ();
// 0x0000010C System.String Facebook.Unity.LoginResult::ToString()
extern void LoginResult_ToString_m081BDBBDF45B5A24AE252DC9961B6F010F455F5F ();
// 0x0000010D System.Void Facebook.Unity.LoginResult::.cctor()
extern void LoginResult__cctor_m152B3FC7788B9F6C847712A915125CC3203324CC ();
// 0x0000010E System.Void Facebook.Unity.LoginStatusResult::.ctor(Facebook.Unity.ResultContainer)
extern void LoginStatusResult__ctor_m758AA176E43D11EDDCF47C6402B8751FB457CA4B ();
// 0x0000010F System.Boolean Facebook.Unity.LoginStatusResult::get_Failed()
extern void LoginStatusResult_get_Failed_m08301D7BCC634FFBD1637837B2EF558BB38A0998 ();
// 0x00000110 System.Void Facebook.Unity.LoginStatusResult::set_Failed(System.Boolean)
extern void LoginStatusResult_set_Failed_m863912CB604F116D86EB38D58AD7FBDF89785DE6 ();
// 0x00000111 System.String Facebook.Unity.LoginStatusResult::ToString()
extern void LoginStatusResult_ToString_m4079ED9FDBC40912BDAFA1EE8C03EAE657017A79 ();
// 0x00000112 System.Void Facebook.Unity.LoginStatusResult::.cctor()
extern void LoginStatusResult__cctor_mD8FA90F376958DCD066971E3CA8BE1F2ACB6D298 ();
// 0x00000113 System.Void Facebook.Unity.PayResult::.ctor(Facebook.Unity.ResultContainer)
extern void PayResult__ctor_mC64D542BD10CC1F00346B8787A6928F02F7BDB7B ();
// 0x00000114 System.Int64 Facebook.Unity.PayResult::get_ErrorCode()
extern void PayResult_get_ErrorCode_mB9105862151AC595ED778F625CDED638BE9076C5 ();
// 0x00000115 System.String Facebook.Unity.PayResult::ToString()
extern void PayResult_ToString_mE53CA326DA9D41CA153B90C102247927C2321F0D ();
// 0x00000116 System.Void Facebook.Unity.ResultBase::.ctor(Facebook.Unity.ResultContainer)
extern void ResultBase__ctor_m2CAE0928483D1A4384DD2C85A9F1F0819784B0C7 ();
// 0x00000117 System.Void Facebook.Unity.ResultBase::.ctor(Facebook.Unity.ResultContainer,System.String,System.Boolean)
extern void ResultBase__ctor_m651B927D6CD2EE83ED1A6C4A40C6A601283167B1 ();
// 0x00000118 System.String Facebook.Unity.ResultBase::get_Error()
extern void ResultBase_get_Error_mD4F7A46804E5B9A4001E9C807A2D799D0AE28D34 ();
// 0x00000119 System.Void Facebook.Unity.ResultBase::set_Error(System.String)
extern void ResultBase_set_Error_m29544B4B3A1D028709EE36A5FCCFFEB86C4E81AC ();
// 0x0000011A System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultBase::get_ResultDictionary()
extern void ResultBase_get_ResultDictionary_mD4CFE4D2BFC792DE389E9F72876A4DFC5528D3F7 ();
// 0x0000011B System.Void Facebook.Unity.ResultBase::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_set_ResultDictionary_m2A19E8F7FB4FCF0CC14D55F10A1C3D7276EAB6A8 ();
// 0x0000011C System.String Facebook.Unity.ResultBase::get_RawResult()
extern void ResultBase_get_RawResult_m6D6BA2683C3E88F01D0B2CB5A821D9ABC0E3B3C3 ();
// 0x0000011D System.Void Facebook.Unity.ResultBase::set_RawResult(System.String)
extern void ResultBase_set_RawResult_mA5CCF9498D5504387446458555DFE7D5A06F1F86 ();
// 0x0000011E System.Boolean Facebook.Unity.ResultBase::get_Cancelled()
extern void ResultBase_get_Cancelled_m19B41E6DB6EEA433580E77E63A71D91A5E953A26 ();
// 0x0000011F System.Void Facebook.Unity.ResultBase::set_Cancelled(System.Boolean)
extern void ResultBase_set_Cancelled_m79BA2A04684E91724BB89A5F78381566B740C8FE ();
// 0x00000120 System.String Facebook.Unity.ResultBase::get_CallbackId()
extern void ResultBase_get_CallbackId_mA0FB4E9F0C1D466B38A21879860C0D00D006BB5A ();
// 0x00000121 System.Void Facebook.Unity.ResultBase::set_CallbackId(System.String)
extern void ResultBase_set_CallbackId_m6CBC1033307D11C695CA729F4B3A5DF0BDE076BE ();
// 0x00000122 System.Nullable`1<System.Int64> Facebook.Unity.ResultBase::get_CanvasErrorCode()
extern void ResultBase_get_CanvasErrorCode_m5AF45E74518F6FB35C55E2ED8156BCBE6C95268D ();
// 0x00000123 System.Void Facebook.Unity.ResultBase::set_CanvasErrorCode(System.Nullable`1<System.Int64>)
extern void ResultBase_set_CanvasErrorCode_m9C0EDDC8EADA20211A6AEA44187BFA07865EC131 ();
// 0x00000124 System.String Facebook.Unity.ResultBase::ToString()
extern void ResultBase_ToString_m2FDB9E8F3A5720370BE0A37B35CA9C850AE67811 ();
// 0x00000125 System.Void Facebook.Unity.ResultBase::Init(Facebook.Unity.ResultContainer,System.String,System.Boolean,System.String)
extern void ResultBase_Init_mA6E2B11E53BF4321BFD52D524E5E6EA1F1194C49 ();
// 0x00000126 System.String Facebook.Unity.ResultBase::GetErrorValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetErrorValue_m84B8E1871AC32FF626953064B84B3053FAC896BA ();
// 0x00000127 System.Boolean Facebook.Unity.ResultBase::GetCancelledValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetCancelledValue_m40E0352BBA906C9B19551BE0BA9E5808F41D4229 ();
// 0x00000128 System.String Facebook.Unity.ResultBase::GetCallbackId(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetCallbackId_m8450CB86BC589CFA5A784F40D7D3C6E54AE067F9 ();
// 0x00000129 System.Void Facebook.Unity.ResultContainer::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer__ctor_mCE2EA3DB9A34DFCC66D0AED36E3CC0C034401AD3 ();
// 0x0000012A System.Void Facebook.Unity.ResultContainer::.ctor(System.String)
extern void ResultContainer__ctor_mDFC7D6BEBF3C775E05BA6E38C41ABEF305BA9032 ();
// 0x0000012B System.String Facebook.Unity.ResultContainer::get_RawResult()
extern void ResultContainer_get_RawResult_m29F9476502E62AA055713C88A57E022BBCC5CD3E ();
// 0x0000012C System.Void Facebook.Unity.ResultContainer::set_RawResult(System.String)
extern void ResultContainer_set_RawResult_m3FB2999AF8136B2BA37E52A093D40BF61111B645 ();
// 0x0000012D System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::get_ResultDictionary()
extern void ResultContainer_get_ResultDictionary_m3C05222908D597BA0C6C2011EE3B56765A60F8BC ();
// 0x0000012E System.Void Facebook.Unity.ResultContainer::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer_set_ResultDictionary_mACF0EA46CAA961CD145422830415B4A6A60CB94E ();
// 0x0000012F System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::GetWebFormattedResponseDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer_GetWebFormattedResponseDictionary_mCFD11D12F34F164976E5C63AE5D9F94DEA6D9298 ();
// 0x00000130 System.Void Facebook.Unity.ShareResult::.ctor(Facebook.Unity.ResultContainer)
extern void ShareResult__ctor_m070AB07FD2F133BB5B97913DA48CC2FBD94122F3 ();
// 0x00000131 System.String Facebook.Unity.ShareResult::get_PostId()
extern void ShareResult_get_PostId_mC22D0ACE189C9518C7002FA6D66214946A24B0E7 ();
// 0x00000132 System.Void Facebook.Unity.ShareResult::set_PostId(System.String)
extern void ShareResult_set_PostId_mC5D73027267DA10D0DF14C7536DD2C2C7520B454 ();
// 0x00000133 System.String Facebook.Unity.ShareResult::get_PostIDKey()
extern void ShareResult_get_PostIDKey_m213C65F61078A2317F612F5A2BFFC9370A45C680 ();
// 0x00000134 System.String Facebook.Unity.ShareResult::ToString()
extern void ShareResult_ToString_m134BB55EAE82332B5B2EF86507535AC3A4B95F2F ();
// 0x00000135 System.Void Facebook.Unity.AsyncRequestString::Post(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Post_mE581ABE2200212C32596FED01F0D6DC4A3628578 ();
// 0x00000136 System.Void Facebook.Unity.AsyncRequestString::Get(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Get_mA256ED89C65D5CA6C0EE318F783D916FF9CA60F0 ();
// 0x00000137 System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Request_mB019C996BC7E5A962E0CA0A06ACEC98B431762A2 ();
// 0x00000138 System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Request_m13082FEB25723AA8B4DA48D759286F1E0F2069EF ();
// 0x00000139 System.Collections.IEnumerator Facebook.Unity.AsyncRequestString::Start()
extern void AsyncRequestString_Start_mAB3FDA9F9ADEC7B6F64B5F46A2B8137FCAA67236 ();
// 0x0000013A Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetUrl(System.Uri)
extern void AsyncRequestString_SetUrl_mA85379325C25387FA7191A390B60B940223A0AC9 ();
// 0x0000013B Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetMethod(Facebook.Unity.HttpMethod)
extern void AsyncRequestString_SetMethod_mC161F0D3F6A8F0F7A69F9730E22BF7E4A13E71F8 ();
// 0x0000013C Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetFormData(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AsyncRequestString_SetFormData_mF7BB7510CCC355CA879C9A465FF675D100C74C7B ();
// 0x0000013D Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetQuery(UnityEngine.WWWForm)
extern void AsyncRequestString_SetQuery_m9A6A2D435027DED0F799FB8F82BE316DCD182926 ();
// 0x0000013E Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetCallback(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_SetCallback_mD669DF021B036782469E97C1D0BB0F9F2B8E62ED ();
// 0x0000013F System.Void Facebook.Unity.AsyncRequestString::.ctor()
extern void AsyncRequestString__ctor_mCC46052C9D7FCC37C2B2C950C76C8A92B7AFFD08 ();
// 0x00000140 System.Void Facebook.Unity.AsyncRequestString_<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_m2C45BEB7767380D5E1D5EB6809A2885BB73F74C2 ();
// 0x00000141 System.Void Facebook.Unity.AsyncRequestString_<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_mB1164E7F5829050169CAB916A0BEFC776A5BE033 ();
// 0x00000142 System.Boolean Facebook.Unity.AsyncRequestString_<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_m524704A9E30FD380677A89BA6B01ED2C20BB43A4 ();
// 0x00000143 System.Object Facebook.Unity.AsyncRequestString_<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C51FCB56DB3F6A50F06B2C203BE21EBA06CA00E ();
// 0x00000144 System.Void Facebook.Unity.AsyncRequestString_<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m5E459DDB3C035064FCD5331E28F98A3918940B27 ();
// 0x00000145 System.Object Facebook.Unity.AsyncRequestString_<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m11943D98D7C47AF9106300194CCE07B16CDA1452 ();
// 0x00000146 System.Void Facebook.Unity.FacebookLogger::.cctor()
extern void FacebookLogger__cctor_m44CDEBB839F913D4A668E2B28238C84D91962A10 ();
// 0x00000147 Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::get_Instance()
extern void FacebookLogger_get_Instance_m3C9694E0230947B553483BCB73112F77F150DEEC ();
// 0x00000148 System.Void Facebook.Unity.FacebookLogger::set_Instance(Facebook.Unity.IFacebookLogger)
extern void FacebookLogger_set_Instance_m48E1B8F191FACEB8F0F36A7ADAD11788F44AFAC6 ();
// 0x00000149 System.Void Facebook.Unity.FacebookLogger::Log(System.String)
extern void FacebookLogger_Log_m49FA01BA88718F187182A21DDA94B59465A7F5A2 ();
// 0x0000014A System.Void Facebook.Unity.FacebookLogger::Info(System.String)
extern void FacebookLogger_Info_m236F8C3F2F8BA48017C03D19426795B05E1B004B ();
// 0x0000014B System.Void Facebook.Unity.FacebookLogger::Warn(System.String)
extern void FacebookLogger_Warn_m84102F2C8C6A1EE3AA9D7DCB48B35A90DC05D55E ();
// 0x0000014C System.Void Facebook.Unity.FacebookLogger::Warn(System.String,System.String[])
extern void FacebookLogger_Warn_m909F9B44B0F0B6E6AE7D10190A71EA7AFB465656 ();
// 0x0000014D System.Void Facebook.Unity.FacebookLogger_DebugLogger::.ctor()
extern void DebugLogger__ctor_mD4194614600B9780168D20328B462065704D9361 ();
// 0x0000014E System.Void Facebook.Unity.FacebookLogger_DebugLogger::Log(System.String)
extern void DebugLogger_Log_m52B321444E0404A27F398D0E14DCEB275AA86DE2 ();
// 0x0000014F System.Void Facebook.Unity.FacebookLogger_DebugLogger::Info(System.String)
extern void DebugLogger_Info_m616A3FF5F1DCB7ACF02CCFE3E7738E6D521080BA ();
// 0x00000150 System.Void Facebook.Unity.FacebookLogger_DebugLogger::Warn(System.String)
extern void DebugLogger_Warn_m6091596AFFE058DC0043E22A85D35245EAD15F01 ();
// 0x00000151 System.Void Facebook.Unity.IFacebookLogger::Log(System.String)
// 0x00000152 System.Void Facebook.Unity.IFacebookLogger::Info(System.String)
// 0x00000153 System.Void Facebook.Unity.IFacebookLogger::Warn(System.String)
// 0x00000154 System.Boolean Facebook.Unity.Utilities::TryGetValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,T&)
// 0x00000155 System.Int64 Facebook.Unity.Utilities::TotalSeconds(System.DateTime)
extern void Utilities_TotalSeconds_mAEC6A02BA33B7D143FA3E912EA0FE20970E86780 ();
// 0x00000156 T Facebook.Unity.Utilities::GetValueOrDefault(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
// 0x00000157 System.String Facebook.Unity.Utilities::ToCommaSeparateList(System.Collections.Generic.IEnumerable`1<System.String>)
extern void Utilities_ToCommaSeparateList_m85995AD184CD4F88C4E55FDD0ED982BE28283838 ();
// 0x00000158 System.String Facebook.Unity.Utilities::AbsoluteUrlOrEmptyString(System.Uri)
extern void Utilities_AbsoluteUrlOrEmptyString_m42BEC0BA40D3B1F90A2E7DBCE48A5DE6D7E856D8 ();
// 0x00000159 System.String Facebook.Unity.Utilities::GetUserAgent(System.String,System.String)
extern void Utilities_GetUserAgent_mA0FCD11FA4D238E9D7E324DF0FEFD6708D71E2BC ();
// 0x0000015A System.String Facebook.Unity.Utilities::ToJson(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ToJson_m461B42D0A31BAA21819FECDA7C688DBF3E3ADE76 ();
// 0x0000015B System.Void Facebook.Unity.Utilities::AddAllKVPFrom(System.Collections.Generic.IDictionary`2<T1,T2>,System.Collections.Generic.IDictionary`2<T1,T2>)
// 0x0000015C Facebook.Unity.AccessToken Facebook.Unity.Utilities::ParseAccessTokenFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseAccessTokenFromResult_mC667C1CF41A8CFCA45E90B891E4D3EC37EF4AC07 ();
// 0x0000015D System.String Facebook.Unity.Utilities::ToStringNullOk(System.Object)
extern void Utilities_ToStringNullOk_mA7ED050EC12DB1F44BE85D7FBD7AE4224A8FC751 ();
// 0x0000015E System.String Facebook.Unity.Utilities::FormatToString(System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void Utilities_FormatToString_m4646645FDD780C289CFD04C939E3A3AC1F9E93AF ();
// 0x0000015F System.DateTime Facebook.Unity.Utilities::ParseExpirationDateFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseExpirationDateFromResult_m8A7B2B820AA3EB13797C0A67096F53D9CD702DA5 ();
// 0x00000160 System.Nullable`1<System.DateTime> Facebook.Unity.Utilities::ParseLastRefreshFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseLastRefreshFromResult_m7980EEE2ED4C5C663CC534CB8F473515C6C71B81 ();
// 0x00000161 System.Collections.Generic.ICollection`1<System.String> Facebook.Unity.Utilities::ParsePermissionFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParsePermissionFromResult_mF1C9377FED110E2F913152809F313D91FD7E355E ();
// 0x00000162 System.DateTime Facebook.Unity.Utilities::FromTimestamp(System.Int32)
extern void Utilities_FromTimestamp_m982317357AE6D261A2B459EF09A4A248D8519140 ();
// 0x00000163 System.Void Facebook.Unity.Utilities_Callback`1::.ctor(System.Object,System.IntPtr)
// 0x00000164 System.Void Facebook.Unity.Utilities_Callback`1::Invoke(T)
// 0x00000165 System.IAsyncResult Facebook.Unity.Utilities_Callback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000166 System.Void Facebook.Unity.Utilities_Callback`1::EndInvoke(System.IAsyncResult)
// 0x00000167 System.Void Facebook.Unity.Utilities_<>c::.cctor()
extern void U3CU3Ec__cctor_m2D1E30C7DD7902F4ABB302193C7E4121D56D8797 ();
// 0x00000168 System.Void Facebook.Unity.Utilities_<>c::.ctor()
extern void U3CU3Ec__ctor_m24A18F69F6F06BA6697811BFECF2BB7A1D8E48B2 ();
// 0x00000169 System.String Facebook.Unity.Utilities_<>c::<ParsePermissionFromResult>b__18_0(System.Object)
extern void U3CU3Ec_U3CParsePermissionFromResultU3Eb__18_0_m00A80EB0337291868EAEF35189A3F446DD24595C ();
// 0x0000016A Facebook.Unity.IAsyncRequestStringWrapper Facebook.Unity.FBUnityUtility::get_AsyncRequestStringWrapper()
extern void FBUnityUtility_get_AsyncRequestStringWrapper_m067CECA6BC7F45D9DF5B3850E19168B986831BF7 ();
// 0x0000016B System.Void Facebook.Unity.AsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestStringWrapper_Request_m7EA103819D83F8EC2F831A7B08D4CD3AEC365FDA ();
// 0x0000016C System.Void Facebook.Unity.AsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestStringWrapper_Request_m2384F96FD70594F8B2ED88DD3D4B4B3AE84A1D37 ();
// 0x0000016D System.Void Facebook.Unity.AsyncRequestStringWrapper::.ctor()
extern void AsyncRequestStringWrapper__ctor_m7513D273A45D18F18DE623519FC26F82C5701913 ();
// 0x0000016E System.Void Facebook.Unity.IAsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x0000016F System.Void Facebook.Unity.IAsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x00000170 System.Void Facebook.Unity.FacebookScheduler::Schedule(System.Action,System.Int64)
extern void FacebookScheduler_Schedule_m5C5A913127E25B58B3C77A1CCA363FDDED317BC5 ();
// 0x00000171 System.Collections.IEnumerator Facebook.Unity.FacebookScheduler::DelayEvent(System.Action,System.Int64)
extern void FacebookScheduler_DelayEvent_m4B990D8DE776D1A24A206EEBB34B14E945CD2979 ();
// 0x00000172 System.Void Facebook.Unity.FacebookScheduler::.ctor()
extern void FacebookScheduler__ctor_m7DD6FB4D5AF56E5CC230ADB9C561109756E2C6CB ();
// 0x00000173 System.Void Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::.ctor(System.Int32)
extern void U3CDelayEventU3Ed__1__ctor_mC6AFB74727EDBDA35869F4C1A073676A4CE2321C ();
// 0x00000174 System.Void Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.IDisposable.Dispose()
extern void U3CDelayEventU3Ed__1_System_IDisposable_Dispose_mE35E5B6A5A1C96DED2E3878A3171E25EE325DC4A ();
// 0x00000175 System.Boolean Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::MoveNext()
extern void U3CDelayEventU3Ed__1_MoveNext_mA81D7E69903B82A9EFA757B906789D90489B689E ();
// 0x00000176 System.Object Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82FF3B0FDDCE53E3C19834961E32F8D6EB70AB11 ();
// 0x00000177 System.Void Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.Collections.IEnumerator.Reset()
extern void U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_mDE6D7AEAABE3544B46EF1C57A746EA6C82E64DE3 ();
// 0x00000178 System.Object Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_m4585781C95E35C9C1E2F2ED0DB7F3873214215D1 ();
// 0x00000179 System.Void Facebook.Unity.CodelessIAPAutoLog::handlePurchaseCompleted(System.Object)
extern void CodelessIAPAutoLog_handlePurchaseCompleted_m6134F2AB0CD021611DE377ACD2A44E2E83DD788D ();
// 0x0000017A System.Void Facebook.Unity.CodelessIAPAutoLog::addListenerToIAPButtons(System.Object)
extern void CodelessIAPAutoLog_addListenerToIAPButtons_m4D4221C32C9275EB51687D013B25F8261C8640EA ();
// 0x0000017B System.Void Facebook.Unity.CodelessIAPAutoLog::addListenerToGameObject(UnityEngine.Object,System.Object)
extern void CodelessIAPAutoLog_addListenerToGameObject_m667719D427F9F547C4B1C7C5C0D83855A2CD271F ();
// 0x0000017C System.Type Facebook.Unity.CodelessIAPAutoLog::FindTypeInAssemblies(System.String,System.String)
extern void CodelessIAPAutoLog_FindTypeInAssemblies_m335975E7C237087592A585B95C0BD33A6E95D699 ();
// 0x0000017D UnityEngine.Object[] Facebook.Unity.CodelessIAPAutoLog::FindObjectsOfTypeByName(System.String,System.String)
extern void CodelessIAPAutoLog_FindObjectsOfTypeByName_mD288289A4A5070AB634B45B21DE6DC1BE13BE5A6 ();
// 0x0000017E System.Object Facebook.Unity.CodelessIAPAutoLog::GetField(System.Object,System.String)
extern void CodelessIAPAutoLog_GetField_mB59909FF146131D013E754F6C9274572B7CF95C9 ();
// 0x0000017F System.Object Facebook.Unity.CodelessIAPAutoLog::GetProperty(System.Object,System.String)
extern void CodelessIAPAutoLog_GetProperty_mBBC912F927574A873B7F257D669D1C830CB111C1 ();
// 0x00000180 System.Void Facebook.Unity.CodelessCrawler::Awake()
extern void CodelessCrawler_Awake_m95F458A3C917120B20DE8C8A1F1BFDB502424E92 ();
// 0x00000181 System.Void Facebook.Unity.CodelessCrawler::CaptureViewHierarchy(System.String)
extern void CodelessCrawler_CaptureViewHierarchy_m81B51B77F6E16873E5DB9529E158550087302735 ();
// 0x00000182 System.Collections.IEnumerator Facebook.Unity.CodelessCrawler::GenSnapshot()
extern void CodelessCrawler_GenSnapshot_m043AA29F118DF4C451C2D6C64398DC8A9077342F ();
// 0x00000183 System.Void Facebook.Unity.CodelessCrawler::SendAndroid(System.String)
extern void CodelessCrawler_SendAndroid_m9872E0B24B68E9433ED2B10BCF6F886287D37B0E ();
// 0x00000184 System.Void Facebook.Unity.CodelessCrawler::SendIos(System.String)
extern void CodelessCrawler_SendIos_mD570EBBBE0F9E64A5E7B1D4E0974CCA39D0EADC9 ();
// 0x00000185 System.String Facebook.Unity.CodelessCrawler::GenBase64Screenshot()
extern void CodelessCrawler_GenBase64Screenshot_m16C99048F0B0975D769DEDC83A70201819222125 ();
// 0x00000186 System.String Facebook.Unity.CodelessCrawler::GenViewJson()
extern void CodelessCrawler_GenViewJson_m028F67AE12B85E8A80B5EB2ECBE0635322A461FB ();
// 0x00000187 System.Void Facebook.Unity.CodelessCrawler::GenChild(UnityEngine.GameObject,System.Text.StringBuilder)
extern void CodelessCrawler_GenChild_m4C731EBDB06AA813622E3AF0163EDFA2AA66DC88 ();
// 0x00000188 System.Void Facebook.Unity.CodelessCrawler::onActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void CodelessCrawler_onActiveSceneChanged_m5F7DB1D4D40E4CEC5BA9C431038B804AE2D56285 ();
// 0x00000189 System.Void Facebook.Unity.CodelessCrawler::updateMainCamera()
extern void CodelessCrawler_updateMainCamera_m27942A3E402FAC208931B892854A93D558FFFAD4 ();
// 0x0000018A UnityEngine.Vector2 Facebook.Unity.CodelessCrawler::getScreenCoordinate(UnityEngine.Vector3,UnityEngine.RenderMode)
extern void CodelessCrawler_getScreenCoordinate_mCDDDA3C308B9B64BCFAF1833EFBDE8F3FA6E01A1 ();
// 0x0000018B System.String Facebook.Unity.CodelessCrawler::getClasstypeBitmaskButton()
extern void CodelessCrawler_getClasstypeBitmaskButton_mEACCC10CEC22C4804D6B15CEAB9DACDBB417CEC8 ();
// 0x0000018C System.String Facebook.Unity.CodelessCrawler::getVisibility(UnityEngine.GameObject)
extern void CodelessCrawler_getVisibility_m60CADC21383B6DFF97B7B1EE4178B33CE9A70576 ();
// 0x0000018D System.Void Facebook.Unity.CodelessCrawler::.ctor()
extern void CodelessCrawler__ctor_mABF7C5D46B7A7CD6F82FB7162A2A901B3329073F ();
// 0x0000018E System.Void Facebook.Unity.CodelessCrawler::.cctor()
extern void CodelessCrawler__cctor_mEFB9B1B9490B7967A6DB657F01229B60537F3AAC ();
// 0x0000018F System.Void Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::.ctor(System.Int32)
extern void U3CGenSnapshotU3Ed__4__ctor_m26BD343D41AB7197E80D3BDA7ADBFCE0FB8B0A1B ();
// 0x00000190 System.Void Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.IDisposable.Dispose()
extern void U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m5F5C4FAA3CFFE3F58EB9821F1F50F28AF3834266 ();
// 0x00000191 System.Boolean Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::MoveNext()
extern void U3CGenSnapshotU3Ed__4_MoveNext_m622B9369A4E14F1A78AB3A6AA821809BBA730F95 ();
// 0x00000192 System.Object Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD763996817A0C1E940652BB67C7BA15BA9ECED08 ();
// 0x00000193 System.Void Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_m0014A73DCEEE4D036C053DA8DC1626CF37A80BDB ();
// 0x00000194 System.Object Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m2AA5C43338C104B58A6381B3DC5483FF609C4168 ();
// 0x00000195 Facebook.Unity.FBSDKEventBindingManager Facebook.Unity.CodelessUIInteractEvent::get_eventBindingManager()
extern void CodelessUIInteractEvent_get_eventBindingManager_m903108DB2AB006B95157E891FAABF005B85E2BF4 ();
// 0x00000196 System.Void Facebook.Unity.CodelessUIInteractEvent::set_eventBindingManager(Facebook.Unity.FBSDKEventBindingManager)
extern void CodelessUIInteractEvent_set_eventBindingManager_m77E3535CAA50232C2E596BD11C8DD03D84EFD689 ();
// 0x00000197 System.Void Facebook.Unity.CodelessUIInteractEvent::Awake()
extern void CodelessUIInteractEvent_Awake_m85767BF4BCA0284F085B8CBFEEAE51ADAF52C73E ();
// 0x00000198 System.Void Facebook.Unity.CodelessUIInteractEvent::SetLoggerInitAndroid()
extern void CodelessUIInteractEvent_SetLoggerInitAndroid_mDA5573B23C0FDF853DB5E847461E59331578B936 ();
// 0x00000199 System.Void Facebook.Unity.CodelessUIInteractEvent::SetLoggerInitIos()
extern void CodelessUIInteractEvent_SetLoggerInitIos_mA1BB76D140F7983193F2778E98F058C50B42E787 ();
// 0x0000019A System.Void Facebook.Unity.CodelessUIInteractEvent::Update()
extern void CodelessUIInteractEvent_Update_mA848F7AE8C04A19F4C28EE87FF8DB580514C4107 ();
// 0x0000019B System.Void Facebook.Unity.CodelessUIInteractEvent::OnReceiveMapping(System.String)
extern void CodelessUIInteractEvent_OnReceiveMapping_m8909E208304B765A6E4D9902666B4BBFF306EDBA ();
// 0x0000019C System.Void Facebook.Unity.CodelessUIInteractEvent::.ctor()
extern void CodelessUIInteractEvent__ctor_m756D383A20DA1E3D4D8681F0F7F132BA3F807127 ();
// 0x0000019D System.Boolean Facebook.Unity.FBSDKViewHiearchy::CheckGameObjectMatchPath(UnityEngine.GameObject,System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKViewHiearchy_CheckGameObjectMatchPath_m504070140D7B5065F3C545958C0B66A1D1357984 ();
// 0x0000019E System.Boolean Facebook.Unity.FBSDKViewHiearchy::CheckPathMatchPath(System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>,System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKViewHiearchy_CheckPathMatchPath_mA875A5EFAE1E22B567040F8A0095A99A5637693B ();
// 0x0000019F System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKViewHiearchy::GetPath(UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetPath_mA087D66D725BAC2974D5EDAAB9F6FC68EDF3B8F8 ();
// 0x000001A0 System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKViewHiearchy::GetPath(UnityEngine.GameObject,System.Int32)
extern void FBSDKViewHiearchy_GetPath_m916F0E4FA8BFC0B6059920D29C257FF3AC7F7D61 ();
// 0x000001A1 UnityEngine.GameObject Facebook.Unity.FBSDKViewHiearchy::GetParent(UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetParent_m180998C7F18791647C0918BB0C6A884CBBA910CF ();
// 0x000001A2 System.Collections.Generic.Dictionary`2<System.String,System.Object> Facebook.Unity.FBSDKViewHiearchy::GetAttribute(UnityEngine.GameObject,UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetAttribute_m7BED792A9B8FEC3FECA0CA4104EF87B2DA8BC854 ();
// 0x000001A3 System.Void Facebook.Unity.FBSDKCodelessPathComponent::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FBSDKCodelessPathComponent__ctor_mC5B75C4D3083506BAE788A3B22F5080D7EFE8C3C ();
// 0x000001A4 System.String Facebook.Unity.FBSDKCodelessPathComponent::get_className()
extern void FBSDKCodelessPathComponent_get_className_mC01F410E3CD1C2C958EB1B236171710CB105CB7F ();
// 0x000001A5 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_className(System.String)
extern void FBSDKCodelessPathComponent_set_className_mA41EDD912035EF42B51450291DBF78EC8A72AA18 ();
// 0x000001A6 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_text(System.String)
extern void FBSDKCodelessPathComponent_set_text_mF1260DCDEAA4CC3D613E185C35BE5E9BFA335D2F ();
// 0x000001A7 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_hint(System.String)
extern void FBSDKCodelessPathComponent_set_hint_mA00EE1799937BE9857DFF85F1A7E546E6671D620 ();
// 0x000001A8 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_desc(System.String)
extern void FBSDKCodelessPathComponent_set_desc_m35B53D04D500EAF815BFCED5ED1567088F9F0147 ();
// 0x000001A9 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_tag(System.String)
extern void FBSDKCodelessPathComponent_set_tag_m82C812E954913BF90F03B86A3D3F2A879C5A7F77 ();
// 0x000001AA System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_index(System.Int64)
extern void FBSDKCodelessPathComponent_set_index_mF94268F506981E18A8B515BA5C5FBBDCE6358109 ();
// 0x000001AB System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_section(System.Int64)
extern void FBSDKCodelessPathComponent_set_section_mF90C205E04A4E1FE1775EA0FF1ACCEFF2F6867D3 ();
// 0x000001AC System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_row(System.Int64)
extern void FBSDKCodelessPathComponent_set_row_mA7C4078D93E04BF095A4DBA208F0C7EC37D5F402 ();
// 0x000001AD System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_matchBitmask(System.Int64)
extern void FBSDKCodelessPathComponent_set_matchBitmask_m0C9B748B4D36DC20D10DB7281E891B55FAB3CE1C ();
// 0x000001AE System.Void Facebook.Unity.FBSDKEventBinding::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FBSDKEventBinding__ctor_mA63F55D4928166006B44C3F439A3EEA2F67103E7 ();
// 0x000001AF System.String Facebook.Unity.FBSDKEventBinding::get_eventName()
extern void FBSDKEventBinding_get_eventName_m201219D55CDFDC6085AC9A601016AA838174A48C ();
// 0x000001B0 System.Void Facebook.Unity.FBSDKEventBinding::set_eventName(System.String)
extern void FBSDKEventBinding_set_eventName_m75E04EE87B8AB2C4F9D66CFA46CE039D415B94AB ();
// 0x000001B1 System.String Facebook.Unity.FBSDKEventBinding::get_eventType()
extern void FBSDKEventBinding_get_eventType_m3C93A2D8F43D556FBD4DA116853ECD87F2A091C8 ();
// 0x000001B2 System.Void Facebook.Unity.FBSDKEventBinding::set_eventType(System.String)
extern void FBSDKEventBinding_set_eventType_m4AB1D037855CFD48838304E37CDE3D73E7D601F7 ();
// 0x000001B3 System.String Facebook.Unity.FBSDKEventBinding::get_appVersion()
extern void FBSDKEventBinding_get_appVersion_m31C3339623CFC95F67525AFAD7A352B915DE8437 ();
// 0x000001B4 System.Void Facebook.Unity.FBSDKEventBinding::set_appVersion(System.String)
extern void FBSDKEventBinding_set_appVersion_m26646F28FF0F956590F5307F88A0611E875E6BB8 ();
// 0x000001B5 System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKEventBinding::get_path()
extern void FBSDKEventBinding_get_path_mE00F3F2F32F6341F2C04F6ECFDD434C265EB01DB ();
// 0x000001B6 System.Void Facebook.Unity.FBSDKEventBinding::set_path(System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKEventBinding_set_path_mAFDB809C79099C6571EBB33716A7ED2F021D477F ();
// 0x000001B7 System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding> Facebook.Unity.FBSDKEventBindingManager::get_eventBindings()
extern void FBSDKEventBindingManager_get_eventBindings_m3B88C2FFFDD625EA8D6A9751174E74AC04A4CE8F ();
// 0x000001B8 System.Void Facebook.Unity.FBSDKEventBindingManager::set_eventBindings(System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding>)
extern void FBSDKEventBindingManager_set_eventBindings_mDCF6A561143E32091163A00BDDC3AB58695E0562 ();
// 0x000001B9 System.Void Facebook.Unity.FBSDKEventBindingManager::.ctor(System.Collections.Generic.List`1<System.Object>)
extern void FBSDKEventBindingManager__ctor_m2254BAC2D498A7A818693AB936BD92E69F3BBB72 ();
// 0x000001BA System.Void Facebook.Unity.MediaUploadResult::.ctor(Facebook.Unity.ResultContainer)
extern void MediaUploadResult__ctor_m086E98643A34D68DFD4DDD2E9FF1EEEBEC6807CB ();
// 0x000001BB System.String Facebook.Unity.MediaUploadResult::get_MediaId()
extern void MediaUploadResult_get_MediaId_m56B597E3628CF1792120CCCD69425AE0F3879FEA ();
// 0x000001BC System.Void Facebook.Unity.MediaUploadResult::set_MediaId(System.String)
extern void MediaUploadResult_set_MediaId_m67EDDE32431653CF4FF9A76FF97D22C40D30DDDA ();
// 0x000001BD System.String Facebook.Unity.MediaUploadResult::ToString()
extern void MediaUploadResult_ToString_m0C03CA0CC689342EC819F3395EF318A6D294D9C4 ();
// 0x000001BE System.Void Facebook.Unity.Gameroom.GameroomFacebook::.ctor()
extern void GameroomFacebook__ctor_m75D9E977D6FABEDDC0AFA66B15D8A266D029A6FC ();
// 0x000001BF System.Void Facebook.Unity.Gameroom.GameroomFacebook::.ctor(Facebook.Unity.Gameroom.IGameroomWrapper,Facebook.Unity.CallbackManager)
extern void GameroomFacebook__ctor_m0AAADA9B93E6B8CE6C5C19B3A7BE365AE437CB5E ();
// 0x000001C0 System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::get_LimitEventUsage()
extern void GameroomFacebook_get_LimitEventUsage_m5A0117100CA465383A60E1EB2BFB6E191A4756F1 ();
// 0x000001C1 System.Void Facebook.Unity.Gameroom.GameroomFacebook::set_LimitEventUsage(System.Boolean)
extern void GameroomFacebook_set_LimitEventUsage_mEA82D0EC24868E0DB9C69F0E11A4951E3C2A4632 ();
// 0x000001C2 System.String Facebook.Unity.Gameroom.GameroomFacebook::get_SDKName()
extern void GameroomFacebook_get_SDKName_m474451D32996754DE84107B4A6115DD00CB4C20C ();
// 0x000001C3 System.String Facebook.Unity.Gameroom.GameroomFacebook::get_SDKVersion()
extern void GameroomFacebook_get_SDKVersion_mE74B90CC9E8A86BAA0BE5641CC36C0D3248D9892 ();
// 0x000001C4 System.Void Facebook.Unity.Gameroom.GameroomFacebook::Init(System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void GameroomFacebook_Init_m26654070FB2EBF288B706D41B998629162403619 ();
// 0x000001C5 System.Void Facebook.Unity.Gameroom.GameroomFacebook::ActivateApp(System.String)
extern void GameroomFacebook_ActivateApp_m9FD80C5E9E0B5B7CD57967B93AC7C93C7E1F638B ();
// 0x000001C6 System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameroomFacebook_AppEventsLogEvent_m0C7BB01C7A7D11A859F463FA5BB2AF4079D2DD34 ();
// 0x000001C7 System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameroomFacebook_AppEventsLogPurchase_m1207C1D2F0992FDB29014EA614C31BC6DD4DCDD5 ();
// 0x000001C8 System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void GameroomFacebook_AppRequest_m9AE6333DAB6DC76268ECC64883E606FAF62BFC75 ();
// 0x000001C9 System.Void Facebook.Unity.Gameroom.GameroomFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void GameroomFacebook_FeedShare_mBF858817705F69278028C4B61AC42D78FC68292D ();
// 0x000001CA System.Void Facebook.Unity.Gameroom.GameroomFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void GameroomFacebook_ShareLink_m21AA489880B8ECDB3DC88833B27C201481ACBFC1 ();
// 0x000001CB System.Void Facebook.Unity.Gameroom.GameroomFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void GameroomFacebook_Pay_mBB86F76B25E826F8DE4C9DAC1CAAC0EA5147CBD9 ();
// 0x000001CC System.Void Facebook.Unity.Gameroom.GameroomFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void GameroomFacebook_GetAppLink_mD9BA2D130EA75A6426FB88860048250BC6FDA993 ();
// 0x000001CD System.Void Facebook.Unity.Gameroom.GameroomFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LogInWithPublishPermissions_mE483E395219B44D27B45409403D40F7AF02D9C83 ();
// 0x000001CE System.Void Facebook.Unity.Gameroom.GameroomFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LogInWithReadPermissions_mF682783384EB9BFA76A1E3C5B82DF2DB317B1B8E ();
// 0x000001CF System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnAppRequestsComplete_m1DED31CF4252C34F4570D9430ECD8085D8D259C7 ();
// 0x000001D0 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnGetAppLinkComplete_m8DF054DF87AF112F4F94E7CF2699BE0E2B52F3D8 ();
// 0x000001D1 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnLoginComplete_m61EB2FBBCE19B70EA6AE47C87FF3A7DAC0ABF71B ();
// 0x000001D2 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnShareLinkComplete_mA379AD89C7F35AB380C6E0B04DB9274F87483AC6 ();
// 0x000001D3 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnPayComplete_mA39DD62E51B0E43AB3CF5C181D228DEC0AD7B63B ();
// 0x000001D4 System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::HaveReceivedPipeResponse()
extern void GameroomFacebook_HaveReceivedPipeResponse_m75A0BDF9C82549B9F73E08DEC3F0D89A9A2FCB0F ();
// 0x000001D5 System.String Facebook.Unity.Gameroom.GameroomFacebook::GetPipeResponse(System.String)
extern void GameroomFacebook_GetPipeResponse_m5C5C06626B6CE520D08B62AA7EFA7771EF19E08A ();
// 0x000001D6 Facebook.Unity.Gameroom.IGameroomWrapper Facebook.Unity.Gameroom.GameroomFacebook::GetGameroomWrapper()
extern void GameroomFacebook_GetGameroomWrapper_m4182830486699CC56AC06B6482708608ACD9BB4C ();
// 0x000001D7 System.Void Facebook.Unity.Gameroom.GameroomFacebook::PayImpl(System.String,System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void GameroomFacebook_PayImpl_m66E7B8DE460E879A94E4F195DED676D9CEA7DCB9 ();
// 0x000001D8 System.Void Facebook.Unity.Gameroom.GameroomFacebook::LoginWithPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LoginWithPermissions_mFE4357DE7339536C644925F6A7E90E55851F2499 ();
// 0x000001D9 System.Void Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::.ctor(System.Object,System.IntPtr)
extern void OnComplete__ctor_m6E67D51EBB676E05D457E85638E7FF3554D2942C ();
// 0x000001DA System.Void Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::Invoke(Facebook.Unity.ResultContainer)
extern void OnComplete_Invoke_m7879FE547463B62F936C960FC00C81CC7E594C65 ();
// 0x000001DB System.IAsyncResult Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::BeginInvoke(Facebook.Unity.ResultContainer,System.AsyncCallback,System.Object)
extern void OnComplete_BeginInvoke_m88A70BCEF109462012DA6805B09E993FC601A06A ();
// 0x000001DC System.Void Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::EndInvoke(System.IAsyncResult)
extern void OnComplete_EndInvoke_mAA3C355E0781B47E13135286D6608C5411DD88C4 ();
// 0x000001DD Facebook.Unity.Gameroom.IGameroomFacebookImplementation Facebook.Unity.Gameroom.GameroomFacebookGameObject::get_GameroomFacebookImpl()
extern void GameroomFacebookGameObject_get_GameroomFacebookImpl_m7BF8041CEE360F2DB134D16798DD7E5AFA1452ED ();
// 0x000001DE System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::WaitForResponse(Facebook.Unity.Gameroom.GameroomFacebook_OnComplete,System.String)
extern void GameroomFacebookGameObject_WaitForResponse_mDC901CD9EAB55E57438D9ED03EC84328A3606B92 ();
// 0x000001DF System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::OnAwake()
extern void GameroomFacebookGameObject_OnAwake_mA861D13691A5C8245E52D8D863DF5BF94CE7A6D3 ();
// 0x000001E0 System.Collections.IEnumerator Facebook.Unity.Gameroom.GameroomFacebookGameObject::WaitForPipeResponse(Facebook.Unity.Gameroom.GameroomFacebook_OnComplete,System.String)
extern void GameroomFacebookGameObject_WaitForPipeResponse_mD812990A356B78E2E458C53831CA35ABFD25A63D ();
// 0x000001E1 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::.ctor()
extern void GameroomFacebookGameObject__ctor_m5BBB180B9078EDEBA1F6DEE225A814C8A2F4D0AF ();
// 0x000001E2 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::.ctor(System.Int32)
extern void U3CWaitForPipeResponseU3Ed__4__ctor_mF073F306B1616D799DC6B40900EA698DBF9EA171 ();
// 0x000001E3 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.IDisposable.Dispose()
extern void U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mA3E69F7B470DA19AB63DEFFC6976E96E7928851D ();
// 0x000001E4 System.Boolean Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::MoveNext()
extern void U3CWaitForPipeResponseU3Ed__4_MoveNext_m6769DA5C13BD0755A697861B4807F7B8122E12E2 ();
// 0x000001E5 System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m249328CA40CDB1C9175566EFACA2E8C336CF6841 ();
// 0x000001E6 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_mE6229A72DECE4FEAF4508665517422C9305DB6BA ();
// 0x000001E7 System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m2F4DF059987E11B58752854C4D5DBB7A6F94E9D7 ();
// 0x000001E8 Facebook.Unity.FacebookGameObject Facebook.Unity.Gameroom.GameroomFacebookLoader::get_FBGameObject()
extern void GameroomFacebookLoader_get_FBGameObject_m1C0C9820EACB3602EF62954C29B88976E8F51333 ();
// 0x000001E9 System.Void Facebook.Unity.Gameroom.GameroomFacebookLoader::.ctor()
extern void GameroomFacebookLoader__ctor_mF8C09DFDBBE0A129F8C10BDEE76C927242237E40 ();
// 0x000001EA System.Boolean Facebook.Unity.Gameroom.IGameroomFacebookImplementation::HaveReceivedPipeResponse()
// 0x000001EB System.String Facebook.Unity.Gameroom.IGameroomFacebookImplementation::GetPipeResponse(System.String)
// 0x000001EC System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.Gameroom.IGameroomWrapper::get_PipeResponse()
// 0x000001ED System.Void Facebook.Unity.Gameroom.IGameroomWrapper::set_PipeResponse(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
// 0x000001EE System.Void Facebook.Unity.Gameroom.IGameroomWrapper::Init(Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001EF System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoLoginRequest(System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001F0 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoPayRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001F1 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoFeedShareRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001F2 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoAppRequestRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001F3 System.Void Facebook.Unity.Editor.EditorFacebook::.ctor(Facebook.Unity.Editor.IEditorWrapper,Facebook.Unity.CallbackManager)
extern void EditorFacebook__ctor_mFA801CC2BC490ACEA1128BBE3E51D605D1DD0406 ();
// 0x000001F4 System.Void Facebook.Unity.Editor.EditorFacebook::.ctor()
extern void EditorFacebook__ctor_m7CE97032E5E441E7E5182374D3C49459E5844BD3 ();
// 0x000001F5 System.Boolean Facebook.Unity.Editor.EditorFacebook::get_LimitEventUsage()
extern void EditorFacebook_get_LimitEventUsage_mFB01AA7051EC052B354CEA0BD1108792580F848C ();
// 0x000001F6 System.Void Facebook.Unity.Editor.EditorFacebook::set_LimitEventUsage(System.Boolean)
extern void EditorFacebook_set_LimitEventUsage_m7A4043707FA05B22266E9F57F5F7126D69755C24 ();
// 0x000001F7 System.Void Facebook.Unity.Editor.EditorFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void EditorFacebook_set_ShareDialogMode_mDB866B9239E05778AE7F3052BE5ACE85809FB255 ();
// 0x000001F8 System.String Facebook.Unity.Editor.EditorFacebook::get_SDKName()
extern void EditorFacebook_get_SDKName_m3F875A63ABA28659C65F9D92893568D97FACD123 ();
// 0x000001F9 System.String Facebook.Unity.Editor.EditorFacebook::get_SDKVersion()
extern void EditorFacebook_get_SDKVersion_m17229683C98D13360595E71C40ED5DB6411DBEF5 ();
// 0x000001FA Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorFacebook::get_EditorGameObject()
extern void EditorFacebook_get_EditorGameObject_m16F8C2106C6F7375E9B1A539C9C8B1E0141E7638 ();
// 0x000001FB System.Void Facebook.Unity.Editor.EditorFacebook::Init(Facebook.Unity.InitDelegate)
extern void EditorFacebook_Init_m5879CA96E1332C0C111927D254C4241F2A73D139 ();
// 0x000001FC System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void EditorFacebook_LogInWithReadPermissions_m260E6292D20E542B68DF4A0DF2AB69AE00DA5479 ();
// 0x000001FD System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void EditorFacebook_LogInWithPublishPermissions_m27FB1A71875C89F499EE07A72260FA8032BC46BB ();
// 0x000001FE System.Void Facebook.Unity.Editor.EditorFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void EditorFacebook_AppRequest_m7D45AC49B7C57595700EE460D0C0630688C73113 ();
// 0x000001FF System.Void Facebook.Unity.Editor.EditorFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void EditorFacebook_ShareLink_m7E81F1042D2CF846B303E54B0EA9A8C4A7A3679F ();
// 0x00000200 System.Void Facebook.Unity.Editor.EditorFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void EditorFacebook_FeedShare_mA21B56EA9CAE446092E31B7162ED55D5F7D9D79E ();
// 0x00000201 System.Void Facebook.Unity.Editor.EditorFacebook::ActivateApp(System.String)
extern void EditorFacebook_ActivateApp_m806544E11B7C2370BAEA88407ED7AB46795AE4A6 ();
// 0x00000202 System.Void Facebook.Unity.Editor.EditorFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void EditorFacebook_GetAppLink_m672147215795D812815D2034559791E90CC2ECD0 ();
// 0x00000203 System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void EditorFacebook_AppEventsLogEvent_m47348C253B96FD792342B4F7AE0F97F9B93BFAAF ();
// 0x00000204 System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void EditorFacebook_AppEventsLogPurchase_m9A627289347D5BCCF091AB94B434D33755B97442 ();
// 0x00000205 System.Boolean Facebook.Unity.Editor.EditorFacebook::IsImplicitPurchaseLoggingEnabled()
extern void EditorFacebook_IsImplicitPurchaseLoggingEnabled_m121A7985FFE16EE7438ACB6B131D6F458BDA72EC ();
// 0x00000206 System.Void Facebook.Unity.Editor.EditorFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void EditorFacebook_FetchDeferredAppLink_m4B977D6FAF58AD23D61DF6CA1311D058E2850AD3 ();
// 0x00000207 System.Void Facebook.Unity.Editor.EditorFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void EditorFacebook_Pay_m5D5B07BBBB6FCE9A0DED40EB2400E2539A718E15 ();
// 0x00000208 System.Void Facebook.Unity.Editor.EditorFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void EditorFacebook_RefreshCurrentAccessToken_mB0320075100F893CCADDFD7191B574F88EAFC30B ();
// 0x00000209 System.Void Facebook.Unity.Editor.EditorFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnAppRequestsComplete_mB7FCBD504172890739C1B78260BC89241488779F ();
// 0x0000020A System.Void Facebook.Unity.Editor.EditorFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnGetAppLinkComplete_mD71DDB9985D9BA784080640B1D356D2E2462C60A ();
// 0x0000020B System.Void Facebook.Unity.Editor.EditorFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnLoginComplete_m285A38D29DEBE87B7760A4D1D5E463B03B2C7297 ();
// 0x0000020C System.Void Facebook.Unity.Editor.EditorFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnShareLinkComplete_m30FB4C191D72A7A795EA1816D2B171FF22F51057 ();
// 0x0000020D System.Void Facebook.Unity.Editor.EditorFacebook::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFetchDeferredAppLinkComplete_m91825C013D59F79836581A11DE437ABDBC5DB851 ();
// 0x0000020E System.Void Facebook.Unity.Editor.EditorFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnPayComplete_m7D2048F3735C25D6EC34683E027E8A88A7FF4E62 ();
// 0x0000020F System.Void Facebook.Unity.Editor.EditorFacebook::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnRefreshCurrentAccessTokenComplete_m0070CB4102384A500FF2A0085079BBBE22546DAA ();
// 0x00000210 System.Void Facebook.Unity.Editor.EditorFacebook::OnFriendFinderComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFriendFinderComplete_m99620A5EC2E98C728DA765FC23FC654177F874A8 ();
// 0x00000211 System.Void Facebook.Unity.Editor.EditorFacebook::OnUploadImageToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnUploadImageToMediaLibraryComplete_m8C3F4141A503546B79023AE2C26F09697D999339 ();
// 0x00000212 System.Void Facebook.Unity.Editor.EditorFacebook::OnUploadVideoToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnUploadVideoToMediaLibraryComplete_m46F1F5A95564DA023F47E8593EA384AB60ED7968 ();
// 0x00000213 System.Void Facebook.Unity.Editor.EditorFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void EditorFacebook_OpenFriendFinderDialog_m0EBF9D785BEF1A15129C9F78101A48D91DFA6ADF ();
// 0x00000214 System.Void Facebook.Unity.Editor.EditorFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void EditorFacebook_UploadImageToMediaLibrary_mBF93BC3CFBF14B9A692F77044DCE200450568E83 ();
// 0x00000215 System.Void Facebook.Unity.Editor.EditorFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void EditorFacebook_UploadVideoToMediaLibrary_m8319686282A77007BBCD12329CFD0624C550184A ();
// 0x00000216 System.Void Facebook.Unity.Editor.EditorFacebook::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFacebookAuthResponseChange_mA87A6BEDD01E6D1EC406B786D853E3A7E2340009 ();
// 0x00000217 System.Void Facebook.Unity.Editor.EditorFacebook::OnUrlResponse(System.String)
extern void EditorFacebook_OnUrlResponse_m8588D18959262C250E42DD04F22378E92C1A40C9 ();
// 0x00000218 System.Void Facebook.Unity.Editor.EditorFacebook::OnHideUnity(System.Boolean)
extern void EditorFacebook_OnHideUnity_mAC1E2D31F16519339FE2CFF2FA65F05E4E8F6FE8 ();
// 0x00000219 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnAwake()
extern void EditorFacebookGameObject_OnAwake_m1E51C63CCD02DFC7C5A515C11F7778C31106F705 ();
// 0x0000021A System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnEnable()
extern void EditorFacebookGameObject_OnEnable_mF7328EBA8E443BFE0A4A23892FE47DFCE2DF304D ();
// 0x0000021B System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void EditorFacebookGameObject_OnSceneLoaded_mFF4D404839F523EB153131DBA08E210C29570C8D ();
// 0x0000021C System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnDisable()
extern void EditorFacebookGameObject_OnDisable_mE2FDE5131237DE1044DEA616A2108203CAB00BB2 ();
// 0x0000021D System.Void Facebook.Unity.Editor.EditorFacebookGameObject::onPurchaseCompleteHandler(System.Object)
extern void EditorFacebookGameObject_onPurchaseCompleteHandler_mFC88C9436109F548C0043051FBF58F4D4764D339 ();
// 0x0000021E System.Void Facebook.Unity.Editor.EditorFacebookGameObject::.ctor()
extern void EditorFacebookGameObject__ctor_m7AB93F33659FB08CE7B03FBC2E1696AA5DC0F116 ();
// 0x0000021F Facebook.Unity.FacebookGameObject Facebook.Unity.Editor.EditorFacebookLoader::get_FBGameObject()
extern void EditorFacebookLoader_get_FBGameObject_mF55FA0448896FC2B275DA75F3020FB3C10068AA2 ();
// 0x00000220 System.Void Facebook.Unity.Editor.EditorFacebookLoader::.ctor()
extern void EditorFacebookLoader__ctor_mE3FDB685084309F33FA83D0E3AE548930499AB00 ();
// 0x00000221 Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::get_Callback()
extern void EditorFacebookMockDialog_get_Callback_mCEE3B3DEA3B9A7EFDB6506712B17051B55EE3AB6 ();
// 0x00000222 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_Callback(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>)
extern void EditorFacebookMockDialog_set_Callback_m29C78371CAD1A83DA7D6AC28990AEB786D557E5D ();
// 0x00000223 System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_CallbackID()
extern void EditorFacebookMockDialog_get_CallbackID_mA2D915A145D79137A2ED7A6CA76E8654BB2DC636 ();
// 0x00000224 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_CallbackID(System.String)
extern void EditorFacebookMockDialog_set_CallbackID_mF5D663C905CC826C09172CD01682674EC3211083 ();
// 0x00000225 System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_DialogTitle()
// 0x00000226 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::Start()
extern void EditorFacebookMockDialog_Start_mFB0EB48D14D95AE01D83DF5EF938C7FF2A1096D0 ();
// 0x00000227 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUI()
extern void EditorFacebookMockDialog_OnGUI_mB99D115254946EBE7E544272CED72E3AA50C286E ();
// 0x00000228 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::DoGui()
// 0x00000229 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendSuccessResult()
// 0x0000022A System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendCancelResult()
extern void EditorFacebookMockDialog_SendCancelResult_mED5FBE23C6C523B3D70779D044CB8EA206FE129D ();
// 0x0000022B System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendErrorResult(System.String)
extern void EditorFacebookMockDialog_SendErrorResult_mB640E7047D4B4D41F8ADCC6ABA0F7A90CA9FAE80 ();
// 0x0000022C System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUIDialog(System.Int32)
extern void EditorFacebookMockDialog_OnGUIDialog_m34FF0BEABF08D955B613AC8B046E4F26CF951F5E ();
// 0x0000022D System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::.ctor()
extern void EditorFacebookMockDialog__ctor_mCC7AFAFC68499E410C1A195BCB64451F08FE7025 ();
// 0x0000022E System.Void Facebook.Unity.Editor.EditorWrapper::.ctor(Facebook.Unity.IFacebookCallbackHandler)
extern void EditorWrapper__ctor_m8E3E1B551900D78694D03E05E517DE8F51233C2A ();
// 0x0000022F System.Void Facebook.Unity.Editor.EditorWrapper::Init()
extern void EditorWrapper_Init_m0E3C47D2A387A018137A90795DAE5F3857D73D2B ();
// 0x00000230 System.Void Facebook.Unity.Editor.EditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowLoginMockDialog_mE58A43F1D76756112867CD9485155C49384CD204 ();
// 0x00000231 System.Void Facebook.Unity.Editor.EditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern void EditorWrapper_ShowAppRequestMockDialog_m31A60FA161D7104BD7B9760971EB6B0FA226A39C ();
// 0x00000232 System.Void Facebook.Unity.Editor.EditorWrapper::ShowPayMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern void EditorWrapper_ShowPayMockDialog_mA47E31819D9863D95CB67D643E4ABC752EC9C552 ();
// 0x00000233 System.Void Facebook.Unity.Editor.EditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowMockShareDialog_m2B73EBD5CBD87F47A0989788641A35F2694A8A6D ();
// 0x00000234 System.Void Facebook.Unity.Editor.EditorWrapper::ShowMockFriendFinderDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowMockFriendFinderDialog_m87067BB95EE23D2E401DA0F6AA7693FFDA2E5D73 ();
// 0x00000235 System.Void Facebook.Unity.Editor.EditorWrapper::ShowEmptyMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowEmptyMockDialog_m259878BB3752ECB8FF93D2AB046D71EF11741BA3 ();
// 0x00000236 System.Void Facebook.Unity.Editor.IEditorWrapper::Init()
// 0x00000237 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x00000238 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String)
// 0x00000239 System.Void Facebook.Unity.Editor.IEditorWrapper::ShowPayMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String)
// 0x0000023A System.Void Facebook.Unity.Editor.IEditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x0000023B System.Void Facebook.Unity.Editor.IEditorWrapper::ShowMockFriendFinderDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x0000023C System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_EmptyDialogTitle()
extern void EmptyMockDialog_get_EmptyDialogTitle_mFEDE84A7E5A703E80B37B37D1DE34D056D6B98BD ();
// 0x0000023D System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::set_EmptyDialogTitle(System.String)
extern void EmptyMockDialog_set_EmptyDialogTitle_m0CA314DC54D21EB042E4E4D62815C2935D019239 ();
// 0x0000023E System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_DialogTitle()
extern void EmptyMockDialog_get_DialogTitle_m005D87C1A7A91BF1A9F23716A3F2A7ACA66EB5E5 ();
// 0x0000023F System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::DoGui()
extern void EmptyMockDialog_DoGui_mFD61120D47EF90A828725C591921AC7FA2B0717B ();
// 0x00000240 System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::SendSuccessResult()
extern void EmptyMockDialog_SendSuccessResult_m2C4E92488ABED70D9F07D089654A2DBD0AA630BE ();
// 0x00000241 System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::.ctor()
extern void EmptyMockDialog__ctor_m38B860A2E95FD888C1C34823FBD1B62FAB5224FD ();
// 0x00000242 System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::get_DialogTitle()
extern void MockLoginDialog_get_DialogTitle_mE759177997D0285091014DB5C1C2B4BA078003C3 ();
// 0x00000243 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::DoGui()
extern void MockLoginDialog_DoGui_m8D58C43A20099387DF06C4B0A4A150184CDA49B7 ();
// 0x00000244 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::SendSuccessResult()
extern void MockLoginDialog_SendSuccessResult_m6F59BAAAA33C4401741C864661DAF66A2E4CF55F ();
// 0x00000245 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::.ctor()
extern void MockLoginDialog__ctor_mD54C6EA5D5B18DEC94D1724365DF06E957681635 ();
// 0x00000246 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF2A5ABD5A07CF4BB9BCADD0072EB65483EC2E62F ();
// 0x00000247 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0::<SendSuccessResult>b__0(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass4_0_U3CSendSuccessResultU3Eb__0_m061E66F29110EE00082A03F7D227D3F6569264A0 ();
// 0x00000248 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mAFFD5DCA86110CEF3E15E475493CD38A6B385E30 ();
// 0x00000249 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_1::<SendSuccessResult>b__1(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass4_1_U3CSendSuccessResultU3Eb__1_m65D7020D94C4C23234D9A1673CC4CC752965EB25 ();
// 0x0000024A System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::get_SubTitle()
extern void MockShareDialog_get_SubTitle_m823A30971FE7427DEB103BF2D2B2C97611404B1D ();
// 0x0000024B System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::set_SubTitle(System.String)
extern void MockShareDialog_set_SubTitle_mBC479EA793258A1980BE0E32FA2113335F78AF89 ();
// 0x0000024C System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::get_DialogTitle()
extern void MockShareDialog_get_DialogTitle_m7AEB561E333C2E0DBAF4F6E0DA9715A2B1F26BAA ();
// 0x0000024D System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::DoGui()
extern void MockShareDialog_DoGui_m08B8D3D04BFC10AE40BB9E9B8C26606391DEE86D ();
// 0x0000024E System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::SendSuccessResult()
extern void MockShareDialog_SendSuccessResult_m0B289A60115FA917712A4C39B3A58ED99E5AF59F ();
// 0x0000024F System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::SendCancelResult()
extern void MockShareDialog_SendCancelResult_mA16A342E8873B72B67C4A704E41FD8C0D84B4A63 ();
// 0x00000250 System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::GenerateFakePostID()
extern void MockShareDialog_GenerateFakePostID_mC9194AFF8438A50C585A5F5824117BCFB1684D9A ();
// 0x00000251 System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::.ctor()
extern void MockShareDialog__ctor_m2A82D041CB346B7708C4E2E3418E47ECE9FBA6B0 ();
// 0x00000252 System.Void Facebook.Unity.Mobile.IMobileFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
// 0x00000253 System.Void Facebook.Unity.Mobile.IMobileFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x00000254 System.Void Facebook.Unity.Mobile.IMobileFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
// 0x00000255 System.Boolean Facebook.Unity.Mobile.IMobileFacebook::IsImplicitPurchaseLoggingEnabled()
// 0x00000256 System.Void Facebook.Unity.Mobile.IMobileFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
// 0x00000257 System.Void Facebook.Unity.Mobile.IMobileFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
// 0x00000258 System.Void Facebook.Unity.Mobile.IMobileFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
// 0x00000259 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x0000025A System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
// 0x0000025B System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnFriendFinderComplete(Facebook.Unity.ResultContainer)
// 0x0000025C System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnUploadImageToMediaLibraryComplete(Facebook.Unity.ResultContainer)
// 0x0000025D System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnUploadVideoToMediaLibraryComplete(Facebook.Unity.ResultContainer)
// 0x0000025E System.Void Facebook.Unity.Mobile.MobileFacebook::.ctor(Facebook.Unity.CallbackManager)
extern void MobileFacebook__ctor_m64301CCE10C1F82E06768DA8FCC9AC8DAC3D013F ();
// 0x0000025F System.Void Facebook.Unity.Mobile.MobileFacebook::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void MobileFacebook_set_ShareDialogMode_m98FA88AF18913065FCCC53B9CA56F4C1975B5010 ();
// 0x00000260 System.Void Facebook.Unity.Mobile.MobileFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x00000261 System.Void Facebook.Unity.Mobile.MobileFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
// 0x00000262 System.Boolean Facebook.Unity.Mobile.MobileFacebook::IsImplicitPurchaseLoggingEnabled()
// 0x00000263 System.Void Facebook.Unity.Mobile.MobileFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnLoginComplete_m0609D52E9F1EE2D04FE9E35C9F0190E2D1A54AA7 ();
// 0x00000264 System.Void Facebook.Unity.Mobile.MobileFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnGetAppLinkComplete_mEE11BD67EEA88FC7B7188F2802EC53F096C5705B ();
// 0x00000265 System.Void Facebook.Unity.Mobile.MobileFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnAppRequestsComplete_m93A9C9DC97B4464EA2059E598528FFC79EF77C80 ();
// 0x00000266 System.Void Facebook.Unity.Mobile.MobileFacebook::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnFetchDeferredAppLinkComplete_m1CCAD5D03BE2E8925F7668504F7AACC086ACF372 ();
// 0x00000267 System.Void Facebook.Unity.Mobile.MobileFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnShareLinkComplete_mE3DC18B678E3F3EDD98B4AC921FEE9727C23BFD0 ();
// 0x00000268 System.Void Facebook.Unity.Mobile.MobileFacebook::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnRefreshCurrentAccessTokenComplete_m3B43A30F8CC73A3632515C63EA44AFBF98C743BE ();
// 0x00000269 System.Void Facebook.Unity.Mobile.MobileFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void MobileFacebook_OpenFriendFinderDialog_m5F3B23D100F1E0AE8054ACDB65F1C8705445A472 ();
// 0x0000026A System.Void Facebook.Unity.Mobile.MobileFacebook::OnFriendFinderComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnFriendFinderComplete_m863B7889DE1E19A8860B2A57C9D90EB9E8A29266 ();
// 0x0000026B System.Void Facebook.Unity.Mobile.MobileFacebook::OnUploadImageToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnUploadImageToMediaLibraryComplete_m67455C34DD5B68D8AE79B13D1E93133C6EBAF4BC ();
// 0x0000026C System.Void Facebook.Unity.Mobile.MobileFacebook::OnUploadVideoToMediaLibraryComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnUploadVideoToMediaLibraryComplete_m2B0FE8CB188A151730548767C0611884CCDB9F88 ();
// 0x0000026D System.Void Facebook.Unity.Mobile.MobileFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void MobileFacebook_UploadImageToMediaLibrary_m952C20E7BD3773FE6F716495530FB77F5D908981 ();
// 0x0000026E System.Void Facebook.Unity.Mobile.MobileFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void MobileFacebook_UploadVideoToMediaLibrary_mB023F5062EAD8B70E6CB21AE1813B202A2AB7EAA ();
// 0x0000026F System.Void Facebook.Unity.Mobile.MobileFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
// 0x00000270 Facebook.Unity.Mobile.IMobileFacebookImplementation Facebook.Unity.Mobile.MobileFacebookGameObject::get_MobileFacebook()
extern void MobileFacebookGameObject_get_MobileFacebook_m35EDB34BF9712FFCC306E3E57426880A23450933 ();
// 0x00000271 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnFetchDeferredAppLinkComplete(System.String)
extern void MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_m967F10654BA20892BD19305CBD5311920DB514E4 ();
// 0x00000272 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnRefreshCurrentAccessTokenComplete(System.String)
extern void MobileFacebookGameObject_OnRefreshCurrentAccessTokenComplete_m7C76331B4D493E49F795B3FC1D964C827A68C75E ();
// 0x00000273 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnFriendFinderComplete(System.String)
extern void MobileFacebookGameObject_OnFriendFinderComplete_m643864C7029A18BB987F99F586B57650BE553C73 ();
// 0x00000274 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnUploadImageToMediaLibraryComplete(System.String)
extern void MobileFacebookGameObject_OnUploadImageToMediaLibraryComplete_m0FC79749DFF4670E9B0879809BF0F73847392D51 ();
// 0x00000275 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnUploadVideoToMediaLibraryComplete(System.String)
extern void MobileFacebookGameObject_OnUploadVideoToMediaLibraryComplete_m39574D18CDC43B925DDFF69DD99027DC9F101AFB ();
// 0x00000276 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::.ctor()
extern void MobileFacebookGameObject__ctor_mCF5F75B7D9E8DFF4999B2347C85FB15B7F5DD937 ();
// 0x00000277 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
// 0x00000278 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
// 0x00000279 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
// 0x0000027A System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogOut()
// 0x0000027B System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::SetShareDialogMode(System.Int32)
// 0x0000027C System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
// 0x0000027D System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
// 0x0000027E System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
// 0x0000027F System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FBAppEventsActivateApp()
// 0x00000280 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
// 0x00000281 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
// 0x00000282 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
// 0x00000283 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::GetAppLink(System.Int32)
// 0x00000284 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::RefreshCurrentAccessToken(System.Int32)
// 0x00000285 System.String Facebook.Unity.Mobile.IOS.IIOSWrapper::FBSdkVersion()
// 0x00000286 System.String Facebook.Unity.Mobile.IOS.IIOSWrapper::FBGetUserID()
// 0x00000287 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::OpenFriendFinderDialog(System.Int32)
// 0x00000288 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::UploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
// 0x00000289 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::UploadVideoToMediaLibrary(System.Int32,System.String,System.String)
// 0x0000028A System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FetchDeferredAppLink(System.Int32)
// 0x0000028B System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor()
extern void IOSFacebook__ctor_m66612F017B01E22AF4200B953EDBB664C80901FA ();
// 0x0000028C System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor(Facebook.Unity.Mobile.IOS.IIOSWrapper,Facebook.Unity.CallbackManager)
extern void IOSFacebook__ctor_mAE9F8C2F9D35ADB0C9DFA8B9F3E5BDE243896364 ();
// 0x0000028D System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::get_LimitEventUsage()
extern void IOSFacebook_get_LimitEventUsage_mA5E590A095B1BBE4E8E3285CE0429FD4EFB01672 ();
// 0x0000028E System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::set_LimitEventUsage(System.Boolean)
extern void IOSFacebook_set_LimitEventUsage_m3723CE720A35A85B434E8D4A337BF374313AC439 ();
// 0x0000028F System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKName()
extern void IOSFacebook_get_SDKName_m5D047EA4CB01307C7BEADDC62C91AB2A9B7522E5 ();
// 0x00000290 System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKVersion()
extern void IOSFacebook_get_SDKVersion_m6BC591D20F4DC586DC7BA68A7F506EA9313286BF ();
// 0x00000291 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::Init(System.String,System.Boolean,System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void IOSFacebook_Init_mAD89E53CB720C9C973FC984BB6878D07ED2BBECB ();
// 0x00000292 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void IOSFacebook_LogInWithReadPermissions_m10AE773BD51319F4D371F6A1345806D0C52BB835 ();
// 0x00000293 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void IOSFacebook_LogInWithPublishPermissions_m6BA64446C13FB593F1656B18688E1F42C3761182 ();
// 0x00000294 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogOut()
extern void IOSFacebook_LogOut_mB9CD5A19E98779E3970FE3D00DC30467C6348728 ();
// 0x00000295 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void IOSFacebook_AppRequest_m5544FD3F8DF8EA732B03C73E92FF81D9B05C3370 ();
// 0x00000296 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void IOSFacebook_ShareLink_mA585AFB25C60B10AAB45FB6C66451CEF1872407D ();
// 0x00000297 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void IOSFacebook_FeedShare_mA5706979D224C9F0834867DDBDE02F2AA11AF063 ();
// 0x00000298 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_AppEventsLogEvent_mD614F1CB0EBD7D4BF61C2965709BC60FC139EB4E ();
// 0x00000299 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_AppEventsLogPurchase_m78C5C159FD8BCD82690B3EE8D6ABA804A168B187 ();
// 0x0000029A System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::IsImplicitPurchaseLoggingEnabled()
extern void IOSFacebook_IsImplicitPurchaseLoggingEnabled_m88E675394465260B2CEAD1D20A4B08C43FA4D19A ();
// 0x0000029B System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ActivateApp(System.String)
extern void IOSFacebook_ActivateApp_mE940AFD42A4ABF6D86B6C4F89CD4A203A95F3612 ();
// 0x0000029C System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void IOSFacebook_FetchDeferredAppLink_m85BFA2DC0D088C693B81EDFFF9A794948C452A4D ();
// 0x0000029D System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void IOSFacebook_GetAppLink_mE987B5A522F5CDE2ECFDF20FA7434D46A7132A44 ();
// 0x0000029E System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void IOSFacebook_OpenFriendFinderDialog_mA80DBF45C7A7A630BA20EA3A7D6F3927A651E88C ();
// 0x0000029F System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void IOSFacebook_RefreshCurrentAccessToken_mEF5849F54B204128141B82602B24F3A4C3EB5FE5 ();
// 0x000002A0 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void IOSFacebook_SetShareDialogMode_m81FD42A4AA6A6B6B41356363E1DC564548EB0FB4 ();
// 0x000002A1 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void IOSFacebook_UploadImageToMediaLibrary_m451642A6A8494541C880770E18C1E44757B2D9BC ();
// 0x000002A2 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void IOSFacebook_UploadVideoToMediaLibrary_mB78BC61FF5A90909EF104AFD82B3D35AE8C59C8B ();
// 0x000002A3 Facebook.Unity.Mobile.IOS.IIOSWrapper Facebook.Unity.Mobile.IOS.IOSFacebook::GetIOSWrapper()
extern void IOSFacebook_GetIOSWrapper_m93739800C1B290A298E02C1D62466C2A9590B9CB ();
// 0x000002A4 Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict Facebook.Unity.Mobile.IOS.IOSFacebook::MarshallDict(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_MarshallDict_m516D3ED4003592C2F9F0E7BBF531CFA7575AEE1A ();
// 0x000002A5 System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook::AddCallback(Facebook.Unity.FacebookDelegate`1<T>)
// 0x000002A6 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::.ctor()
extern void NativeDict__ctor_mBC808DB561E6E697E9D81544254525C1EE322E96 ();
// 0x000002A7 System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::get_NumEntries()
extern void NativeDict_get_NumEntries_mA4035D61E41A7267AC7CCD531CA7E4181299B198 ();
// 0x000002A8 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::set_NumEntries(System.Int32)
extern void NativeDict_set_NumEntries_mC5EBBD8A2CFC4418058276C06DFCACDAFBBB2A43 ();
// 0x000002A9 System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::get_Keys()
extern void NativeDict_get_Keys_m9EC76C4C54010B19B6312671B748C0C10D9E44D4 ();
// 0x000002AA System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::set_Keys(System.String[])
extern void NativeDict_set_Keys_m00E0CE25465A48D54E26F0D8B5BDA92DFFFBD912 ();
// 0x000002AB System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::get_Values()
extern void NativeDict_get_Values_m61379EC768664B378BA4297B546E186E0564F2F3 ();
// 0x000002AC System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::set_Values(System.String[])
extern void NativeDict_set_Values_m083A5DA8B5D49D43A2D972AA5C60D847E5CDC9D2 ();
// 0x000002AD System.Void Facebook.Unity.Mobile.IOS.IOSFacebookGameObject::.ctor()
extern void IOSFacebookGameObject__ctor_m8A13B35BE8A48DB191E2EF6F22337D9E32E6809A ();
// 0x000002AE Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.IOS.IOSFacebookLoader::get_FBGameObject()
extern void IOSFacebookLoader_get_FBGameObject_mC5E93577DCC84FE1E8B2FAA6D29801172FD8F9C6 ();
// 0x000002AF System.Void Facebook.Unity.Mobile.IOS.IOSFacebookLoader::.ctor()
extern void IOSFacebookLoader__ctor_mCC8417942DBA9DA0CA81FC6C4ACFE145AD84FF0C ();
// 0x000002B0 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::.ctor()
extern void AndroidFacebook__ctor_m800B0B0BCE70FCCF3304C1AD7720E3F0592FF367 ();
// 0x000002B1 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::.ctor(Facebook.Unity.Mobile.Android.IAndroidWrapper,Facebook.Unity.CallbackManager)
extern void AndroidFacebook__ctor_m6B6223DCABDE2372A473E4BF67B63717CA49E6E2 ();
// 0x000002B2 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::set_KeyHash(System.String)
extern void AndroidFacebook_set_KeyHash_mAF5F5CC14C52CB5BCCDC4ABE6964170113C86E41 ();
// 0x000002B3 System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::get_LimitEventUsage()
extern void AndroidFacebook_get_LimitEventUsage_mB85D17C46FE27AB60B292976AE7DAF71A4BBA20F ();
// 0x000002B4 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::set_LimitEventUsage(System.Boolean)
extern void AndroidFacebook_set_LimitEventUsage_m6FF98E6C4EE77AD6DE6D5AF4ED8F0F40E9CAC478 ();
// 0x000002B5 System.String Facebook.Unity.Mobile.Android.AndroidFacebook::get_SDKName()
extern void AndroidFacebook_get_SDKName_m352E98581EF8467E2445803D01046BFF32797AE9 ();
// 0x000002B6 System.String Facebook.Unity.Mobile.Android.AndroidFacebook::get_SDKVersion()
extern void AndroidFacebook_get_SDKVersion_mB82A585ABBEF09C0399B2E773012864D4ABE1F42 ();
// 0x000002B7 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::Init(System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void AndroidFacebook_Init_m224D6B6E25629F946A14CE95EBCAB471065C5BD5 ();
// 0x000002B8 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void AndroidFacebook_LogInWithReadPermissions_mBA0DBA4B6EABAFEB3A98F64B79320AC0DE3BE4E8 ();
// 0x000002B9 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void AndroidFacebook_LogInWithPublishPermissions_m6905FFC3A1B9DFE7E38C9AE146DD63ABCC836381 ();
// 0x000002BA System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogOut()
extern void AndroidFacebook_LogOut_m701021EAF696BE586564B3DD6B8AA0A5BBA8FFD6 ();
// 0x000002BB System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::OnLoginStatusRetrieved(Facebook.Unity.ResultContainer)
extern void AndroidFacebook_OnLoginStatusRetrieved_mEA3348D2AF0E5C41E544C72470FD30C372BF1399 ();
// 0x000002BC System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void AndroidFacebook_AppRequest_mA7E92F6EED4D28261C714BF65B2901566915F833 ();
// 0x000002BD System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void AndroidFacebook_ShareLink_m68C9AF5C32AD9CFDA1F6848F103216BC3BCB3DD9 ();
// 0x000002BE System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void AndroidFacebook_FeedShare_m907B87E18718BBB49CBE3B833896E3278E723D44 ();
// 0x000002BF System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void AndroidFacebook_GetAppLink_mA0A4D763DED55A2F3A8FE94B5B3511AA35FA365A ();
// 0x000002C0 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AndroidFacebook_AppEventsLogEvent_m74F1B4D7B5FF87D9076FA51201CCA4A430718FC0 ();
// 0x000002C1 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AndroidFacebook_AppEventsLogPurchase_mFB7DA169F23F5FFF7CECA7071E0417CC7FED8C42 ();
// 0x000002C2 System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::IsImplicitPurchaseLoggingEnabled()
extern void AndroidFacebook_IsImplicitPurchaseLoggingEnabled_mD8DBA49C7803286A76F38559936779A7CF523026 ();
// 0x000002C3 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ActivateApp(System.String)
extern void AndroidFacebook_ActivateApp_mBCF3BEBA898F8C6D50094D1A17AA89E58FD47C2C ();
// 0x000002C4 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::FetchDeferredAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void AndroidFacebook_FetchDeferredAppLink_m2CF239925BF1EBDF4C98ECD1CB45DDB262AC97E1 ();
// 0x000002C5 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern void AndroidFacebook_RefreshCurrentAccessToken_m6A7194ECC41C03C663FDDB65EC176415BAF88F72 ();
// 0x000002C6 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::OpenFriendFinderDialog(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGamingServicesFriendFinderResult>)
extern void AndroidFacebook_OpenFriendFinderDialog_m7165566A778DC538CFF01C291D035CC506086799 ();
// 0x000002C7 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::UploadImageToMediaLibrary(System.String,System.Uri,System.Boolean,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void AndroidFacebook_UploadImageToMediaLibrary_m27BC076D3A825F6E1007ED07263E7BA416D0EAA2 ();
// 0x000002C8 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::UploadVideoToMediaLibrary(System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IMediaUploadResult>)
extern void AndroidFacebook_UploadVideoToMediaLibrary_m1171F1827A44F0A5EBE6CD58F48BAA299F850D6C ();
// 0x000002C9 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::SetShareDialogMode(Facebook.Unity.ShareDialogMode)
extern void AndroidFacebook_SetShareDialogMode_m0FF7CC1894B561EB614202A37091D665B15CF375 ();
// 0x000002CA Facebook.Unity.Mobile.Android.IAndroidWrapper Facebook.Unity.Mobile.Android.AndroidFacebook::GetAndroidWrapper()
extern void AndroidFacebook_GetAndroidWrapper_m3D896094780BABFCCB04229CC55E955B8C81A21F ();
// 0x000002CB System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::CallFB(System.String,System.String)
extern void AndroidFacebook_CallFB_mFDE5EFB9BA0F76DFDED07133026443BE996CF484 ();
// 0x000002CC System.Void Facebook.Unity.Mobile.Android.AndroidFacebook_JavaMethodCall`1::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
// 0x000002CD System.Void Facebook.Unity.Mobile.Android.AndroidFacebook_JavaMethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x000002CE System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnAwake()
extern void AndroidFacebookGameObject_OnAwake_m70BA8568725EDF5158885029A7DADB28C0E1D90D ();
// 0x000002CF System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnEnable()
extern void AndroidFacebookGameObject_OnEnable_m94851BD756E9C63D4426002B9E50A39AB98B6B53 ();
// 0x000002D0 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void AndroidFacebookGameObject_OnSceneLoaded_mD354C4AFCCB51A867A73BD9229DA36761CEA2559 ();
// 0x000002D1 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnDisable()
extern void AndroidFacebookGameObject_OnDisable_mB4BD63EFF9FAE9B7F04433E2D8B531E63B931AF0 ();
// 0x000002D2 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::onPurchaseCompleteHandler(System.Object)
extern void AndroidFacebookGameObject_onPurchaseCompleteHandler_m3344999539A9709FCE4E81409D6F7AA0086E68F6 ();
// 0x000002D3 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnLoginStatusRetrieved(System.String)
extern void AndroidFacebookGameObject_OnLoginStatusRetrieved_mD6E19C1763E6E53AD29B6A2F3668DD0A88AB2FB0 ();
// 0x000002D4 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::.ctor()
extern void AndroidFacebookGameObject__ctor_m1A573E9903D5D3819941809894EC2B87FE86EA40 ();
// 0x000002D5 Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.Android.AndroidFacebookLoader::get_FBGameObject()
extern void AndroidFacebookLoader_get_FBGameObject_m5C2200E46EF11DE5D3B216740D617A0F4B985910 ();
// 0x000002D6 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookLoader::.ctor()
extern void AndroidFacebookLoader__ctor_m3A28A42BA8DBFD20624977568405C156DD517C94 ();
// 0x000002D7 T Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic(System.String)
// 0x000002D8 System.Void Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic(System.String,System.Object[])
// 0x000002D9 System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor()
extern void CanvasFacebook__ctor_mAF4280810AE65FACD32D9F2A406B120BC5C0978E ();
// 0x000002DA System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor(Facebook.Unity.Canvas.ICanvasJSWrapper,Facebook.Unity.CallbackManager)
extern void CanvasFacebook__ctor_mFADC2680DDB21C5C75FC627902A7473B4A4F8D2C ();
// 0x000002DB Facebook.Unity.Canvas.ICanvasJSWrapper Facebook.Unity.Canvas.CanvasFacebook::GetCanvasJSWrapper()
extern void CanvasFacebook_GetCanvasJSWrapper_m5C906D34F407334A421A7C62937E789844397BE2 ();
// 0x000002DC System.Boolean Facebook.Unity.Canvas.CanvasFacebook::get_LimitEventUsage()
extern void CanvasFacebook_get_LimitEventUsage_mDFAC3E98E7DC016D792129BDABCFDA987417C257 ();
// 0x000002DD System.Void Facebook.Unity.Canvas.CanvasFacebook::set_LimitEventUsage(System.Boolean)
extern void CanvasFacebook_set_LimitEventUsage_mA5CAE0BDD28B8E60AE681E3B00370ABF90DE38C8 ();
// 0x000002DE System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKName()
extern void CanvasFacebook_get_SDKName_m52514C262E7990DC7B41410F10DB9B4F32F2793B ();
// 0x000002DF System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKVersion()
extern void CanvasFacebook_get_SDKVersion_m7880B755A503B19B268E71558D9A14C1EE34375D ();
// 0x000002E0 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKUserAgent()
extern void CanvasFacebook_get_SDKUserAgent_m19431C58E396634D7429D2D49D6C6C37EA779A21 ();
// 0x000002E1 System.Void Facebook.Unity.Canvas.CanvasFacebook::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void CanvasFacebook_Init_mA43AC226A703BA8677E8BD9713B6CC79872E2429 ();
// 0x000002E2 System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void CanvasFacebook_LogInWithPublishPermissions_m7A170A9C73CEE157C17932BD339D90A75B80EA77 ();
// 0x000002E3 System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void CanvasFacebook_LogInWithReadPermissions_m64B3E3C2AD44AEDE09E5B96191D1AB26834837DF ();
// 0x000002E4 System.Void Facebook.Unity.Canvas.CanvasFacebook::LogOut()
extern void CanvasFacebook_LogOut_m25024805EC3651911654C2E046B6D74CDB60F2F2 ();
// 0x000002E5 System.Void Facebook.Unity.Canvas.CanvasFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void CanvasFacebook_AppRequest_m5812F7A82FF6D31C357DA6048838D03F44FBC3A4 ();
// 0x000002E6 System.Void Facebook.Unity.Canvas.CanvasFacebook::ActivateApp(System.String)
extern void CanvasFacebook_ActivateApp_mDCACD1EA9ACF5745F0F514D389CDEE1639035E45 ();
// 0x000002E7 System.Void Facebook.Unity.Canvas.CanvasFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void CanvasFacebook_ShareLink_mC9F8DE654F767AD417256EBFB617724FEE927211 ();
// 0x000002E8 System.Void Facebook.Unity.Canvas.CanvasFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void CanvasFacebook_FeedShare_m2E5F8273A317FEC6F30594A26351C7C8D1D77F0E ();
// 0x000002E9 System.Void Facebook.Unity.Canvas.CanvasFacebook::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void CanvasFacebook_Pay_mED1A10923D32675F0EA667CCED168AD2BBD167BD ();
// 0x000002EA System.Void Facebook.Unity.Canvas.CanvasFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void CanvasFacebook_GetAppLink_m4A8A761C399DA40C926B59DE918708DAC448E3CB ();
// 0x000002EB System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void CanvasFacebook_AppEventsLogEvent_m3FB61636EFDF1AF85F5D8CD9DD5736C43D6A24F5 ();
// 0x000002EC System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void CanvasFacebook_AppEventsLogPurchase_mF4643714209EE1B3A223C07E230C72B6B672B504 ();
// 0x000002ED System.Void Facebook.Unity.Canvas.CanvasFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnLoginComplete_mBA95037F894DA26A90C080843392EAC5ED7DCC85 ();
// 0x000002EE System.Void Facebook.Unity.Canvas.CanvasFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnGetAppLinkComplete_mF03DC22FE451BA303D6ED9A2E98AAD9056808F9F ();
// 0x000002EF System.Void Facebook.Unity.Canvas.CanvasFacebook::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnFacebookAuthResponseChange_m59510BB88E1862F409C219780E4F3A12C423CF8E ();
// 0x000002F0 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnPayComplete_mEF0FA3268C48B395B4EA8DCBB67E0A4D0240129D ();
// 0x000002F1 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnAppRequestsComplete_m42FEEB4555D523C49FBAF2D28FDAD392B3007E89 ();
// 0x000002F2 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnShareLinkComplete_m30972AB940C0066206F399059C87CA6B628B886F ();
// 0x000002F3 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnUrlResponse(System.String)
extern void CanvasFacebook_OnUrlResponse_mA0B2F834F29ABC42756D9780EE3728566411A7A8 ();
// 0x000002F4 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnHideUnity(System.Boolean)
extern void CanvasFacebook_OnHideUnity_m1D8B2ACFB991E4CADE939B3BC048F21D5899B762 ();
// 0x000002F5 System.Void Facebook.Unity.Canvas.CanvasFacebook::FormatAuthResponse(Facebook.Unity.ResultContainer,Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>)
extern void CanvasFacebook_FormatAuthResponse_m29957544DDD56A5A7911E90E4FA882E01CF1E6C6 ();
// 0x000002F6 System.Void Facebook.Unity.Canvas.CanvasFacebook::PayImpl(System.String,System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern void CanvasFacebook_PayImpl_m191F4B5B34CC18F647BE3D74D843D2E2E716A2F8 ();
// 0x000002F7 System.Void Facebook.Unity.Canvas.CanvasFacebook::<OnLoginComplete>b__37_0(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m4976D88F6EC1F51DDE66F742272949D98B167914 ();
// 0x000002F8 System.Void Facebook.Unity.Canvas.CanvasFacebook_CanvasUIMethodCall`1::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
// 0x000002F9 System.Void Facebook.Unity.Canvas.CanvasFacebook_CanvasUIMethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x000002FA System.Void Facebook.Unity.Canvas.CanvasFacebook_CanvasUIMethodCall`1::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
// 0x000002FB System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c::.cctor()
extern void U3CU3Ec__cctor_m244FD850ECB15E1A1A7C09A540C0F713F498E1C4 ();
// 0x000002FC System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c::.ctor()
extern void U3CU3Ec__ctor_m60D600AA093FBB576041472E737BCA5AE1A07B75 ();
// 0x000002FD System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c::<OnFacebookAuthResponseChange>b__40_0(Facebook.Unity.ResultContainer)
extern void U3CU3Ec_U3COnFacebookAuthResponseChangeU3Eb__40_0_m353B3A6103E036AFA439E1A6878664840189723B ();
// 0x000002FE System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_mB9419A09D98EFB529D535C8BAEE69B23BB7A2D04 ();
// 0x000002FF System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0::<FormatAuthResponse>b__0(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass47_0_U3CFormatAuthResponseU3Eb__0_m318AED6BDC7EE45070AEC6A005A736DAD7D30D2B ();
// 0x00000300 Facebook.Unity.Canvas.ICanvasFacebookImplementation Facebook.Unity.Canvas.CanvasFacebookGameObject::get_CanvasFacebookImpl()
extern void CanvasFacebookGameObject_get_CanvasFacebookImpl_mF03728D98E491DD5427B077857F9F7392477BBA9 ();
// 0x00000301 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnPayComplete(System.String)
extern void CanvasFacebookGameObject_OnPayComplete_m45BB56D2AD9499D3384FC4A871973790C8A4D042 ();
// 0x00000302 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnFacebookAuthResponseChange(System.String)
extern void CanvasFacebookGameObject_OnFacebookAuthResponseChange_mCCB70C044A695B52168DAEDC7B03ED33C4C0E168 ();
// 0x00000303 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnUrlResponse(System.String)
extern void CanvasFacebookGameObject_OnUrlResponse_m210BCF7226AC5875AD5ABB6D15CE349C828E3146 ();
// 0x00000304 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnHideUnity(System.Boolean)
extern void CanvasFacebookGameObject_OnHideUnity_mCA39A8B0FD8140F5501C98A3390484544A1C4852 ();
// 0x00000305 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnAwake()
extern void CanvasFacebookGameObject_OnAwake_m540CDA0C56DF537DA311C724A58C563D51D17429 ();
// 0x00000306 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::.ctor()
extern void CanvasFacebookGameObject__ctor_m000BB25669D557B3FAA159D5DF9C14C165F3C7C0 ();
// 0x00000307 Facebook.Unity.FacebookGameObject Facebook.Unity.Canvas.CanvasFacebookLoader::get_FBGameObject()
extern void CanvasFacebookLoader_get_FBGameObject_mBFCA70D2129F2AE1EECA0A85E82925944F078987 ();
// 0x00000308 System.Void Facebook.Unity.Canvas.CanvasFacebookLoader::.ctor()
extern void CanvasFacebookLoader__ctor_m338849AC01B654E2DAE491A297A5E81FAC2DB822 ();
// 0x00000309 System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnPayComplete(System.String)
// 0x0000030A System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnFacebookAuthResponseChange(System.String)
// 0x0000030B System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnUrlResponse(System.String)
// 0x0000030C System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnHideUnity(System.Boolean)
// 0x0000030D System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnPayComplete(Facebook.Unity.ResultContainer)
// 0x0000030E System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
// 0x0000030F System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnUrlResponse(System.String)
// 0x00000310 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnHideUnity(System.Boolean)
// 0x00000311 System.String Facebook.Unity.Canvas.ICanvasJSWrapper::GetSDKVersion()
// 0x00000312 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::DisableFullScreen()
// 0x00000313 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Init(System.String,System.String,System.Int32,System.String,System.Int32)
// 0x00000314 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Login(System.Collections.Generic.IEnumerable`1<System.String>,System.String)
// 0x00000315 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Logout()
// 0x00000316 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::ActivateApp()
// 0x00000317 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.String)
// 0x00000318 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::LogPurchase(System.Single,System.String,System.String)
// 0x00000319 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Ui(System.String,System.String,System.String)
// 0x0000031A System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::InitScreenPosition()
// 0x0000031B System.Void Facebook.Unity.Canvas.JsBridge::Start()
extern void JsBridge_Start_m4205C6750F943E0DC06530D67565414BB515EBEC ();
// 0x0000031C System.Void Facebook.Unity.Canvas.JsBridge::OnLoginComplete(System.String)
extern void JsBridge_OnLoginComplete_m2CE331CD13A23446CD908DBA3B053599F88B66C8 ();
// 0x0000031D System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookAuthResponseChange(System.String)
extern void JsBridge_OnFacebookAuthResponseChange_m0AAFA9598A4A71A0D5F9F10D84444794527C26E3 ();
// 0x0000031E System.Void Facebook.Unity.Canvas.JsBridge::OnPayComplete(System.String)
extern void JsBridge_OnPayComplete_mA92E40AD47675A5BBBBA3AD541FB858B3E6B462B ();
// 0x0000031F System.Void Facebook.Unity.Canvas.JsBridge::OnAppRequestsComplete(System.String)
extern void JsBridge_OnAppRequestsComplete_mACAB31A9A07B73ABDFEBD7C7DBEE4460425A732B ();
// 0x00000320 System.Void Facebook.Unity.Canvas.JsBridge::OnShareLinkComplete(System.String)
extern void JsBridge_OnShareLinkComplete_mC7DAEC7062D471DE350BA1F29366682408E45C49 ();
// 0x00000321 System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookFocus(System.String)
extern void JsBridge_OnFacebookFocus_mFA68B4FD1B25525FC46B32196AAB8564264A9AAC ();
// 0x00000322 System.Void Facebook.Unity.Canvas.JsBridge::OnInitComplete(System.String)
extern void JsBridge_OnInitComplete_m0E64BC6B0EBAB3F0E62B4F18608011EB95AACB20 ();
// 0x00000323 System.Void Facebook.Unity.Canvas.JsBridge::OnUrlResponse(System.String)
extern void JsBridge_OnUrlResponse_m740A08D5288D7FFB1D29CDB499FD182A9B76EAD9 ();
// 0x00000324 System.Void Facebook.Unity.Canvas.JsBridge::.ctor()
extern void JsBridge__ctor_m420BF13BA948B69C701DCA7F8144E502AD63EA04 ();
static Il2CppMethodPointer s_methodPointers[804] = 
{
	Json_Deserialize_mD457D1E5A4D052EFDE5216A6D29E6A5173CFC129,
	Json_Serialize_m7A8C1B1AE5AD8547DE3B0A5FA67C4DF79A097AE0,
	Json__cctor_mF29D657C09E73D9144F1AA429A97388241BB039A,
	Parser__ctor_m774D5188FD4910AD900F1EA7EA8FCCF1F14B4219,
	Parser_get_PeekChar_m318D1925B32A148068BAED86C52BB1E3BE1684A3,
	Parser_get_NextChar_m3C8D7E32AF4450D24FE19448704394EAEC7757FF,
	Parser_get_NextWord_m319391DE7A33D2D492C0F34F3C66B884CED4EC01,
	Parser_get_NextToken_m99B8BB73199B24DE25ADB808FCB6D73E300ABC1E,
	Parser_Parse_m11269AA6FC5AEC43D48761292A9E3183661C620D,
	Parser_Dispose_m12A54D23F022479AF8413C7A396198887830F6A6,
	Parser_ParseObject_mBDCCE4D00E8715B1CD5BF7EF8481B513C22E5020,
	Parser_ParseArray_m44D0E4652B03ABF3555C88FDE4618A97ED1E0F88,
	Parser_ParseValue_mBF5D02EF66E7837C5A2064C37B01963062079B7D,
	Parser_ParseByToken_m668183645AF32273B0B973C72B1DE0BD22B4C98B,
	Parser_ParseString_m811F635CE7678BED3ED10B931CDC6792B8090FBE,
	Parser_ParseNumber_m970BA8742514F0F851697D43D6DFFC68FB462A0E,
	Parser_EatWhitespace_m593814103F5069BC08E4B3BD1C1ABD943DDE4FAB,
	Serializer__ctor_m08B6F57E91D667D48003A087D00209947F5DF8A4,
	Serializer_Serialize_mAB4A450BFFA0BA3B6EA4FA1343D2F0FE27CDF0C6,
	Serializer_SerializeValue_m44DF1E1239708E84DED26228E0B19FD31F2F2814,
	Serializer_SerializeObject_m417DA6002FB1E7CAC4EF032DC8245488E98CA12A,
	Serializer_SerializeArray_mF863F8896BF00D0F66326B47BF13B7E30FDDEFE1,
	Serializer_SerializeString_mF84FEBC2C6794A547A1831215E545289DB9361EB,
	Serializer_SerializeOther_mD67D9D2C840DD3A88D5DA92C431FD1CAE2A1AD92,
	AccessToken__ctor_mB8A77C718530154A525F31F0EBEDD80DB559D1ED,
	AccessToken_get_CurrentAccessToken_m13595BFE82BABD89E825951DBD385A777B111456,
	AccessToken_set_CurrentAccessToken_m4CC5129C6FEC856E1C80E03A770BB46D789EFD81,
	AccessToken_get_TokenString_m5358AABD0A44AA1F5664F93A9543F990DCA46D74,
	AccessToken_set_TokenString_mF814F1B8E97395B4FDF4CAB88E69A0B6AA5C88C1,
	AccessToken_get_ExpirationTime_m68DFE53A1F10E3059BBE2781D5E4C5BFBDE955AE,
	AccessToken_set_ExpirationTime_m07B3B011E8CB81A3D7F703C8168599F2227FDB17,
	AccessToken_get_Permissions_m9C12C0140717961695A07170D29997F234086B25,
	AccessToken_set_Permissions_m6D221CB2E6956192F5AC05DC0F8372D842EBA734,
	AccessToken_get_UserId_m6A292624EB7B830F117057DD79EEB0C5BA2809BC,
	AccessToken_set_UserId_mD406272464DA7806C22D3973A1466F2CF2729939,
	AccessToken_get_LastRefresh_m0D58E94638FA92C7222CFF32447653B8E5131D72,
	AccessToken_set_LastRefresh_m614BFAB83B147F84931E19D03EF61F7A4D0D1BE1,
	AccessToken_get_GraphDomain_m052F2656A15F3A311922AE24A876098151764B66,
	AccessToken_set_GraphDomain_m12C8E2D6DED115896636A316A4E37DA88704E029,
	AccessToken_ToString_m76F38B75D62CF33C0E3CA0F628A0A2EFCD221E1A,
	AccessToken_ToJson_m636079271D15A9F225A9DE63736EDBF2C412083F,
	NULL,
	CallbackManager_OnFacebookResponse_mD054881D43474F70B5882EC382C2EE40F2800B4C,
	CallbackManager_CallCallback_m4503B80223B4FDB720FE56F1889F3CCA9D0E2198,
	NULL,
	CallbackManager__ctor_mC0168BDF32B716ADD5CCBDB939AD7DFF90FF742C,
	ComponentFactory_get_FacebookGameObject_m0A194DADC5B8AD0BE03909FC45B8D73127A6615E,
	NULL,
	NULL,
	Constants_get_GraphUrl_m5238E7C321F36003524DE171D8283BB6E69C4685,
	Constants_get_GraphApiUserAgent_mD87358FFD5A372B88DDF28688BBAC2AC7773E3D1,
	Constants_get_IsMobile_m993267365AA7A5997A8847F7B9AAAD9085FA593E,
	Constants_get_IsEditor_mF25D5FD4D5E6634B2946B23CB5384A482A35FF9D,
	Constants_get_IsWeb_m7355944255950FA4BC21FC474193646B6DF70835,
	Constants_get_IsGameroom_m96DD7D4F9C7591EBA8F0785DA042B5AD24FD9B72,
	Constants_get_UnitySDKUserAgentSuffixLegacy_mF4299844782178B55576CEC64191BB8C1C3B07B3,
	Constants_get_UnitySDKUserAgent_m9B09D7176AC2B77DC563D1C2CB2192BE39E52FC9,
	Constants_get_DebugMode_mC1BDD7AF06975C5BB0DBD72579236BC00C91005B,
	Constants_get_CurrentPlatform_mB0F7FCDB1EC2D4FAEE2B03EF0D7CA0F15BA9F767,
	Constants_GetCurrentPlatform_m7BBC95D5E47A64B81CBAFDDCAB962B52140BD9C2,
	FB_get_AppId_m1489BBE6274C96BCE43A5DF4AD76EC6A65FD27D5,
	FB_set_AppId_m2412746196389AE1014CA4363C92805BB681E6EA,
	FB_get_ClientToken_m8EF152654D7B2172128D2FB5A14219B850DF3B9B,
	FB_set_ClientToken_mC373D3B586E8333D44C9CB46FFAD766EC9B5FAE5,
	FB_get_GraphApiVersion_m5372280099E5B014E5457F5ABF6AAA40C15047FD,
	FB_set_GraphApiVersion_mCF555205CB4276F6B4693EC511B977A1BCED6B9A,
	FB_get_IsLoggedIn_m1BE9E0F416A9AF5A7B6DEF648C80F616A508A51D,
	FB_get_IsInitialized_m7D638468F8A8CD50CC2C67E32FFF295E79D8D9B4,
	FB_get_LimitAppEventUsage_m079FE8AF3219757075749AF63D90970E34348B1E,
	FB_set_LimitAppEventUsage_m23835B5DFA342006D43634738FA4CBA51C10BA55,
	FB_get_FacebookImpl_m63590A128C20A71E977DB12B4779C22175D28AB0,
	FB_set_FacebookImpl_m3B81A77E08A0304719EACAF972ECA92A7E4286E2,
	FB_get_FacebookDomain_m153C3DCD60AB3C50CBEF5BF54354E8ECCE487AF3,
	FB_set_FacebookDomain_mEC4EDACFA2C197646CCA9AADB8644F01D68B1CC1,
	FB_get_OnDLLLoadedDelegate_mDF0CD83548CAE0D591E32D1AF14AFCF2AB0721D5,
	FB_set_OnDLLLoadedDelegate_mFD5ED2115E5D1CE1CCDF0DF3F310BE46CF3A89EF,
	FB_Init_mB6B57CA36F605F9F609A7D42AA9CE920B515D268,
	FB_Init_mFEB9120C2C1B29BFFC4E6E443272F34E8A9F94E3,
	FB_LogInWithPublishPermissions_m7F47751E84794A4E3AE77A674D828146F2718CBE,
	FB_LogInWithReadPermissions_m4ECC85AD931B78DEECFA1084FAC2AFB58341710C,
	FB_LogOut_mD584DA2202B57BFB8CAD936BB832EB1681F1F867,
	FB_AppRequest_m5FAC156DE18EB4FAF52031E378EB262E7AF094D9,
	FB_AppRequest_m62BDFEB81DF3266D19211546675766E3A426C4A2,
	FB_AppRequest_m8AD947D050E3955D25C42B0C2D686DE0B690D9E3,
	FB_ShareLink_m53E9A04825EB1CBD416AFD537F29AA3A8C5FB90E,
	FB_FeedShare_m9E00AB534B163AF9BF8A26D24F6D4B99FC407C49,
	FB_API_m9948056643882A44BB7A99A36D4AAB8EAF1823F6,
	FB_API_m092EB1628F038A356C081EA93D6A0B7333CE46C6,
	FB_ActivateApp_m29519DDADC61EFC4FBD1597384D767A1AD94B76B,
	FB_GetAppLink_mEB6C1A0F0DA93B19230BA1DD9AE4C5A17D24CD90,
	FB_ClearAppLink_m2DDF420FEB425F2D5A56015BE4207AECA63DE3CB,
	FB_LogAppEvent_mB49D6D0BD4D73D64DA84BAFC96FD8B04C237B89F,
	FB_LogPurchase_mFDA037CAD9DB8C180B1408C6DB962FC75C4D1FA2,
	FB_LogPurchase_mCB8CB69EFDE7DAFC2D08139E8DD9999D6BD2E89D,
	FB_LogVersion_mB06DB28E9661B5C64AB751CE443D10787C32160F,
	FB__ctor_mAD07653B33A22EA448D20821520C53288385827E,
	FB__cctor_m0C93E0230071AD18B70832FC11F6198CBDAB0343,
	OnDLLLoaded__ctor_m4740E133CF9AF20863045F7CDBBC75AC967BA08A,
	OnDLLLoaded_Invoke_m5A60D20E804F13BF0B065BCA1956E806D542B94F,
	OnDLLLoaded_BeginInvoke_m772C09243226136A8FF317DB6B6921BD71493AA3,
	OnDLLLoaded_EndInvoke_m8A44591C82E83A1ECF6C933206C7119842DE8354,
	Canvas_get_FacebookPayImpl_mAB0E2F3BE1D5BBE8BB2805676CA062865F0E62DC,
	Canvas_Pay_m4D886BF85C9ECD532CF39BD6F1B791098030B173,
	Mobile_set_ShareDialogMode_m99A2957C9226B65D18ACBB6DDF83D2C5F25CBB2A,
	Mobile_get_MobileFacebookImpl_mEB711C6EF15966CAA14F87E19C6C7CD7AAE5CB26,
	Mobile_FetchDeferredAppLinkData_m7328195B26434CDC789CF26095E751087CD12F00,
	Mobile_RefreshCurrentAccessToken_m7AD37E813F087C0AD28BE1119F74A6DB5EE1252C,
	Mobile_IsImplicitPurchaseLoggingEnabled_m453D5DC8536D809900D1BF4D568ABE16A6FD7520,
	NULL,
	CompiledFacebookLoader_Start_mD8581407131D0FE65C4DE014F585FEFB654B54C3,
	CompiledFacebookLoader__ctor_mB320F10ABDDFE441221BA38ADDA0C1A3D760AE26,
	U3CU3Ec__DisplayClass36_0__ctor_mAB6F7C4FA96C02B80E4696A39A92B5932FE4C152,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__0_mFE950F43B02994F7E220985D5218B65447CB8915,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__1_mB4D317BEF67A7F06A9BB38EA241211448D7A3092,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__2_m6AA4B7C73842A9B5A74906BEE8057D24C92E0E36,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__3_mAEA05F41CB6536A57E5DE76FC986ED4BC3F292B9,
	U3CU3Ec__DisplayClass36_0_U3CInitU3Eb__4_m9C9317AC8D3A6E3136F06AC6F1FCE2349C8F0258,
	FBGamingServices_OpenFriendFinderDialog_m3EDAC959E0130BF64F402E5A55A8905330EA9DAA,
	FBGamingServices_UploadImageToMediaLibrary_m807ADEE82483109434CAF8A93734B1430CFB69AD,
	FBGamingServices_UploadVideoToMediaLibrary_mEC3D77F26B6FF0545CF2ECB6FA17FDE2626817FB,
	FBGamingServices_get_MobileFacebookImpl_mD23C632BD9D0A9958EC70F0AF90A893FC1099082,
	FBGamingServices__ctor_m72856C111E894E11AC8AD8F11A58C409F164FD04,
	FacebookBase__ctor_m0170BED48D2A6D65D31DA00BCF7C473E3F291F03,
	NULL,
	NULL,
	NULL,
	NULL,
	FacebookBase_get_SDKUserAgent_m898B03A73DF563DB791AC1DF0B6845681586A3D7,
	FacebookBase_get_LoggedIn_mB760F006DD10F7F7851224DD04BFFC729603B842,
	FacebookBase_get_Initialized_m64D5DD277FB710ADFC9A7EE2B4B155415F50A275,
	FacebookBase_set_Initialized_mFEF325E118282493BD607C2E48018DE5DFE8A9F9,
	FacebookBase_get_CallbackManager_m6277A4C7F3B2FF628BB3307D0F2CB4426DFC79D2,
	FacebookBase_set_CallbackManager_mFD31AC262A5348912AD870E1F59371AD32D36096,
	FacebookBase_Init_m72BB5AEAA6E4A817E1F0AF3E5B5FEEEF5D477B69,
	NULL,
	NULL,
	FacebookBase_LogOut_m472F02624D74CE38AF02F5AEFFE5092D4D51CD27,
	NULL,
	NULL,
	NULL,
	FacebookBase_API_mA643418E3AC41C4BD02F15C897FF97784E19B151,
	FacebookBase_API_m10A05AF1F4C066CFCE02F41C8E84AD4299524A54,
	NULL,
	NULL,
	NULL,
	NULL,
	FacebookBase_OnInitComplete_m51DE408F9CFA69B17CEAD2C54C1B85D0E0A9EDA2,
	NULL,
	FacebookBase_OnLogoutComplete_mBDA27D0C7E74CAC758E1387243836EF05882A189,
	NULL,
	NULL,
	NULL,
	FacebookBase_ValidateAppRequestArgs_m354428BCE63BA4A4199FB742AC9D7EFD1EAB1F99,
	FacebookBase_OnAuthResponse_m198C37E188A9F614197FB7FEF8B0B0BD9080DC09,
	FacebookBase_CopyByValue_m36AEFA6C6724782E70FBFB0FD91E9B6D9AFCE05F,
	FacebookBase_GetGraphUrl_m68445D2B00523B04F79669C68DCC1EED4A31403B,
	FacebookBase_U3COnInitCompleteU3Eb__35_0_m2B64B634700D72D1755A23E0537940CD7465DDEF,
	U3CU3Ec__cctor_mFEACD9475775AC530C18E40EC62767F9E4DB453A,
	U3CU3Ec__ctor_m3937E429B1DAB2A6390579F525D87CD705306528,
	U3CU3Ec_U3CValidateAppRequestArgsU3Eb__41_0_mB0D69F4F62704D337D1623E77A751A556C1F9DF0,
	InitDelegate__ctor_m3B50265F79D8DF8D1E8FAD737965D2C7FC57ED50,
	InitDelegate_Invoke_m93BAC2CDD6BAD72CEF92943490142B9E2D5E1311,
	InitDelegate_BeginInvoke_m85E47DCD6AEF2E0F3064879630F7F7709EA9F542,
	InitDelegate_EndInvoke_m19AF528352A49E8B96EF7BAB14A39100537423E7,
	NULL,
	NULL,
	NULL,
	NULL,
	HideUnityDelegate__ctor_mF491837CE67C28E52F8DAAB9DF8B71F195149063,
	HideUnityDelegate_Invoke_mCA0B59C7D14B5C248069F4A9ECC7EE070D4D674C,
	HideUnityDelegate_BeginInvoke_mB41A5458F30C075A1891B68F43A369D41EE5C055,
	HideUnityDelegate_EndInvoke_m610888250BA07306F6100BABDA62B884262338E6,
	FacebookGameObject_get_Facebook_m895AC7FFAF0F66F74C9E756494660E26A5920114,
	FacebookGameObject_set_Facebook_mB57E522C6DB990DDB7F3E9CA9B2E5C769C6A7862,
	FacebookGameObject_Awake_mBA12D0A607908EAE100AF983B1B5EAA75A236EF9,
	FacebookGameObject_OnInitComplete_m00090D77BEE8E0090FF2B8DE7DB78A920743E393,
	FacebookGameObject_OnLoginComplete_mCE5EDBE1166EF1745BF18018F38D9ED9C5157E7A,
	FacebookGameObject_OnLogoutComplete_m243E1C0F885F143D6AB799434EBCB94AFBDC3FFA,
	FacebookGameObject_OnGetAppLinkComplete_m44C0A233E1FA79CAEA9F1985E80FE1DC14BBC8A9,
	FacebookGameObject_OnAppRequestsComplete_mE2F4B76808EF48E63F4DF51212367A3572B7B3E9,
	FacebookGameObject_OnShareLinkComplete_mC5927F0C0B874B7E0549C5064BB3AF8563F3256B,
	FacebookGameObject_OnAwake_m15415D2BC7F8F912199C89C42DC22A4F6DE2BC93,
	FacebookGameObject__ctor_mA738180F76F2C190481692B8F4A4C389D7121EC2,
	FacebookSdkVersion_get_Build_mBD35BBA90D719AE98FA66E269A29420E7E4E45AF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MethodArguments__ctor_m386044A8B3C685AE0AABB18251E10FDBEA11A03C,
	MethodArguments__ctor_mEFF2A6F8D8D7BE60B2A79C5964382E3BD5B7BE04,
	MethodArguments__ctor_mC8ACEA4A4E630AE683DE86979DED99649B6A8168,
	NULL,
	NULL,
	MethodArguments_AddString_m8EFB3D4292AE489D9EE657588137BD1FDB059923,
	MethodArguments_AddCommaSeparatedList_mA3D6E35D9F6FF2F973BE3720BF03CEE69E238EAF,
	MethodArguments_AddDictionary_mC9D7D520A5B3804A6B20211B33DDCA1E9112B80F,
	NULL,
	MethodArguments_AddUri_mA2AAF7BC3C9E7E8CBF2157D7D0616471C6394DEA,
	MethodArguments_ToJsonString_m18DDE098F5D2249E3FAC5C1502981B45C1EE3A24,
	MethodArguments_ToStringDict_mD7AD5E7239868ECCC804CDD2CB3DBF58EA05F4DA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AccessTokenRefreshResult__ctor_mF2038CFA769B53E2610D59FDD350B740DEB183A4,
	AccessTokenRefreshResult_get_AccessToken_mBE835E6356640BA0B070C533A6F8D22964EC4048,
	AccessTokenRefreshResult_set_AccessToken_m15D989B19DDDD38D9DC5375CFEAE6EB3AF5F09A1,
	AccessTokenRefreshResult_ToString_mE3259D4D4AC178A023C79B031D8D5347564A22C1,
	AppLinkResult__ctor_mAFE640A41C69087201E90CD189054755DB3F354D,
	AppLinkResult_get_Url_m3901581148CA1CFF2919FAA7D09831798E7AE41C,
	AppLinkResult_set_Url_m8E3BCCAA0072AEDEE537D6DB12442C0E42B4C10F,
	AppLinkResult_get_TargetUrl_m6949D521A7A2FF60E540C0646C33212120A701A6,
	AppLinkResult_set_TargetUrl_m4954599E465E50429F00DE15D4B62CAA7C65432B,
	AppLinkResult_get_Ref_mB08E7ACBE6150EBB39CAB582A5C520E24FBADE4A,
	AppLinkResult_set_Ref_m633906A27F0A75BBFDEF6A93C8C1920568EB859D,
	AppLinkResult_get_Extras_mD8677AC2751360664795737C93D50E0FEF23F1B4,
	AppLinkResult_set_Extras_mD78F58CDBBF7CE94B4B24E5984F4421C4A9DAD6D,
	AppLinkResult_ToString_mA89DD76B5872401C6434CCCD64A5D19FB32C00C3,
	AppRequestResult__ctor_m16A2CBD36B61B99A284DD4F414BD45D3D93EB3E7,
	AppRequestResult_get_RequestID_mE251958C92C77BB41B529C742B8EE86A9E8BD76D,
	AppRequestResult_set_RequestID_mCBAAB37D93451C70A995F61FD587298C444776E8,
	AppRequestResult_get_To_m6B51C1328A39FA434AF54736E6B738BE0A858E73,
	AppRequestResult_set_To_m89C22563F28CDC50E60D52EC3DF5BEE284AF33E9,
	AppRequestResult_ToString_mC7E6B5B358943FA73C478C4E799AEB4ECEC172CB,
	GamingServicesFriendFinderResult__ctor_mBDA75E41C0FDF72924F51C912F4851429D8760A3,
	GraphResult__ctor_m5754C0FC9077CA7E8288774646AA3E46B478578E,
	GraphResult_set_ResultList_mC7DCF98D485AAB5C9413705E03DF5D4A4F59C29C,
	GraphResult_get_Texture_m3B4B5577997E5BB0BB7BCF574A27C22C257F503D,
	GraphResult_set_Texture_m76584D43DFE384A562539EF6F3F53A6B9B2A4D4E,
	GraphResult_Init_m0FBBBEE51D496DB73F1FFD27BC5CD4B94E016E4C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LoginResult__ctor_m4E8637B7A3F87227D05C3F18CCB2E8592899E8A1,
	LoginResult_get_AccessToken_m47F9E37AC46C9F96DB467E1F382CFD5CB83ECA22,
	LoginResult_set_AccessToken_m456AF71BAB7E7821EB31D8E7EA6503A69BD98925,
	LoginResult_ToString_m081BDBBDF45B5A24AE252DC9961B6F010F455F5F,
	LoginResult__cctor_m152B3FC7788B9F6C847712A915125CC3203324CC,
	LoginStatusResult__ctor_m758AA176E43D11EDDCF47C6402B8751FB457CA4B,
	LoginStatusResult_get_Failed_m08301D7BCC634FFBD1637837B2EF558BB38A0998,
	LoginStatusResult_set_Failed_m863912CB604F116D86EB38D58AD7FBDF89785DE6,
	LoginStatusResult_ToString_m4079ED9FDBC40912BDAFA1EE8C03EAE657017A79,
	LoginStatusResult__cctor_mD8FA90F376958DCD066971E3CA8BE1F2ACB6D298,
	PayResult__ctor_mC64D542BD10CC1F00346B8787A6928F02F7BDB7B,
	PayResult_get_ErrorCode_mB9105862151AC595ED778F625CDED638BE9076C5,
	PayResult_ToString_mE53CA326DA9D41CA153B90C102247927C2321F0D,
	ResultBase__ctor_m2CAE0928483D1A4384DD2C85A9F1F0819784B0C7,
	ResultBase__ctor_m651B927D6CD2EE83ED1A6C4A40C6A601283167B1,
	ResultBase_get_Error_mD4F7A46804E5B9A4001E9C807A2D799D0AE28D34,
	ResultBase_set_Error_m29544B4B3A1D028709EE36A5FCCFFEB86C4E81AC,
	ResultBase_get_ResultDictionary_mD4CFE4D2BFC792DE389E9F72876A4DFC5528D3F7,
	ResultBase_set_ResultDictionary_m2A19E8F7FB4FCF0CC14D55F10A1C3D7276EAB6A8,
	ResultBase_get_RawResult_m6D6BA2683C3E88F01D0B2CB5A821D9ABC0E3B3C3,
	ResultBase_set_RawResult_mA5CCF9498D5504387446458555DFE7D5A06F1F86,
	ResultBase_get_Cancelled_m19B41E6DB6EEA433580E77E63A71D91A5E953A26,
	ResultBase_set_Cancelled_m79BA2A04684E91724BB89A5F78381566B740C8FE,
	ResultBase_get_CallbackId_mA0FB4E9F0C1D466B38A21879860C0D00D006BB5A,
	ResultBase_set_CallbackId_m6CBC1033307D11C695CA729F4B3A5DF0BDE076BE,
	ResultBase_get_CanvasErrorCode_m5AF45E74518F6FB35C55E2ED8156BCBE6C95268D,
	ResultBase_set_CanvasErrorCode_m9C0EDDC8EADA20211A6AEA44187BFA07865EC131,
	ResultBase_ToString_m2FDB9E8F3A5720370BE0A37B35CA9C850AE67811,
	ResultBase_Init_mA6E2B11E53BF4321BFD52D524E5E6EA1F1194C49,
	ResultBase_GetErrorValue_m84B8E1871AC32FF626953064B84B3053FAC896BA,
	ResultBase_GetCancelledValue_m40E0352BBA906C9B19551BE0BA9E5808F41D4229,
	ResultBase_GetCallbackId_m8450CB86BC589CFA5A784F40D7D3C6E54AE067F9,
	ResultContainer__ctor_mCE2EA3DB9A34DFCC66D0AED36E3CC0C034401AD3,
	ResultContainer__ctor_mDFC7D6BEBF3C775E05BA6E38C41ABEF305BA9032,
	ResultContainer_get_RawResult_m29F9476502E62AA055713C88A57E022BBCC5CD3E,
	ResultContainer_set_RawResult_m3FB2999AF8136B2BA37E52A093D40BF61111B645,
	ResultContainer_get_ResultDictionary_m3C05222908D597BA0C6C2011EE3B56765A60F8BC,
	ResultContainer_set_ResultDictionary_mACF0EA46CAA961CD145422830415B4A6A60CB94E,
	ResultContainer_GetWebFormattedResponseDictionary_mCFD11D12F34F164976E5C63AE5D9F94DEA6D9298,
	ShareResult__ctor_m070AB07FD2F133BB5B97913DA48CC2FBD94122F3,
	ShareResult_get_PostId_mC22D0ACE189C9518C7002FA6D66214946A24B0E7,
	ShareResult_set_PostId_mC5D73027267DA10D0DF14C7536DD2C2C7520B454,
	ShareResult_get_PostIDKey_m213C65F61078A2317F612F5A2BFFC9370A45C680,
	ShareResult_ToString_m134BB55EAE82332B5B2EF86507535AC3A4B95F2F,
	AsyncRequestString_Post_mE581ABE2200212C32596FED01F0D6DC4A3628578,
	AsyncRequestString_Get_mA256ED89C65D5CA6C0EE318F783D916FF9CA60F0,
	AsyncRequestString_Request_mB019C996BC7E5A962E0CA0A06ACEC98B431762A2,
	AsyncRequestString_Request_m13082FEB25723AA8B4DA48D759286F1E0F2069EF,
	AsyncRequestString_Start_mAB3FDA9F9ADEC7B6F64B5F46A2B8137FCAA67236,
	AsyncRequestString_SetUrl_mA85379325C25387FA7191A390B60B940223A0AC9,
	AsyncRequestString_SetMethod_mC161F0D3F6A8F0F7A69F9730E22BF7E4A13E71F8,
	AsyncRequestString_SetFormData_mF7BB7510CCC355CA879C9A465FF675D100C74C7B,
	AsyncRequestString_SetQuery_m9A6A2D435027DED0F799FB8F82BE316DCD182926,
	AsyncRequestString_SetCallback_mD669DF021B036782469E97C1D0BB0F9F2B8E62ED,
	AsyncRequestString__ctor_mCC46052C9D7FCC37C2B2C950C76C8A92B7AFFD08,
	U3CStartU3Ed__9__ctor_m2C45BEB7767380D5E1D5EB6809A2885BB73F74C2,
	U3CStartU3Ed__9_System_IDisposable_Dispose_mB1164E7F5829050169CAB916A0BEFC776A5BE033,
	U3CStartU3Ed__9_MoveNext_m524704A9E30FD380677A89BA6B01ED2C20BB43A4,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C51FCB56DB3F6A50F06B2C203BE21EBA06CA00E,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m5E459DDB3C035064FCD5331E28F98A3918940B27,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m11943D98D7C47AF9106300194CCE07B16CDA1452,
	FacebookLogger__cctor_m44CDEBB839F913D4A668E2B28238C84D91962A10,
	FacebookLogger_get_Instance_m3C9694E0230947B553483BCB73112F77F150DEEC,
	FacebookLogger_set_Instance_m48E1B8F191FACEB8F0F36A7ADAD11788F44AFAC6,
	FacebookLogger_Log_m49FA01BA88718F187182A21DDA94B59465A7F5A2,
	FacebookLogger_Info_m236F8C3F2F8BA48017C03D19426795B05E1B004B,
	FacebookLogger_Warn_m84102F2C8C6A1EE3AA9D7DCB48B35A90DC05D55E,
	FacebookLogger_Warn_m909F9B44B0F0B6E6AE7D10190A71EA7AFB465656,
	DebugLogger__ctor_mD4194614600B9780168D20328B462065704D9361,
	DebugLogger_Log_m52B321444E0404A27F398D0E14DCEB275AA86DE2,
	DebugLogger_Info_m616A3FF5F1DCB7ACF02CCFE3E7738E6D521080BA,
	DebugLogger_Warn_m6091596AFFE058DC0043E22A85D35245EAD15F01,
	NULL,
	NULL,
	NULL,
	NULL,
	Utilities_TotalSeconds_mAEC6A02BA33B7D143FA3E912EA0FE20970E86780,
	NULL,
	Utilities_ToCommaSeparateList_m85995AD184CD4F88C4E55FDD0ED982BE28283838,
	Utilities_AbsoluteUrlOrEmptyString_m42BEC0BA40D3B1F90A2E7DBCE48A5DE6D7E856D8,
	Utilities_GetUserAgent_mA0FCD11FA4D238E9D7E324DF0FEFD6708D71E2BC,
	Utilities_ToJson_m461B42D0A31BAA21819FECDA7C688DBF3E3ADE76,
	NULL,
	Utilities_ParseAccessTokenFromResult_mC667C1CF41A8CFCA45E90B891E4D3EC37EF4AC07,
	Utilities_ToStringNullOk_mA7ED050EC12DB1F44BE85D7FBD7AE4224A8FC751,
	Utilities_FormatToString_m4646645FDD780C289CFD04C939E3A3AC1F9E93AF,
	Utilities_ParseExpirationDateFromResult_m8A7B2B820AA3EB13797C0A67096F53D9CD702DA5,
	Utilities_ParseLastRefreshFromResult_m7980EEE2ED4C5C663CC534CB8F473515C6C71B81,
	Utilities_ParsePermissionFromResult_mF1C9377FED110E2F913152809F313D91FD7E355E,
	Utilities_FromTimestamp_m982317357AE6D261A2B459EF09A4A248D8519140,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m2D1E30C7DD7902F4ABB302193C7E4121D56D8797,
	U3CU3Ec__ctor_m24A18F69F6F06BA6697811BFECF2BB7A1D8E48B2,
	U3CU3Ec_U3CParsePermissionFromResultU3Eb__18_0_m00A80EB0337291868EAEF35189A3F446DD24595C,
	FBUnityUtility_get_AsyncRequestStringWrapper_m067CECA6BC7F45D9DF5B3850E19168B986831BF7,
	AsyncRequestStringWrapper_Request_m7EA103819D83F8EC2F831A7B08D4CD3AEC365FDA,
	AsyncRequestStringWrapper_Request_m2384F96FD70594F8B2ED88DD3D4B4B3AE84A1D37,
	AsyncRequestStringWrapper__ctor_m7513D273A45D18F18DE623519FC26F82C5701913,
	NULL,
	NULL,
	FacebookScheduler_Schedule_m5C5A913127E25B58B3C77A1CCA363FDDED317BC5,
	FacebookScheduler_DelayEvent_m4B990D8DE776D1A24A206EEBB34B14E945CD2979,
	FacebookScheduler__ctor_m7DD6FB4D5AF56E5CC230ADB9C561109756E2C6CB,
	U3CDelayEventU3Ed__1__ctor_mC6AFB74727EDBDA35869F4C1A073676A4CE2321C,
	U3CDelayEventU3Ed__1_System_IDisposable_Dispose_mE35E5B6A5A1C96DED2E3878A3171E25EE325DC4A,
	U3CDelayEventU3Ed__1_MoveNext_mA81D7E69903B82A9EFA757B906789D90489B689E,
	U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82FF3B0FDDCE53E3C19834961E32F8D6EB70AB11,
	U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_mDE6D7AEAABE3544B46EF1C57A746EA6C82E64DE3,
	U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_m4585781C95E35C9C1E2F2ED0DB7F3873214215D1,
	CodelessIAPAutoLog_handlePurchaseCompleted_m6134F2AB0CD021611DE377ACD2A44E2E83DD788D,
	CodelessIAPAutoLog_addListenerToIAPButtons_m4D4221C32C9275EB51687D013B25F8261C8640EA,
	CodelessIAPAutoLog_addListenerToGameObject_m667719D427F9F547C4B1C7C5C0D83855A2CD271F,
	CodelessIAPAutoLog_FindTypeInAssemblies_m335975E7C237087592A585B95C0BD33A6E95D699,
	CodelessIAPAutoLog_FindObjectsOfTypeByName_mD288289A4A5070AB634B45B21DE6DC1BE13BE5A6,
	CodelessIAPAutoLog_GetField_mB59909FF146131D013E754F6C9274572B7CF95C9,
	CodelessIAPAutoLog_GetProperty_mBBC912F927574A873B7F257D669D1C830CB111C1,
	CodelessCrawler_Awake_m95F458A3C917120B20DE8C8A1F1BFDB502424E92,
	CodelessCrawler_CaptureViewHierarchy_m81B51B77F6E16873E5DB9529E158550087302735,
	CodelessCrawler_GenSnapshot_m043AA29F118DF4C451C2D6C64398DC8A9077342F,
	CodelessCrawler_SendAndroid_m9872E0B24B68E9433ED2B10BCF6F886287D37B0E,
	CodelessCrawler_SendIos_mD570EBBBE0F9E64A5E7B1D4E0974CCA39D0EADC9,
	CodelessCrawler_GenBase64Screenshot_m16C99048F0B0975D769DEDC83A70201819222125,
	CodelessCrawler_GenViewJson_m028F67AE12B85E8A80B5EB2ECBE0635322A461FB,
	CodelessCrawler_GenChild_m4C731EBDB06AA813622E3AF0163EDFA2AA66DC88,
	CodelessCrawler_onActiveSceneChanged_m5F7DB1D4D40E4CEC5BA9C431038B804AE2D56285,
	CodelessCrawler_updateMainCamera_m27942A3E402FAC208931B892854A93D558FFFAD4,
	CodelessCrawler_getScreenCoordinate_mCDDDA3C308B9B64BCFAF1833EFBDE8F3FA6E01A1,
	CodelessCrawler_getClasstypeBitmaskButton_mEACCC10CEC22C4804D6B15CEAB9DACDBB417CEC8,
	CodelessCrawler_getVisibility_m60CADC21383B6DFF97B7B1EE4178B33CE9A70576,
	CodelessCrawler__ctor_mABF7C5D46B7A7CD6F82FB7162A2A901B3329073F,
	CodelessCrawler__cctor_mEFB9B1B9490B7967A6DB657F01229B60537F3AAC,
	U3CGenSnapshotU3Ed__4__ctor_m26BD343D41AB7197E80D3BDA7ADBFCE0FB8B0A1B,
	U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m5F5C4FAA3CFFE3F58EB9821F1F50F28AF3834266,
	U3CGenSnapshotU3Ed__4_MoveNext_m622B9369A4E14F1A78AB3A6AA821809BBA730F95,
	U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD763996817A0C1E940652BB67C7BA15BA9ECED08,
	U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_m0014A73DCEEE4D036C053DA8DC1626CF37A80BDB,
	U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m2AA5C43338C104B58A6381B3DC5483FF609C4168,
	CodelessUIInteractEvent_get_eventBindingManager_m903108DB2AB006B95157E891FAABF005B85E2BF4,
	CodelessUIInteractEvent_set_eventBindingManager_m77E3535CAA50232C2E596BD11C8DD03D84EFD689,
	CodelessUIInteractEvent_Awake_m85767BF4BCA0284F085B8CBFEEAE51ADAF52C73E,
	CodelessUIInteractEvent_SetLoggerInitAndroid_mDA5573B23C0FDF853DB5E847461E59331578B936,
	CodelessUIInteractEvent_SetLoggerInitIos_mA1BB76D140F7983193F2778E98F058C50B42E787,
	CodelessUIInteractEvent_Update_mA848F7AE8C04A19F4C28EE87FF8DB580514C4107,
	CodelessUIInteractEvent_OnReceiveMapping_m8909E208304B765A6E4D9902666B4BBFF306EDBA,
	CodelessUIInteractEvent__ctor_m756D383A20DA1E3D4D8681F0F7F132BA3F807127,
	FBSDKViewHiearchy_CheckGameObjectMatchPath_m504070140D7B5065F3C545958C0B66A1D1357984,
	FBSDKViewHiearchy_CheckPathMatchPath_mA875A5EFAE1E22B567040F8A0095A99A5637693B,
	FBSDKViewHiearchy_GetPath_mA087D66D725BAC2974D5EDAAB9F6FC68EDF3B8F8,
	FBSDKViewHiearchy_GetPath_m916F0E4FA8BFC0B6059920D29C257FF3AC7F7D61,
	FBSDKViewHiearchy_GetParent_m180998C7F18791647C0918BB0C6A884CBBA910CF,
	FBSDKViewHiearchy_GetAttribute_m7BED792A9B8FEC3FECA0CA4104EF87B2DA8BC854,
	FBSDKCodelessPathComponent__ctor_mC5B75C4D3083506BAE788A3B22F5080D7EFE8C3C,
	FBSDKCodelessPathComponent_get_className_mC01F410E3CD1C2C958EB1B236171710CB105CB7F,
	FBSDKCodelessPathComponent_set_className_mA41EDD912035EF42B51450291DBF78EC8A72AA18,
	FBSDKCodelessPathComponent_set_text_mF1260DCDEAA4CC3D613E185C35BE5E9BFA335D2F,
	FBSDKCodelessPathComponent_set_hint_mA00EE1799937BE9857DFF85F1A7E546E6671D620,
	FBSDKCodelessPathComponent_set_desc_m35B53D04D500EAF815BFCED5ED1567088F9F0147,
	FBSDKCodelessPathComponent_set_tag_m82C812E954913BF90F03B86A3D3F2A879C5A7F77,
	FBSDKCodelessPathComponent_set_index_mF94268F506981E18A8B515BA5C5FBBDCE6358109,
	FBSDKCodelessPathComponent_set_section_mF90C205E04A4E1FE1775EA0FF1ACCEFF2F6867D3,
	FBSDKCodelessPathComponent_set_row_mA7C4078D93E04BF095A4DBA208F0C7EC37D5F402,
	FBSDKCodelessPathComponent_set_matchBitmask_m0C9B748B4D36DC20D10DB7281E891B55FAB3CE1C,
	FBSDKEventBinding__ctor_mA63F55D4928166006B44C3F439A3EEA2F67103E7,
	FBSDKEventBinding_get_eventName_m201219D55CDFDC6085AC9A601016AA838174A48C,
	FBSDKEventBinding_set_eventName_m75E04EE87B8AB2C4F9D66CFA46CE039D415B94AB,
	FBSDKEventBinding_get_eventType_m3C93A2D8F43D556FBD4DA116853ECD87F2A091C8,
	FBSDKEventBinding_set_eventType_m4AB1D037855CFD48838304E37CDE3D73E7D601F7,
	FBSDKEventBinding_get_appVersion_m31C3339623CFC95F67525AFAD7A352B915DE8437,
	FBSDKEventBinding_set_appVersion_m26646F28FF0F956590F5307F88A0611E875E6BB8,
	FBSDKEventBinding_get_path_mE00F3F2F32F6341F2C04F6ECFDD434C265EB01DB,
	FBSDKEventBinding_set_path_mAFDB809C79099C6571EBB33716A7ED2F021D477F,
	FBSDKEventBindingManager_get_eventBindings_m3B88C2FFFDD625EA8D6A9751174E74AC04A4CE8F,
	FBSDKEventBindingManager_set_eventBindings_mDCF6A561143E32091163A00BDDC3AB58695E0562,
	FBSDKEventBindingManager__ctor_m2254BAC2D498A7A818693AB936BD92E69F3BBB72,
	MediaUploadResult__ctor_m086E98643A34D68DFD4DDD2E9FF1EEEBEC6807CB,
	MediaUploadResult_get_MediaId_m56B597E3628CF1792120CCCD69425AE0F3879FEA,
	MediaUploadResult_set_MediaId_m67EDDE32431653CF4FF9A76FF97D22C40D30DDDA,
	MediaUploadResult_ToString_m0C03CA0CC689342EC819F3395EF318A6D294D9C4,
	GameroomFacebook__ctor_m75D9E977D6FABEDDC0AFA66B15D8A266D029A6FC,
	GameroomFacebook__ctor_m0AAADA9B93E6B8CE6C5C19B3A7BE365AE437CB5E,
	GameroomFacebook_get_LimitEventUsage_m5A0117100CA465383A60E1EB2BFB6E191A4756F1,
	GameroomFacebook_set_LimitEventUsage_mEA82D0EC24868E0DB9C69F0E11A4951E3C2A4632,
	GameroomFacebook_get_SDKName_m474451D32996754DE84107B4A6115DD00CB4C20C,
	GameroomFacebook_get_SDKVersion_mE74B90CC9E8A86BAA0BE5641CC36C0D3248D9892,
	GameroomFacebook_Init_m26654070FB2EBF288B706D41B998629162403619,
	GameroomFacebook_ActivateApp_m9FD80C5E9E0B5B7CD57967B93AC7C93C7E1F638B,
	GameroomFacebook_AppEventsLogEvent_m0C7BB01C7A7D11A859F463FA5BB2AF4079D2DD34,
	GameroomFacebook_AppEventsLogPurchase_m1207C1D2F0992FDB29014EA614C31BC6DD4DCDD5,
	GameroomFacebook_AppRequest_m9AE6333DAB6DC76268ECC64883E606FAF62BFC75,
	GameroomFacebook_FeedShare_mBF858817705F69278028C4B61AC42D78FC68292D,
	GameroomFacebook_ShareLink_m21AA489880B8ECDB3DC88833B27C201481ACBFC1,
	GameroomFacebook_Pay_mBB86F76B25E826F8DE4C9DAC1CAAC0EA5147CBD9,
	GameroomFacebook_GetAppLink_mD9BA2D130EA75A6426FB88860048250BC6FDA993,
	GameroomFacebook_LogInWithPublishPermissions_mE483E395219B44D27B45409403D40F7AF02D9C83,
	GameroomFacebook_LogInWithReadPermissions_mF682783384EB9BFA76A1E3C5B82DF2DB317B1B8E,
	GameroomFacebook_OnAppRequestsComplete_m1DED31CF4252C34F4570D9430ECD8085D8D259C7,
	GameroomFacebook_OnGetAppLinkComplete_m8DF054DF87AF112F4F94E7CF2699BE0E2B52F3D8,
	GameroomFacebook_OnLoginComplete_m61EB2FBBCE19B70EA6AE47C87FF3A7DAC0ABF71B,
	GameroomFacebook_OnShareLinkComplete_mA379AD89C7F35AB380C6E0B04DB9274F87483AC6,
	GameroomFacebook_OnPayComplete_mA39DD62E51B0E43AB3CF5C181D228DEC0AD7B63B,
	GameroomFacebook_HaveReceivedPipeResponse_m75A0BDF9C82549B9F73E08DEC3F0D89A9A2FCB0F,
	GameroomFacebook_GetPipeResponse_m5C5C06626B6CE520D08B62AA7EFA7771EF19E08A,
	GameroomFacebook_GetGameroomWrapper_m4182830486699CC56AC06B6482708608ACD9BB4C,
	GameroomFacebook_PayImpl_m66E7B8DE460E879A94E4F195DED676D9CEA7DCB9,
	GameroomFacebook_LoginWithPermissions_mFE4357DE7339536C644925F6A7E90E55851F2499,
	OnComplete__ctor_m6E67D51EBB676E05D457E85638E7FF3554D2942C,
	OnComplete_Invoke_m7879FE547463B62F936C960FC00C81CC7E594C65,
	OnComplete_BeginInvoke_m88A70BCEF109462012DA6805B09E993FC601A06A,
	OnComplete_EndInvoke_mAA3C355E0781B47E13135286D6608C5411DD88C4,
	GameroomFacebookGameObject_get_GameroomFacebookImpl_m7BF8041CEE360F2DB134D16798DD7E5AFA1452ED,
	GameroomFacebookGameObject_WaitForResponse_mDC901CD9EAB55E57438D9ED03EC84328A3606B92,
	GameroomFacebookGameObject_OnAwake_mA861D13691A5C8245E52D8D863DF5BF94CE7A6D3,
	GameroomFacebookGameObject_WaitForPipeResponse_mD812990A356B78E2E458C53831CA35ABFD25A63D,
	GameroomFacebookGameObject__ctor_m5BBB180B9078EDEBA1F6DEE225A814C8A2F4D0AF,
	U3CWaitForPipeResponseU3Ed__4__ctor_mF073F306B1616D799DC6B40900EA698DBF9EA171,
	U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mA3E69F7B470DA19AB63DEFFC6976E96E7928851D,
	U3CWaitForPipeResponseU3Ed__4_MoveNext_m6769DA5C13BD0755A697861B4807F7B8122E12E2,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m249328CA40CDB1C9175566EFACA2E8C336CF6841,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_mE6229A72DECE4FEAF4508665517422C9305DB6BA,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m2F4DF059987E11B58752854C4D5DBB7A6F94E9D7,
	GameroomFacebookLoader_get_FBGameObject_m1C0C9820EACB3602EF62954C29B88976E8F51333,
	GameroomFacebookLoader__ctor_mF8C09DFDBBE0A129F8C10BDEE76C927242237E40,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EditorFacebook__ctor_mFA801CC2BC490ACEA1128BBE3E51D605D1DD0406,
	EditorFacebook__ctor_m7CE97032E5E441E7E5182374D3C49459E5844BD3,
	EditorFacebook_get_LimitEventUsage_mFB01AA7051EC052B354CEA0BD1108792580F848C,
	EditorFacebook_set_LimitEventUsage_m7A4043707FA05B22266E9F57F5F7126D69755C24,
	EditorFacebook_set_ShareDialogMode_mDB866B9239E05778AE7F3052BE5ACE85809FB255,
	EditorFacebook_get_SDKName_m3F875A63ABA28659C65F9D92893568D97FACD123,
	EditorFacebook_get_SDKVersion_m17229683C98D13360595E71C40ED5DB6411DBEF5,
	EditorFacebook_get_EditorGameObject_m16F8C2106C6F7375E9B1A539C9C8B1E0141E7638,
	EditorFacebook_Init_m5879CA96E1332C0C111927D254C4241F2A73D139,
	EditorFacebook_LogInWithReadPermissions_m260E6292D20E542B68DF4A0DF2AB69AE00DA5479,
	EditorFacebook_LogInWithPublishPermissions_m27FB1A71875C89F499EE07A72260FA8032BC46BB,
	EditorFacebook_AppRequest_m7D45AC49B7C57595700EE460D0C0630688C73113,
	EditorFacebook_ShareLink_m7E81F1042D2CF846B303E54B0EA9A8C4A7A3679F,
	EditorFacebook_FeedShare_mA21B56EA9CAE446092E31B7162ED55D5F7D9D79E,
	EditorFacebook_ActivateApp_m806544E11B7C2370BAEA88407ED7AB46795AE4A6,
	EditorFacebook_GetAppLink_m672147215795D812815D2034559791E90CC2ECD0,
	EditorFacebook_AppEventsLogEvent_m47348C253B96FD792342B4F7AE0F97F9B93BFAAF,
	EditorFacebook_AppEventsLogPurchase_m9A627289347D5BCCF091AB94B434D33755B97442,
	EditorFacebook_IsImplicitPurchaseLoggingEnabled_m121A7985FFE16EE7438ACB6B131D6F458BDA72EC,
	EditorFacebook_FetchDeferredAppLink_m4B977D6FAF58AD23D61DF6CA1311D058E2850AD3,
	EditorFacebook_Pay_m5D5B07BBBB6FCE9A0DED40EB2400E2539A718E15,
	EditorFacebook_RefreshCurrentAccessToken_mB0320075100F893CCADDFD7191B574F88EAFC30B,
	EditorFacebook_OnAppRequestsComplete_mB7FCBD504172890739C1B78260BC89241488779F,
	EditorFacebook_OnGetAppLinkComplete_mD71DDB9985D9BA784080640B1D356D2E2462C60A,
	EditorFacebook_OnLoginComplete_m285A38D29DEBE87B7760A4D1D5E463B03B2C7297,
	EditorFacebook_OnShareLinkComplete_m30FB4C191D72A7A795EA1816D2B171FF22F51057,
	EditorFacebook_OnFetchDeferredAppLinkComplete_m91825C013D59F79836581A11DE437ABDBC5DB851,
	EditorFacebook_OnPayComplete_m7D2048F3735C25D6EC34683E027E8A88A7FF4E62,
	EditorFacebook_OnRefreshCurrentAccessTokenComplete_m0070CB4102384A500FF2A0085079BBBE22546DAA,
	EditorFacebook_OnFriendFinderComplete_m99620A5EC2E98C728DA765FC23FC654177F874A8,
	EditorFacebook_OnUploadImageToMediaLibraryComplete_m8C3F4141A503546B79023AE2C26F09697D999339,
	EditorFacebook_OnUploadVideoToMediaLibraryComplete_m46F1F5A95564DA023F47E8593EA384AB60ED7968,
	EditorFacebook_OpenFriendFinderDialog_m0EBF9D785BEF1A15129C9F78101A48D91DFA6ADF,
	EditorFacebook_UploadImageToMediaLibrary_mBF93BC3CFBF14B9A692F77044DCE200450568E83,
	EditorFacebook_UploadVideoToMediaLibrary_m8319686282A77007BBCD12329CFD0624C550184A,
	EditorFacebook_OnFacebookAuthResponseChange_mA87A6BEDD01E6D1EC406B786D853E3A7E2340009,
	EditorFacebook_OnUrlResponse_m8588D18959262C250E42DD04F22378E92C1A40C9,
	EditorFacebook_OnHideUnity_mAC1E2D31F16519339FE2CFF2FA65F05E4E8F6FE8,
	EditorFacebookGameObject_OnAwake_m1E51C63CCD02DFC7C5A515C11F7778C31106F705,
	EditorFacebookGameObject_OnEnable_mF7328EBA8E443BFE0A4A23892FE47DFCE2DF304D,
	EditorFacebookGameObject_OnSceneLoaded_mFF4D404839F523EB153131DBA08E210C29570C8D,
	EditorFacebookGameObject_OnDisable_mE2FDE5131237DE1044DEA616A2108203CAB00BB2,
	EditorFacebookGameObject_onPurchaseCompleteHandler_mFC88C9436109F548C0043051FBF58F4D4764D339,
	EditorFacebookGameObject__ctor_m7AB93F33659FB08CE7B03FBC2E1696AA5DC0F116,
	EditorFacebookLoader_get_FBGameObject_mF55FA0448896FC2B275DA75F3020FB3C10068AA2,
	EditorFacebookLoader__ctor_mE3FDB685084309F33FA83D0E3AE548930499AB00,
	EditorFacebookMockDialog_get_Callback_mCEE3B3DEA3B9A7EFDB6506712B17051B55EE3AB6,
	EditorFacebookMockDialog_set_Callback_m29C78371CAD1A83DA7D6AC28990AEB786D557E5D,
	EditorFacebookMockDialog_get_CallbackID_mA2D915A145D79137A2ED7A6CA76E8654BB2DC636,
	EditorFacebookMockDialog_set_CallbackID_mF5D663C905CC826C09172CD01682674EC3211083,
	NULL,
	EditorFacebookMockDialog_Start_mFB0EB48D14D95AE01D83DF5EF938C7FF2A1096D0,
	EditorFacebookMockDialog_OnGUI_mB99D115254946EBE7E544272CED72E3AA50C286E,
	NULL,
	NULL,
	EditorFacebookMockDialog_SendCancelResult_mED5FBE23C6C523B3D70779D044CB8EA206FE129D,
	EditorFacebookMockDialog_SendErrorResult_mB640E7047D4B4D41F8ADCC6ABA0F7A90CA9FAE80,
	EditorFacebookMockDialog_OnGUIDialog_m34FF0BEABF08D955B613AC8B046E4F26CF951F5E,
	EditorFacebookMockDialog__ctor_mCC7AFAFC68499E410C1A195BCB64451F08FE7025,
	EditorWrapper__ctor_m8E3E1B551900D78694D03E05E517DE8F51233C2A,
	EditorWrapper_Init_m0E3C47D2A387A018137A90795DAE5F3857D73D2B,
	EditorWrapper_ShowLoginMockDialog_mE58A43F1D76756112867CD9485155C49384CD204,
	EditorWrapper_ShowAppRequestMockDialog_m31A60FA161D7104BD7B9760971EB6B0FA226A39C,
	EditorWrapper_ShowPayMockDialog_mA47E31819D9863D95CB67D643E4ABC752EC9C552,
	EditorWrapper_ShowMockShareDialog_m2B73EBD5CBD87F47A0989788641A35F2694A8A6D,
	EditorWrapper_ShowMockFriendFinderDialog_m87067BB95EE23D2E401DA0F6AA7693FFDA2E5D73,
	EditorWrapper_ShowEmptyMockDialog_m259878BB3752ECB8FF93D2AB046D71EF11741BA3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmptyMockDialog_get_EmptyDialogTitle_mFEDE84A7E5A703E80B37B37D1DE34D056D6B98BD,
	EmptyMockDialog_set_EmptyDialogTitle_m0CA314DC54D21EB042E4E4D62815C2935D019239,
	EmptyMockDialog_get_DialogTitle_m005D87C1A7A91BF1A9F23716A3F2A7ACA66EB5E5,
	EmptyMockDialog_DoGui_mFD61120D47EF90A828725C591921AC7FA2B0717B,
	EmptyMockDialog_SendSuccessResult_m2C4E92488ABED70D9F07D089654A2DBD0AA630BE,
	EmptyMockDialog__ctor_m38B860A2E95FD888C1C34823FBD1B62FAB5224FD,
	MockLoginDialog_get_DialogTitle_mE759177997D0285091014DB5C1C2B4BA078003C3,
	MockLoginDialog_DoGui_m8D58C43A20099387DF06C4B0A4A150184CDA49B7,
	MockLoginDialog_SendSuccessResult_m6F59BAAAA33C4401741C864661DAF66A2E4CF55F,
	MockLoginDialog__ctor_mD54C6EA5D5B18DEC94D1724365DF06E957681635,
	U3CU3Ec__DisplayClass4_0__ctor_mF2A5ABD5A07CF4BB9BCADD0072EB65483EC2E62F,
	U3CU3Ec__DisplayClass4_0_U3CSendSuccessResultU3Eb__0_m061E66F29110EE00082A03F7D227D3F6569264A0,
	U3CU3Ec__DisplayClass4_1__ctor_mAFFD5DCA86110CEF3E15E475493CD38A6B385E30,
	U3CU3Ec__DisplayClass4_1_U3CSendSuccessResultU3Eb__1_m65D7020D94C4C23234D9A1673CC4CC752965EB25,
	MockShareDialog_get_SubTitle_m823A30971FE7427DEB103BF2D2B2C97611404B1D,
	MockShareDialog_set_SubTitle_mBC479EA793258A1980BE0E32FA2113335F78AF89,
	MockShareDialog_get_DialogTitle_m7AEB561E333C2E0DBAF4F6E0DA9715A2B1F26BAA,
	MockShareDialog_DoGui_m08B8D3D04BFC10AE40BB9E9B8C26606391DEE86D,
	MockShareDialog_SendSuccessResult_m0B289A60115FA917712A4C39B3A58ED99E5AF59F,
	MockShareDialog_SendCancelResult_mA16A342E8873B72B67C4A704E41FD8C0D84B4A63,
	MockShareDialog_GenerateFakePostID_mC9194AFF8438A50C585A5F5824117BCFB1684D9A,
	MockShareDialog__ctor_m2A82D041CB346B7708C4E2E3418E47ECE9FBA6B0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MobileFacebook__ctor_m64301CCE10C1F82E06768DA8FCC9AC8DAC3D013F,
	MobileFacebook_set_ShareDialogMode_m98FA88AF18913065FCCC53B9CA56F4C1975B5010,
	NULL,
	NULL,
	NULL,
	MobileFacebook_OnLoginComplete_m0609D52E9F1EE2D04FE9E35C9F0190E2D1A54AA7,
	MobileFacebook_OnGetAppLinkComplete_mEE11BD67EEA88FC7B7188F2802EC53F096C5705B,
	MobileFacebook_OnAppRequestsComplete_m93A9C9DC97B4464EA2059E598528FFC79EF77C80,
	MobileFacebook_OnFetchDeferredAppLinkComplete_m1CCAD5D03BE2E8925F7668504F7AACC086ACF372,
	MobileFacebook_OnShareLinkComplete_mE3DC18B678E3F3EDD98B4AC921FEE9727C23BFD0,
	MobileFacebook_OnRefreshCurrentAccessTokenComplete_m3B43A30F8CC73A3632515C63EA44AFBF98C743BE,
	MobileFacebook_OpenFriendFinderDialog_m5F3B23D100F1E0AE8054ACDB65F1C8705445A472,
	MobileFacebook_OnFriendFinderComplete_m863B7889DE1E19A8860B2A57C9D90EB9E8A29266,
	MobileFacebook_OnUploadImageToMediaLibraryComplete_m67455C34DD5B68D8AE79B13D1E93133C6EBAF4BC,
	MobileFacebook_OnUploadVideoToMediaLibraryComplete_m2B0FE8CB188A151730548767C0611884CCDB9F88,
	MobileFacebook_UploadImageToMediaLibrary_m952C20E7BD3773FE6F716495530FB77F5D908981,
	MobileFacebook_UploadVideoToMediaLibrary_mB023F5062EAD8B70E6CB21AE1813B202A2AB7EAA,
	NULL,
	MobileFacebookGameObject_get_MobileFacebook_m35EDB34BF9712FFCC306E3E57426880A23450933,
	MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_m967F10654BA20892BD19305CBD5311920DB514E4,
	MobileFacebookGameObject_OnRefreshCurrentAccessTokenComplete_m7C76331B4D493E49F795B3FC1D964C827A68C75E,
	MobileFacebookGameObject_OnFriendFinderComplete_m643864C7029A18BB987F99F586B57650BE553C73,
	MobileFacebookGameObject_OnUploadImageToMediaLibraryComplete_m0FC79749DFF4670E9B0879809BF0F73847392D51,
	MobileFacebookGameObject_OnUploadVideoToMediaLibraryComplete_m39574D18CDC43B925DDFF69DD99027DC9F101AFB,
	MobileFacebookGameObject__ctor_mCF5F75B7D9E8DFF4999B2347C85FB15B7F5DD937,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IOSFacebook__ctor_m66612F017B01E22AF4200B953EDBB664C80901FA,
	IOSFacebook__ctor_mAE9F8C2F9D35ADB0C9DFA8B9F3E5BDE243896364,
	IOSFacebook_get_LimitEventUsage_mA5E590A095B1BBE4E8E3285CE0429FD4EFB01672,
	IOSFacebook_set_LimitEventUsage_m3723CE720A35A85B434E8D4A337BF374313AC439,
	IOSFacebook_get_SDKName_m5D047EA4CB01307C7BEADDC62C91AB2A9B7522E5,
	IOSFacebook_get_SDKVersion_m6BC591D20F4DC586DC7BA68A7F506EA9313286BF,
	IOSFacebook_Init_mAD89E53CB720C9C973FC984BB6878D07ED2BBECB,
	IOSFacebook_LogInWithReadPermissions_m10AE773BD51319F4D371F6A1345806D0C52BB835,
	IOSFacebook_LogInWithPublishPermissions_m6BA64446C13FB593F1656B18688E1F42C3761182,
	IOSFacebook_LogOut_mB9CD5A19E98779E3970FE3D00DC30467C6348728,
	IOSFacebook_AppRequest_m5544FD3F8DF8EA732B03C73E92FF81D9B05C3370,
	IOSFacebook_ShareLink_mA585AFB25C60B10AAB45FB6C66451CEF1872407D,
	IOSFacebook_FeedShare_mA5706979D224C9F0834867DDBDE02F2AA11AF063,
	IOSFacebook_AppEventsLogEvent_mD614F1CB0EBD7D4BF61C2965709BC60FC139EB4E,
	IOSFacebook_AppEventsLogPurchase_m78C5C159FD8BCD82690B3EE8D6ABA804A168B187,
	IOSFacebook_IsImplicitPurchaseLoggingEnabled_m88E675394465260B2CEAD1D20A4B08C43FA4D19A,
	IOSFacebook_ActivateApp_mE940AFD42A4ABF6D86B6C4F89CD4A203A95F3612,
	IOSFacebook_FetchDeferredAppLink_m85BFA2DC0D088C693B81EDFFF9A794948C452A4D,
	IOSFacebook_GetAppLink_mE987B5A522F5CDE2ECFDF20FA7434D46A7132A44,
	IOSFacebook_OpenFriendFinderDialog_mA80DBF45C7A7A630BA20EA3A7D6F3927A651E88C,
	IOSFacebook_RefreshCurrentAccessToken_mEF5849F54B204128141B82602B24F3A4C3EB5FE5,
	IOSFacebook_SetShareDialogMode_m81FD42A4AA6A6B6B41356363E1DC564548EB0FB4,
	IOSFacebook_UploadImageToMediaLibrary_m451642A6A8494541C880770E18C1E44757B2D9BC,
	IOSFacebook_UploadVideoToMediaLibrary_mB78BC61FF5A90909EF104AFD82B3D35AE8C59C8B,
	IOSFacebook_GetIOSWrapper_m93739800C1B290A298E02C1D62466C2A9590B9CB,
	IOSFacebook_MarshallDict_m516D3ED4003592C2F9F0E7BBF531CFA7575AEE1A,
	NULL,
	NativeDict__ctor_mBC808DB561E6E697E9D81544254525C1EE322E96,
	NativeDict_get_NumEntries_mA4035D61E41A7267AC7CCD531CA7E4181299B198,
	NativeDict_set_NumEntries_mC5EBBD8A2CFC4418058276C06DFCACDAFBBB2A43,
	NativeDict_get_Keys_m9EC76C4C54010B19B6312671B748C0C10D9E44D4,
	NativeDict_set_Keys_m00E0CE25465A48D54E26F0D8B5BDA92DFFFBD912,
	NativeDict_get_Values_m61379EC768664B378BA4297B546E186E0564F2F3,
	NativeDict_set_Values_m083A5DA8B5D49D43A2D972AA5C60D847E5CDC9D2,
	IOSFacebookGameObject__ctor_m8A13B35BE8A48DB191E2EF6F22337D9E32E6809A,
	IOSFacebookLoader_get_FBGameObject_mC5E93577DCC84FE1E8B2FAA6D29801172FD8F9C6,
	IOSFacebookLoader__ctor_mCC8417942DBA9DA0CA81FC6C4ACFE145AD84FF0C,
	AndroidFacebook__ctor_m800B0B0BCE70FCCF3304C1AD7720E3F0592FF367,
	AndroidFacebook__ctor_m6B6223DCABDE2372A473E4BF67B63717CA49E6E2,
	AndroidFacebook_set_KeyHash_mAF5F5CC14C52CB5BCCDC4ABE6964170113C86E41,
	AndroidFacebook_get_LimitEventUsage_mB85D17C46FE27AB60B292976AE7DAF71A4BBA20F,
	AndroidFacebook_set_LimitEventUsage_m6FF98E6C4EE77AD6DE6D5AF4ED8F0F40E9CAC478,
	AndroidFacebook_get_SDKName_m352E98581EF8467E2445803D01046BFF32797AE9,
	AndroidFacebook_get_SDKVersion_mB82A585ABBEF09C0399B2E773012864D4ABE1F42,
	AndroidFacebook_Init_m224D6B6E25629F946A14CE95EBCAB471065C5BD5,
	AndroidFacebook_LogInWithReadPermissions_mBA0DBA4B6EABAFEB3A98F64B79320AC0DE3BE4E8,
	AndroidFacebook_LogInWithPublishPermissions_m6905FFC3A1B9DFE7E38C9AE146DD63ABCC836381,
	AndroidFacebook_LogOut_m701021EAF696BE586564B3DD6B8AA0A5BBA8FFD6,
	AndroidFacebook_OnLoginStatusRetrieved_mEA3348D2AF0E5C41E544C72470FD30C372BF1399,
	AndroidFacebook_AppRequest_mA7E92F6EED4D28261C714BF65B2901566915F833,
	AndroidFacebook_ShareLink_m68C9AF5C32AD9CFDA1F6848F103216BC3BCB3DD9,
	AndroidFacebook_FeedShare_m907B87E18718BBB49CBE3B833896E3278E723D44,
	AndroidFacebook_GetAppLink_mA0A4D763DED55A2F3A8FE94B5B3511AA35FA365A,
	AndroidFacebook_AppEventsLogEvent_m74F1B4D7B5FF87D9076FA51201CCA4A430718FC0,
	AndroidFacebook_AppEventsLogPurchase_mFB7DA169F23F5FFF7CECA7071E0417CC7FED8C42,
	AndroidFacebook_IsImplicitPurchaseLoggingEnabled_mD8DBA49C7803286A76F38559936779A7CF523026,
	AndroidFacebook_ActivateApp_mBCF3BEBA898F8C6D50094D1A17AA89E58FD47C2C,
	AndroidFacebook_FetchDeferredAppLink_m2CF239925BF1EBDF4C98ECD1CB45DDB262AC97E1,
	AndroidFacebook_RefreshCurrentAccessToken_m6A7194ECC41C03C663FDDB65EC176415BAF88F72,
	AndroidFacebook_OpenFriendFinderDialog_m7165566A778DC538CFF01C291D035CC506086799,
	AndroidFacebook_UploadImageToMediaLibrary_m27BC076D3A825F6E1007ED07263E7BA416D0EAA2,
	AndroidFacebook_UploadVideoToMediaLibrary_m1171F1827A44F0A5EBE6CD58F48BAA299F850D6C,
	AndroidFacebook_SetShareDialogMode_m0FF7CC1894B561EB614202A37091D665B15CF375,
	AndroidFacebook_GetAndroidWrapper_m3D896094780BABFCCB04229CC55E955B8C81A21F,
	AndroidFacebook_CallFB_mFDE5EFB9BA0F76DFDED07133026443BE996CF484,
	NULL,
	NULL,
	AndroidFacebookGameObject_OnAwake_m70BA8568725EDF5158885029A7DADB28C0E1D90D,
	AndroidFacebookGameObject_OnEnable_m94851BD756E9C63D4426002B9E50A39AB98B6B53,
	AndroidFacebookGameObject_OnSceneLoaded_mD354C4AFCCB51A867A73BD9229DA36761CEA2559,
	AndroidFacebookGameObject_OnDisable_mB4BD63EFF9FAE9B7F04433E2D8B531E63B931AF0,
	AndroidFacebookGameObject_onPurchaseCompleteHandler_m3344999539A9709FCE4E81409D6F7AA0086E68F6,
	AndroidFacebookGameObject_OnLoginStatusRetrieved_mD6E19C1763E6E53AD29B6A2F3668DD0A88AB2FB0,
	AndroidFacebookGameObject__ctor_m1A573E9903D5D3819941809894EC2B87FE86EA40,
	AndroidFacebookLoader_get_FBGameObject_m5C2200E46EF11DE5D3B216740D617A0F4B985910,
	AndroidFacebookLoader__ctor_m3A28A42BA8DBFD20624977568405C156DD517C94,
	NULL,
	NULL,
	CanvasFacebook__ctor_mAF4280810AE65FACD32D9F2A406B120BC5C0978E,
	CanvasFacebook__ctor_mFADC2680DDB21C5C75FC627902A7473B4A4F8D2C,
	CanvasFacebook_GetCanvasJSWrapper_m5C906D34F407334A421A7C62937E789844397BE2,
	CanvasFacebook_get_LimitEventUsage_mDFAC3E98E7DC016D792129BDABCFDA987417C257,
	CanvasFacebook_set_LimitEventUsage_mA5CAE0BDD28B8E60AE681E3B00370ABF90DE38C8,
	CanvasFacebook_get_SDKName_m52514C262E7990DC7B41410F10DB9B4F32F2793B,
	CanvasFacebook_get_SDKVersion_m7880B755A503B19B268E71558D9A14C1EE34375D,
	CanvasFacebook_get_SDKUserAgent_m19431C58E396634D7429D2D49D6C6C37EA779A21,
	CanvasFacebook_Init_mA43AC226A703BA8677E8BD9713B6CC79872E2429,
	CanvasFacebook_LogInWithPublishPermissions_m7A170A9C73CEE157C17932BD339D90A75B80EA77,
	CanvasFacebook_LogInWithReadPermissions_m64B3E3C2AD44AEDE09E5B96191D1AB26834837DF,
	CanvasFacebook_LogOut_m25024805EC3651911654C2E046B6D74CDB60F2F2,
	CanvasFacebook_AppRequest_m5812F7A82FF6D31C357DA6048838D03F44FBC3A4,
	CanvasFacebook_ActivateApp_mDCACD1EA9ACF5745F0F514D389CDEE1639035E45,
	CanvasFacebook_ShareLink_mC9F8DE654F767AD417256EBFB617724FEE927211,
	CanvasFacebook_FeedShare_m2E5F8273A317FEC6F30594A26351C7C8D1D77F0E,
	CanvasFacebook_Pay_mED1A10923D32675F0EA667CCED168AD2BBD167BD,
	CanvasFacebook_GetAppLink_m4A8A761C399DA40C926B59DE918708DAC448E3CB,
	CanvasFacebook_AppEventsLogEvent_m3FB61636EFDF1AF85F5D8CD9DD5736C43D6A24F5,
	CanvasFacebook_AppEventsLogPurchase_mF4643714209EE1B3A223C07E230C72B6B672B504,
	CanvasFacebook_OnLoginComplete_mBA95037F894DA26A90C080843392EAC5ED7DCC85,
	CanvasFacebook_OnGetAppLinkComplete_mF03DC22FE451BA303D6ED9A2E98AAD9056808F9F,
	CanvasFacebook_OnFacebookAuthResponseChange_m59510BB88E1862F409C219780E4F3A12C423CF8E,
	CanvasFacebook_OnPayComplete_mEF0FA3268C48B395B4EA8DCBB67E0A4D0240129D,
	CanvasFacebook_OnAppRequestsComplete_m42FEEB4555D523C49FBAF2D28FDAD392B3007E89,
	CanvasFacebook_OnShareLinkComplete_m30972AB940C0066206F399059C87CA6B628B886F,
	CanvasFacebook_OnUrlResponse_mA0B2F834F29ABC42756D9780EE3728566411A7A8,
	CanvasFacebook_OnHideUnity_m1D8B2ACFB991E4CADE939B3BC048F21D5899B762,
	CanvasFacebook_FormatAuthResponse_m29957544DDD56A5A7911E90E4FA882E01CF1E6C6,
	CanvasFacebook_PayImpl_m191F4B5B34CC18F647BE3D74D843D2E2E716A2F8,
	CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m4976D88F6EC1F51DDE66F742272949D98B167914,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m244FD850ECB15E1A1A7C09A540C0F713F498E1C4,
	U3CU3Ec__ctor_m60D600AA093FBB576041472E737BCA5AE1A07B75,
	U3CU3Ec_U3COnFacebookAuthResponseChangeU3Eb__40_0_m353B3A6103E036AFA439E1A6878664840189723B,
	U3CU3Ec__DisplayClass47_0__ctor_mB9419A09D98EFB529D535C8BAEE69B23BB7A2D04,
	U3CU3Ec__DisplayClass47_0_U3CFormatAuthResponseU3Eb__0_m318AED6BDC7EE45070AEC6A005A736DAD7D30D2B,
	CanvasFacebookGameObject_get_CanvasFacebookImpl_mF03728D98E491DD5427B077857F9F7392477BBA9,
	CanvasFacebookGameObject_OnPayComplete_m45BB56D2AD9499D3384FC4A871973790C8A4D042,
	CanvasFacebookGameObject_OnFacebookAuthResponseChange_mCCB70C044A695B52168DAEDC7B03ED33C4C0E168,
	CanvasFacebookGameObject_OnUrlResponse_m210BCF7226AC5875AD5ABB6D15CE349C828E3146,
	CanvasFacebookGameObject_OnHideUnity_mCA39A8B0FD8140F5501C98A3390484544A1C4852,
	CanvasFacebookGameObject_OnAwake_m540CDA0C56DF537DA311C724A58C563D51D17429,
	CanvasFacebookGameObject__ctor_m000BB25669D557B3FAA159D5DF9C14C165F3C7C0,
	CanvasFacebookLoader_get_FBGameObject_mBFCA70D2129F2AE1EECA0A85E82925944F078987,
	CanvasFacebookLoader__ctor_m338849AC01B654E2DAE491A297A5E81FAC2DB822,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsBridge_Start_m4205C6750F943E0DC06530D67565414BB515EBEC,
	JsBridge_OnLoginComplete_m2CE331CD13A23446CD908DBA3B053599F88B66C8,
	JsBridge_OnFacebookAuthResponseChange_m0AAFA9598A4A71A0D5F9F10D84444794527C26E3,
	JsBridge_OnPayComplete_mA92E40AD47675A5BBBBA3AD541FB858B3E6B462B,
	JsBridge_OnAppRequestsComplete_mACAB31A9A07B73ABDFEBD7C7DBEE4460425A732B,
	JsBridge_OnShareLinkComplete_mC7DAEC7062D471DE350BA1F29366682408E45C49,
	JsBridge_OnFacebookFocus_mFA68B4FD1B25525FC46B32196AAB8564264A9AAC,
	JsBridge_OnInitComplete_m0E64BC6B0EBAB3F0E62B4F18608011EB95AACB20,
	JsBridge_OnUrlResponse_m740A08D5288D7FFB1D29CDB499FD182A9B76EAD9,
	JsBridge__ctor_m420BF13BA948B69C701DCA7F8144E502AD63EA04,
};
static const int32_t s_InvokerIndices[804] = 
{
	0,
	0,
	3,
	26,
	212,
	212,
	14,
	10,
	0,
	23,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	1623,
	4,
	122,
	14,
	26,
	279,
	878,
	14,
	26,
	14,
	26,
	1624,
	1625,
	14,
	26,
	14,
	14,
	-1,
	26,
	134,
	-1,
	23,
	4,
	-1,
	-1,
	4,
	4,
	49,
	49,
	49,
	49,
	4,
	4,
	49,
	131,
	131,
	4,
	122,
	4,
	122,
	4,
	122,
	49,
	49,
	49,
	792,
	4,
	122,
	4,
	122,
	4,
	122,
	156,
	1626,
	134,
	134,
	3,
	1627,
	1628,
	1629,
	1630,
	1631,
	525,
	525,
	3,
	122,
	3,
	1632,
	1633,
	1634,
	3,
	23,
	3,
	102,
	23,
	113,
	26,
	4,
	1635,
	133,
	4,
	122,
	122,
	49,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	122,
	1636,
	156,
	4,
	23,
	26,
	114,
	31,
	14,
	14,
	14,
	114,
	114,
	31,
	14,
	26,
	26,
	27,
	27,
	23,
	1637,
	761,
	1638,
	1639,
	1639,
	26,
	26,
	1640,
	1641,
	26,
	26,
	26,
	26,
	26,
	26,
	1637,
	26,
	28,
	28,
	26,
	3,
	23,
	9,
	102,
	23,
	113,
	26,
	-1,
	-1,
	-1,
	-1,
	102,
	31,
	1298,
	26,
	14,
	26,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	4,
	114,
	114,
	31,
	14,
	114,
	27,
	27,
	23,
	1637,
	761,
	1638,
	1639,
	1639,
	26,
	26,
	1640,
	1641,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	1642,
	23,
	26,
	26,
	-1,
	-1,
	27,
	27,
	27,
	-1,
	27,
	14,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	14,
	26,
	26,
	14,
	14,
	14,
	14,
	14,
	114,
	26,
	14,
	26,
	14,
	3,
	26,
	114,
	31,
	14,
	3,
	26,
	142,
	14,
	26,
	110,
	14,
	26,
	14,
	26,
	14,
	26,
	114,
	31,
	14,
	26,
	1643,
	1644,
	14,
	1645,
	0,
	94,
	0,
	26,
	26,
	14,
	26,
	14,
	26,
	28,
	26,
	14,
	26,
	4,
	14,
	156,
	156,
	525,
	525,
	14,
	28,
	34,
	28,
	28,
	28,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	4,
	122,
	122,
	122,
	122,
	134,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	-1,
	1646,
	-1,
	0,
	0,
	1,
	0,
	-1,
	0,
	0,
	2,
	1647,
	1648,
	0,
	1649,
	-1,
	-1,
	-1,
	-1,
	3,
	23,
	28,
	4,
	1639,
	1639,
	23,
	1639,
	1639,
	141,
	1650,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	122,
	122,
	134,
	1,
	1,
	1,
	1,
	23,
	26,
	14,
	122,
	122,
	4,
	4,
	134,
	1651,
	3,
	1652,
	4,
	0,
	23,
	3,
	32,
	23,
	114,
	14,
	23,
	14,
	14,
	26,
	23,
	3,
	3,
	23,
	26,
	23,
	111,
	111,
	0,
	164,
	0,
	1,
	26,
	14,
	26,
	26,
	26,
	26,
	26,
	172,
	172,
	172,
	172,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	14,
	26,
	14,
	23,
	27,
	114,
	31,
	14,
	14,
	168,
	26,
	1640,
	1641,
	1637,
	1638,
	761,
	1642,
	26,
	27,
	27,
	26,
	26,
	26,
	26,
	26,
	114,
	28,
	4,
	1653,
	27,
	102,
	26,
	177,
	26,
	14,
	27,
	23,
	113,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	14,
	23,
	114,
	28,
	14,
	26,
	26,
	386,
	1654,
	1655,
	1656,
	27,
	23,
	114,
	31,
	32,
	14,
	14,
	4,
	26,
	27,
	27,
	1637,
	761,
	1638,
	26,
	26,
	1640,
	1641,
	114,
	26,
	1642,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	1645,
	168,
	26,
	26,
	31,
	23,
	23,
	1657,
	23,
	26,
	23,
	14,
	23,
	14,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	32,
	23,
	26,
	23,
	168,
	27,
	27,
	168,
	168,
	168,
	23,
	168,
	27,
	27,
	168,
	168,
	14,
	26,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	14,
	23,
	32,
	26,
	26,
	114,
	26,
	1645,
	168,
	26,
	26,
	26,
	26,
	26,
	26,
	32,
	26,
	26,
	114,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	1645,
	168,
	32,
	14,
	26,
	26,
	26,
	26,
	26,
	23,
	1658,
	62,
	62,
	23,
	32,
	377,
	1659,
	1660,
	23,
	1661,
	1662,
	31,
	32,
	32,
	14,
	14,
	32,
	1663,
	319,
	32,
	23,
	27,
	114,
	31,
	14,
	14,
	1664,
	27,
	27,
	23,
	1637,
	761,
	1638,
	1640,
	1641,
	114,
	26,
	26,
	26,
	26,
	26,
	32,
	1645,
	168,
	4,
	0,
	-1,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	14,
	23,
	23,
	27,
	26,
	114,
	31,
	14,
	14,
	168,
	27,
	27,
	23,
	26,
	1637,
	761,
	1638,
	26,
	1640,
	1641,
	114,
	26,
	26,
	26,
	26,
	1645,
	168,
	32,
	4,
	27,
	-1,
	-1,
	23,
	23,
	1657,
	23,
	26,
	26,
	23,
	14,
	23,
	-1,
	27,
	23,
	27,
	4,
	114,
	31,
	14,
	14,
	14,
	1665,
	27,
	27,
	23,
	1637,
	26,
	761,
	1638,
	1642,
	26,
	1640,
	1641,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	31,
	134,
	1653,
	26,
	-1,
	-1,
	-1,
	3,
	23,
	26,
	23,
	26,
	14,
	26,
	26,
	26,
	31,
	23,
	23,
	14,
	23,
	26,
	26,
	26,
	31,
	26,
	26,
	26,
	31,
	14,
	23,
	1666,
	27,
	23,
	23,
	1640,
	1641,
	168,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[12] = 
{
	{ 0x02000020, { 11, 3 } },
	{ 0x02000071, { 20, 5 } },
	{ 0x02000076, { 25, 6 } },
	{ 0x0600002D, { 0, 3 } },
	{ 0x06000030, { 3, 3 } },
	{ 0x06000031, { 6, 1 } },
	{ 0x060000D8, { 7, 1 } },
	{ 0x060000D9, { 8, 3 } },
	{ 0x06000154, { 14, 1 } },
	{ 0x06000156, { 15, 1 } },
	{ 0x0600015B, { 16, 3 } },
	{ 0x060002A5, { 19, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[31] = 
{
	{ (Il2CppRGCTXDataType)2, 14994 },
	{ (Il2CppRGCTXDataType)2, 14995 },
	{ (Il2CppRGCTXDataType)3, 10729 },
	{ (Il2CppRGCTXDataType)3, 10730 },
	{ (Il2CppRGCTXDataType)2, 14302 },
	{ (Il2CppRGCTXDataType)3, 10731 },
	{ (Il2CppRGCTXDataType)3, 10732 },
	{ (Il2CppRGCTXDataType)2, 14385 },
	{ (Il2CppRGCTXDataType)3, 10733 },
	{ (Il2CppRGCTXDataType)3, 10734 },
	{ (Il2CppRGCTXDataType)2, 14387 },
	{ (Il2CppRGCTXDataType)3, 10735 },
	{ (Il2CppRGCTXDataType)3, 10736 },
	{ (Il2CppRGCTXDataType)3, 10737 },
	{ (Il2CppRGCTXDataType)2, 14463 },
	{ (Il2CppRGCTXDataType)3, 10738 },
	{ (Il2CppRGCTXDataType)2, 14465 },
	{ (Il2CppRGCTXDataType)2, 14996 },
	{ (Il2CppRGCTXDataType)2, 14997 },
	{ (Il2CppRGCTXDataType)3, 10739 },
	{ (Il2CppRGCTXDataType)3, 10740 },
	{ (Il2CppRGCTXDataType)2, 14581 },
	{ (Il2CppRGCTXDataType)3, 10741 },
	{ (Il2CppRGCTXDataType)3, 10742 },
	{ (Il2CppRGCTXDataType)3, 10743 },
	{ (Il2CppRGCTXDataType)3, 10744 },
	{ (Il2CppRGCTXDataType)2, 14595 },
	{ (Il2CppRGCTXDataType)3, 10745 },
	{ (Il2CppRGCTXDataType)3, 10746 },
	{ (Il2CppRGCTXDataType)3, 10747 },
	{ (Il2CppRGCTXDataType)3, 10748 },
};
extern const Il2CppCodeGenModule g_Facebook_UnityCodeGenModule;
const Il2CppCodeGenModule g_Facebook_UnityCodeGenModule = 
{
	"Facebook.Unity.dll",
	804,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	12,
	s_rgctxIndices,
	31,
	s_rgctxValues,
	NULL,
};
