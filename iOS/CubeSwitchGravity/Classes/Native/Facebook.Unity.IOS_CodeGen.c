﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Facebook.Unity.IOS.IOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
extern void IOSWrapper_Init_mF339D24A58CC8F0A2E9405A958DF42508F8BAC09 ();
// 0x00000002 System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
extern void IOSWrapper_LogInWithReadPermissions_m2B9035CE9C91DDA800E37E2D2FA5F90895B54B5F ();
// 0x00000003 System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
extern void IOSWrapper_LogInWithPublishPermissions_m470494418FBEB34CDB932AD86C44668D1CEB7315 ();
// 0x00000004 System.Void Facebook.Unity.IOS.IOSWrapper::LogOut()
extern void IOSWrapper_LogOut_m142A34D4DE3BCC54BB90C34C82B9F5B55D0F7757 ();
// 0x00000005 System.Void Facebook.Unity.IOS.IOSWrapper::SetPushNotificationsDeviceTokenString(System.String)
extern void IOSWrapper_SetPushNotificationsDeviceTokenString_m9818980A980056E5B0CCEC2BA704B62923B6DD45 ();
// 0x00000006 System.Void Facebook.Unity.IOS.IOSWrapper::SetShareDialogMode(System.Int32)
extern void IOSWrapper_SetShareDialogMode_m50891CBA598604EF3208BBDB3C7EE2F8FCDB1739 ();
// 0x00000007 System.Void Facebook.Unity.IOS.IOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern void IOSWrapper_ShareLink_m7CE97BF6A46CA7A62FFA09398E5919332C2A13F5 ();
// 0x00000008 System.Void Facebook.Unity.IOS.IOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void IOSWrapper_FeedShare_mAC0D4F3E8F821160412EEF0E53B8A70FCBD22E60 ();
// 0x00000009 System.Void Facebook.Unity.IOS.IOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern void IOSWrapper_AppRequest_mEB1840B858A4DEC7C2BC66C5A58BDF160A643570 ();
// 0x0000000A System.Void Facebook.Unity.IOS.IOSWrapper::OpenFriendFinderDialog(System.Int32)
extern void IOSWrapper_OpenFriendFinderDialog_m86600B9C035DE8AC7D1D3CA63CF1AB4AD3B2A1BB ();
// 0x0000000B System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsActivateApp()
extern void IOSWrapper_FBAppEventsActivateApp_m43AB634055F36A4808C70ADCBF72E72DEFF40153 ();
// 0x0000000C System.Void Facebook.Unity.IOS.IOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern void IOSWrapper_LogAppEvent_m5AA0FF03116C046C37B1F9E405733030243D2D3B ();
// 0x0000000D System.Void Facebook.Unity.IOS.IOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
extern void IOSWrapper_LogPurchaseAppEvent_mADD88626D53D4D32FC1F3A566D1E0BD12C2FFD22 ();
// 0x0000000E System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
extern void IOSWrapper_FBAppEventsSetLimitEventUsage_m7D7CA6FE002262FAC89548162083EBDF9725BF6D ();
// 0x0000000F System.Void Facebook.Unity.IOS.IOSWrapper::FBAutoLogAppEventsEnabled(System.Boolean)
extern void IOSWrapper_FBAutoLogAppEventsEnabled_m6DB85A295479F013A1FD66EBB175C7A86A2AA522 ();
// 0x00000010 System.Void Facebook.Unity.IOS.IOSWrapper::FBAdvertiserIDCollectionEnabled(System.Boolean)
extern void IOSWrapper_FBAdvertiserIDCollectionEnabled_mB761EC8C70206BDBFF0B8158E44385C904D92FA5 ();
// 0x00000011 System.Void Facebook.Unity.IOS.IOSWrapper::GetAppLink(System.Int32)
extern void IOSWrapper_GetAppLink_m0B3AA47D81EB8F19B8ED58147A255F8CA987556C ();
// 0x00000012 System.String Facebook.Unity.IOS.IOSWrapper::FBSdkVersion()
extern void IOSWrapper_FBSdkVersion_m55B4B48EB58CBA43C653DB36839226D5D6115527 ();
// 0x00000013 System.Void Facebook.Unity.IOS.IOSWrapper::FBSetUserID(System.String)
extern void IOSWrapper_FBSetUserID_mE30218BC90AA4FC20422E3E4655D2828854140FC ();
// 0x00000014 System.String Facebook.Unity.IOS.IOSWrapper::FBGetUserID()
extern void IOSWrapper_FBGetUserID_m33EF0E135AAAEED9885EA265A14D391E0D601217 ();
// 0x00000015 System.Void Facebook.Unity.IOS.IOSWrapper::UpdateUserProperties(System.Int32,System.String[],System.String[])
extern void IOSWrapper_UpdateUserProperties_mAB17D1D4011132AB0301DE9152C6CE689EF24FC3 ();
// 0x00000016 System.Void Facebook.Unity.IOS.IOSWrapper::SetDataProcessingOptions(System.String[],System.Int32,System.Int32)
extern void IOSWrapper_SetDataProcessingOptions_m557CD86173C4ED68EE4D0EA779179E35B892863A ();
// 0x00000017 System.Void Facebook.Unity.IOS.IOSWrapper::UploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
extern void IOSWrapper_UploadImageToMediaLibrary_m3EFA9A44CAF61BDD4B3F932B5E6CDEC0D28A0230 ();
// 0x00000018 System.Void Facebook.Unity.IOS.IOSWrapper::UploadVideoToMediaLibrary(System.Int32,System.String,System.String)
extern void IOSWrapper_UploadVideoToMediaLibrary_mA5B01C8BA5FA8F50C24D70AD216553D865E6CFAA ();
// 0x00000019 System.Void Facebook.Unity.IOS.IOSWrapper::FetchDeferredAppLink(System.Int32)
extern void IOSWrapper_FetchDeferredAppLink_mCB0AC641CAD4F4F637567295000A12686FE704A2 ();
// 0x0000001A System.Void Facebook.Unity.IOS.IOSWrapper::RefreshCurrentAccessToken(System.Int32)
extern void IOSWrapper_RefreshCurrentAccessToken_mA3F332A96AFD3D0C5091F88EC711B6F2AE6B60DA ();
// 0x0000001B System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBInit(System.String,System.Boolean,System.String,System.String)
extern void IOSWrapper_IOSFBInit_m73A461972CD6DA6FBEF5F1D9F4E9254F1E809514 ();
// 0x0000001C System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithReadPermissions(System.Int32,System.String)
extern void IOSWrapper_IOSFBLogInWithReadPermissions_m1C152188B7874AEF7D89AF60B117E5067CCE0C22 ();
// 0x0000001D System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithPublishPermissions(System.Int32,System.String)
extern void IOSWrapper_IOSFBLogInWithPublishPermissions_m55CB5F0007D3C64CAD33FD7F0AF3AA946C469B32 ();
// 0x0000001E System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogOut()
extern void IOSWrapper_IOSFBLogOut_m8309ED28CA838CA644BECC7122A743EDA9D80914 ();
// 0x0000001F System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetPushNotificationsDeviceTokenString(System.String)
extern void IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1ACB30F8415547BDD88079FC07DE875593562F41 ();
// 0x00000020 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetShareDialogMode(System.Int32)
extern void IOSWrapper_IOSFBSetShareDialogMode_m1969574075BA2A2823AC2134A65FE760C699578D ();
// 0x00000021 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern void IOSWrapper_IOSFBShareLink_mBF4AF29F3004B562575A4B00EDF6927A7DBB158B ();
// 0x00000022 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void IOSWrapper_IOSFBFeedShare_m92FB752493C3CE161DC05F4EB08ECF4F6D9603E5 ();
// 0x00000023 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern void IOSWrapper_IOSFBAppRequest_mC12553BDEB7401A9D53D5A226FEC76D8C66F0F11 ();
// 0x00000024 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsActivateApp()
extern void IOSWrapper_IOSFBAppEventsActivateApp_m886B369E937973A94740F7C6B5418C146BE75027 ();
// 0x00000025 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern void IOSWrapper_IOSFBAppEventsLogEvent_mDC12B628F018C49CA0469CEA8504F8DAEF2D038A ();
// 0x00000026 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
extern void IOSWrapper_IOSFBAppEventsLogPurchase_mFDEE1BBF22E213A15CC8444A28D02D4568D1865F ();
// 0x00000027 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
extern void IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mDBD018B1E0047E1D36F952235E148CC5CFF85ADC ();
// 0x00000028 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAutoLogAppEventsEnabled(System.Boolean)
extern void IOSWrapper_IOSFBAutoLogAppEventsEnabled_mA56C8A8A2AD89B3CADA8C5D42E0AEE6D3D31C19B ();
// 0x00000029 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAdvertiserIDCollectionEnabled(System.Boolean)
extern void IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_mC5E07567FC71CDD6CD00951C190F0F32EDF99AD3 ();
// 0x0000002A System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBGetAppLink(System.Int32)
extern void IOSWrapper_IOSFBGetAppLink_mB419CD847C3F97472C60A5ECC3FD951CA2C62845 ();
// 0x0000002B System.String Facebook.Unity.IOS.IOSWrapper::IOSFBSdkVersion()
extern void IOSWrapper_IOSFBSdkVersion_m5580A6DD80E0F840EA7A9114223FF85E02528F2D ();
// 0x0000002C System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFetchDeferredAppLink(System.Int32)
extern void IOSWrapper_IOSFBFetchDeferredAppLink_m577296A1DA96204DE40AFCCBA26662087F6D2C33 ();
// 0x0000002D System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBRefreshCurrentAccessToken(System.Int32)
extern void IOSWrapper_IOSFBRefreshCurrentAccessToken_mE5C7DBCF9EF6EAA319499F4BD1C3157BB8FAD0DE ();
// 0x0000002E System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetUserID(System.String)
extern void IOSWrapper_IOSFBSetUserID_m3F6DA7CC00BCEA167E21977412DEE2E12181D3E1 ();
// 0x0000002F System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBOpenGamingServicesFriendFinder(System.Int32)
extern void IOSWrapper_IOSFBOpenGamingServicesFriendFinder_mEEE93B70E6138C53DD77B13DAF61C58BD6AC9A2D ();
// 0x00000030 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
extern void IOSWrapper_IOSFBUploadImageToMediaLibrary_m6FBB5647FE4BC4690E4596123BA4D3F2A190701E ();
// 0x00000031 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUploadVideoToMediaLibrary(System.Int32,System.String,System.String)
extern void IOSWrapper_IOSFBUploadVideoToMediaLibrary_m1EF1013FC33352A9041499903E67A321C416FDBC ();
// 0x00000032 System.String Facebook.Unity.IOS.IOSWrapper::IOSFBGetUserID()
extern void IOSWrapper_IOSFBGetUserID_m3272CC953583D0A16B55732B9A630FA6800DE8B2 ();
// 0x00000033 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetDataProcessingOptions(System.String[],System.Int32,System.Int32,System.Int32)
extern void IOSWrapper_IOSFBSetDataProcessingOptions_mDA958C071487B6FF8DD90FE683563BF59ED73BFB ();
// 0x00000034 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUpdateUserProperties(System.Int32,System.String[],System.String[])
extern void IOSWrapper_IOSFBUpdateUserProperties_mBDE8E194CD48792427A9B2573F7D81C68ECF0365 ();
// 0x00000035 System.Void Facebook.Unity.IOS.IOSWrapper::.ctor()
extern void IOSWrapper__ctor_m7422353F3B57D9F3069E82D8C7CD5257834AA437 ();
static Il2CppMethodPointer s_methodPointers[53] = 
{
	IOSWrapper_Init_mF339D24A58CC8F0A2E9405A958DF42508F8BAC09,
	IOSWrapper_LogInWithReadPermissions_m2B9035CE9C91DDA800E37E2D2FA5F90895B54B5F,
	IOSWrapper_LogInWithPublishPermissions_m470494418FBEB34CDB932AD86C44668D1CEB7315,
	IOSWrapper_LogOut_m142A34D4DE3BCC54BB90C34C82B9F5B55D0F7757,
	IOSWrapper_SetPushNotificationsDeviceTokenString_m9818980A980056E5B0CCEC2BA704B62923B6DD45,
	IOSWrapper_SetShareDialogMode_m50891CBA598604EF3208BBDB3C7EE2F8FCDB1739,
	IOSWrapper_ShareLink_m7CE97BF6A46CA7A62FFA09398E5919332C2A13F5,
	IOSWrapper_FeedShare_mAC0D4F3E8F821160412EEF0E53B8A70FCBD22E60,
	IOSWrapper_AppRequest_mEB1840B858A4DEC7C2BC66C5A58BDF160A643570,
	IOSWrapper_OpenFriendFinderDialog_m86600B9C035DE8AC7D1D3CA63CF1AB4AD3B2A1BB,
	IOSWrapper_FBAppEventsActivateApp_m43AB634055F36A4808C70ADCBF72E72DEFF40153,
	IOSWrapper_LogAppEvent_m5AA0FF03116C046C37B1F9E405733030243D2D3B,
	IOSWrapper_LogPurchaseAppEvent_mADD88626D53D4D32FC1F3A566D1E0BD12C2FFD22,
	IOSWrapper_FBAppEventsSetLimitEventUsage_m7D7CA6FE002262FAC89548162083EBDF9725BF6D,
	IOSWrapper_FBAutoLogAppEventsEnabled_m6DB85A295479F013A1FD66EBB175C7A86A2AA522,
	IOSWrapper_FBAdvertiserIDCollectionEnabled_mB761EC8C70206BDBFF0B8158E44385C904D92FA5,
	IOSWrapper_GetAppLink_m0B3AA47D81EB8F19B8ED58147A255F8CA987556C,
	IOSWrapper_FBSdkVersion_m55B4B48EB58CBA43C653DB36839226D5D6115527,
	IOSWrapper_FBSetUserID_mE30218BC90AA4FC20422E3E4655D2828854140FC,
	IOSWrapper_FBGetUserID_m33EF0E135AAAEED9885EA265A14D391E0D601217,
	IOSWrapper_UpdateUserProperties_mAB17D1D4011132AB0301DE9152C6CE689EF24FC3,
	IOSWrapper_SetDataProcessingOptions_m557CD86173C4ED68EE4D0EA779179E35B892863A,
	IOSWrapper_UploadImageToMediaLibrary_m3EFA9A44CAF61BDD4B3F932B5E6CDEC0D28A0230,
	IOSWrapper_UploadVideoToMediaLibrary_mA5B01C8BA5FA8F50C24D70AD216553D865E6CFAA,
	IOSWrapper_FetchDeferredAppLink_mCB0AC641CAD4F4F637567295000A12686FE704A2,
	IOSWrapper_RefreshCurrentAccessToken_mA3F332A96AFD3D0C5091F88EC711B6F2AE6B60DA,
	IOSWrapper_IOSFBInit_m73A461972CD6DA6FBEF5F1D9F4E9254F1E809514,
	IOSWrapper_IOSFBLogInWithReadPermissions_m1C152188B7874AEF7D89AF60B117E5067CCE0C22,
	IOSWrapper_IOSFBLogInWithPublishPermissions_m55CB5F0007D3C64CAD33FD7F0AF3AA946C469B32,
	IOSWrapper_IOSFBLogOut_m8309ED28CA838CA644BECC7122A743EDA9D80914,
	IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1ACB30F8415547BDD88079FC07DE875593562F41,
	IOSWrapper_IOSFBSetShareDialogMode_m1969574075BA2A2823AC2134A65FE760C699578D,
	IOSWrapper_IOSFBShareLink_mBF4AF29F3004B562575A4B00EDF6927A7DBB158B,
	IOSWrapper_IOSFBFeedShare_m92FB752493C3CE161DC05F4EB08ECF4F6D9603E5,
	IOSWrapper_IOSFBAppRequest_mC12553BDEB7401A9D53D5A226FEC76D8C66F0F11,
	IOSWrapper_IOSFBAppEventsActivateApp_m886B369E937973A94740F7C6B5418C146BE75027,
	IOSWrapper_IOSFBAppEventsLogEvent_mDC12B628F018C49CA0469CEA8504F8DAEF2D038A,
	IOSWrapper_IOSFBAppEventsLogPurchase_mFDEE1BBF22E213A15CC8444A28D02D4568D1865F,
	IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mDBD018B1E0047E1D36F952235E148CC5CFF85ADC,
	IOSWrapper_IOSFBAutoLogAppEventsEnabled_mA56C8A8A2AD89B3CADA8C5D42E0AEE6D3D31C19B,
	IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_mC5E07567FC71CDD6CD00951C190F0F32EDF99AD3,
	IOSWrapper_IOSFBGetAppLink_mB419CD847C3F97472C60A5ECC3FD951CA2C62845,
	IOSWrapper_IOSFBSdkVersion_m5580A6DD80E0F840EA7A9114223FF85E02528F2D,
	IOSWrapper_IOSFBFetchDeferredAppLink_m577296A1DA96204DE40AFCCBA26662087F6D2C33,
	IOSWrapper_IOSFBRefreshCurrentAccessToken_mE5C7DBCF9EF6EAA319499F4BD1C3157BB8FAD0DE,
	IOSWrapper_IOSFBSetUserID_m3F6DA7CC00BCEA167E21977412DEE2E12181D3E1,
	IOSWrapper_IOSFBOpenGamingServicesFriendFinder_mEEE93B70E6138C53DD77B13DAF61C58BD6AC9A2D,
	IOSWrapper_IOSFBUploadImageToMediaLibrary_m6FBB5647FE4BC4690E4596123BA4D3F2A190701E,
	IOSWrapper_IOSFBUploadVideoToMediaLibrary_m1EF1013FC33352A9041499903E67A321C416FDBC,
	IOSWrapper_IOSFBGetUserID_m3272CC953583D0A16B55732B9A630FA6800DE8B2,
	IOSWrapper_IOSFBSetDataProcessingOptions_mDA958C071487B6FF8DD90FE683563BF59ED73BFB,
	IOSWrapper_IOSFBUpdateUserProperties_mBDE8E194CD48792427A9B2573F7D81C68ECF0365,
	IOSWrapper__ctor_m7422353F3B57D9F3069E82D8C7CD5257834AA437,
};
static const int32_t s_InvokerIndices[53] = 
{
	1658,
	62,
	62,
	23,
	26,
	32,
	377,
	1659,
	1660,
	32,
	23,
	1661,
	1662,
	31,
	31,
	31,
	32,
	14,
	26,
	14,
	319,
	35,
	1663,
	319,
	32,
	32,
	1667,
	527,
	527,
	3,
	122,
	133,
	1668,
	1669,
	1670,
	3,
	1671,
	1672,
	792,
	792,
	792,
	133,
	4,
	133,
	133,
	122,
	133,
	1673,
	934,
	4,
	1674,
	934,
	23,
};
extern const Il2CppCodeGenModule g_Facebook_Unity_IOSCodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_IOSCodeGenModule = 
{
	"Facebook.Unity.IOS.dll",
	53,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
