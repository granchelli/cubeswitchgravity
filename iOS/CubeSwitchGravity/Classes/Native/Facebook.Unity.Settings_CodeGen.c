﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern void FacebookSettings_get_SelectedAppIndex_m15C24B67BD5F3FDF670F5692A443701E9F486A1D ();
// 0x00000002 System.Void Facebook.Unity.Settings.FacebookSettings::set_SelectedAppIndex(System.Int32)
extern void FacebookSettings_set_SelectedAppIndex_mD719D3EAD8300C2454DA8F80261763F662E15152 ();
// 0x00000003 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern void FacebookSettings_get_AppIds_mA13653D722FD1744C910BB9DEE8246F744CD026B ();
// 0x00000004 System.Void Facebook.Unity.Settings.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_AppIds_m8001A101F952CA7B38CDF41A9B3B73B666AA7063 ();
// 0x00000005 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppLabels()
extern void FacebookSettings_get_AppLabels_m91548254F5A9539B375973E2B9FE7D24012CF32A ();
// 0x00000006 System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_AppLabels_m9E988CE2E78EAF77782470E5639ED54E7EEBBCFC ();
// 0x00000007 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern void FacebookSettings_get_ClientTokens_mDA1130C3375EEA0935DEF1B0EFB4178ACCDAAE00 ();
// 0x00000008 System.Void Facebook.Unity.Settings.FacebookSettings::set_ClientTokens(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_ClientTokens_m4B88AB8AFBB1B716D1FC4D9C9E3B726B6986771D ();
// 0x00000009 System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern void FacebookSettings_get_AppId_mFA5644F44B92278C63D7A7F0A8B3FFE3C8CF1339 ();
// 0x0000000A System.String Facebook.Unity.Settings.FacebookSettings::get_ClientToken()
extern void FacebookSettings_get_ClientToken_m1BEE9D1A29D803FBFE4373AD152EF23D82D6DB34 ();
// 0x0000000B System.Boolean Facebook.Unity.Settings.FacebookSettings::get_IsValidAppId()
extern void FacebookSettings_get_IsValidAppId_m3B953D9CB257221277FAE9D4B2B15203F6AD4FDA ();
// 0x0000000C System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Cookie()
extern void FacebookSettings_get_Cookie_mFD26906B75C0D0551BF66A8300451581F7487CA6 ();
// 0x0000000D System.Void Facebook.Unity.Settings.FacebookSettings::set_Cookie(System.Boolean)
extern void FacebookSettings_set_Cookie_mBD2CFDFA096F0CE071C2C838B5DDF78D09FCEECC ();
// 0x0000000E System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Logging()
extern void FacebookSettings_get_Logging_m0D1A93F9B28D4D267EC5D6AC0EA950C7D19334C0 ();
// 0x0000000F System.Void Facebook.Unity.Settings.FacebookSettings::set_Logging(System.Boolean)
extern void FacebookSettings_set_Logging_mED7B7CA17E024D69FCEDE0129957F99674FC07FF ();
// 0x00000010 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Status()
extern void FacebookSettings_get_Status_mC570E7935B44EC8D6421507787B2B467BFB51A74 ();
// 0x00000011 System.Void Facebook.Unity.Settings.FacebookSettings::set_Status(System.Boolean)
extern void FacebookSettings_set_Status_m5EE83D480EFF7E8650BA2658E4EAD9F6D7F351A2 ();
// 0x00000012 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Xfbml()
extern void FacebookSettings_get_Xfbml_m565C3861A504C1B403CAA0976E103C17CEA2DACA ();
// 0x00000013 System.Void Facebook.Unity.Settings.FacebookSettings::set_Xfbml(System.Boolean)
extern void FacebookSettings_set_Xfbml_mA1DBD2EE6707CCB18E4A385D23D933191418561E ();
// 0x00000014 System.String Facebook.Unity.Settings.FacebookSettings::get_AndroidKeystorePath()
extern void FacebookSettings_get_AndroidKeystorePath_m2BF9213216266171F7C1778AE8FF65E94756FCA3 ();
// 0x00000015 System.Void Facebook.Unity.Settings.FacebookSettings::set_AndroidKeystorePath(System.String)
extern void FacebookSettings_set_AndroidKeystorePath_mAFAF4E98044BE931F5C05D98B7D92F2F4279C59D ();
// 0x00000016 System.String Facebook.Unity.Settings.FacebookSettings::get_IosURLSuffix()
extern void FacebookSettings_get_IosURLSuffix_m6EA5B08777DAFA433DF27CE98F59A1854FBE7BBD ();
// 0x00000017 System.Void Facebook.Unity.Settings.FacebookSettings::set_IosURLSuffix(System.String)
extern void FacebookSettings_set_IosURLSuffix_mFC2E0CF2E4D3BE9507DAC374CDA669F10EAF32F1 ();
// 0x00000018 System.String Facebook.Unity.Settings.FacebookSettings::get_ChannelUrl()
extern void FacebookSettings_get_ChannelUrl_mF3A45FAAAD4640806580D84E3C7B85EE15F61B6E ();
// 0x00000019 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_FrictionlessRequests()
extern void FacebookSettings_get_FrictionlessRequests_m91B6446D6A40DD3A9991023B396E47CA1A2CBE38 ();
// 0x0000001A System.Void Facebook.Unity.Settings.FacebookSettings::set_FrictionlessRequests(System.Boolean)
extern void FacebookSettings_set_FrictionlessRequests_m1EE9A84D158DD49D2235119B8BE86F89847F3D4C ();
// 0x0000001B System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::get_AppLinkSchemes()
extern void FacebookSettings_get_AppLinkSchemes_mD9F32E3DF8603C189B78CC5F374EFA13A0567475 ();
// 0x0000001C System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes>)
extern void FacebookSettings_set_AppLinkSchemes_m3C8483DA5B29D760FC1D70F1E207360FBA7846F3 ();
// 0x0000001D System.String Facebook.Unity.Settings.FacebookSettings::get_UploadAccessToken()
extern void FacebookSettings_get_UploadAccessToken_m406A6BA05C5DB5AA72AAD9A5DFB17F622B7E8745 ();
// 0x0000001E System.Void Facebook.Unity.Settings.FacebookSettings::set_UploadAccessToken(System.String)
extern void FacebookSettings_set_UploadAccessToken_mAB5E41D7538F6DFEEBDDD700F4996E0758186235 ();
// 0x0000001F System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AutoLogAppEventsEnabled()
extern void FacebookSettings_get_AutoLogAppEventsEnabled_mE3917C4000F6475670591421314FEAB3E972E801 ();
// 0x00000020 System.Void Facebook.Unity.Settings.FacebookSettings::set_AutoLogAppEventsEnabled(System.Boolean)
extern void FacebookSettings_set_AutoLogAppEventsEnabled_m2B76C87F231F41D8FCD73616080FD8DA410DF703 ();
// 0x00000021 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AdvertiserIDCollectionEnabled()
extern void FacebookSettings_get_AdvertiserIDCollectionEnabled_m65516FD7F538021B1B162F67400874D0CAB3FDB1 ();
// 0x00000022 System.Void Facebook.Unity.Settings.FacebookSettings::set_AdvertiserIDCollectionEnabled(System.Boolean)
extern void FacebookSettings_set_AdvertiserIDCollectionEnabled_mE954E96A6E52618150E41357114418B0392A6DE4 ();
// 0x00000023 Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern void FacebookSettings_get_Instance_m5ABBB87B5A501FDDCC14F42DE95CA2E27CD10E3F ();
// 0x00000024 Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern void FacebookSettings_get_NullableInstance_m9CA56101129FC2B4881955E26744DD4C1ED71CE9 ();
// 0x00000025 System.Void Facebook.Unity.Settings.FacebookSettings::RegisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern void FacebookSettings_RegisterChangeEventCallback_m631273CECBE0AA07A71DCD13C5739370CECBC611 ();
// 0x00000026 System.Void Facebook.Unity.Settings.FacebookSettings::UnregisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern void FacebookSettings_UnregisterChangeEventCallback_m9DF6B193E3B0E515B62384FF2A0FE73CBC5A2290 ();
// 0x00000027 System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern void FacebookSettings_SettingsChanged_m08CC1DDF0EF6F6ACAC24583D5F803298A0F5C0E6 ();
// 0x00000028 System.Void Facebook.Unity.Settings.FacebookSettings::.ctor()
extern void FacebookSettings__ctor_mDCAD9CCB9AB50933AE30CA63D7E40205F833B799 ();
// 0x00000029 System.Void Facebook.Unity.Settings.FacebookSettings::.cctor()
extern void FacebookSettings__cctor_m319BC348BE17C4F440AF60C0F779A2778C7A863B ();
// 0x0000002A System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::.ctor(System.Object,System.IntPtr)
extern void OnChangeCallback__ctor_m158D6E64BDBC7CBECE2F9FE9F2E36DFE7DA74827 ();
// 0x0000002B System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::Invoke()
extern void OnChangeCallback_Invoke_mE30D75749A614808362AC08C0544AC60C7706952 ();
// 0x0000002C System.IAsyncResult Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnChangeCallback_BeginInvoke_mA9A34972C6B316672C3212D1B59D0960875AF902 ();
// 0x0000002D System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::EndInvoke(System.IAsyncResult)
extern void OnChangeCallback_EndInvoke_mDD3F119E2E8A556C30A4FD05646305C584588A4F ();
// 0x0000002E System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern void UrlSchemes__ctor_mCEFFF9E80F0E13A8EBE2B89076860FCD89A82007 ();
// 0x0000002F System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::get_Schemes()
extern void UrlSchemes_get_Schemes_mA792C89E2470470DDB268B27298D3179FB270E21 ();
// 0x00000030 System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
extern void UrlSchemes_set_Schemes_m7AC4F5CC69AC8AF8F1252D06B2715A780E869683 ();
// 0x00000031 System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.cctor()
extern void U3CU3Ec__cctor_mDB771ACB0987044F7ABA02134612A4757733AC6B ();
// 0x00000032 System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.ctor()
extern void U3CU3Ec__ctor_m0648898966E4CB384A07C773BEF7437229BE4219 ();
// 0x00000033 System.Void Facebook.Unity.Settings.FacebookSettings_<>c::<SettingsChanged>b__80_0(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern void U3CU3Ec_U3CSettingsChangedU3Eb__80_0_m9E397C4D4500A4B3357A7476F74181ADC1DA29B1 ();
static Il2CppMethodPointer s_methodPointers[51] = 
{
	FacebookSettings_get_SelectedAppIndex_m15C24B67BD5F3FDF670F5692A443701E9F486A1D,
	FacebookSettings_set_SelectedAppIndex_mD719D3EAD8300C2454DA8F80261763F662E15152,
	FacebookSettings_get_AppIds_mA13653D722FD1744C910BB9DEE8246F744CD026B,
	FacebookSettings_set_AppIds_m8001A101F952CA7B38CDF41A9B3B73B666AA7063,
	FacebookSettings_get_AppLabels_m91548254F5A9539B375973E2B9FE7D24012CF32A,
	FacebookSettings_set_AppLabels_m9E988CE2E78EAF77782470E5639ED54E7EEBBCFC,
	FacebookSettings_get_ClientTokens_mDA1130C3375EEA0935DEF1B0EFB4178ACCDAAE00,
	FacebookSettings_set_ClientTokens_m4B88AB8AFBB1B716D1FC4D9C9E3B726B6986771D,
	FacebookSettings_get_AppId_mFA5644F44B92278C63D7A7F0A8B3FFE3C8CF1339,
	FacebookSettings_get_ClientToken_m1BEE9D1A29D803FBFE4373AD152EF23D82D6DB34,
	FacebookSettings_get_IsValidAppId_m3B953D9CB257221277FAE9D4B2B15203F6AD4FDA,
	FacebookSettings_get_Cookie_mFD26906B75C0D0551BF66A8300451581F7487CA6,
	FacebookSettings_set_Cookie_mBD2CFDFA096F0CE071C2C838B5DDF78D09FCEECC,
	FacebookSettings_get_Logging_m0D1A93F9B28D4D267EC5D6AC0EA950C7D19334C0,
	FacebookSettings_set_Logging_mED7B7CA17E024D69FCEDE0129957F99674FC07FF,
	FacebookSettings_get_Status_mC570E7935B44EC8D6421507787B2B467BFB51A74,
	FacebookSettings_set_Status_m5EE83D480EFF7E8650BA2658E4EAD9F6D7F351A2,
	FacebookSettings_get_Xfbml_m565C3861A504C1B403CAA0976E103C17CEA2DACA,
	FacebookSettings_set_Xfbml_mA1DBD2EE6707CCB18E4A385D23D933191418561E,
	FacebookSettings_get_AndroidKeystorePath_m2BF9213216266171F7C1778AE8FF65E94756FCA3,
	FacebookSettings_set_AndroidKeystorePath_mAFAF4E98044BE931F5C05D98B7D92F2F4279C59D,
	FacebookSettings_get_IosURLSuffix_m6EA5B08777DAFA433DF27CE98F59A1854FBE7BBD,
	FacebookSettings_set_IosURLSuffix_mFC2E0CF2E4D3BE9507DAC374CDA669F10EAF32F1,
	FacebookSettings_get_ChannelUrl_mF3A45FAAAD4640806580D84E3C7B85EE15F61B6E,
	FacebookSettings_get_FrictionlessRequests_m91B6446D6A40DD3A9991023B396E47CA1A2CBE38,
	FacebookSettings_set_FrictionlessRequests_m1EE9A84D158DD49D2235119B8BE86F89847F3D4C,
	FacebookSettings_get_AppLinkSchemes_mD9F32E3DF8603C189B78CC5F374EFA13A0567475,
	FacebookSettings_set_AppLinkSchemes_m3C8483DA5B29D760FC1D70F1E207360FBA7846F3,
	FacebookSettings_get_UploadAccessToken_m406A6BA05C5DB5AA72AAD9A5DFB17F622B7E8745,
	FacebookSettings_set_UploadAccessToken_mAB5E41D7538F6DFEEBDDD700F4996E0758186235,
	FacebookSettings_get_AutoLogAppEventsEnabled_mE3917C4000F6475670591421314FEAB3E972E801,
	FacebookSettings_set_AutoLogAppEventsEnabled_m2B76C87F231F41D8FCD73616080FD8DA410DF703,
	FacebookSettings_get_AdvertiserIDCollectionEnabled_m65516FD7F538021B1B162F67400874D0CAB3FDB1,
	FacebookSettings_set_AdvertiserIDCollectionEnabled_mE954E96A6E52618150E41357114418B0392A6DE4,
	FacebookSettings_get_Instance_m5ABBB87B5A501FDDCC14F42DE95CA2E27CD10E3F,
	FacebookSettings_get_NullableInstance_m9CA56101129FC2B4881955E26744DD4C1ED71CE9,
	FacebookSettings_RegisterChangeEventCallback_m631273CECBE0AA07A71DCD13C5739370CECBC611,
	FacebookSettings_UnregisterChangeEventCallback_m9DF6B193E3B0E515B62384FF2A0FE73CBC5A2290,
	FacebookSettings_SettingsChanged_m08CC1DDF0EF6F6ACAC24583D5F803298A0F5C0E6,
	FacebookSettings__ctor_mDCAD9CCB9AB50933AE30CA63D7E40205F833B799,
	FacebookSettings__cctor_m319BC348BE17C4F440AF60C0F779A2778C7A863B,
	OnChangeCallback__ctor_m158D6E64BDBC7CBECE2F9FE9F2E36DFE7DA74827,
	OnChangeCallback_Invoke_mE30D75749A614808362AC08C0544AC60C7706952,
	OnChangeCallback_BeginInvoke_mA9A34972C6B316672C3212D1B59D0960875AF902,
	OnChangeCallback_EndInvoke_mDD3F119E2E8A556C30A4FD05646305C584588A4F,
	UrlSchemes__ctor_mCEFFF9E80F0E13A8EBE2B89076860FCD89A82007,
	UrlSchemes_get_Schemes_mA792C89E2470470DDB268B27298D3179FB270E21,
	UrlSchemes_set_Schemes_m7AC4F5CC69AC8AF8F1252D06B2715A780E869683,
	U3CU3Ec__cctor_mDB771ACB0987044F7ABA02134612A4757733AC6B,
	U3CU3Ec__ctor_m0648898966E4CB384A07C773BEF7437229BE4219,
	U3CU3Ec_U3CSettingsChangedU3Eb__80_0_m9E397C4D4500A4B3357A7476F74181ADC1DA29B1,
};
static const int32_t s_InvokerIndices[51] = 
{
	131,
	133,
	4,
	122,
	4,
	122,
	4,
	122,
	4,
	4,
	49,
	49,
	792,
	49,
	792,
	49,
	792,
	49,
	792,
	4,
	122,
	4,
	122,
	4,
	49,
	792,
	4,
	122,
	4,
	122,
	49,
	792,
	49,
	792,
	4,
	4,
	122,
	122,
	3,
	23,
	3,
	102,
	23,
	113,
	26,
	26,
	14,
	26,
	3,
	23,
	26,
};
extern const Il2CppCodeGenModule g_Facebook_Unity_SettingsCodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_SettingsCodeGenModule = 
{
	"Facebook.Unity.Settings.dll",
	51,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
