﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CameraFollow::Start()
extern void CameraFollow_Start_mA9B0BF6D65FDE2EC07572180BF67C67F711C1C8C ();
// 0x00000002 System.Void CameraFollow::Update()
extern void CameraFollow_Update_mE47A8BFDC5449AF1D397AD9D049A2CB76BD0BE9D ();
// 0x00000003 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m27AF37B0C19243374F9376EBB2C40A2F605DB16E ();
// 0x00000004 System.Void ObstacleDie::Start()
extern void ObstacleDie_Start_m81C298CCD5A358BC98AF7EA6B4287B4CDE4E7028 ();
// 0x00000005 System.Void ObstacleDie::Update()
extern void ObstacleDie_Update_m9FC0405D3F7C9DA15CD4169720E2434071CC7ADD ();
// 0x00000006 System.Void ObstacleDie::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ObstacleDie_OnCollisionEnter2D_mA0A6716A38990CE55BC3DDF02F52EEE4123ABD5A ();
// 0x00000007 System.Void ObstacleDie::.ctor()
extern void ObstacleDie__ctor_m99AC2387C7DBBA027C8D0FC9D3589D10FB3E4AA9 ();
// 0x00000008 System.Void SceneChanger::Start()
extern void SceneChanger_Start_mC7E940B8708C19EA2936ED7B2E22CB9892247790 ();
// 0x00000009 System.Void SceneChanger::Update()
extern void SceneChanger_Update_m675F8AB0A127874FB5AF92A1651A6421CEDC0F00 ();
// 0x0000000A System.Void SceneChanger::FadeToMenu(System.Int32)
extern void SceneChanger_FadeToMenu_m2583D34BC869966F8E062D60E1FD6471743B1011 ();
// 0x0000000B System.Void SceneChanger::FadeToLevel(System.Int32)
extern void SceneChanger_FadeToLevel_mEB5353B04355AAFC9130D873A598CEF98EF553DB ();
// 0x0000000C System.Void SceneChanger::OnFadeComplete()
extern void SceneChanger_OnFadeComplete_m9EAD236194AABC1A0A3615B762EC92D99738C6F8 ();
// 0x0000000D System.Void SceneChanger::.ctor()
extern void SceneChanger__ctor_mE3CE8CC419FFEC01F8C75F84ADC9B7DD7EF012FE ();
// 0x0000000E System.Void BackgroundScroll::Awake()
extern void BackgroundScroll_Awake_m242F8E3187E9B74286CBA8021E69CC6B990A7931 ();
// 0x0000000F System.Void BackgroundScroll::Start()
extern void BackgroundScroll_Start_m85B0449B7877013EE361904474460BB0FA9AAE5B ();
// 0x00000010 System.Void BackgroundScroll::Update()
extern void BackgroundScroll_Update_mD280AD534605267AD3F1A526ADF3F7B3AD8D00B2 ();
// 0x00000011 System.Void BackgroundScroll::.ctor()
extern void BackgroundScroll__ctor_mB2C680BED7D257055B6A2FCB04044CB4A01C25B5 ();
// 0x00000012 DataManager DataManager::get_Instance()
extern void DataManager_get_Instance_m33A82DF3D68C78989E730896969F6248CCA14709 ();
// 0x00000013 System.Void DataManager::Awake()
extern void DataManager_Awake_mDAA142952ABB5A7C496F5B305AAB2CC55CDCD5DD ();
// 0x00000014 System.Void DataManager::ParseData()
extern void DataManager_ParseData_m76D9B692CF32277214E55004B60086BA9D353ED1 ();
// 0x00000015 System.Void DataManager::SetAccount(System.String)
extern void DataManager_SetAccount_m108A405332AB9C11AFCA5C3781F615474350B586 ();
// 0x00000016 System.Void DataManager::SetLogin(System.String)
extern void DataManager_SetLogin_m6B18FF14CBCD04A990B744BD6B0EA652732761E1 ();
// 0x00000017 System.Void DataManager::Logout()
extern void DataManager_Logout_m939C6230BEB7F39A7D215AE92F754B779748F98F ();
// 0x00000018 System.Void DataManager::SingInSuccess()
extern void DataManager_SingInSuccess_mED9702E506CE45A0D1EE898565478A806C98F85C ();
// 0x00000019 System.Void DataManager::SingInFBSuccess()
extern void DataManager_SingInFBSuccess_mD87CA8642D598D4E4615494631D338FFCF92DDC3 ();
// 0x0000001A System.Void DataManager::GetHighscoresTable()
extern void DataManager_GetHighscoresTable_m57768EF8336A359851C7C221356B9D60C16B14BA ();
// 0x0000001B System.Collections.IEnumerator DataManager::GetHighscoresTableRequest(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void DataManager_GetHighscoresTableRequest_m333D6770055A7C331365C1194606A3B000503C29 ();
// 0x0000001C System.String DataManager::ReturnHighscore(System.String)
extern void DataManager_ReturnHighscore_mA4341E90F47161D5D3F9063ECC3FE85A438DEB01 ();
// 0x0000001D System.Void DataManager::UpdateHighscore()
extern void DataManager_UpdateHighscore_m8578A009F72085A31F96312DBD19A4D402C2B55C ();
// 0x0000001E System.Collections.IEnumerator DataManager::UpdateHighscoreRequest(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void DataManager_UpdateHighscoreRequest_mC7F81211248AA58F1EC427FDDE4677FCE2552DB8 ();
// 0x0000001F System.Void DataManager::SetHighscorePref()
extern void DataManager_SetHighscorePref_m4A0980F4E20F71509F02630233434A09A5FDF3F1 ();
// 0x00000020 System.String DataManager::FixJson(System.String)
extern void DataManager_FixJson_m9FEF3B856E01600C52ECED895FA8170E554F4FB2 ();
// 0x00000021 System.Void DataManager::SetItalianLanguage()
extern void DataManager_SetItalianLanguage_m2965955BFE85C8B532E627C74374D5B4785C2306 ();
// 0x00000022 System.Void DataManager::SetEnglishLanguage()
extern void DataManager_SetEnglishLanguage_mB1455A86DB3BD49CBEB042EA39FEA0061C9AC255 ();
// 0x00000023 System.Void DataManager::SetCheckLanguage()
extern void DataManager_SetCheckLanguage_m0BC01F1F512391C1F270EFAC18F50DC5A2623857 ();
// 0x00000024 System.Void DataManager::SetHighscoreLanguage(System.Int32)
extern void DataManager_SetHighscoreLanguage_mFBB3B2DC4EDE44430C9C7DEB4F939E3D375669AC ();
// 0x00000025 System.Void DataManager::.ctor()
extern void DataManager__ctor_mAFD6DE9EC9CC3131A8DD12328AE89D6817225D61 ();
// 0x00000026 System.Void DataManager::<GetHighscoresTable>b__24_0(UnityEngine.Networking.UnityWebRequest)
extern void DataManager_U3CGetHighscoresTableU3Eb__24_0_m828D280306CDDC26ADDB05AFB31B04A3EBF423E0 ();
// 0x00000027 System.Void Enemy::Start()
extern void Enemy_Start_m0681B66D4522F045EB7A33A21467994960D1E435 ();
// 0x00000028 System.Void Enemy::Update()
extern void Enemy_Update_mE957FE3BFB8CBB7C30E43D4A81C13E02B1C1FC32 ();
// 0x00000029 System.Void Enemy::.ctor()
extern void Enemy__ctor_mCD4E016A02FE662E339AA011EBA74D77B09556C5 ();
// 0x0000002A System.Void FacebookManager::Awake()
extern void FacebookManager_Awake_m75912ECA734B4E696C2E11A85876EBD55ADA5091 ();
// 0x0000002B System.Void FacebookManager::FacebookLogin()
extern void FacebookManager_FacebookLogin_m928D8D00A9C5F11D3D04724DC4E2117A0ADF0CD9 ();
// 0x0000002C System.Void FacebookManager::FacebookLogout()
extern void FacebookManager_FacebookLogout_mF4BA84F52753221D6254C041BA4D2B175F8C056A ();
// 0x0000002D System.Void FacebookManager::FacebookShare()
extern void FacebookManager_FacebookShare_m1287D5F074BBE039B41AB985BE59778A0D68C787 ();
// 0x0000002E System.Void FacebookManager::FacebookGameRequest()
extern void FacebookManager_FacebookGameRequest_mD2F6EE8A9B6C20FEF2F9E1451F610C860915D3E3 ();
// 0x0000002F System.Void FacebookManager::FacebookInvite()
extern void FacebookManager_FacebookInvite_m6C547A1E7C9C1918D551669ECCA2708737CE766C ();
// 0x00000030 System.Void FacebookManager::GetFacebookInfo(Facebook.Unity.IResult)
extern void FacebookManager_GetFacebookInfo_m8DF17B5175D0041C9F97EA1733E2470871C614CA ();
// 0x00000031 System.Void FacebookManager::CheckIDFacebookDB()
extern void FacebookManager_CheckIDFacebookDB_mCA0CE7726228D857217984C8C7F0DAE0E02D66BE ();
// 0x00000032 System.Collections.IEnumerator FacebookManager::CheckIDFacebookDBRequest(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void FacebookManager_CheckIDFacebookDBRequest_mF76484A5F3FDCB066FEE4B5612B85214468121DC ();
// 0x00000033 System.Void FacebookManager::RegisterIDFacebookDB()
extern void FacebookManager_RegisterIDFacebookDB_mE8A1E205C73BC9A9BA3A3E0D60C3591C245CFD97 ();
// 0x00000034 System.Collections.IEnumerator FacebookManager::RegisterIDFacebookDBRequest(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void FacebookManager_RegisterIDFacebookDBRequest_m081E150C2BB3F81E651E1E305CB4D86AA049455F ();
// 0x00000035 System.Void FacebookManager::.ctor()
extern void FacebookManager__ctor_m79647B42B0C92EDEFCB34635448A78DAEC91BC1A ();
// 0x00000036 System.Void FacebookManager::<FacebookLogin>b__7_0(Facebook.Unity.IGraphResult)
extern void FacebookManager_U3CFacebookLoginU3Eb__7_0_mBDD507406064D0C3A08D9F13EA5E1B68C1F5191C ();
// 0x00000037 System.Void FacebookManager::<CheckIDFacebookDB>b__13_0(UnityEngine.Networking.UnityWebRequest)
extern void FacebookManager_U3CCheckIDFacebookDBU3Eb__13_0_mDC9794E7AC01365034D0F3DD9F133F357F178AA0 ();
// 0x00000038 System.Void FadeMusic::Start()
extern void FadeMusic_Start_m9C3BBFD2D88CABE4EB637DD66FD1A58DBA74F768 ();
// 0x00000039 System.Void FadeMusic::Update()
extern void FadeMusic_Update_m4BD2D24C5FCB161F3B208FAE627C2E5F6C1CDFC2 ();
// 0x0000003A System.Void FadeMusic::SetFadeOut()
extern void FadeMusic_SetFadeOut_m19425117579C9B75BB7CA1F267E9795969C2A341 ();
// 0x0000003B System.Void FadeMusic::.ctor()
extern void FadeMusic__ctor_mF1990A616A0309789E67515B5316FA0D7E4928FC ();
// 0x0000003C System.Void GameManager::Start()
extern void GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E ();
// 0x0000003D System.Void GameManager::Update()
extern void GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A ();
// 0x0000003E System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 ();
// 0x0000003F System.Void Highscore::.ctor(System.String,System.String)
extern void Highscore__ctor_m1B67838BE8569772542982BE5931A4049FB63237 ();
// 0x00000040 System.Int32 Highscore::GetHashCode()
extern void Highscore_GetHashCode_m9C7ED4A2F0F706C1E037F1AB045B63858FDF6CD5 ();
// 0x00000041 System.Boolean Highscore::Equals(System.Object)
extern void Highscore_Equals_mDD10C008EF8403FC32FD8AEAF9C6ECB27D0E3817 ();
// 0x00000042 System.Int32 Highscore::CompareTo(Highscore)
extern void Highscore_CompareTo_m6DA56613E6D706D9DB74F41123799CF31FEAB49D ();
// 0x00000043 System.String Highscore::GetScore()
extern void Highscore_GetScore_mF3E7F2DB098219A507120C1945A2B5A8FD2AF4F9 ();
// 0x00000044 System.String Highscore::GetNome()
extern void Highscore_GetNome_m94F5BE273F22A40854D3D1A44581BD5090D06D24 ();
// 0x00000045 System.Void HighscoreTable::Awake()
extern void HighscoreTable_Awake_m24DFFC813D2CF74B93171B19EAF939105437CB62 ();
// 0x00000046 System.Void HighscoreTable::Start()
extern void HighscoreTable_Start_m88E728DECB1A50AE9569CBDC9EB41620E69EC2F4 ();
// 0x00000047 System.Void HighscoreTable::CreateHighscoreEntryTrasform(Highscore,UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void HighscoreTable_CreateHighscoreEntryTrasform_m7E0F2C97624E489435326DE6BFD91DC8A00835C4 ();
// 0x00000048 System.String HighscoreTable::FixJson(System.String)
extern void HighscoreTable_FixJson_mB5A567753BD80FB7410B6F6C0B66167C62C80AE0 ();
// 0x00000049 System.Void HighscoreTable::.ctor()
extern void HighscoreTable__ctor_m9C20B4596A77290216ADBBC7220C7369D33DB675 ();
// 0x0000004A System.Void HighscoreTableFB::Awake()
extern void HighscoreTableFB_Awake_m2F08008AC04AF4632FD4DC839D17DC938991C880 ();
// 0x0000004B System.Void HighscoreTableFB::CreateHighscoreEntryTrasform(Highscore,UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void HighscoreTableFB_CreateHighscoreEntryTrasform_m1C48254C98BC48180BD0A5724471A369FC08F4F3 ();
// 0x0000004C System.String HighscoreTableFB::FixJson(System.String)
extern void HighscoreTableFB_FixJson_mD10E62E18219AF382A8FC8D225B320F4C066FDC1 ();
// 0x0000004D System.Void HighscoreTableFB::.ctor()
extern void HighscoreTableFB__ctor_mC93DAA1CB8F38169BE8D331E8A6973483E90E68C ();
// 0x0000004E System.Void Login::Start()
extern void Login_Start_mC4391C73942BC3CE27127B51C3B30D32BEC47CF7 ();
// 0x0000004F System.Void Login::Update()
extern void Login_Update_m13C601B87C99B223CF5D1E382750566DF46308B5 ();
// 0x00000050 System.Void Login::CheckIsLogin()
extern void Login_CheckIsLogin_mE669B7C2FFF089ABC2250DCC200C130030BAD28A ();
// 0x00000051 System.Void Login::CreateAccount()
extern void Login_CreateAccount_mC89B986E1E50B2434B5B1050B36A0D043C42F419 ();
// 0x00000052 System.Void Login::SingIn()
extern void Login_SingIn_mA1B45C54139438337E0325363F85A85968379892 ();
// 0x00000053 System.Collections.IEnumerator Login::SingInRequest(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void Login_SingInRequest_m9B6C8AB9710CE5DEF7E1771C6D81B39AEBD4B3AA ();
// 0x00000054 System.Collections.IEnumerator Login::CreateAccountRequest(System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void Login_CreateAccountRequest_mE0AF242B750FA783C74BA1C626920214B97957C1 ();
// 0x00000055 System.Void Login::Logout()
extern void Login_Logout_m9BBFEF95B9DBBA8F623947632872797BD7191E45 ();
// 0x00000056 System.Boolean Login::IsBase64String(System.String)
extern void Login_IsBase64String_m26AD8CC5CEE36205EB4D95B27B9E554A57F2A7B7 ();
// 0x00000057 System.Void Login::EmailValidator(System.String)
extern void Login_EmailValidator_mC5778C8F4D7CBF1034C066EA995FE36529B61F4F ();
// 0x00000058 System.Void Login::ConfirmPasswordValidator(System.String)
extern void Login_ConfirmPasswordValidator_mFE88903D91A89264B60941233C64C008AEF466AC ();
// 0x00000059 System.Void Login::PasswordValidator(System.String)
extern void Login_PasswordValidator_m5134F03C2E70E49EAA31CA1149CDF815FF0B2EC9 ();
// 0x0000005A System.Boolean Login::IsValidEmail(System.String)
extern void Login_IsValidEmail_m0A6AF978681EA1652512146259560F1E5790DA69 ();
// 0x0000005B System.Boolean Login::IsValidPassword(System.String)
extern void Login_IsValidPassword_m3BDA31473510E1DF528EE6B0CEF5EB71029DC741 ();
// 0x0000005C System.Void Login::ErrorLogin(System.String)
extern void Login_ErrorLogin_mD2FE30F2F4562070AFB8625165A2F0570434F5EE ();
// 0x0000005D System.Void Login::CheckUser(System.String)
extern void Login_CheckUser_m167BF7C86C797E19285B2276BB10873C2CBF2E37 ();
// 0x0000005E System.Collections.IEnumerator Login::IsNewUser()
extern void Login_IsNewUser_m00EABE0452733790697F70E7882A21D1A8A3577B ();
// 0x0000005F System.Void Login::.ctor()
extern void Login__ctor_m077828C912B448F09A5148022AC7C9E8607225D1 ();
// 0x00000060 System.Void Login::.cctor()
extern void Login__cctor_m43CE63FECB345313D070AF3B09A51D182360C5B6 ();
// 0x00000061 System.Void Login::<SingIn>b__26_0(UnityEngine.Networking.UnityWebRequest)
extern void Login_U3CSingInU3Eb__26_0_m26B6DA22EDDCDD5A37CFF20AFE35A06CBF8194E4 ();
// 0x00000062 System.Void MainMenu::Start()
extern void MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821 ();
// 0x00000063 System.Void MainMenu::Update()
extern void MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866 ();
// 0x00000064 System.Void MainMenu::PlayGame()
extern void MainMenu_PlayGame_m4CD3D61E23D84AD1A018C84D561EAE39ED2D76F7 ();
// 0x00000065 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5 ();
// 0x00000066 System.Void Obstacle::Start()
extern void Obstacle_Start_mBE9A2C144E4C0BF4161D381473712395E98FFD75 ();
// 0x00000067 System.Void Obstacle::Update()
extern void Obstacle_Update_mF887C681ECA48EE9CFE80B2F13DF3CC0BA36FCA3 ();
// 0x00000068 System.Void Obstacle::SetSpeed(System.Single)
extern void Obstacle_SetSpeed_m6ECAE23C3C57B36A7DB966EA1E80622566F53C9D ();
// 0x00000069 System.Single[] Obstacle::GetObstaclePositionX(System.String)
extern void Obstacle_GetObstaclePositionX_m5BD0AA08DA64CB232B75B1CE19D332F857766655 ();
// 0x0000006A System.Void Obstacle::.ctor()
extern void Obstacle__ctor_mD2207B1C3A1DD1E461DB628242435A19A48E6EE8 ();
// 0x0000006B System.Void OptionsMenu::Start()
extern void OptionsMenu_Start_mA7964719282C1ED0598CF5135997CF56D087E824 ();
// 0x0000006C System.Void OptionsMenu::Update()
extern void OptionsMenu_Update_m0DA07A08B0758009BB083DEE7737F41B31FCB3FA ();
// 0x0000006D System.Void OptionsMenu::SetOnOffMusic()
extern void OptionsMenu_SetOnOffMusic_mF112C1BCBB1FDF1D73BA37389026E247C1694C4B ();
// 0x0000006E System.Void OptionsMenu::SetOnOffEffects()
extern void OptionsMenu_SetOnOffEffects_m77AB6E8E480D7B152311664CAF4FF221465C170F ();
// 0x0000006F System.Void OptionsMenu::SetOffMusic()
extern void OptionsMenu_SetOffMusic_m1C6F1804AEA896677CE56AE1333DF48EB2FC068B ();
// 0x00000070 System.Void OptionsMenu::SetOnMusic()
extern void OptionsMenu_SetOnMusic_m49E27C6D5D1877F351201BC5D8BA1DB2BEA78FF2 ();
// 0x00000071 System.Void OptionsMenu::SetOffEffects()
extern void OptionsMenu_SetOffEffects_m68F2DB74F7493F39CE77B059D4BA19F63236E597 ();
// 0x00000072 System.Void OptionsMenu::SetOnEffects()
extern void OptionsMenu_SetOnEffects_m1B34EB32FD61FCF563405763B81CD15A33EB73D8 ();
// 0x00000073 System.Void OptionsMenu::.ctor()
extern void OptionsMenu__ctor_mF71A00F27EA929ADE40891F6956F1759FACB514B ();
// 0x00000074 System.Byte PasswordHasher::get_Version()
extern void PasswordHasher_get_Version_m4FFA79970179A01AA94D6D5B057FE25F82AB1BF0 ();
// 0x00000075 System.Int32 PasswordHasher::get_Pbkdf2IterCount()
extern void PasswordHasher_get_Pbkdf2IterCount_m1D8695FBBC730F4DFEB4915306EB9CBE9DE60E9F ();
// 0x00000076 System.Int32 PasswordHasher::get_Pbkdf2SubkeyLength()
extern void PasswordHasher_get_Pbkdf2SubkeyLength_m8D48428CC34DFD0452733006F35868DE4A883265 ();
// 0x00000077 System.Int32 PasswordHasher::get_SaltSize()
extern void PasswordHasher_get_SaltSize_m0241857DCFE44734EC7C2A4223779E1B5D9F4FBA ();
// 0x00000078 System.Security.Cryptography.HashAlgorithmName PasswordHasher::get_HashAlgorithmName()
extern void PasswordHasher_get_HashAlgorithmName_mAB9EC3F0D657F43E0AB42FD3DAE837E7C7CBD695 ();
// 0x00000079 System.String PasswordHasher::HashPassword(System.String)
extern void PasswordHasher_HashPassword_mC851C056E7EE515229739FB382313D7840B7363F ();
// 0x0000007A PasswordVerificationResult PasswordHasher::VerifyHashedPassword(System.String,System.String)
extern void PasswordHasher_VerifyHashedPassword_m8D7271106B5DC92BC1AA1AA26A1607829AE2AE47 ();
// 0x0000007B System.Boolean PasswordHasher::FixedTimeEquals(System.Byte[],System.Byte[])
extern void PasswordHasher_FixedTimeEquals_m9A226F1A09637C8EFA5BAFB170BC6774C4FA89D7 ();
// 0x0000007C System.Void PasswordHasher::.ctor()
extern void PasswordHasher__ctor_m490C06B6510BFD9C1412A01B49C4B44278A629F7 ();
// 0x0000007D System.Void PathSpawnCollider::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PathSpawnCollider_OnTriggerEnter2D_mAC9CE0F28C20E53972479B9755E22B34A7676050 ();
// 0x0000007E System.Void PathSpawnCollider::.ctor()
extern void PathSpawnCollider__ctor_m8FAFC0A32CD227D6069562239A25DFF0C83077BA ();
// 0x0000007F System.Void PauseMenu::Start()
extern void PauseMenu_Start_m87C554A1CF71A5E4A233FB35A6E0DB276388DF3B ();
// 0x00000080 System.Void PauseMenu::Update()
extern void PauseMenu_Update_m59D9098B173533E082A41253B251B1E5F5AB9EA9 ();
// 0x00000081 System.Void PauseMenu::PauseGame()
extern void PauseMenu_PauseGame_m565EBD7A8427786B696FA446F226E42A8F40C320 ();
// 0x00000082 System.Void PauseMenu::ResumeGame()
extern void PauseMenu_ResumeGame_mCCB9232463ECD336BA3A0E202023DB3230830C15 ();
// 0x00000083 System.Void PauseMenu::ReloadGame()
extern void PauseMenu_ReloadGame_m93BAF00D9627450354F90F5A9A1B4825644F4EFD ();
// 0x00000084 System.Void PauseMenu::ExitGame()
extern void PauseMenu_ExitGame_m4440FC161D6CBD0EB1D2A6A8DA1BECC6F24FB12C ();
// 0x00000085 System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_m1DE7CAFF465FE5BEB4E0ABDF165AFA208631EF28 ();
// 0x00000086 System.Void Player::Start()
extern void Player_Start_mE9ACCAA7FEBF4020693FFB73C0839CF3AAD0B0C4 ();
// 0x00000087 System.Void Player::Update()
extern void Player_Update_m6F977BAE3756AB7073D64042B766B442E4EC6FD2 ();
// 0x00000088 System.Void Player::Rotation()
extern void Player_Rotation_m8C1AC21A9431A2728BFCDEBEDCAE969B49425790 ();
// 0x00000089 System.Void Player::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Player_OnCollisionEnter2D_m250C0143A5AE43821134712D9306FA483745A39C ();
// 0x0000008A System.Void Player::OnCollisionExit2D(UnityEngine.Collision2D)
extern void Player_OnCollisionExit2D_m16F57E27652E2C4ABE4F940F05AEBC4703B240B2 ();
// 0x0000008B System.Void Player::OnDrawGizmos()
extern void Player_OnDrawGizmos_mF959B653B44C83FA34CF4C08CC1DDFDF1B6A6420 ();
// 0x0000008C System.Void Player::SetSkinAndColor(System.String,System.String)
extern void Player_SetSkinAndColor_m7BA549331ABEBC6781A66F8E343D0627DDE9A56C ();
// 0x0000008D System.Void Player::Die()
extern void Player_Die_m2273E1901C7A6A2FCD9AA58EA5D0243D836679C9 ();
// 0x0000008E System.Int32 Player::GetScore()
extern void Player_GetScore_mD7FCBAACFE52DE75DBABBA6C495EB6A4079651CA ();
// 0x0000008F System.Void Player::.ctor()
extern void Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2 ();
// 0x00000090 System.Void Score::.ctor(System.Int32)
extern void Score__ctor_m253573F24BB13CC104B847383A3B27F0A7EADD5D ();
// 0x00000091 System.Void Score::Start()
extern void Score_Start_m1821AC2C6C0505E9EEFA5DD52733BE5C3037AD9E ();
// 0x00000092 System.Void Score::Update()
extern void Score_Update_m034295E32383253E391ECFA2599FD3720345D1DB ();
// 0x00000093 System.Void Score::SetSpeed(System.Single)
extern void Score_SetSpeed_m9CF9E8EAF05F506CFB14A53E3700642539F0BA8D ();
// 0x00000094 System.Void Score::IncreaseScore(System.Int32)
extern void Score_IncreaseScore_m6C65DC3D88F554BBD315DA24FD40F56B475026A4 ();
// 0x00000095 System.Int32 Score::GetScore()
extern void Score_GetScore_m64B86C577600E1B75D518B9A23289639D35FB558 ();
// 0x00000096 System.Void SoundManager::Start()
extern void SoundManager_Start_mB9D238182CC4B1023DE4C4D331E88EFB7E82F24F ();
// 0x00000097 System.Void SoundManager::Update()
extern void SoundManager_Update_mA43265016234A06D51D1D96DAB646B758F6E4AB6 ();
// 0x00000098 System.Void SoundManager::StopBackgroundSound()
extern void SoundManager_StopBackgroundSound_mC634ECA7550B617D25065988E9D39CFE19727145 ();
// 0x00000099 System.Void SoundManager::PlayBackgroundSound()
extern void SoundManager_PlayBackgroundSound_m7D14613B97095BFE8DB6799F6EE0D64C0AEA582E ();
// 0x0000009A System.Void SoundManager::PlaySound(System.String)
extern void SoundManager_PlaySound_mF0B7231A196F591C25BC19455496627A3FD52FB4 ();
// 0x0000009B System.Void SoundManager::PlayMusic()
extern void SoundManager_PlayMusic_mC13A54C83D7F3F01C649A32610F84E9BD3257409 ();
// 0x0000009C System.Void SoundManager::PlaySoundButton()
extern void SoundManager_PlaySoundButton_mBBB59D792FE992C91172C3D6C3EE166FC1E9D5E4 ();
// 0x0000009D System.Void SoundManager::PlaySoundCloseButton()
extern void SoundManager_PlaySoundCloseButton_m6436B89331765EC61335DF44A1D6DCC96BC66E4E ();
// 0x0000009E System.Void SoundManager::.ctor()
extern void SoundManager__ctor_mFF8C696A5B666ABC1E2344581FE7FB06E038D422 ();
// 0x0000009F System.Void SpawnEnemy::Start()
extern void SpawnEnemy_Start_m11B73FA87652E5A756CB4B0ECA4AB655F950D606 ();
// 0x000000A0 System.Void SpawnEnemy::spawnEnemy()
extern void SpawnEnemy_spawnEnemy_mA882C077E3786D27000E3D1BC181DA1E69B2DC98 ();
// 0x000000A1 System.Collections.IEnumerator SpawnEnemy::asteroidWave()
extern void SpawnEnemy_asteroidWave_mD7EC39852FE042C7062CF81675B321FECB828C2B ();
// 0x000000A2 System.Void SpawnEnemy::.ctor()
extern void SpawnEnemy__ctor_m987D9D871D1B8410EE06C366C989685928DEFCA1 ();
// 0x000000A3 System.Void SpawnObstacles::Start()
extern void SpawnObstacles_Start_mE41B52821E9F023BD447676789E9E9B2953B7041 ();
// 0x000000A4 System.Void SpawnObstacles::spawnEnemy()
extern void SpawnObstacles_spawnEnemy_m1643545BB6A23A5A2B1851D26121E3D0112006CB ();
// 0x000000A5 System.Void SpawnObstacles::IncreaseSpeed(System.Single)
extern void SpawnObstacles_IncreaseSpeed_m4B0D03AB34D213239401DEC85ED57562C95DCA4B ();
// 0x000000A6 System.Collections.IEnumerator SpawnObstacles::asteroidWave()
extern void SpawnObstacles_asteroidWave_m50604FCB4A83B7920DD0332F6CD0449E8638F4D7 ();
// 0x000000A7 System.Void SpawnObstacles::.ctor()
extern void SpawnObstacles__ctor_m99A4EA5C4D6117E115EAFCAF8546106AAB2292A3 ();
// 0x000000A8 System.Void SpawnScore::Start()
extern void SpawnScore_Start_m98AD1F70BC7B50C836257D5D9137348C326C3EE2 ();
// 0x000000A9 System.Void SpawnScore::spawnEnemy()
extern void SpawnScore_spawnEnemy_mBB4EE5B565085A53CDE3CE9C72D8CB6B96AE821E ();
// 0x000000AA System.Collections.IEnumerator SpawnScore::asteroidWave()
extern void SpawnScore_asteroidWave_mD9E7A30182E309331B0BC3B6D9AF4E6BFE42567F ();
// 0x000000AB System.Void SpawnScore::.ctor()
extern void SpawnScore__ctor_m3A72DAB7765B54BCF787AC79E26609DC73695236 ();
// 0x000000AC System.Void SwitchGravity::Start()
extern void SwitchGravity_Start_m30C0C7608F098346DA5876FB0C45B0B9F8771878 ();
// 0x000000AD System.Void SwitchGravity::Update()
extern void SwitchGravity_Update_m2F813581F7719C0A29BC88BB917A96C4A5B8EA58 ();
// 0x000000AE System.Void SwitchGravity::Rotation()
extern void SwitchGravity_Rotation_m51D4531CA73AE49D2703ECC750DB82E0A52301AA ();
// 0x000000AF System.Void SwitchGravity::.ctor()
extern void SwitchGravity__ctor_m500E5659769D1CB987BFB1257D4BB7ED3C48BBF2 ();
// 0x000000B0 TextLocalizer TextLocalizer::get_Instance()
extern void TextLocalizer_get_Instance_mE3A9EFD78F36B3E4469780C096AC6FCE600B1EDD ();
// 0x000000B1 System.String TextLocalizer::ResolveStringValue(System.String)
extern void TextLocalizer_ResolveStringValue_m6359A61ED6CAD063C69820810FD7905C70EF83B3 ();
// 0x000000B2 System.Void TextLocalizer::Start()
extern void TextLocalizer_Start_m4081FD0CAC5F1798EE7001284C0C30B5F2F3C63F ();
// 0x000000B3 System.Void TextLocalizer::Update()
extern void TextLocalizer_Update_mCA869E4B7D83A30CC5AA619ADF32EDCCD9FC1072 ();
// 0x000000B4 System.Void TextLocalizer::OnValidate()
extern void TextLocalizer_OnValidate_m95B9324469C2F121FADE20A2CDD2289340107375 ();
// 0x000000B5 System.String TextLocalizer::Traslate(System.String)
extern void TextLocalizer_Traslate_m38A38060E164C8E0A0C9F4B1F60D86D19A15B66B ();
// 0x000000B6 System.Void TextLocalizer::.ctor()
extern void TextLocalizer__ctor_m1693C2554402ED8A96C7436A5926A353872A9FD7 ();
// 0x000000B7 System.Void TextLocalizer::.cctor()
extern void TextLocalizer__cctor_m5C91EE74DDB94DB2F6B9E9FB991855B3C8C1D437 ();
// 0x000000B8 System.Void SkinMenu::Start()
extern void SkinMenu_Start_m57D2D3FBB1F0850EA4DE6759A7BF46C72DBF6A8C ();
// 0x000000B9 System.Void SkinMenu::SetSkin1()
extern void SkinMenu_SetSkin1_m252F3AC4ED12D1F80042E22834A3DEF4A23102A8 ();
// 0x000000BA System.Void SkinMenu::SetSkin2()
extern void SkinMenu_SetSkin2_mB94FD0BE830B4CBF01FE389FEE5E8D3EF7DA913B ();
// 0x000000BB System.Void SkinMenu::SetSkin3()
extern void SkinMenu_SetSkin3_m8CFE088FD8B2DD479896C42183F3D71620B1350C ();
// 0x000000BC System.Void SkinMenu::SetSkin4()
extern void SkinMenu_SetSkin4_m648B62AFF0A9A24EE470D658C1BA1A691717D2A1 ();
// 0x000000BD System.Void SkinMenu::SetSkin5()
extern void SkinMenu_SetSkin5_m4BDFD3F70B85A25B1933791D4C3CA4730C5D8830 ();
// 0x000000BE System.Void SkinMenu::SetSkin6()
extern void SkinMenu_SetSkin6_mFB366BD32B3535DF194DC9D5C66EB50645C75769 ();
// 0x000000BF System.Void SkinMenu::SetRedColor()
extern void SkinMenu_SetRedColor_m1270C0D8298B92D8B722B36FD45707CBE4D4A990 ();
// 0x000000C0 System.Void SkinMenu::SetGreenColor()
extern void SkinMenu_SetGreenColor_mF089ACA18C435F38A0ABA5E83E0DA7FDBDF86541 ();
// 0x000000C1 System.Void SkinMenu::SetYellowColor()
extern void SkinMenu_SetYellowColor_mB4A13F1D2B308A0CF0B4F9BBBC1CF57713DE066F ();
// 0x000000C2 System.Void SkinMenu::SetLightBlueColor()
extern void SkinMenu_SetLightBlueColor_mDA851627E64B24F775B5A1783BADA07554AD5487 ();
// 0x000000C3 System.Void SkinMenu::SetOrangeColor()
extern void SkinMenu_SetOrangeColor_m2DC7A8321D0849CCC03F232A98F196F01562AA32 ();
// 0x000000C4 System.Void SkinMenu::SetPurpleColor()
extern void SkinMenu_SetPurpleColor_mD2F85B9389E634772767DBACD39BD198E8D9228E ();
// 0x000000C5 System.Void SkinMenu::SetColorSkins(UnityEngine.Color32,System.String)
extern void SkinMenu_SetColorSkins_mB7C8CD2B2BF41A1350CB7B1FD0BBD95799B5DEDB ();
// 0x000000C6 System.Void SkinMenu::SetSkinColors(UnityEngine.Sprite)
extern void SkinMenu_SetSkinColors_m9A77B5D7ADFB6F590F0C18504967F2F5DEF1AE55 ();
// 0x000000C7 System.Void SkinMenu::SetSkinsAndColors(System.String,System.String)
extern void SkinMenu_SetSkinsAndColors_mFE7F0AC4913F48BAAC571DD3B9659A0CEBE71B75 ();
// 0x000000C8 System.Void SkinMenu::.ctor()
extern void SkinMenu__ctor_mBA8D08A6732FF2A03796AA51107EED487989D6DF ();
// 0x000000C9 System.Void SkinMenu::.cctor()
extern void SkinMenu__cctor_mE9EC03CF16CD752C76F4AC7D3AD023946F222431 ();
// 0x000000CA System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern void ConsoleBase_get_ButtonHeight_mD6796BF7D04D036E17954E52E37F156C15207CA5 ();
// 0x000000CB System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern void ConsoleBase_get_MainWindowWidth_m19DCEEA57F769E12993C2D0B5D4C7D07ABEBF08C ();
// 0x000000CC System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern void ConsoleBase_get_MainWindowFullWidth_m7A4603551C41BC2CD25726C39C42F023A222D8BB ();
// 0x000000CD System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern void ConsoleBase_get_MarginFix_m07098733F527787846417EEDF7648B78AE368B79 ();
// 0x000000CE System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern void ConsoleBase_get_MenuStack_m2A084509DB58962B6F06DDBC7A01A3A31CDE99BC ();
// 0x000000CF System.Void Facebook.Unity.Example.ConsoleBase::set_MenuStack(System.Collections.Generic.Stack`1<System.String>)
extern void ConsoleBase_set_MenuStack_mB137307FEB4D04CCFA479094D84F3C014E98939F ();
// 0x000000D0 System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern void ConsoleBase_get_Status_m0AAC8BBEDD91380EF0F5E61893EC446A9E4BE3B5 ();
// 0x000000D1 System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern void ConsoleBase_set_Status_mC77922E8270F74F814F65EF8A90E6D3E3DDC4CA3 ();
// 0x000000D2 UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::get_LastResponseTexture()
extern void ConsoleBase_get_LastResponseTexture_mF77BC51EF52CB32EB97E09B17D011CBD77E386CA ();
// 0x000000D3 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern void ConsoleBase_set_LastResponseTexture_m673429A2AA388C5A3AFBBF32CF0409C59CC6F9C6 ();
// 0x000000D4 System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern void ConsoleBase_get_LastResponse_m85EA4F944CF1763203F673D3AB12B18471114A00 ();
// 0x000000D5 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern void ConsoleBase_set_LastResponse_mDBF8B57FA98474D359C2E82FC59DBA66FF4760B5 ();
// 0x000000D6 UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern void ConsoleBase_get_ScrollPosition_mBE23454596E97F671992C6E3A69D10E44FCCA7AA ();
// 0x000000D7 System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern void ConsoleBase_set_ScrollPosition_m4EFB0377EF967C8B1CFD6B49C7D0E8107D8924FC ();
// 0x000000D8 System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern void ConsoleBase_get_ScaleFactor_m6DBCC64812BD73777F8CDC290DED19EF154AD67C ();
// 0x000000D9 System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern void ConsoleBase_get_FontSize_m949A90F9EE85E2BF3C862CB57EDA7E7253CBA42B ();
// 0x000000DA UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern void ConsoleBase_get_TextStyle_m2C5B95DED6749DEF5E0BC83A6FCAA236D9CC8619 ();
// 0x000000DB UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern void ConsoleBase_get_ButtonStyle_m3F36462B5EA45C5088A30A523968C1EC914172BB ();
// 0x000000DC UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern void ConsoleBase_get_TextInputStyle_m7715C64446C4503F151B7134F62FEF38372DBC18 ();
// 0x000000DD UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern void ConsoleBase_get_LabelStyle_mB7E43BEED5043E2F9CBC7911366F1B36744AE9D9 ();
// 0x000000DE System.Void Facebook.Unity.Example.ConsoleBase::Awake()
extern void ConsoleBase_Awake_m1FD6E034452A59B8A021A7072B916A2D23F3B5D5 ();
// 0x000000DF System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern void ConsoleBase_Button_mBCE04002B2C04BB153A76FDEBEEAAB23D0BAECF9 ();
// 0x000000E0 System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern void ConsoleBase_LabelAndTextField_mFD378CC735F9B8F45E78F2AB1370F0DA78A785E3 ();
// 0x000000E1 System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern void ConsoleBase_IsHorizontalLayout_m0AC4D0C36E62F3484015A87A5AFEC750630C0076 ();
// 0x000000E2 System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern void ConsoleBase_SwitchMenu_m5CACD54AF32E4AFE5750D255E348265775FA45B4 ();
// 0x000000E3 System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern void ConsoleBase_GoBack_mA08B9EADF1690ABC5DFF57DF064DEA1B790BA094 ();
// 0x000000E4 System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern void ConsoleBase__ctor_m470E3137F3CA730B7B181BE783D22EFE2B0E1B4B ();
// 0x000000E5 System.Void Facebook.Unity.Example.ConsoleBase::.cctor()
extern void ConsoleBase__cctor_mE0C3DC7162CE1B68B3B47D64EE1BC2E135AE1C6E ();
// 0x000000E6 System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern void LogView_AddLog_mE6A026BAA7B6368F41B9FB13BD175C18F0D32373 ();
// 0x000000E7 System.Void Facebook.Unity.Example.LogView::OnGUI()
extern void LogView_OnGUI_mF0A849E64B666ADD171E00CC73A0F1CDDAE04364 ();
// 0x000000E8 System.Void Facebook.Unity.Example.LogView::.ctor()
extern void LogView__ctor_m3E2783B223F06F9CEB65DA167AE95EBD0BCC582C ();
// 0x000000E9 System.Void Facebook.Unity.Example.LogView::.cctor()
extern void LogView__cctor_m79D024FAE554C6D45A8CAB6008F7182BB69458C4 ();
// 0x000000EA System.Void Facebook.Unity.Example.MenuBase::GetGui()
// 0x000000EB System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector()
extern void MenuBase_ShowDialogModeSelector_m324800BD86013CAE28A04574BD85B485159AFAA0 ();
// 0x000000EC System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton()
extern void MenuBase_ShowBackButton_m8DF4DB9BB3902BA3BD9760C8F469770146D8E577 ();
// 0x000000ED System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern void MenuBase_HandleResult_m4A9E778AB30F7D0CBA4B6315DDD3DC3E581E820A ();
// 0x000000EE System.Void Facebook.Unity.Example.MenuBase::OnGUI()
extern void MenuBase_OnGUI_m2CA018B434F79284101DBD1C48F94EE802B6BE6C ();
// 0x000000EF System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern void MenuBase_AddStatus_m5B47C2A547733E41DE49A04A215D3C4957B47984 ();
// 0x000000F0 System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern void MenuBase_AddBackButton_mD0420505F101FD664A23AE8E6D192450EF09E87B ();
// 0x000000F1 System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern void MenuBase_AddLogButton_mF9453B9DBC34C3358E13B3E2212B5E9B9123C899 ();
// 0x000000F2 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern void MenuBase_AddDialogModeButtons_m992687F9A5F3739164DA48B459F3ADC57C0B8C3F ();
// 0x000000F3 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern void MenuBase_AddDialogModeButton_m524D1F80FBF7CAEC453DA0D7084FFA609C0BD69B ();
// 0x000000F4 System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern void MenuBase__ctor_m7632B394B61767B54CCD6665E656C9DD94D461FD ();
// 0x000000F5 System.Void Facebook.Unity.Example.AccessTokenMenu::GetGui()
extern void AccessTokenMenu_GetGui_mF50B240450BB8950208F5CCA92D9DB19A2C5788D ();
// 0x000000F6 System.Void Facebook.Unity.Example.AccessTokenMenu::.ctor()
extern void AccessTokenMenu__ctor_m816CBC0B7DC83617BB3964B37D43A5F4FEC1F606 ();
// 0x000000F7 System.Void Facebook.Unity.Example.AppEvents::GetGui()
extern void AppEvents_GetGui_m2CA0B8AA9D5389CD2135C7AB307ECEB9697B271C ();
// 0x000000F8 System.Void Facebook.Unity.Example.AppEvents::.ctor()
extern void AppEvents__ctor_m4B39A59B111BD88198FC653B308913F92AAB4A29 ();
// 0x000000F9 System.Void Facebook.Unity.Example.AppLinks::GetGui()
extern void AppLinks_GetGui_m6A7B19D799397D99707A12A8DC4D24A9AF4652E4 ();
// 0x000000FA System.Void Facebook.Unity.Example.AppLinks::.ctor()
extern void AppLinks__ctor_m11439422C75055B559AF9B69DE071CB8270026DA ();
// 0x000000FB System.Void Facebook.Unity.Example.AppRequests::GetGui()
extern void AppRequests_GetGui_mCEDDF7E12A29E6C83B26703619222BA7E09B5657 ();
// 0x000000FC System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern void AppRequests_GetSelectedOGActionType_m61C17A1CD6303AC2757FB7256B02AB39FE715C66 ();
// 0x000000FD System.Void Facebook.Unity.Example.AppRequests::.ctor()
extern void AppRequests__ctor_mE6DBC84D2D6DE967B0B232820F7BE9DC8DAC5A38 ();
// 0x000000FE System.Boolean Facebook.Unity.Example.DialogShare::ShowDialogModeSelector()
extern void DialogShare_ShowDialogModeSelector_mEED5D21873BC565D2B7405777FF0254479CA3CE1 ();
// 0x000000FF System.Void Facebook.Unity.Example.DialogShare::GetGui()
extern void DialogShare_GetGui_mCBDD1A37ECA65AA48A7BFDBCB2210D4B8FF99927 ();
// 0x00000100 System.Void Facebook.Unity.Example.DialogShare::.ctor()
extern void DialogShare__ctor_m0CD472A363CF96DD661816530EC967B7E223BB9F ();
// 0x00000101 System.Void Facebook.Unity.Example.GraphRequest::GetGui()
extern void GraphRequest_GetGui_m1D9A9F8D11F9D8E5D06FA64507797DF0559CE2BF ();
// 0x00000102 System.Void Facebook.Unity.Example.GraphRequest::ProfilePhotoCallback(Facebook.Unity.IGraphResult)
extern void GraphRequest_ProfilePhotoCallback_mA003EAE8C5F935F2A745CEBB05214BF038AE4AFC ();
// 0x00000103 System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern void GraphRequest_TakeScreenshot_m61931CD8C35171219F42D9916DB97253D9AEE967 ();
// 0x00000104 System.Void Facebook.Unity.Example.GraphRequest::.ctor()
extern void GraphRequest__ctor_m76FF017B5E3D19347A1FA7BF21B656E6C92E9814 ();
// 0x00000105 System.Boolean Facebook.Unity.Example.MainMenu::ShowBackButton()
extern void MainMenu_ShowBackButton_mDB1AE0B4004A77060F718E5B8F71F893E8C0396B ();
// 0x00000106 System.Void Facebook.Unity.Example.MainMenu::GetGui()
extern void MainMenu_GetGui_mD25F94536C838B897A5898B43DA4D603B2338782 ();
// 0x00000107 System.Void Facebook.Unity.Example.MainMenu::CallFBLogin()
extern void MainMenu_CallFBLogin_mBBCC97AE79ECF3897AD2F1FB34DB46A35C5D9A33 ();
// 0x00000108 System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern void MainMenu_CallFBLoginForPublish_m2EF32E95A1B55419F8F82316E3B028CAB0829823 ();
// 0x00000109 System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern void MainMenu_CallFBLogout_m4A99B5C87829AEC6C7B256463DBB44244843E27A ();
// 0x0000010A System.Void Facebook.Unity.Example.MainMenu::OnInitComplete()
extern void MainMenu_OnInitComplete_mF83BD38A0C567092F4FD6E8DA20BA9A9C4CE7353 ();
// 0x0000010B System.Void Facebook.Unity.Example.MainMenu::OnHideUnity(System.Boolean)
extern void MainMenu_OnHideUnity_mE443DEB16426DCBF2D4A80FC3DB6E0E096445414 ();
// 0x0000010C System.Void Facebook.Unity.Example.MainMenu::.ctor()
extern void MainMenu__ctor_m18A96BABBB8ABA3A17C3ED1C71B15E37613EDCEF ();
// 0x0000010D System.Void Facebook.Unity.Example.Pay::GetGui()
extern void Pay_GetGui_mCB1F735FEB7A64E3CA0C2851FA6D0E000D121B76 ();
// 0x0000010E System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern void Pay_CallFBPay_m9E0BD295ED823B136CD451AF6E8133D567C51C59 ();
// 0x0000010F System.Void Facebook.Unity.Example.Pay::.ctor()
extern void Pay__ctor_m80DDC8B433865B2653DBD16E15D8CE233FE92D92 ();
// 0x00000110 System.Void DataManager_HighscoreList::.ctor()
extern void HighscoreList__ctor_m42D78625761945DE116F45A3C0E02D1CCA0200AB ();
// 0x00000111 System.Void DataManager_<GetHighscoresTableRequest>d__25::.ctor(System.Int32)
extern void U3CGetHighscoresTableRequestU3Ed__25__ctor_m56FFAEEA76CB41E4D3EA2AA6E93F26F091628732 ();
// 0x00000112 System.Void DataManager_<GetHighscoresTableRequest>d__25::System.IDisposable.Dispose()
extern void U3CGetHighscoresTableRequestU3Ed__25_System_IDisposable_Dispose_mD90DCC8F31661EEC086B8DA90A267F787ECC5321 ();
// 0x00000113 System.Boolean DataManager_<GetHighscoresTableRequest>d__25::MoveNext()
extern void U3CGetHighscoresTableRequestU3Ed__25_MoveNext_m9B18DBFDD0C99182EFCE9A20640B3AD19E4377C2 ();
// 0x00000114 System.Object DataManager_<GetHighscoresTableRequest>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetHighscoresTableRequestU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4898EE034FC85D981A189B8DD13239A0F5E651A2 ();
// 0x00000115 System.Void DataManager_<GetHighscoresTableRequest>d__25::System.Collections.IEnumerator.Reset()
extern void U3CGetHighscoresTableRequestU3Ed__25_System_Collections_IEnumerator_Reset_mCA655FC15A8784B2D77859DE567B9B852F5E58E5 ();
// 0x00000116 System.Object DataManager_<GetHighscoresTableRequest>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CGetHighscoresTableRequestU3Ed__25_System_Collections_IEnumerator_get_Current_m316AE3949DDD53BCAB36371047EE2B100002155D ();
// 0x00000117 System.Void DataManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m4705F54D6DE891B689C6394A5CF3B1995D3D6D1A ();
// 0x00000118 System.Void DataManager_<>c::.ctor()
extern void U3CU3Ec__ctor_m1485061173EE65243F665B2566C1DE55D2294607 ();
// 0x00000119 System.Void DataManager_<>c::<UpdateHighscore>b__27_0(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec_U3CUpdateHighscoreU3Eb__27_0_m8B85E52881148BE725810BB93D7F633ED93C63EB ();
// 0x0000011A System.Void DataManager_<UpdateHighscoreRequest>d__28::.ctor(System.Int32)
extern void U3CUpdateHighscoreRequestU3Ed__28__ctor_m6D2AB1CDA9F9169B60BE5BDA6AF9672A37C56D99 ();
// 0x0000011B System.Void DataManager_<UpdateHighscoreRequest>d__28::System.IDisposable.Dispose()
extern void U3CUpdateHighscoreRequestU3Ed__28_System_IDisposable_Dispose_mB3C08EE55079BB3BFACE52ED615CDAF210422E01 ();
// 0x0000011C System.Boolean DataManager_<UpdateHighscoreRequest>d__28::MoveNext()
extern void U3CUpdateHighscoreRequestU3Ed__28_MoveNext_m8B17EA6B2305F5708A74CD23ABFA4ECB396C4587 ();
// 0x0000011D System.Object DataManager_<UpdateHighscoreRequest>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateHighscoreRequestU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC114254582B83839402472D405F0BA67696B3278 ();
// 0x0000011E System.Void DataManager_<UpdateHighscoreRequest>d__28::System.Collections.IEnumerator.Reset()
extern void U3CUpdateHighscoreRequestU3Ed__28_System_Collections_IEnumerator_Reset_m509042A25D9DF88386755FC62C71060BC9FE43E4 ();
// 0x0000011F System.Object DataManager_<UpdateHighscoreRequest>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateHighscoreRequestU3Ed__28_System_Collections_IEnumerator_get_Current_mC6A4535409E871D60D65CB7C3C0D71FC45B28076 ();
// 0x00000120 System.Void FacebookManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m436D7A8845E2B6F8606030B669684C7AB1BE29B7 ();
// 0x00000121 System.Void FacebookManager_<>c::.ctor()
extern void U3CU3Ec__ctor_m86C7076F43C7A6547EAB729AE4F7A6B14A64B556 ();
// 0x00000122 System.Void FacebookManager_<>c::<Awake>b__6_0()
extern void U3CU3Ec_U3CAwakeU3Eb__6_0_m70FDD78F5016F3B3F2D622E38FC1BDF76CD48777 ();
// 0x00000123 System.Void FacebookManager_<>c::<Awake>b__6_1(System.Boolean)
extern void U3CU3Ec_U3CAwakeU3Eb__6_1_m9BDF9D3ACD658E5F5F68EF623907FE5AB920D62D ();
// 0x00000124 System.Void FacebookManager_<CheckIDFacebookDBRequest>d__14::.ctor(System.Int32)
extern void U3CCheckIDFacebookDBRequestU3Ed__14__ctor_m8DA2738139103AF905095CFD3F094331DA93BB11 ();
// 0x00000125 System.Void FacebookManager_<CheckIDFacebookDBRequest>d__14::System.IDisposable.Dispose()
extern void U3CCheckIDFacebookDBRequestU3Ed__14_System_IDisposable_Dispose_mFF3AEF802923B9BF46EA2940C028A717334F1CE8 ();
// 0x00000126 System.Boolean FacebookManager_<CheckIDFacebookDBRequest>d__14::MoveNext()
extern void U3CCheckIDFacebookDBRequestU3Ed__14_MoveNext_m4F4FD7A6E302F8591926DB4788DF2C335BFCF889 ();
// 0x00000127 System.Object FacebookManager_<CheckIDFacebookDBRequest>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckIDFacebookDBRequestU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m874F9F007A4D64D8B448332F3C223C4C0CF6EB5A ();
// 0x00000128 System.Void FacebookManager_<CheckIDFacebookDBRequest>d__14::System.Collections.IEnumerator.Reset()
extern void U3CCheckIDFacebookDBRequestU3Ed__14_System_Collections_IEnumerator_Reset_mE4BC5D7029703100544839CF02EB0F29469B5A07 ();
// 0x00000129 System.Object FacebookManager_<CheckIDFacebookDBRequest>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CCheckIDFacebookDBRequestU3Ed__14_System_Collections_IEnumerator_get_Current_m9D310C083EBF8EA38298D728AE9FDB89DDB94544 ();
// 0x0000012A System.Void FacebookManager_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m05A1B163F0D3104028020C2CD5EF5D3B61F781F7 ();
// 0x0000012B System.Void FacebookManager_<>c__DisplayClass15_0::<RegisterIDFacebookDB>b__0(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec__DisplayClass15_0_U3CRegisterIDFacebookDBU3Eb__0_mCAB2E69F7B0C4C100793A3985821782C2100BA7F ();
// 0x0000012C System.Void FacebookManager_<RegisterIDFacebookDBRequest>d__16::.ctor(System.Int32)
extern void U3CRegisterIDFacebookDBRequestU3Ed__16__ctor_m867A258F206771A1A569FAF8D7AF6C51A63CEBD8 ();
// 0x0000012D System.Void FacebookManager_<RegisterIDFacebookDBRequest>d__16::System.IDisposable.Dispose()
extern void U3CRegisterIDFacebookDBRequestU3Ed__16_System_IDisposable_Dispose_m04A0FF2517C79258C06833945C8F71DE5E657B56 ();
// 0x0000012E System.Boolean FacebookManager_<RegisterIDFacebookDBRequest>d__16::MoveNext()
extern void U3CRegisterIDFacebookDBRequestU3Ed__16_MoveNext_mD5960C1CF3FBAFD16E3AABE7E8E86CB8D44F2CCF ();
// 0x0000012F System.Object FacebookManager_<RegisterIDFacebookDBRequest>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRegisterIDFacebookDBRequestU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29322345345B6C8721D0ED4C2BC4090D728247D3 ();
// 0x00000130 System.Void FacebookManager_<RegisterIDFacebookDBRequest>d__16::System.Collections.IEnumerator.Reset()
extern void U3CRegisterIDFacebookDBRequestU3Ed__16_System_Collections_IEnumerator_Reset_m4D03031BBAC45BC3ACB55BC760BD32AD9581DC1A ();
// 0x00000131 System.Object FacebookManager_<RegisterIDFacebookDBRequest>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CRegisterIDFacebookDBRequestU3Ed__16_System_Collections_IEnumerator_get_Current_m9D3968578EB546A71F146AB01DAE53FAF20C8E4A ();
// 0x00000132 System.Void HighscoreTable_HighscoreList::.ctor()
extern void HighscoreList__ctor_mA132AA559D5DC10A1098059C68DB5CF8D768F414 ();
// 0x00000133 System.Void HighscoreTableFB_HighscoreList::.ctor()
extern void HighscoreList__ctor_mBCB183351ECE001D708284B23F0A098C1CCF2037 ();
// 0x00000134 System.Void Login_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m557E504B7FE05F14A60766BA86F4EE0D3ADADB34 ();
// 0x00000135 System.Void Login_<>c__DisplayClass25_0::<CreateAccount>b__0(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec__DisplayClass25_0_U3CCreateAccountU3Eb__0_mC7B209A9B22F585A62EFA8FEF1F7C71923C63102 ();
// 0x00000136 System.Void Login_<SingInRequest>d__27::.ctor(System.Int32)
extern void U3CSingInRequestU3Ed__27__ctor_mFD772B7F7014E65673F6D0B32559A534EE258E08 ();
// 0x00000137 System.Void Login_<SingInRequest>d__27::System.IDisposable.Dispose()
extern void U3CSingInRequestU3Ed__27_System_IDisposable_Dispose_m032A9BB458CCFBEAEA9DA45DCDC1B0F9781768FB ();
// 0x00000138 System.Boolean Login_<SingInRequest>d__27::MoveNext()
extern void U3CSingInRequestU3Ed__27_MoveNext_mE4C01CA2DD98D7D8709BA8C6A6F71049C56CA895 ();
// 0x00000139 System.Object Login_<SingInRequest>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSingInRequestU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DCF1A8D5F3197990B5DCC5BAA9FC818D61CA2E7 ();
// 0x0000013A System.Void Login_<SingInRequest>d__27::System.Collections.IEnumerator.Reset()
extern void U3CSingInRequestU3Ed__27_System_Collections_IEnumerator_Reset_mAFAA3EDB5866D1BE566B31CD14C3A92BA718C10D ();
// 0x0000013B System.Object Login_<SingInRequest>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CSingInRequestU3Ed__27_System_Collections_IEnumerator_get_Current_m7DB5C73C6262533A72051C9F94F85AAA2A1838C9 ();
// 0x0000013C System.Void Login_<CreateAccountRequest>d__28::.ctor(System.Int32)
extern void U3CCreateAccountRequestU3Ed__28__ctor_mC00F7180AB29EF06665EC71A099184381B2DF0BD ();
// 0x0000013D System.Void Login_<CreateAccountRequest>d__28::System.IDisposable.Dispose()
extern void U3CCreateAccountRequestU3Ed__28_System_IDisposable_Dispose_m3E2DB2C0F01AE7A95443356B29A7487EED1722A7 ();
// 0x0000013E System.Boolean Login_<CreateAccountRequest>d__28::MoveNext()
extern void U3CCreateAccountRequestU3Ed__28_MoveNext_m7B07EB26381312DC8BAE766396303ADD7D10D670 ();
// 0x0000013F System.Object Login_<CreateAccountRequest>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateAccountRequestU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAAA80CC99573ED5EEF39156AF8689C3F0561F36 ();
// 0x00000140 System.Void Login_<CreateAccountRequest>d__28::System.Collections.IEnumerator.Reset()
extern void U3CCreateAccountRequestU3Ed__28_System_Collections_IEnumerator_Reset_mCD0E0556A097E6803B1595832389D59BB32DFC65 ();
// 0x00000141 System.Object Login_<CreateAccountRequest>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CCreateAccountRequestU3Ed__28_System_Collections_IEnumerator_get_Current_m474359D2E25F766D9A45E476D2E83D8835676104 ();
// 0x00000142 System.Void Login_<>c::.cctor()
extern void U3CU3Ec__cctor_m1982CC4A9C90DF44A8C51873316CC87123345D07 ();
// 0x00000143 System.Void Login_<>c::.ctor()
extern void U3CU3Ec__ctor_m2E7C851469FCD6C3B9E32F3ED593345B7E8FF00D ();
// 0x00000144 System.String Login_<>c::<IsValidEmail>g__DomainMapperU7C34_0(System.Text.RegularExpressions.Match)
extern void U3CU3Ec_U3CIsValidEmailU3Eg__DomainMapperU7C34_0_mB56C6EC95DD3FC1AB9E76E44CC97584C186DFEFE ();
// 0x00000145 System.Void Login_<IsNewUser>d__38::.ctor(System.Int32)
extern void U3CIsNewUserU3Ed__38__ctor_m292D64C2D9BF05A04318B768A25706BC40467E1E ();
// 0x00000146 System.Void Login_<IsNewUser>d__38::System.IDisposable.Dispose()
extern void U3CIsNewUserU3Ed__38_System_IDisposable_Dispose_m83E56E21B82A070C6241D5DDDCD30AEC57C5282C ();
// 0x00000147 System.Boolean Login_<IsNewUser>d__38::MoveNext()
extern void U3CIsNewUserU3Ed__38_MoveNext_m0727AB089A98711952845174FE9D3CA438997463 ();
// 0x00000148 System.Object Login_<IsNewUser>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIsNewUserU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBCB2E5130C1B3242C6DB823170697F37902971B6 ();
// 0x00000149 System.Void Login_<IsNewUser>d__38::System.Collections.IEnumerator.Reset()
extern void U3CIsNewUserU3Ed__38_System_Collections_IEnumerator_Reset_m8EDA155E912A51D688DC33F998CAAE41907FD60C ();
// 0x0000014A System.Object Login_<IsNewUser>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CIsNewUserU3Ed__38_System_Collections_IEnumerator_get_Current_mB4A7927ECC5C636ACEE1196F317E0E6A8A11B168 ();
// 0x0000014B System.Void SpawnEnemy_<asteroidWave>d__6::.ctor(System.Int32)
extern void U3CasteroidWaveU3Ed__6__ctor_mA02848E61E314DCBCDED68BF01C5DBAF15C0929C ();
// 0x0000014C System.Void SpawnEnemy_<asteroidWave>d__6::System.IDisposable.Dispose()
extern void U3CasteroidWaveU3Ed__6_System_IDisposable_Dispose_mC72A228BBF1C28678C7B3ADC69CDA06264461896 ();
// 0x0000014D System.Boolean SpawnEnemy_<asteroidWave>d__6::MoveNext()
extern void U3CasteroidWaveU3Ed__6_MoveNext_mD5247BF1C12ED6D9FAC6ADAAD3CC62D08FDF9D4C ();
// 0x0000014E System.Object SpawnEnemy_<asteroidWave>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CasteroidWaveU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB849820C30CF74E64546A5B13CF16574A318954 ();
// 0x0000014F System.Void SpawnEnemy_<asteroidWave>d__6::System.Collections.IEnumerator.Reset()
extern void U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_Reset_m7C58EE19A20A0521F8628ED8E09271D1E0C6A9E8 ();
// 0x00000150 System.Object SpawnEnemy_<asteroidWave>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_get_Current_mCB79F938AEB9DC895E91142D9870BFCB5DC9F337 ();
// 0x00000151 System.Void SpawnObstacles_<asteroidWave>d__8::.ctor(System.Int32)
extern void U3CasteroidWaveU3Ed__8__ctor_mFD99860DDA9ED5CCC6CD9FD808220055D00218DF ();
// 0x00000152 System.Void SpawnObstacles_<asteroidWave>d__8::System.IDisposable.Dispose()
extern void U3CasteroidWaveU3Ed__8_System_IDisposable_Dispose_m3B539F9030BAA5E62C5FFF3214A6F96017B53790 ();
// 0x00000153 System.Boolean SpawnObstacles_<asteroidWave>d__8::MoveNext()
extern void U3CasteroidWaveU3Ed__8_MoveNext_m3FD0916C4F3E87F1601FB60EF7286FE8EBB16A69 ();
// 0x00000154 System.Object SpawnObstacles_<asteroidWave>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CasteroidWaveU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BCF396F204C64A40E71311A2FF92242F887FE95 ();
// 0x00000155 System.Void SpawnObstacles_<asteroidWave>d__8::System.Collections.IEnumerator.Reset()
extern void U3CasteroidWaveU3Ed__8_System_Collections_IEnumerator_Reset_m62E0107BA668007BCD270EB47B81BD5A4B124ADB ();
// 0x00000156 System.Object SpawnObstacles_<asteroidWave>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CasteroidWaveU3Ed__8_System_Collections_IEnumerator_get_Current_m2A582B935317E238D98E57AC29C217480A99D57B ();
// 0x00000157 System.Void SpawnScore_<asteroidWave>d__6::.ctor(System.Int32)
extern void U3CasteroidWaveU3Ed__6__ctor_mB978A1DE3E3B824A3AA47F8A73623F5AF89E0878 ();
// 0x00000158 System.Void SpawnScore_<asteroidWave>d__6::System.IDisposable.Dispose()
extern void U3CasteroidWaveU3Ed__6_System_IDisposable_Dispose_mEEDB90603A74661AA7D53FC911CD93BB2E56E14C ();
// 0x00000159 System.Boolean SpawnScore_<asteroidWave>d__6::MoveNext()
extern void U3CasteroidWaveU3Ed__6_MoveNext_mE947A506FC5B5F3B138EF900D76BD5007444573F ();
// 0x0000015A System.Object SpawnScore_<asteroidWave>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CasteroidWaveU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m382153D3C41EEA8A75109076D9AEE84F56E83B48 ();
// 0x0000015B System.Void SpawnScore_<asteroidWave>d__6::System.Collections.IEnumerator.Reset()
extern void U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_Reset_m0FBB89120A0A6652328BEDFCEF8F9159DE833728 ();
// 0x0000015C System.Object SpawnScore_<asteroidWave>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_get_Current_m1670BBF051ED11BDFEF2CC31E6300090A3EFCFFF ();
// 0x0000015D System.Void Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::.ctor(System.Int32)
extern void U3CTakeScreenshotU3Ed__4__ctor_m80CE40D404E301D3006E187D1895D040300F18EF ();
// 0x0000015E System.Void Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::System.IDisposable.Dispose()
extern void U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_m454F1C0A61C618628D75F23F4C0BB1901002F1C9 ();
// 0x0000015F System.Boolean Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::MoveNext()
extern void U3CTakeScreenshotU3Ed__4_MoveNext_m96965A488E2C6949389435D59E02FC009AAB2935 ();
// 0x00000160 System.Object Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DAC71D34D1CFDB79E38D3A56A4E41CC437D51DC ();
// 0x00000161 System.Void Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m1F0B12BBB8AA4BFD581AE55AC23524FA7A3FC60A ();
// 0x00000162 System.Object Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m2A0C27075849E84A0F869BC66DA40117F5D7D6D5 ();
static Il2CppMethodPointer s_methodPointers[354] = 
{
	CameraFollow_Start_mA9B0BF6D65FDE2EC07572180BF67C67F711C1C8C,
	CameraFollow_Update_mE47A8BFDC5449AF1D397AD9D049A2CB76BD0BE9D,
	CameraFollow__ctor_m27AF37B0C19243374F9376EBB2C40A2F605DB16E,
	ObstacleDie_Start_m81C298CCD5A358BC98AF7EA6B4287B4CDE4E7028,
	ObstacleDie_Update_m9FC0405D3F7C9DA15CD4169720E2434071CC7ADD,
	ObstacleDie_OnCollisionEnter2D_mA0A6716A38990CE55BC3DDF02F52EEE4123ABD5A,
	ObstacleDie__ctor_m99AC2387C7DBBA027C8D0FC9D3589D10FB3E4AA9,
	SceneChanger_Start_mC7E940B8708C19EA2936ED7B2E22CB9892247790,
	SceneChanger_Update_m675F8AB0A127874FB5AF92A1651A6421CEDC0F00,
	SceneChanger_FadeToMenu_m2583D34BC869966F8E062D60E1FD6471743B1011,
	SceneChanger_FadeToLevel_mEB5353B04355AAFC9130D873A598CEF98EF553DB,
	SceneChanger_OnFadeComplete_m9EAD236194AABC1A0A3615B762EC92D99738C6F8,
	SceneChanger__ctor_mE3CE8CC419FFEC01F8C75F84ADC9B7DD7EF012FE,
	BackgroundScroll_Awake_m242F8E3187E9B74286CBA8021E69CC6B990A7931,
	BackgroundScroll_Start_m85B0449B7877013EE361904474460BB0FA9AAE5B,
	BackgroundScroll_Update_mD280AD534605267AD3F1A526ADF3F7B3AD8D00B2,
	BackgroundScroll__ctor_mB2C680BED7D257055B6A2FCB04044CB4A01C25B5,
	DataManager_get_Instance_m33A82DF3D68C78989E730896969F6248CCA14709,
	DataManager_Awake_mDAA142952ABB5A7C496F5B305AAB2CC55CDCD5DD,
	DataManager_ParseData_m76D9B692CF32277214E55004B60086BA9D353ED1,
	DataManager_SetAccount_m108A405332AB9C11AFCA5C3781F615474350B586,
	DataManager_SetLogin_m6B18FF14CBCD04A990B744BD6B0EA652732761E1,
	DataManager_Logout_m939C6230BEB7F39A7D215AE92F754B779748F98F,
	DataManager_SingInSuccess_mED9702E506CE45A0D1EE898565478A806C98F85C,
	DataManager_SingInFBSuccess_mD87CA8642D598D4E4615494631D338FFCF92DDC3,
	DataManager_GetHighscoresTable_m57768EF8336A359851C7C221356B9D60C16B14BA,
	DataManager_GetHighscoresTableRequest_m333D6770055A7C331365C1194606A3B000503C29,
	DataManager_ReturnHighscore_mA4341E90F47161D5D3F9063ECC3FE85A438DEB01,
	DataManager_UpdateHighscore_m8578A009F72085A31F96312DBD19A4D402C2B55C,
	DataManager_UpdateHighscoreRequest_mC7F81211248AA58F1EC427FDDE4677FCE2552DB8,
	DataManager_SetHighscorePref_m4A0980F4E20F71509F02630233434A09A5FDF3F1,
	DataManager_FixJson_m9FEF3B856E01600C52ECED895FA8170E554F4FB2,
	DataManager_SetItalianLanguage_m2965955BFE85C8B532E627C74374D5B4785C2306,
	DataManager_SetEnglishLanguage_mB1455A86DB3BD49CBEB042EA39FEA0061C9AC255,
	DataManager_SetCheckLanguage_m0BC01F1F512391C1F270EFAC18F50DC5A2623857,
	DataManager_SetHighscoreLanguage_mFBB3B2DC4EDE44430C9C7DEB4F939E3D375669AC,
	DataManager__ctor_mAFD6DE9EC9CC3131A8DD12328AE89D6817225D61,
	DataManager_U3CGetHighscoresTableU3Eb__24_0_m828D280306CDDC26ADDB05AFB31B04A3EBF423E0,
	Enemy_Start_m0681B66D4522F045EB7A33A21467994960D1E435,
	Enemy_Update_mE957FE3BFB8CBB7C30E43D4A81C13E02B1C1FC32,
	Enemy__ctor_mCD4E016A02FE662E339AA011EBA74D77B09556C5,
	FacebookManager_Awake_m75912ECA734B4E696C2E11A85876EBD55ADA5091,
	FacebookManager_FacebookLogin_m928D8D00A9C5F11D3D04724DC4E2117A0ADF0CD9,
	FacebookManager_FacebookLogout_mF4BA84F52753221D6254C041BA4D2B175F8C056A,
	FacebookManager_FacebookShare_m1287D5F074BBE039B41AB985BE59778A0D68C787,
	FacebookManager_FacebookGameRequest_mD2F6EE8A9B6C20FEF2F9E1451F610C860915D3E3,
	FacebookManager_FacebookInvite_m6C547A1E7C9C1918D551669ECCA2708737CE766C,
	FacebookManager_GetFacebookInfo_m8DF17B5175D0041C9F97EA1733E2470871C614CA,
	FacebookManager_CheckIDFacebookDB_mCA0CE7726228D857217984C8C7F0DAE0E02D66BE,
	FacebookManager_CheckIDFacebookDBRequest_mF76484A5F3FDCB066FEE4B5612B85214468121DC,
	FacebookManager_RegisterIDFacebookDB_mE8A1E205C73BC9A9BA3A3E0D60C3591C245CFD97,
	FacebookManager_RegisterIDFacebookDBRequest_m081E150C2BB3F81E651E1E305CB4D86AA049455F,
	FacebookManager__ctor_m79647B42B0C92EDEFCB34635448A78DAEC91BC1A,
	FacebookManager_U3CFacebookLoginU3Eb__7_0_mBDD507406064D0C3A08D9F13EA5E1B68C1F5191C,
	FacebookManager_U3CCheckIDFacebookDBU3Eb__13_0_mDC9794E7AC01365034D0F3DD9F133F357F178AA0,
	FadeMusic_Start_m9C3BBFD2D88CABE4EB637DD66FD1A58DBA74F768,
	FadeMusic_Update_m4BD2D24C5FCB161F3B208FAE627C2E5F6C1CDFC2,
	FadeMusic_SetFadeOut_m19425117579C9B75BB7CA1F267E9795969C2A341,
	FadeMusic__ctor_mF1990A616A0309789E67515B5316FA0D7E4928FC,
	GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E,
	GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	Highscore__ctor_m1B67838BE8569772542982BE5931A4049FB63237,
	Highscore_GetHashCode_m9C7ED4A2F0F706C1E037F1AB045B63858FDF6CD5,
	Highscore_Equals_mDD10C008EF8403FC32FD8AEAF9C6ECB27D0E3817,
	Highscore_CompareTo_m6DA56613E6D706D9DB74F41123799CF31FEAB49D,
	Highscore_GetScore_mF3E7F2DB098219A507120C1945A2B5A8FD2AF4F9,
	Highscore_GetNome_m94F5BE273F22A40854D3D1A44581BD5090D06D24,
	HighscoreTable_Awake_m24DFFC813D2CF74B93171B19EAF939105437CB62,
	HighscoreTable_Start_m88E728DECB1A50AE9569CBDC9EB41620E69EC2F4,
	HighscoreTable_CreateHighscoreEntryTrasform_m7E0F2C97624E489435326DE6BFD91DC8A00835C4,
	HighscoreTable_FixJson_mB5A567753BD80FB7410B6F6C0B66167C62C80AE0,
	HighscoreTable__ctor_m9C20B4596A77290216ADBBC7220C7369D33DB675,
	HighscoreTableFB_Awake_m2F08008AC04AF4632FD4DC839D17DC938991C880,
	HighscoreTableFB_CreateHighscoreEntryTrasform_m1C48254C98BC48180BD0A5724471A369FC08F4F3,
	HighscoreTableFB_FixJson_mD10E62E18219AF382A8FC8D225B320F4C066FDC1,
	HighscoreTableFB__ctor_mC93DAA1CB8F38169BE8D331E8A6973483E90E68C,
	Login_Start_mC4391C73942BC3CE27127B51C3B30D32BEC47CF7,
	Login_Update_m13C601B87C99B223CF5D1E382750566DF46308B5,
	Login_CheckIsLogin_mE669B7C2FFF089ABC2250DCC200C130030BAD28A,
	Login_CreateAccount_mC89B986E1E50B2434B5B1050B36A0D043C42F419,
	Login_SingIn_mA1B45C54139438337E0325363F85A85968379892,
	Login_SingInRequest_m9B6C8AB9710CE5DEF7E1771C6D81B39AEBD4B3AA,
	Login_CreateAccountRequest_mE0AF242B750FA783C74BA1C626920214B97957C1,
	Login_Logout_m9BBFEF95B9DBBA8F623947632872797BD7191E45,
	Login_IsBase64String_m26AD8CC5CEE36205EB4D95B27B9E554A57F2A7B7,
	Login_EmailValidator_mC5778C8F4D7CBF1034C066EA995FE36529B61F4F,
	Login_ConfirmPasswordValidator_mFE88903D91A89264B60941233C64C008AEF466AC,
	Login_PasswordValidator_m5134F03C2E70E49EAA31CA1149CDF815FF0B2EC9,
	Login_IsValidEmail_m0A6AF978681EA1652512146259560F1E5790DA69,
	Login_IsValidPassword_m3BDA31473510E1DF528EE6B0CEF5EB71029DC741,
	Login_ErrorLogin_mD2FE30F2F4562070AFB8625165A2F0570434F5EE,
	Login_CheckUser_m167BF7C86C797E19285B2276BB10873C2CBF2E37,
	Login_IsNewUser_m00EABE0452733790697F70E7882A21D1A8A3577B,
	Login__ctor_m077828C912B448F09A5148022AC7C9E8607225D1,
	Login__cctor_m43CE63FECB345313D070AF3B09A51D182360C5B6,
	Login_U3CSingInU3Eb__26_0_m26B6DA22EDDCDD5A37CFF20AFE35A06CBF8194E4,
	MainMenu_Start_m0E4D98D402AC049948D1CA7E386641DCF5652821,
	MainMenu_Update_mF9DD9038CA373A8F069A57E41F6D530D8B017866,
	MainMenu_PlayGame_m4CD3D61E23D84AD1A018C84D561EAE39ED2D76F7,
	MainMenu__ctor_mF17B753D99BD98B88E949A5B9CA53892E19A6CD5,
	Obstacle_Start_mBE9A2C144E4C0BF4161D381473712395E98FFD75,
	Obstacle_Update_mF887C681ECA48EE9CFE80B2F13DF3CC0BA36FCA3,
	Obstacle_SetSpeed_m6ECAE23C3C57B36A7DB966EA1E80622566F53C9D,
	Obstacle_GetObstaclePositionX_m5BD0AA08DA64CB232B75B1CE19D332F857766655,
	Obstacle__ctor_mD2207B1C3A1DD1E461DB628242435A19A48E6EE8,
	OptionsMenu_Start_mA7964719282C1ED0598CF5135997CF56D087E824,
	OptionsMenu_Update_m0DA07A08B0758009BB083DEE7737F41B31FCB3FA,
	OptionsMenu_SetOnOffMusic_mF112C1BCBB1FDF1D73BA37389026E247C1694C4B,
	OptionsMenu_SetOnOffEffects_m77AB6E8E480D7B152311664CAF4FF221465C170F,
	OptionsMenu_SetOffMusic_m1C6F1804AEA896677CE56AE1333DF48EB2FC068B,
	OptionsMenu_SetOnMusic_m49E27C6D5D1877F351201BC5D8BA1DB2BEA78FF2,
	OptionsMenu_SetOffEffects_m68F2DB74F7493F39CE77B059D4BA19F63236E597,
	OptionsMenu_SetOnEffects_m1B34EB32FD61FCF563405763B81CD15A33EB73D8,
	OptionsMenu__ctor_mF71A00F27EA929ADE40891F6956F1759FACB514B,
	PasswordHasher_get_Version_m4FFA79970179A01AA94D6D5B057FE25F82AB1BF0,
	PasswordHasher_get_Pbkdf2IterCount_m1D8695FBBC730F4DFEB4915306EB9CBE9DE60E9F,
	PasswordHasher_get_Pbkdf2SubkeyLength_m8D48428CC34DFD0452733006F35868DE4A883265,
	PasswordHasher_get_SaltSize_m0241857DCFE44734EC7C2A4223779E1B5D9F4FBA,
	PasswordHasher_get_HashAlgorithmName_mAB9EC3F0D657F43E0AB42FD3DAE837E7C7CBD695,
	PasswordHasher_HashPassword_mC851C056E7EE515229739FB382313D7840B7363F,
	PasswordHasher_VerifyHashedPassword_m8D7271106B5DC92BC1AA1AA26A1607829AE2AE47,
	PasswordHasher_FixedTimeEquals_m9A226F1A09637C8EFA5BAFB170BC6774C4FA89D7,
	PasswordHasher__ctor_m490C06B6510BFD9C1412A01B49C4B44278A629F7,
	PathSpawnCollider_OnTriggerEnter2D_mAC9CE0F28C20E53972479B9755E22B34A7676050,
	PathSpawnCollider__ctor_m8FAFC0A32CD227D6069562239A25DFF0C83077BA,
	PauseMenu_Start_m87C554A1CF71A5E4A233FB35A6E0DB276388DF3B,
	PauseMenu_Update_m59D9098B173533E082A41253B251B1E5F5AB9EA9,
	PauseMenu_PauseGame_m565EBD7A8427786B696FA446F226E42A8F40C320,
	PauseMenu_ResumeGame_mCCB9232463ECD336BA3A0E202023DB3230830C15,
	PauseMenu_ReloadGame_m93BAF00D9627450354F90F5A9A1B4825644F4EFD,
	PauseMenu_ExitGame_m4440FC161D6CBD0EB1D2A6A8DA1BECC6F24FB12C,
	PauseMenu__ctor_m1DE7CAFF465FE5BEB4E0ABDF165AFA208631EF28,
	Player_Start_mE9ACCAA7FEBF4020693FFB73C0839CF3AAD0B0C4,
	Player_Update_m6F977BAE3756AB7073D64042B766B442E4EC6FD2,
	Player_Rotation_m8C1AC21A9431A2728BFCDEBEDCAE969B49425790,
	Player_OnCollisionEnter2D_m250C0143A5AE43821134712D9306FA483745A39C,
	Player_OnCollisionExit2D_m16F57E27652E2C4ABE4F940F05AEBC4703B240B2,
	Player_OnDrawGizmos_mF959B653B44C83FA34CF4C08CC1DDFDF1B6A6420,
	Player_SetSkinAndColor_m7BA549331ABEBC6781A66F8E343D0627DDE9A56C,
	Player_Die_m2273E1901C7A6A2FCD9AA58EA5D0243D836679C9,
	Player_GetScore_mD7FCBAACFE52DE75DBABBA6C495EB6A4079651CA,
	Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2,
	Score__ctor_m253573F24BB13CC104B847383A3B27F0A7EADD5D,
	Score_Start_m1821AC2C6C0505E9EEFA5DD52733BE5C3037AD9E,
	Score_Update_m034295E32383253E391ECFA2599FD3720345D1DB,
	Score_SetSpeed_m9CF9E8EAF05F506CFB14A53E3700642539F0BA8D,
	Score_IncreaseScore_m6C65DC3D88F554BBD315DA24FD40F56B475026A4,
	Score_GetScore_m64B86C577600E1B75D518B9A23289639D35FB558,
	SoundManager_Start_mB9D238182CC4B1023DE4C4D331E88EFB7E82F24F,
	SoundManager_Update_mA43265016234A06D51D1D96DAB646B758F6E4AB6,
	SoundManager_StopBackgroundSound_mC634ECA7550B617D25065988E9D39CFE19727145,
	SoundManager_PlayBackgroundSound_m7D14613B97095BFE8DB6799F6EE0D64C0AEA582E,
	SoundManager_PlaySound_mF0B7231A196F591C25BC19455496627A3FD52FB4,
	SoundManager_PlayMusic_mC13A54C83D7F3F01C649A32610F84E9BD3257409,
	SoundManager_PlaySoundButton_mBBB59D792FE992C91172C3D6C3EE166FC1E9D5E4,
	SoundManager_PlaySoundCloseButton_m6436B89331765EC61335DF44A1D6DCC96BC66E4E,
	SoundManager__ctor_mFF8C696A5B666ABC1E2344581FE7FB06E038D422,
	SpawnEnemy_Start_m11B73FA87652E5A756CB4B0ECA4AB655F950D606,
	SpawnEnemy_spawnEnemy_mA882C077E3786D27000E3D1BC181DA1E69B2DC98,
	SpawnEnemy_asteroidWave_mD7EC39852FE042C7062CF81675B321FECB828C2B,
	SpawnEnemy__ctor_m987D9D871D1B8410EE06C366C989685928DEFCA1,
	SpawnObstacles_Start_mE41B52821E9F023BD447676789E9E9B2953B7041,
	SpawnObstacles_spawnEnemy_m1643545BB6A23A5A2B1851D26121E3D0112006CB,
	SpawnObstacles_IncreaseSpeed_m4B0D03AB34D213239401DEC85ED57562C95DCA4B,
	SpawnObstacles_asteroidWave_m50604FCB4A83B7920DD0332F6CD0449E8638F4D7,
	SpawnObstacles__ctor_m99A4EA5C4D6117E115EAFCAF8546106AAB2292A3,
	SpawnScore_Start_m98AD1F70BC7B50C836257D5D9137348C326C3EE2,
	SpawnScore_spawnEnemy_mBB4EE5B565085A53CDE3CE9C72D8CB6B96AE821E,
	SpawnScore_asteroidWave_mD9E7A30182E309331B0BC3B6D9AF4E6BFE42567F,
	SpawnScore__ctor_m3A72DAB7765B54BCF787AC79E26609DC73695236,
	SwitchGravity_Start_m30C0C7608F098346DA5876FB0C45B0B9F8771878,
	SwitchGravity_Update_m2F813581F7719C0A29BC88BB917A96C4A5B8EA58,
	SwitchGravity_Rotation_m51D4531CA73AE49D2703ECC750DB82E0A52301AA,
	SwitchGravity__ctor_m500E5659769D1CB987BFB1257D4BB7ED3C48BBF2,
	TextLocalizer_get_Instance_mE3A9EFD78F36B3E4469780C096AC6FCE600B1EDD,
	TextLocalizer_ResolveStringValue_m6359A61ED6CAD063C69820810FD7905C70EF83B3,
	TextLocalizer_Start_m4081FD0CAC5F1798EE7001284C0C30B5F2F3C63F,
	TextLocalizer_Update_mCA869E4B7D83A30CC5AA619ADF32EDCCD9FC1072,
	TextLocalizer_OnValidate_m95B9324469C2F121FADE20A2CDD2289340107375,
	TextLocalizer_Traslate_m38A38060E164C8E0A0C9F4B1F60D86D19A15B66B,
	TextLocalizer__ctor_m1693C2554402ED8A96C7436A5926A353872A9FD7,
	TextLocalizer__cctor_m5C91EE74DDB94DB2F6B9E9FB991855B3C8C1D437,
	SkinMenu_Start_m57D2D3FBB1F0850EA4DE6759A7BF46C72DBF6A8C,
	SkinMenu_SetSkin1_m252F3AC4ED12D1F80042E22834A3DEF4A23102A8,
	SkinMenu_SetSkin2_mB94FD0BE830B4CBF01FE389FEE5E8D3EF7DA913B,
	SkinMenu_SetSkin3_m8CFE088FD8B2DD479896C42183F3D71620B1350C,
	SkinMenu_SetSkin4_m648B62AFF0A9A24EE470D658C1BA1A691717D2A1,
	SkinMenu_SetSkin5_m4BDFD3F70B85A25B1933791D4C3CA4730C5D8830,
	SkinMenu_SetSkin6_mFB366BD32B3535DF194DC9D5C66EB50645C75769,
	SkinMenu_SetRedColor_m1270C0D8298B92D8B722B36FD45707CBE4D4A990,
	SkinMenu_SetGreenColor_mF089ACA18C435F38A0ABA5E83E0DA7FDBDF86541,
	SkinMenu_SetYellowColor_mB4A13F1D2B308A0CF0B4F9BBBC1CF57713DE066F,
	SkinMenu_SetLightBlueColor_mDA851627E64B24F775B5A1783BADA07554AD5487,
	SkinMenu_SetOrangeColor_m2DC7A8321D0849CCC03F232A98F196F01562AA32,
	SkinMenu_SetPurpleColor_mD2F85B9389E634772767DBACD39BD198E8D9228E,
	SkinMenu_SetColorSkins_mB7C8CD2B2BF41A1350CB7B1FD0BBD95799B5DEDB,
	SkinMenu_SetSkinColors_m9A77B5D7ADFB6F590F0C18504967F2F5DEF1AE55,
	SkinMenu_SetSkinsAndColors_mFE7F0AC4913F48BAAC571DD3B9659A0CEBE71B75,
	SkinMenu__ctor_mBA8D08A6732FF2A03796AA51107EED487989D6DF,
	SkinMenu__cctor_mE9EC03CF16CD752C76F4AC7D3AD023946F222431,
	ConsoleBase_get_ButtonHeight_mD6796BF7D04D036E17954E52E37F156C15207CA5,
	ConsoleBase_get_MainWindowWidth_m19DCEEA57F769E12993C2D0B5D4C7D07ABEBF08C,
	ConsoleBase_get_MainWindowFullWidth_m7A4603551C41BC2CD25726C39C42F023A222D8BB,
	ConsoleBase_get_MarginFix_m07098733F527787846417EEDF7648B78AE368B79,
	ConsoleBase_get_MenuStack_m2A084509DB58962B6F06DDBC7A01A3A31CDE99BC,
	ConsoleBase_set_MenuStack_mB137307FEB4D04CCFA479094D84F3C014E98939F,
	ConsoleBase_get_Status_m0AAC8BBEDD91380EF0F5E61893EC446A9E4BE3B5,
	ConsoleBase_set_Status_mC77922E8270F74F814F65EF8A90E6D3E3DDC4CA3,
	ConsoleBase_get_LastResponseTexture_mF77BC51EF52CB32EB97E09B17D011CBD77E386CA,
	ConsoleBase_set_LastResponseTexture_m673429A2AA388C5A3AFBBF32CF0409C59CC6F9C6,
	ConsoleBase_get_LastResponse_m85EA4F944CF1763203F673D3AB12B18471114A00,
	ConsoleBase_set_LastResponse_mDBF8B57FA98474D359C2E82FC59DBA66FF4760B5,
	ConsoleBase_get_ScrollPosition_mBE23454596E97F671992C6E3A69D10E44FCCA7AA,
	ConsoleBase_set_ScrollPosition_m4EFB0377EF967C8B1CFD6B49C7D0E8107D8924FC,
	ConsoleBase_get_ScaleFactor_m6DBCC64812BD73777F8CDC290DED19EF154AD67C,
	ConsoleBase_get_FontSize_m949A90F9EE85E2BF3C862CB57EDA7E7253CBA42B,
	ConsoleBase_get_TextStyle_m2C5B95DED6749DEF5E0BC83A6FCAA236D9CC8619,
	ConsoleBase_get_ButtonStyle_m3F36462B5EA45C5088A30A523968C1EC914172BB,
	ConsoleBase_get_TextInputStyle_m7715C64446C4503F151B7134F62FEF38372DBC18,
	ConsoleBase_get_LabelStyle_mB7E43BEED5043E2F9CBC7911366F1B36744AE9D9,
	ConsoleBase_Awake_m1FD6E034452A59B8A021A7072B916A2D23F3B5D5,
	ConsoleBase_Button_mBCE04002B2C04BB153A76FDEBEEAAB23D0BAECF9,
	ConsoleBase_LabelAndTextField_mFD378CC735F9B8F45E78F2AB1370F0DA78A785E3,
	ConsoleBase_IsHorizontalLayout_m0AC4D0C36E62F3484015A87A5AFEC750630C0076,
	ConsoleBase_SwitchMenu_m5CACD54AF32E4AFE5750D255E348265775FA45B4,
	ConsoleBase_GoBack_mA08B9EADF1690ABC5DFF57DF064DEA1B790BA094,
	ConsoleBase__ctor_m470E3137F3CA730B7B181BE783D22EFE2B0E1B4B,
	ConsoleBase__cctor_mE0C3DC7162CE1B68B3B47D64EE1BC2E135AE1C6E,
	LogView_AddLog_mE6A026BAA7B6368F41B9FB13BD175C18F0D32373,
	LogView_OnGUI_mF0A849E64B666ADD171E00CC73A0F1CDDAE04364,
	LogView__ctor_m3E2783B223F06F9CEB65DA167AE95EBD0BCC582C,
	LogView__cctor_m79D024FAE554C6D45A8CAB6008F7182BB69458C4,
	NULL,
	MenuBase_ShowDialogModeSelector_m324800BD86013CAE28A04574BD85B485159AFAA0,
	MenuBase_ShowBackButton_m8DF4DB9BB3902BA3BD9760C8F469770146D8E577,
	MenuBase_HandleResult_m4A9E778AB30F7D0CBA4B6315DDD3DC3E581E820A,
	MenuBase_OnGUI_m2CA018B434F79284101DBD1C48F94EE802B6BE6C,
	MenuBase_AddStatus_m5B47C2A547733E41DE49A04A215D3C4957B47984,
	MenuBase_AddBackButton_mD0420505F101FD664A23AE8E6D192450EF09E87B,
	MenuBase_AddLogButton_mF9453B9DBC34C3358E13B3E2212B5E9B9123C899,
	MenuBase_AddDialogModeButtons_m992687F9A5F3739164DA48B459F3ADC57C0B8C3F,
	MenuBase_AddDialogModeButton_m524D1F80FBF7CAEC453DA0D7084FFA609C0BD69B,
	MenuBase__ctor_m7632B394B61767B54CCD6665E656C9DD94D461FD,
	AccessTokenMenu_GetGui_mF50B240450BB8950208F5CCA92D9DB19A2C5788D,
	AccessTokenMenu__ctor_m816CBC0B7DC83617BB3964B37D43A5F4FEC1F606,
	AppEvents_GetGui_m2CA0B8AA9D5389CD2135C7AB307ECEB9697B271C,
	AppEvents__ctor_m4B39A59B111BD88198FC653B308913F92AAB4A29,
	AppLinks_GetGui_m6A7B19D799397D99707A12A8DC4D24A9AF4652E4,
	AppLinks__ctor_m11439422C75055B559AF9B69DE071CB8270026DA,
	AppRequests_GetGui_mCEDDF7E12A29E6C83B26703619222BA7E09B5657,
	AppRequests_GetSelectedOGActionType_m61C17A1CD6303AC2757FB7256B02AB39FE715C66,
	AppRequests__ctor_mE6DBC84D2D6DE967B0B232820F7BE9DC8DAC5A38,
	DialogShare_ShowDialogModeSelector_mEED5D21873BC565D2B7405777FF0254479CA3CE1,
	DialogShare_GetGui_mCBDD1A37ECA65AA48A7BFDBCB2210D4B8FF99927,
	DialogShare__ctor_m0CD472A363CF96DD661816530EC967B7E223BB9F,
	GraphRequest_GetGui_m1D9A9F8D11F9D8E5D06FA64507797DF0559CE2BF,
	GraphRequest_ProfilePhotoCallback_mA003EAE8C5F935F2A745CEBB05214BF038AE4AFC,
	GraphRequest_TakeScreenshot_m61931CD8C35171219F42D9916DB97253D9AEE967,
	GraphRequest__ctor_m76FF017B5E3D19347A1FA7BF21B656E6C92E9814,
	MainMenu_ShowBackButton_mDB1AE0B4004A77060F718E5B8F71F893E8C0396B,
	MainMenu_GetGui_mD25F94536C838B897A5898B43DA4D603B2338782,
	MainMenu_CallFBLogin_mBBCC97AE79ECF3897AD2F1FB34DB46A35C5D9A33,
	MainMenu_CallFBLoginForPublish_m2EF32E95A1B55419F8F82316E3B028CAB0829823,
	MainMenu_CallFBLogout_m4A99B5C87829AEC6C7B256463DBB44244843E27A,
	MainMenu_OnInitComplete_mF83BD38A0C567092F4FD6E8DA20BA9A9C4CE7353,
	MainMenu_OnHideUnity_mE443DEB16426DCBF2D4A80FC3DB6E0E096445414,
	MainMenu__ctor_m18A96BABBB8ABA3A17C3ED1C71B15E37613EDCEF,
	Pay_GetGui_mCB1F735FEB7A64E3CA0C2851FA6D0E000D121B76,
	Pay_CallFBPay_m9E0BD295ED823B136CD451AF6E8133D567C51C59,
	Pay__ctor_m80DDC8B433865B2653DBD16E15D8CE233FE92D92,
	HighscoreList__ctor_m42D78625761945DE116F45A3C0E02D1CCA0200AB,
	U3CGetHighscoresTableRequestU3Ed__25__ctor_m56FFAEEA76CB41E4D3EA2AA6E93F26F091628732,
	U3CGetHighscoresTableRequestU3Ed__25_System_IDisposable_Dispose_mD90DCC8F31661EEC086B8DA90A267F787ECC5321,
	U3CGetHighscoresTableRequestU3Ed__25_MoveNext_m9B18DBFDD0C99182EFCE9A20640B3AD19E4377C2,
	U3CGetHighscoresTableRequestU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4898EE034FC85D981A189B8DD13239A0F5E651A2,
	U3CGetHighscoresTableRequestU3Ed__25_System_Collections_IEnumerator_Reset_mCA655FC15A8784B2D77859DE567B9B852F5E58E5,
	U3CGetHighscoresTableRequestU3Ed__25_System_Collections_IEnumerator_get_Current_m316AE3949DDD53BCAB36371047EE2B100002155D,
	U3CU3Ec__cctor_m4705F54D6DE891B689C6394A5CF3B1995D3D6D1A,
	U3CU3Ec__ctor_m1485061173EE65243F665B2566C1DE55D2294607,
	U3CU3Ec_U3CUpdateHighscoreU3Eb__27_0_m8B85E52881148BE725810BB93D7F633ED93C63EB,
	U3CUpdateHighscoreRequestU3Ed__28__ctor_m6D2AB1CDA9F9169B60BE5BDA6AF9672A37C56D99,
	U3CUpdateHighscoreRequestU3Ed__28_System_IDisposable_Dispose_mB3C08EE55079BB3BFACE52ED615CDAF210422E01,
	U3CUpdateHighscoreRequestU3Ed__28_MoveNext_m8B17EA6B2305F5708A74CD23ABFA4ECB396C4587,
	U3CUpdateHighscoreRequestU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC114254582B83839402472D405F0BA67696B3278,
	U3CUpdateHighscoreRequestU3Ed__28_System_Collections_IEnumerator_Reset_m509042A25D9DF88386755FC62C71060BC9FE43E4,
	U3CUpdateHighscoreRequestU3Ed__28_System_Collections_IEnumerator_get_Current_mC6A4535409E871D60D65CB7C3C0D71FC45B28076,
	U3CU3Ec__cctor_m436D7A8845E2B6F8606030B669684C7AB1BE29B7,
	U3CU3Ec__ctor_m86C7076F43C7A6547EAB729AE4F7A6B14A64B556,
	U3CU3Ec_U3CAwakeU3Eb__6_0_m70FDD78F5016F3B3F2D622E38FC1BDF76CD48777,
	U3CU3Ec_U3CAwakeU3Eb__6_1_m9BDF9D3ACD658E5F5F68EF623907FE5AB920D62D,
	U3CCheckIDFacebookDBRequestU3Ed__14__ctor_m8DA2738139103AF905095CFD3F094331DA93BB11,
	U3CCheckIDFacebookDBRequestU3Ed__14_System_IDisposable_Dispose_mFF3AEF802923B9BF46EA2940C028A717334F1CE8,
	U3CCheckIDFacebookDBRequestU3Ed__14_MoveNext_m4F4FD7A6E302F8591926DB4788DF2C335BFCF889,
	U3CCheckIDFacebookDBRequestU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m874F9F007A4D64D8B448332F3C223C4C0CF6EB5A,
	U3CCheckIDFacebookDBRequestU3Ed__14_System_Collections_IEnumerator_Reset_mE4BC5D7029703100544839CF02EB0F29469B5A07,
	U3CCheckIDFacebookDBRequestU3Ed__14_System_Collections_IEnumerator_get_Current_m9D310C083EBF8EA38298D728AE9FDB89DDB94544,
	U3CU3Ec__DisplayClass15_0__ctor_m05A1B163F0D3104028020C2CD5EF5D3B61F781F7,
	U3CU3Ec__DisplayClass15_0_U3CRegisterIDFacebookDBU3Eb__0_mCAB2E69F7B0C4C100793A3985821782C2100BA7F,
	U3CRegisterIDFacebookDBRequestU3Ed__16__ctor_m867A258F206771A1A569FAF8D7AF6C51A63CEBD8,
	U3CRegisterIDFacebookDBRequestU3Ed__16_System_IDisposable_Dispose_m04A0FF2517C79258C06833945C8F71DE5E657B56,
	U3CRegisterIDFacebookDBRequestU3Ed__16_MoveNext_mD5960C1CF3FBAFD16E3AABE7E8E86CB8D44F2CCF,
	U3CRegisterIDFacebookDBRequestU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29322345345B6C8721D0ED4C2BC4090D728247D3,
	U3CRegisterIDFacebookDBRequestU3Ed__16_System_Collections_IEnumerator_Reset_m4D03031BBAC45BC3ACB55BC760BD32AD9581DC1A,
	U3CRegisterIDFacebookDBRequestU3Ed__16_System_Collections_IEnumerator_get_Current_m9D3968578EB546A71F146AB01DAE53FAF20C8E4A,
	HighscoreList__ctor_mA132AA559D5DC10A1098059C68DB5CF8D768F414,
	HighscoreList__ctor_mBCB183351ECE001D708284B23F0A098C1CCF2037,
	U3CU3Ec__DisplayClass25_0__ctor_m557E504B7FE05F14A60766BA86F4EE0D3ADADB34,
	U3CU3Ec__DisplayClass25_0_U3CCreateAccountU3Eb__0_mC7B209A9B22F585A62EFA8FEF1F7C71923C63102,
	U3CSingInRequestU3Ed__27__ctor_mFD772B7F7014E65673F6D0B32559A534EE258E08,
	U3CSingInRequestU3Ed__27_System_IDisposable_Dispose_m032A9BB458CCFBEAEA9DA45DCDC1B0F9781768FB,
	U3CSingInRequestU3Ed__27_MoveNext_mE4C01CA2DD98D7D8709BA8C6A6F71049C56CA895,
	U3CSingInRequestU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DCF1A8D5F3197990B5DCC5BAA9FC818D61CA2E7,
	U3CSingInRequestU3Ed__27_System_Collections_IEnumerator_Reset_mAFAA3EDB5866D1BE566B31CD14C3A92BA718C10D,
	U3CSingInRequestU3Ed__27_System_Collections_IEnumerator_get_Current_m7DB5C73C6262533A72051C9F94F85AAA2A1838C9,
	U3CCreateAccountRequestU3Ed__28__ctor_mC00F7180AB29EF06665EC71A099184381B2DF0BD,
	U3CCreateAccountRequestU3Ed__28_System_IDisposable_Dispose_m3E2DB2C0F01AE7A95443356B29A7487EED1722A7,
	U3CCreateAccountRequestU3Ed__28_MoveNext_m7B07EB26381312DC8BAE766396303ADD7D10D670,
	U3CCreateAccountRequestU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAAA80CC99573ED5EEF39156AF8689C3F0561F36,
	U3CCreateAccountRequestU3Ed__28_System_Collections_IEnumerator_Reset_mCD0E0556A097E6803B1595832389D59BB32DFC65,
	U3CCreateAccountRequestU3Ed__28_System_Collections_IEnumerator_get_Current_m474359D2E25F766D9A45E476D2E83D8835676104,
	U3CU3Ec__cctor_m1982CC4A9C90DF44A8C51873316CC87123345D07,
	U3CU3Ec__ctor_m2E7C851469FCD6C3B9E32F3ED593345B7E8FF00D,
	U3CU3Ec_U3CIsValidEmailU3Eg__DomainMapperU7C34_0_mB56C6EC95DD3FC1AB9E76E44CC97584C186DFEFE,
	U3CIsNewUserU3Ed__38__ctor_m292D64C2D9BF05A04318B768A25706BC40467E1E,
	U3CIsNewUserU3Ed__38_System_IDisposable_Dispose_m83E56E21B82A070C6241D5DDDCD30AEC57C5282C,
	U3CIsNewUserU3Ed__38_MoveNext_m0727AB089A98711952845174FE9D3CA438997463,
	U3CIsNewUserU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBCB2E5130C1B3242C6DB823170697F37902971B6,
	U3CIsNewUserU3Ed__38_System_Collections_IEnumerator_Reset_m8EDA155E912A51D688DC33F998CAAE41907FD60C,
	U3CIsNewUserU3Ed__38_System_Collections_IEnumerator_get_Current_mB4A7927ECC5C636ACEE1196F317E0E6A8A11B168,
	U3CasteroidWaveU3Ed__6__ctor_mA02848E61E314DCBCDED68BF01C5DBAF15C0929C,
	U3CasteroidWaveU3Ed__6_System_IDisposable_Dispose_mC72A228BBF1C28678C7B3ADC69CDA06264461896,
	U3CasteroidWaveU3Ed__6_MoveNext_mD5247BF1C12ED6D9FAC6ADAAD3CC62D08FDF9D4C,
	U3CasteroidWaveU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB849820C30CF74E64546A5B13CF16574A318954,
	U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_Reset_m7C58EE19A20A0521F8628ED8E09271D1E0C6A9E8,
	U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_get_Current_mCB79F938AEB9DC895E91142D9870BFCB5DC9F337,
	U3CasteroidWaveU3Ed__8__ctor_mFD99860DDA9ED5CCC6CD9FD808220055D00218DF,
	U3CasteroidWaveU3Ed__8_System_IDisposable_Dispose_m3B539F9030BAA5E62C5FFF3214A6F96017B53790,
	U3CasteroidWaveU3Ed__8_MoveNext_m3FD0916C4F3E87F1601FB60EF7286FE8EBB16A69,
	U3CasteroidWaveU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BCF396F204C64A40E71311A2FF92242F887FE95,
	U3CasteroidWaveU3Ed__8_System_Collections_IEnumerator_Reset_m62E0107BA668007BCD270EB47B81BD5A4B124ADB,
	U3CasteroidWaveU3Ed__8_System_Collections_IEnumerator_get_Current_m2A582B935317E238D98E57AC29C217480A99D57B,
	U3CasteroidWaveU3Ed__6__ctor_mB978A1DE3E3B824A3AA47F8A73623F5AF89E0878,
	U3CasteroidWaveU3Ed__6_System_IDisposable_Dispose_mEEDB90603A74661AA7D53FC911CD93BB2E56E14C,
	U3CasteroidWaveU3Ed__6_MoveNext_mE947A506FC5B5F3B138EF900D76BD5007444573F,
	U3CasteroidWaveU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m382153D3C41EEA8A75109076D9AEE84F56E83B48,
	U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_Reset_m0FBB89120A0A6652328BEDFCEF8F9159DE833728,
	U3CasteroidWaveU3Ed__6_System_Collections_IEnumerator_get_Current_m1670BBF051ED11BDFEF2CC31E6300090A3EFCFFF,
	U3CTakeScreenshotU3Ed__4__ctor_m80CE40D404E301D3006E187D1895D040300F18EF,
	U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_m454F1C0A61C618628D75F23F4C0BB1901002F1C9,
	U3CTakeScreenshotU3Ed__4_MoveNext_m96965A488E2C6949389435D59E02FC009AAB2935,
	U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DAC71D34D1CFDB79E38D3A56A4E41CC437D51DC,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m1F0B12BBB8AA4BFD581AE55AC23524FA7A3FC60A,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m2A0C27075849E84A0F869BC66DA40117F5D7D6D5,
};
static const int32_t s_InvokerIndices[354] = 
{
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	4,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	28,
	28,
	23,
	28,
	23,
	28,
	23,
	23,
	23,
	32,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	28,
	23,
	28,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	10,
	9,
	116,
	14,
	14,
	23,
	23,
	168,
	28,
	23,
	23,
	168,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	28,
	23,
	9,
	26,
	26,
	26,
	94,
	94,
	26,
	26,
	14,
	23,
	3,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	290,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	10,
	10,
	10,
	1675,
	28,
	41,
	111,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	27,
	23,
	10,
	23,
	32,
	23,
	23,
	290,
	32,
	10,
	23,
	23,
	3,
	3,
	122,
	3,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	290,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	4,
	28,
	23,
	23,
	23,
	28,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1676,
	26,
	27,
	23,
	3,
	131,
	131,
	131,
	131,
	4,
	122,
	14,
	26,
	14,
	26,
	14,
	26,
	1115,
	1116,
	677,
	10,
	14,
	14,
	14,
	14,
	23,
	9,
	580,
	114,
	26,
	23,
	23,
	3,
	122,
	23,
	23,
	3,
	23,
	114,
	114,
	26,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1677,
	23,
	114,
	23,
	23,
	23,
	26,
	14,
	23,
	114,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	23,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	23,
	23,
	31,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	23,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	23,
	28,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	354,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
