﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000008 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x0000000C TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000010 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000012 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000015 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000016 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000017 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000018 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000001A System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001B System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001F System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000020 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000021 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000024 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000025 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000026 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000002E System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000002F System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000030 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000031 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000032 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000033 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000034 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000038 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000003D System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000003E System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000003F TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000040 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x00000041 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x00000042 System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x00000043 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x00000044 TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000045 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x00000046 System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x00000047 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000048 System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000049 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000004A TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000004B System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000004C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000004D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000004E System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000004F System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000050 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000051 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000052 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000053 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000054 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000055 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000056 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000057 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000058 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000059 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000005A System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000005B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000005C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000005D System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000005E System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000005F System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000060 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000061 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000062 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000063 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000064 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000065 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000066 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000067 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[103] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[103] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[32] = 
{
	{ 0x02000004, { 51, 4 } },
	{ 0x02000005, { 55, 9 } },
	{ 0x02000006, { 66, 7 } },
	{ 0x02000007, { 75, 10 } },
	{ 0x02000008, { 87, 11 } },
	{ 0x02000009, { 101, 9 } },
	{ 0x0200000A, { 113, 12 } },
	{ 0x0200000B, { 128, 1 } },
	{ 0x0200000C, { 129, 2 } },
	{ 0x0200000D, { 131, 6 } },
	{ 0x0200000E, { 137, 4 } },
	{ 0x0200000F, { 141, 21 } },
	{ 0x02000011, { 162, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 10 } },
	{ 0x06000006, { 20, 5 } },
	{ 0x06000007, { 25, 5 } },
	{ 0x06000008, { 30, 3 } },
	{ 0x06000009, { 33, 2 } },
	{ 0x0600000A, { 35, 1 } },
	{ 0x0600000B, { 36, 2 } },
	{ 0x0600000C, { 38, 4 } },
	{ 0x0600000D, { 42, 3 } },
	{ 0x0600000E, { 45, 1 } },
	{ 0x0600000F, { 46, 3 } },
	{ 0x06000010, { 49, 2 } },
	{ 0x06000020, { 64, 2 } },
	{ 0x06000025, { 73, 2 } },
	{ 0x0600002A, { 85, 2 } },
	{ 0x06000030, { 98, 3 } },
	{ 0x06000035, { 110, 3 } },
	{ 0x0600003A, { 125, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[164] = 
{
	{ (Il2CppRGCTXDataType)2, 14913 },
	{ (Il2CppRGCTXDataType)3, 10534 },
	{ (Il2CppRGCTXDataType)2, 14914 },
	{ (Il2CppRGCTXDataType)2, 14915 },
	{ (Il2CppRGCTXDataType)3, 10535 },
	{ (Il2CppRGCTXDataType)2, 14916 },
	{ (Il2CppRGCTXDataType)2, 14917 },
	{ (Il2CppRGCTXDataType)3, 10536 },
	{ (Il2CppRGCTXDataType)2, 14918 },
	{ (Il2CppRGCTXDataType)3, 10537 },
	{ (Il2CppRGCTXDataType)2, 14919 },
	{ (Il2CppRGCTXDataType)3, 10538 },
	{ (Il2CppRGCTXDataType)2, 14920 },
	{ (Il2CppRGCTXDataType)2, 14921 },
	{ (Il2CppRGCTXDataType)3, 10539 },
	{ (Il2CppRGCTXDataType)2, 14922 },
	{ (Il2CppRGCTXDataType)2, 14923 },
	{ (Il2CppRGCTXDataType)3, 10540 },
	{ (Il2CppRGCTXDataType)2, 14924 },
	{ (Il2CppRGCTXDataType)3, 10541 },
	{ (Il2CppRGCTXDataType)2, 14925 },
	{ (Il2CppRGCTXDataType)3, 10542 },
	{ (Il2CppRGCTXDataType)3, 10543 },
	{ (Il2CppRGCTXDataType)2, 11688 },
	{ (Il2CppRGCTXDataType)3, 10544 },
	{ (Il2CppRGCTXDataType)2, 14926 },
	{ (Il2CppRGCTXDataType)3, 10545 },
	{ (Il2CppRGCTXDataType)3, 10546 },
	{ (Il2CppRGCTXDataType)2, 11695 },
	{ (Il2CppRGCTXDataType)3, 10547 },
	{ (Il2CppRGCTXDataType)2, 14927 },
	{ (Il2CppRGCTXDataType)3, 10548 },
	{ (Il2CppRGCTXDataType)3, 10549 },
	{ (Il2CppRGCTXDataType)2, 11701 },
	{ (Il2CppRGCTXDataType)3, 10550 },
	{ (Il2CppRGCTXDataType)3, 10551 },
	{ (Il2CppRGCTXDataType)2, 14928 },
	{ (Il2CppRGCTXDataType)3, 10552 },
	{ (Il2CppRGCTXDataType)2, 14929 },
	{ (Il2CppRGCTXDataType)2, 14930 },
	{ (Il2CppRGCTXDataType)2, 11706 },
	{ (Il2CppRGCTXDataType)2, 14931 },
	{ (Il2CppRGCTXDataType)2, 11708 },
	{ (Il2CppRGCTXDataType)2, 14932 },
	{ (Il2CppRGCTXDataType)3, 10553 },
	{ (Il2CppRGCTXDataType)2, 11711 },
	{ (Il2CppRGCTXDataType)2, 11713 },
	{ (Il2CppRGCTXDataType)2, 14933 },
	{ (Il2CppRGCTXDataType)3, 10554 },
	{ (Il2CppRGCTXDataType)2, 14934 },
	{ (Il2CppRGCTXDataType)2, 11716 },
	{ (Il2CppRGCTXDataType)3, 10555 },
	{ (Il2CppRGCTXDataType)3, 10556 },
	{ (Il2CppRGCTXDataType)2, 11720 },
	{ (Il2CppRGCTXDataType)3, 10557 },
	{ (Il2CppRGCTXDataType)3, 10558 },
	{ (Il2CppRGCTXDataType)2, 11732 },
	{ (Il2CppRGCTXDataType)2, 14935 },
	{ (Il2CppRGCTXDataType)3, 10559 },
	{ (Il2CppRGCTXDataType)3, 10560 },
	{ (Il2CppRGCTXDataType)2, 11734 },
	{ (Il2CppRGCTXDataType)2, 14811 },
	{ (Il2CppRGCTXDataType)3, 10561 },
	{ (Il2CppRGCTXDataType)3, 10562 },
	{ (Il2CppRGCTXDataType)2, 14936 },
	{ (Il2CppRGCTXDataType)3, 10563 },
	{ (Il2CppRGCTXDataType)3, 10564 },
	{ (Il2CppRGCTXDataType)2, 11744 },
	{ (Il2CppRGCTXDataType)2, 14937 },
	{ (Il2CppRGCTXDataType)3, 10565 },
	{ (Il2CppRGCTXDataType)3, 10566 },
	{ (Il2CppRGCTXDataType)3, 10120 },
	{ (Il2CppRGCTXDataType)3, 10567 },
	{ (Il2CppRGCTXDataType)2, 14938 },
	{ (Il2CppRGCTXDataType)3, 10568 },
	{ (Il2CppRGCTXDataType)3, 10569 },
	{ (Il2CppRGCTXDataType)2, 11756 },
	{ (Il2CppRGCTXDataType)2, 14939 },
	{ (Il2CppRGCTXDataType)3, 10570 },
	{ (Il2CppRGCTXDataType)3, 10571 },
	{ (Il2CppRGCTXDataType)3, 10572 },
	{ (Il2CppRGCTXDataType)3, 10573 },
	{ (Il2CppRGCTXDataType)3, 10574 },
	{ (Il2CppRGCTXDataType)3, 10126 },
	{ (Il2CppRGCTXDataType)3, 10575 },
	{ (Il2CppRGCTXDataType)2, 14940 },
	{ (Il2CppRGCTXDataType)3, 10576 },
	{ (Il2CppRGCTXDataType)3, 10577 },
	{ (Il2CppRGCTXDataType)2, 11769 },
	{ (Il2CppRGCTXDataType)2, 14941 },
	{ (Il2CppRGCTXDataType)3, 10578 },
	{ (Il2CppRGCTXDataType)3, 10579 },
	{ (Il2CppRGCTXDataType)2, 11771 },
	{ (Il2CppRGCTXDataType)2, 14942 },
	{ (Il2CppRGCTXDataType)3, 10580 },
	{ (Il2CppRGCTXDataType)3, 10581 },
	{ (Il2CppRGCTXDataType)2, 14943 },
	{ (Il2CppRGCTXDataType)3, 10582 },
	{ (Il2CppRGCTXDataType)3, 10583 },
	{ (Il2CppRGCTXDataType)2, 14944 },
	{ (Il2CppRGCTXDataType)3, 10584 },
	{ (Il2CppRGCTXDataType)3, 10585 },
	{ (Il2CppRGCTXDataType)2, 11786 },
	{ (Il2CppRGCTXDataType)2, 14945 },
	{ (Il2CppRGCTXDataType)3, 10586 },
	{ (Il2CppRGCTXDataType)3, 10587 },
	{ (Il2CppRGCTXDataType)3, 10588 },
	{ (Il2CppRGCTXDataType)3, 10137 },
	{ (Il2CppRGCTXDataType)2, 14946 },
	{ (Il2CppRGCTXDataType)3, 10589 },
	{ (Il2CppRGCTXDataType)3, 10590 },
	{ (Il2CppRGCTXDataType)2, 14947 },
	{ (Il2CppRGCTXDataType)3, 10591 },
	{ (Il2CppRGCTXDataType)3, 10592 },
	{ (Il2CppRGCTXDataType)2, 11802 },
	{ (Il2CppRGCTXDataType)2, 14948 },
	{ (Il2CppRGCTXDataType)3, 10593 },
	{ (Il2CppRGCTXDataType)3, 10594 },
	{ (Il2CppRGCTXDataType)3, 10595 },
	{ (Il2CppRGCTXDataType)3, 10596 },
	{ (Il2CppRGCTXDataType)3, 10597 },
	{ (Il2CppRGCTXDataType)3, 10598 },
	{ (Il2CppRGCTXDataType)3, 10143 },
	{ (Il2CppRGCTXDataType)2, 14949 },
	{ (Il2CppRGCTXDataType)3, 10599 },
	{ (Il2CppRGCTXDataType)3, 10600 },
	{ (Il2CppRGCTXDataType)2, 14950 },
	{ (Il2CppRGCTXDataType)3, 10601 },
	{ (Il2CppRGCTXDataType)3, 10602 },
	{ (Il2CppRGCTXDataType)3, 10603 },
	{ (Il2CppRGCTXDataType)3, 10604 },
	{ (Il2CppRGCTXDataType)3, 10605 },
	{ (Il2CppRGCTXDataType)2, 11830 },
	{ (Il2CppRGCTXDataType)3, 10606 },
	{ (Il2CppRGCTXDataType)2, 14951 },
	{ (Il2CppRGCTXDataType)3, 10607 },
	{ (Il2CppRGCTXDataType)3, 10608 },
	{ (Il2CppRGCTXDataType)2, 14952 },
	{ (Il2CppRGCTXDataType)2, 11840 },
	{ (Il2CppRGCTXDataType)2, 11838 },
	{ (Il2CppRGCTXDataType)2, 14953 },
	{ (Il2CppRGCTXDataType)3, 10609 },
	{ (Il2CppRGCTXDataType)2, 14954 },
	{ (Il2CppRGCTXDataType)3, 10610 },
	{ (Il2CppRGCTXDataType)3, 10611 },
	{ (Il2CppRGCTXDataType)3, 10612 },
	{ (Il2CppRGCTXDataType)2, 11844 },
	{ (Il2CppRGCTXDataType)3, 10613 },
	{ (Il2CppRGCTXDataType)3, 10614 },
	{ (Il2CppRGCTXDataType)2, 11847 },
	{ (Il2CppRGCTXDataType)3, 10615 },
	{ (Il2CppRGCTXDataType)1, 14955 },
	{ (Il2CppRGCTXDataType)2, 11846 },
	{ (Il2CppRGCTXDataType)3, 10616 },
	{ (Il2CppRGCTXDataType)1, 11846 },
	{ (Il2CppRGCTXDataType)1, 11844 },
	{ (Il2CppRGCTXDataType)2, 14956 },
	{ (Il2CppRGCTXDataType)2, 11846 },
	{ (Il2CppRGCTXDataType)3, 10617 },
	{ (Il2CppRGCTXDataType)3, 10618 },
	{ (Il2CppRGCTXDataType)3, 10619 },
	{ (Il2CppRGCTXDataType)2, 11845 },
	{ (Il2CppRGCTXDataType)3, 10620 },
	{ (Il2CppRGCTXDataType)2, 11858 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	103,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	32,
	s_rgctxIndices,
	164,
	s_rgctxValues,
	NULL,
};
