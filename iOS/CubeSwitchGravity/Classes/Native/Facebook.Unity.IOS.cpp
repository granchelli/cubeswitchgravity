﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.IOS.IOSWrapper
struct IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;


struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t7D8656312CF9ECCFEE86FE1EDEDF7F60FF7B8A7C 
{
public:

public:
};


// System.Object


// Facebook.Unity.IOS.IOSWrapper
struct  IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C  : public RuntimeObject
{
public:

public:
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBInit(System.String,System.Boolean,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBInit_m73A461972CD6DA6FBEF5F1D9F4E9254F1E809514 (String_t* ___appId0, bool ___frictionlessRequests1, String_t* ___urlSuffix2, String_t* ___unityUserAgentSuffix3, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithReadPermissions(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithReadPermissions_m1C152188B7874AEF7D89AF60B117E5067CCE0C22 (int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithPublishPermissions(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithPublishPermissions_m55CB5F0007D3C64CAD33FD7F0AF3AA946C469B32 (int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogOut_m8309ED28CA838CA644BECC7122A743EDA9D80914 (const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetPushNotificationsDeviceTokenString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1ACB30F8415547BDD88079FC07DE875593562F41 (String_t* ___token0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetShareDialogMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetShareDialogMode_m1969574075BA2A2823AC2134A65FE760C699578D (int32_t ___mode0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBShareLink(System.Int32,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBShareLink_mBF4AF29F3004B562575A4B00EDF6927A7DBB158B (int32_t ___requestId0, String_t* ___contentURL1, String_t* ___contentTitle2, String_t* ___contentDescription3, String_t* ___photoURL4, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFeedShare_m92FB752493C3CE161DC05F4EB08ECF4F6D9603E5 (int32_t ___requestId0, String_t* ___toId1, String_t* ___link2, String_t* ___linkName3, String_t* ___linkCaption4, String_t* ___linkDescription5, String_t* ___picture6, String_t* ___mediaSource7, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppRequest_mC12553BDEB7401A9D53D5A226FEC76D8C66F0F11 (int32_t ___requestId0, String_t* ___message1, String_t* ___actionType2, String_t* ___objectId3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___to4, int32_t ___toLength5, String_t* ___filters6, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___excludeIds7, int32_t ___excludeIdsLength8, bool ___hasMaxRecipients9, int32_t ___maxRecipients10, String_t* ___data11, String_t* ___title12, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBOpenGamingServicesFriendFinder(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBOpenGamingServicesFriendFinder_mEEE93B70E6138C53DD77B13DAF61C58BD6AC9A2D (int32_t ___requestID0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsActivateApp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsActivateApp_m886B369E937973A94740F7C6B5418C146BE75027 (const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogEvent_mDC12B628F018C49CA0469CEA8504F8DAEF2D038A (String_t* ___logEvent0, double ___valueToSum1, int32_t ___numParams2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals4, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogPurchase_mFDEE1BBF22E213A15CC8444A28D02D4568D1865F (double ___logPurchase0, String_t* ___currency1, int32_t ___numParams2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals4, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mDBD018B1E0047E1D36F952235E148CC5CFF85ADC (bool ___limitEventUsage0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAutoLogAppEventsEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAutoLogAppEventsEnabled_mA56C8A8A2AD89B3CADA8C5D42E0AEE6D3D31C19B (bool ___autoLogAppEventsEnabled0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAdvertiserIDCollectionEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_mC5E07567FC71CDD6CD00951C190F0F32EDF99AD3 (bool ___advertiserIDCollectionEnabledID0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBGetAppLink(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBGetAppLink_mB419CD847C3F97472C60A5ECC3FD951CA2C62845 (int32_t ___requestID0, const RuntimeMethod* method);
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBSdkVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBSdkVersion_m5580A6DD80E0F840EA7A9114223FF85E02528F2D (const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetUserID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetUserID_m3F6DA7CC00BCEA167E21977412DEE2E12181D3E1 (String_t* ___userID0, const RuntimeMethod* method);
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBGetUserID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBGetUserID_m3272CC953583D0A16B55732B9A630FA6800DE8B2 (const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUpdateUserProperties(System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUpdateUserProperties_mBDE8E194CD48792427A9B2573F7D81C68ECF0365 (int32_t ___numParams0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys1, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals2, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetDataProcessingOptions(System.String[],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetDataProcessingOptions_mDA958C071487B6FF8DD90FE683563BF59ED73BFB (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___options0, int32_t ___numOptions1, int32_t ___country2, int32_t ___state3, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUploadImageToMediaLibrary_m6FBB5647FE4BC4690E4596123BA4D3F2A190701E (int32_t ___requestID0, String_t* ___caption1, String_t* ___imageUri2, bool ___shouldLaunchMediaDialog3, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUploadVideoToMediaLibrary(System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUploadVideoToMediaLibrary_m1EF1013FC33352A9041499903E67A321C416FDBC (int32_t ___requestID0, String_t* ___caption1, String_t* ___videoUri2, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFetchDeferredAppLink(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFetchDeferredAppLink_m577296A1DA96204DE40AFCCBA26662087F6D2C33 (int32_t ___requestID0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBRefreshCurrentAccessToken(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBRefreshCurrentAccessToken_mE5C7DBCF9EF6EAA319499F4BD1C3157BB8FAD0DE (int32_t ___requestID0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.IOS.IOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_Init_mF339D24A58CC8F0A2E9405A958DF42508F8BAC09 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, String_t* ___appId0, bool ___frictionlessRequests1, String_t* ___urlSuffix2, String_t* ___unityUserAgentSuffix3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___appId0;
		bool L_1 = ___frictionlessRequests1;
		String_t* L_2 = ___urlSuffix2;
		String_t* L_3 = ___unityUserAgentSuffix3;
		IOSWrapper_IOSFBInit_m73A461972CD6DA6FBEF5F1D9F4E9254F1E809514(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_LogInWithReadPermissions_m2B9035CE9C91DDA800E37E2D2FA5F90895B54B5F (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___scope1;
		IOSWrapper_IOSFBLogInWithReadPermissions_m1C152188B7874AEF7D89AF60B117E5067CCE0C22(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_LogInWithPublishPermissions_m470494418FBEB34CDB932AD86C44668D1CEB7315 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___scope1;
		IOSWrapper_IOSFBLogInWithPublishPermissions_m55CB5F0007D3C64CAD33FD7F0AF3AA946C469B32(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_LogOut_m142A34D4DE3BCC54BB90C34C82B9F5B55D0F7757 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, const RuntimeMethod* method)
{
	{
		IOSWrapper_IOSFBLogOut_m8309ED28CA838CA644BECC7122A743EDA9D80914(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::SetPushNotificationsDeviceTokenString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_SetPushNotificationsDeviceTokenString_m9818980A980056E5B0CCEC2BA704B62923B6DD45 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, String_t* ___token0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___token0;
		IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1ACB30F8415547BDD88079FC07DE875593562F41(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::SetShareDialogMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_SetShareDialogMode_m50891CBA598604EF3208BBDB3C7EE2F8FCDB1739 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		IOSWrapper_IOSFBSetShareDialogMode_m1969574075BA2A2823AC2134A65FE760C699578D(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_ShareLink_m7CE97BF6A46CA7A62FFA09398E5919332C2A13F5 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___contentURL1, String_t* ___contentTitle2, String_t* ___contentDescription3, String_t* ___photoURL4, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___contentURL1;
		String_t* L_2 = ___contentTitle2;
		String_t* L_3 = ___contentDescription3;
		String_t* L_4 = ___photoURL4;
		IOSWrapper_IOSFBShareLink_mBF4AF29F3004B562575A4B00EDF6927A7DBB158B(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FeedShare_mAC0D4F3E8F821160412EEF0E53B8A70FCBD22E60 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___toId1, String_t* ___link2, String_t* ___linkName3, String_t* ___linkCaption4, String_t* ___linkDescription5, String_t* ___picture6, String_t* ___mediaSource7, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___toId1;
		String_t* L_2 = ___link2;
		String_t* L_3 = ___linkName3;
		String_t* L_4 = ___linkCaption4;
		String_t* L_5 = ___linkDescription5;
		String_t* L_6 = ___picture6;
		String_t* L_7 = ___mediaSource7;
		IOSWrapper_IOSFBFeedShare_m92FB752493C3CE161DC05F4EB08ECF4F6D9603E5(L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_AppRequest_mEB1840B858A4DEC7C2BC66C5A58BDF160A643570 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___message1, String_t* ___actionType2, String_t* ___objectId3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___to4, int32_t ___toLength5, String_t* ___filters6, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___excludeIds7, int32_t ___excludeIdsLength8, bool ___hasMaxRecipients9, int32_t ___maxRecipients10, String_t* ___data11, String_t* ___title12, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___message1;
		String_t* L_2 = ___actionType2;
		String_t* L_3 = ___objectId3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = ___to4;
		int32_t L_5 = ___toLength5;
		String_t* L_6 = ___filters6;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = ___excludeIds7;
		int32_t L_8 = ___excludeIdsLength8;
		bool L_9 = ___hasMaxRecipients9;
		int32_t L_10 = ___maxRecipients10;
		String_t* L_11 = ___data11;
		String_t* L_12 = ___title12;
		IOSWrapper_IOSFBAppRequest_mC12553BDEB7401A9D53D5A226FEC76D8C66F0F11(L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::OpenFriendFinderDialog(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_OpenFriendFinderDialog_m86600B9C035DE8AC7D1D3CA63CF1AB4AD3B2A1BB (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBOpenGamingServicesFriendFinder_mEEE93B70E6138C53DD77B13DAF61C58BD6AC9A2D(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsActivateApp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FBAppEventsActivateApp_m43AB634055F36A4808C70ADCBF72E72DEFF40153 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, const RuntimeMethod* method)
{
	{
		IOSWrapper_IOSFBAppEventsActivateApp_m886B369E937973A94740F7C6B5418C146BE75027(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_LogAppEvent_m5AA0FF03116C046C37B1F9E405733030243D2D3B (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, String_t* ___logEvent0, double ___valueToSum1, int32_t ___numParams2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals4, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___logEvent0;
		double L_1 = ___valueToSum1;
		int32_t L_2 = ___numParams2;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = ___paramKeys3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = ___paramVals4;
		IOSWrapper_IOSFBAppEventsLogEvent_mDC12B628F018C49CA0469CEA8504F8DAEF2D038A(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_LogPurchaseAppEvent_mADD88626D53D4D32FC1F3A566D1E0BD12C2FFD22 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, double ___logPurchase0, String_t* ___currency1, int32_t ___numParams2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals4, const RuntimeMethod* method)
{
	{
		double L_0 = ___logPurchase0;
		String_t* L_1 = ___currency1;
		int32_t L_2 = ___numParams2;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = ___paramKeys3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = ___paramVals4;
		IOSWrapper_IOSFBAppEventsLogPurchase_mFDEE1BBF22E213A15CC8444A28D02D4568D1865F(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FBAppEventsSetLimitEventUsage_m7D7CA6FE002262FAC89548162083EBDF9725BF6D (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, bool ___limitEventUsage0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___limitEventUsage0;
		IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mDBD018B1E0047E1D36F952235E148CC5CFF85ADC(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAutoLogAppEventsEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FBAutoLogAppEventsEnabled_m6DB85A295479F013A1FD66EBB175C7A86A2AA522 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, bool ___autoLogAppEventsEnabled0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___autoLogAppEventsEnabled0;
		IOSWrapper_IOSFBAutoLogAppEventsEnabled_mA56C8A8A2AD89B3CADA8C5D42E0AEE6D3D31C19B(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAdvertiserIDCollectionEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FBAdvertiserIDCollectionEnabled_mB761EC8C70206BDBFF0B8158E44385C904D92FA5 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, bool ___advertiserIDCollectionEnabled0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___advertiserIDCollectionEnabled0;
		IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_mC5E07567FC71CDD6CD00951C190F0F32EDF99AD3(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::GetAppLink(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_GetAppLink_m0B3AA47D81EB8F19B8ED58147A255F8CA987556C (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBGetAppLink_mB419CD847C3F97472C60A5ECC3FD951CA2C62845(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Facebook.Unity.IOS.IOSWrapper::FBSdkVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWrapper_FBSdkVersion_m55B4B48EB58CBA43C653DB36839226D5D6115527 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = IOSWrapper_IOSFBSdkVersion_m5580A6DD80E0F840EA7A9114223FF85E02528F2D(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBSetUserID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FBSetUserID_mE30218BC90AA4FC20422E3E4655D2828854140FC (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, String_t* ___userID0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___userID0;
		IOSWrapper_IOSFBSetUserID_m3F6DA7CC00BCEA167E21977412DEE2E12181D3E1(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Facebook.Unity.IOS.IOSWrapper::FBGetUserID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWrapper_FBGetUserID_m33EF0E135AAAEED9885EA265A14D391E0D601217 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = IOSWrapper_IOSFBGetUserID_m3272CC953583D0A16B55732B9A630FA6800DE8B2(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::UpdateUserProperties(System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_UpdateUserProperties_mAB17D1D4011132AB0301DE9152C6CE689EF24FC3 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___numParams0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys1, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___numParams0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = ___paramKeys1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = ___paramVals2;
		IOSWrapper_IOSFBUpdateUserProperties_mBDE8E194CD48792427A9B2573F7D81C68ECF0365(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::SetDataProcessingOptions(System.String[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_SetDataProcessingOptions_m557CD86173C4ED68EE4D0EA779179E35B892863A (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___options0, int32_t ___country1, int32_t ___state2, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = ___options0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = ___options0;
		NullCheck(L_1);
		int32_t L_2 = ___country1;
		int32_t L_3 = ___state2;
		IOSWrapper_IOSFBSetDataProcessingOptions_mDA958C071487B6FF8DD90FE683563BF59ED73BFB(L_0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::UploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_UploadImageToMediaLibrary_m3EFA9A44CAF61BDD4B3F932B5E6CDEC0D28A0230 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___caption1, String_t* ___imageUri2, bool ___shouldLaunchMediaDialog3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___caption1;
		String_t* L_2 = ___imageUri2;
		bool L_3 = ___shouldLaunchMediaDialog3;
		IOSWrapper_IOSFBUploadImageToMediaLibrary_m6FBB5647FE4BC4690E4596123BA4D3F2A190701E(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::UploadVideoToMediaLibrary(System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_UploadVideoToMediaLibrary_mA5B01C8BA5FA8F50C24D70AD216553D865E6CFAA (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, String_t* ___caption1, String_t* ___videoUri2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___caption1;
		String_t* L_2 = ___videoUri2;
		IOSWrapper_IOSFBUploadVideoToMediaLibrary_m1EF1013FC33352A9041499903E67A321C416FDBC(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FetchDeferredAppLink(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_FetchDeferredAppLink_mCB0AC641CAD4F4F637567295000A12686FE704A2 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBFetchDeferredAppLink_m577296A1DA96204DE40AFCCBA26662087F6D2C33(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::RefreshCurrentAccessToken(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_RefreshCurrentAccessToken_mA3F332A96AFD3D0C5091F88EC711B6F2AE6B60DA (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBRefreshCurrentAccessToken_mE5C7DBCF9EF6EAA319499F4BD1C3157BB8FAD0DE(L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBInit(char*, int32_t, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBInit(System.String,System.Boolean,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBInit_m73A461972CD6DA6FBEF5F1D9F4E9254F1E809514 (String_t* ___appId0, bool ___frictionlessRequests1, String_t* ___urlSuffix2, String_t* ___unityUserAgentSuffix3, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, char*, char*);

	// Marshaling of parameter '___appId0' to native representation
	char* ____appId0_marshaled = NULL;
	____appId0_marshaled = il2cpp_codegen_marshal_string(___appId0);

	// Marshaling of parameter '___urlSuffix2' to native representation
	char* ____urlSuffix2_marshaled = NULL;
	____urlSuffix2_marshaled = il2cpp_codegen_marshal_string(___urlSuffix2);

	// Marshaling of parameter '___unityUserAgentSuffix3' to native representation
	char* ____unityUserAgentSuffix3_marshaled = NULL;
	____unityUserAgentSuffix3_marshaled = il2cpp_codegen_marshal_string(___unityUserAgentSuffix3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBInit)(____appId0_marshaled, static_cast<int32_t>(___frictionlessRequests1), ____urlSuffix2_marshaled, ____unityUserAgentSuffix3_marshaled);

	// Marshaling cleanup of parameter '___appId0' native representation
	il2cpp_codegen_marshal_free(____appId0_marshaled);
	____appId0_marshaled = NULL;

	// Marshaling cleanup of parameter '___urlSuffix2' native representation
	il2cpp_codegen_marshal_free(____urlSuffix2_marshaled);
	____urlSuffix2_marshaled = NULL;

	// Marshaling cleanup of parameter '___unityUserAgentSuffix3' native representation
	il2cpp_codegen_marshal_free(____unityUserAgentSuffix3_marshaled);
	____unityUserAgentSuffix3_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBLogInWithReadPermissions(int32_t, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithReadPermissions(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithReadPermissions_m1C152188B7874AEF7D89AF60B117E5067CCE0C22 (int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);

	// Marshaling of parameter '___scope1' to native representation
	char* ____scope1_marshaled = NULL;
	____scope1_marshaled = il2cpp_codegen_marshal_string(___scope1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBLogInWithReadPermissions)(___requestId0, ____scope1_marshaled);

	// Marshaling cleanup of parameter '___scope1' native representation
	il2cpp_codegen_marshal_free(____scope1_marshaled);
	____scope1_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBLogInWithPublishPermissions(int32_t, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithPublishPermissions(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithPublishPermissions_m55CB5F0007D3C64CAD33FD7F0AF3AA946C469B32 (int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);

	// Marshaling of parameter '___scope1' to native representation
	char* ____scope1_marshaled = NULL;
	____scope1_marshaled = il2cpp_codegen_marshal_string(___scope1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBLogInWithPublishPermissions)(___requestId0, ____scope1_marshaled);

	// Marshaling cleanup of parameter '___scope1' native representation
	il2cpp_codegen_marshal_free(____scope1_marshaled);
	____scope1_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBLogOut();
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogOut_m8309ED28CA838CA644BECC7122A743EDA9D80914 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBLogOut)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBSetPushNotificationsDeviceTokenString(char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetPushNotificationsDeviceTokenString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1ACB30F8415547BDD88079FC07DE875593562F41 (String_t* ___token0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___token0' to native representation
	char* ____token0_marshaled = NULL;
	____token0_marshaled = il2cpp_codegen_marshal_string(___token0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetPushNotificationsDeviceTokenString)(____token0_marshaled);

	// Marshaling cleanup of parameter '___token0' native representation
	il2cpp_codegen_marshal_free(____token0_marshaled);
	____token0_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBSetShareDialogMode(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetShareDialogMode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetShareDialogMode_m1969574075BA2A2823AC2134A65FE760C699578D (int32_t ___mode0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetShareDialogMode)(___mode0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBShareLink(int32_t, char*, char*, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBShareLink(System.Int32,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBShareLink_mBF4AF29F3004B562575A4B00EDF6927A7DBB158B (int32_t ___requestId0, String_t* ___contentURL1, String_t* ___contentTitle2, String_t* ___contentDescription3, String_t* ___photoURL4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char*);

	// Marshaling of parameter '___contentURL1' to native representation
	char* ____contentURL1_marshaled = NULL;
	____contentURL1_marshaled = il2cpp_codegen_marshal_string(___contentURL1);

	// Marshaling of parameter '___contentTitle2' to native representation
	char* ____contentTitle2_marshaled = NULL;
	____contentTitle2_marshaled = il2cpp_codegen_marshal_string(___contentTitle2);

	// Marshaling of parameter '___contentDescription3' to native representation
	char* ____contentDescription3_marshaled = NULL;
	____contentDescription3_marshaled = il2cpp_codegen_marshal_string(___contentDescription3);

	// Marshaling of parameter '___photoURL4' to native representation
	char* ____photoURL4_marshaled = NULL;
	____photoURL4_marshaled = il2cpp_codegen_marshal_string(___photoURL4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBShareLink)(___requestId0, ____contentURL1_marshaled, ____contentTitle2_marshaled, ____contentDescription3_marshaled, ____photoURL4_marshaled);

	// Marshaling cleanup of parameter '___contentURL1' native representation
	il2cpp_codegen_marshal_free(____contentURL1_marshaled);
	____contentURL1_marshaled = NULL;

	// Marshaling cleanup of parameter '___contentTitle2' native representation
	il2cpp_codegen_marshal_free(____contentTitle2_marshaled);
	____contentTitle2_marshaled = NULL;

	// Marshaling cleanup of parameter '___contentDescription3' native representation
	il2cpp_codegen_marshal_free(____contentDescription3_marshaled);
	____contentDescription3_marshaled = NULL;

	// Marshaling cleanup of parameter '___photoURL4' native representation
	il2cpp_codegen_marshal_free(____photoURL4_marshaled);
	____photoURL4_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBFeedShare(int32_t, char*, char*, char*, char*, char*, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFeedShare_m92FB752493C3CE161DC05F4EB08ECF4F6D9603E5 (int32_t ___requestId0, String_t* ___toId1, String_t* ___link2, String_t* ___linkName3, String_t* ___linkCaption4, String_t* ___linkDescription5, String_t* ___picture6, String_t* ___mediaSource7, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char*, char*, char*, char*);

	// Marshaling of parameter '___toId1' to native representation
	char* ____toId1_marshaled = NULL;
	____toId1_marshaled = il2cpp_codegen_marshal_string(___toId1);

	// Marshaling of parameter '___link2' to native representation
	char* ____link2_marshaled = NULL;
	____link2_marshaled = il2cpp_codegen_marshal_string(___link2);

	// Marshaling of parameter '___linkName3' to native representation
	char* ____linkName3_marshaled = NULL;
	____linkName3_marshaled = il2cpp_codegen_marshal_string(___linkName3);

	// Marshaling of parameter '___linkCaption4' to native representation
	char* ____linkCaption4_marshaled = NULL;
	____linkCaption4_marshaled = il2cpp_codegen_marshal_string(___linkCaption4);

	// Marshaling of parameter '___linkDescription5' to native representation
	char* ____linkDescription5_marshaled = NULL;
	____linkDescription5_marshaled = il2cpp_codegen_marshal_string(___linkDescription5);

	// Marshaling of parameter '___picture6' to native representation
	char* ____picture6_marshaled = NULL;
	____picture6_marshaled = il2cpp_codegen_marshal_string(___picture6);

	// Marshaling of parameter '___mediaSource7' to native representation
	char* ____mediaSource7_marshaled = NULL;
	____mediaSource7_marshaled = il2cpp_codegen_marshal_string(___mediaSource7);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBFeedShare)(___requestId0, ____toId1_marshaled, ____link2_marshaled, ____linkName3_marshaled, ____linkCaption4_marshaled, ____linkDescription5_marshaled, ____picture6_marshaled, ____mediaSource7_marshaled);

	// Marshaling cleanup of parameter '___toId1' native representation
	il2cpp_codegen_marshal_free(____toId1_marshaled);
	____toId1_marshaled = NULL;

	// Marshaling cleanup of parameter '___link2' native representation
	il2cpp_codegen_marshal_free(____link2_marshaled);
	____link2_marshaled = NULL;

	// Marshaling cleanup of parameter '___linkName3' native representation
	il2cpp_codegen_marshal_free(____linkName3_marshaled);
	____linkName3_marshaled = NULL;

	// Marshaling cleanup of parameter '___linkCaption4' native representation
	il2cpp_codegen_marshal_free(____linkCaption4_marshaled);
	____linkCaption4_marshaled = NULL;

	// Marshaling cleanup of parameter '___linkDescription5' native representation
	il2cpp_codegen_marshal_free(____linkDescription5_marshaled);
	____linkDescription5_marshaled = NULL;

	// Marshaling cleanup of parameter '___picture6' native representation
	il2cpp_codegen_marshal_free(____picture6_marshaled);
	____picture6_marshaled = NULL;

	// Marshaling cleanup of parameter '___mediaSource7' native representation
	il2cpp_codegen_marshal_free(____mediaSource7_marshaled);
	____mediaSource7_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAppRequest(int32_t, char*, char*, char*, char**, int32_t, char*, char**, int32_t, int32_t, int32_t, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppRequest_mC12553BDEB7401A9D53D5A226FEC76D8C66F0F11 (int32_t ___requestId0, String_t* ___message1, String_t* ___actionType2, String_t* ___objectId3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___to4, int32_t ___toLength5, String_t* ___filters6, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___excludeIds7, int32_t ___excludeIdsLength8, bool ___hasMaxRecipients9, int32_t ___maxRecipients10, String_t* ___data11, String_t* ___title12, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char**, int32_t, char*, char**, int32_t, int32_t, int32_t, char*, char*);

	// Marshaling of parameter '___message1' to native representation
	char* ____message1_marshaled = NULL;
	____message1_marshaled = il2cpp_codegen_marshal_string(___message1);

	// Marshaling of parameter '___actionType2' to native representation
	char* ____actionType2_marshaled = NULL;
	____actionType2_marshaled = il2cpp_codegen_marshal_string(___actionType2);

	// Marshaling of parameter '___objectId3' to native representation
	char* ____objectId3_marshaled = NULL;
	____objectId3_marshaled = il2cpp_codegen_marshal_string(___objectId3);

	// Marshaling of parameter '___to4' to native representation
	char** ____to4_marshaled = NULL;
	if (___to4 != NULL)
	{
		il2cpp_array_size_t ____to4_Length = (___to4)->max_length;
		____to4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____to4_Length + 1);
		(____to4_marshaled)[____to4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____to4_Length); i++)
		{
			(____to4_marshaled)[i] = il2cpp_codegen_marshal_string((___to4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____to4_marshaled = NULL;
	}

	// Marshaling of parameter '___filters6' to native representation
	char* ____filters6_marshaled = NULL;
	____filters6_marshaled = il2cpp_codegen_marshal_string(___filters6);

	// Marshaling of parameter '___excludeIds7' to native representation
	char** ____excludeIds7_marshaled = NULL;
	if (___excludeIds7 != NULL)
	{
		il2cpp_array_size_t ____excludeIds7_Length = (___excludeIds7)->max_length;
		____excludeIds7_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____excludeIds7_Length + 1);
		(____excludeIds7_marshaled)[____excludeIds7_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____excludeIds7_Length); i++)
		{
			(____excludeIds7_marshaled)[i] = il2cpp_codegen_marshal_string((___excludeIds7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____excludeIds7_marshaled = NULL;
	}

	// Marshaling of parameter '___data11' to native representation
	char* ____data11_marshaled = NULL;
	____data11_marshaled = il2cpp_codegen_marshal_string(___data11);

	// Marshaling of parameter '___title12' to native representation
	char* ____title12_marshaled = NULL;
	____title12_marshaled = il2cpp_codegen_marshal_string(___title12);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppRequest)(___requestId0, ____message1_marshaled, ____actionType2_marshaled, ____objectId3_marshaled, ____to4_marshaled, ___toLength5, ____filters6_marshaled, ____excludeIds7_marshaled, ___excludeIdsLength8, static_cast<int32_t>(___hasMaxRecipients9), ___maxRecipients10, ____data11_marshaled, ____title12_marshaled);

	// Marshaling cleanup of parameter '___message1' native representation
	il2cpp_codegen_marshal_free(____message1_marshaled);
	____message1_marshaled = NULL;

	// Marshaling cleanup of parameter '___actionType2' native representation
	il2cpp_codegen_marshal_free(____actionType2_marshaled);
	____actionType2_marshaled = NULL;

	// Marshaling cleanup of parameter '___objectId3' native representation
	il2cpp_codegen_marshal_free(____objectId3_marshaled);
	____objectId3_marshaled = NULL;

	// Marshaling cleanup of parameter '___to4' native representation
	if (____to4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____to4_marshaled_CleanupLoopCount = (___to4 != NULL) ? (___to4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____to4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____to4_marshaled)[i]);
			(____to4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____to4_marshaled);
		____to4_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___filters6' native representation
	il2cpp_codegen_marshal_free(____filters6_marshaled);
	____filters6_marshaled = NULL;

	// Marshaling cleanup of parameter '___excludeIds7' native representation
	if (____excludeIds7_marshaled != NULL)
	{
		const il2cpp_array_size_t ____excludeIds7_marshaled_CleanupLoopCount = (___excludeIds7 != NULL) ? (___excludeIds7)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____excludeIds7_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____excludeIds7_marshaled)[i]);
			(____excludeIds7_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____excludeIds7_marshaled);
		____excludeIds7_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___data11' native representation
	il2cpp_codegen_marshal_free(____data11_marshaled);
	____data11_marshaled = NULL;

	// Marshaling cleanup of parameter '___title12' native representation
	il2cpp_codegen_marshal_free(____title12_marshaled);
	____title12_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAppEventsActivateApp();
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsActivateApp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsActivateApp_m886B369E937973A94740F7C6B5418C146BE75027 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsActivateApp)();

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAppEventsLogEvent(char*, double, int32_t, char**, char**);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogEvent_mDC12B628F018C49CA0469CEA8504F8DAEF2D038A (String_t* ___logEvent0, double ___valueToSum1, int32_t ___numParams2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, double, int32_t, char**, char**);

	// Marshaling of parameter '___logEvent0' to native representation
	char* ____logEvent0_marshaled = NULL;
	____logEvent0_marshaled = il2cpp_codegen_marshal_string(___logEvent0);

	// Marshaling of parameter '___paramKeys3' to native representation
	char** ____paramKeys3_marshaled = NULL;
	if (___paramKeys3 != NULL)
	{
		il2cpp_array_size_t ____paramKeys3_Length = (___paramKeys3)->max_length;
		____paramKeys3_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramKeys3_Length + 1);
		(____paramKeys3_marshaled)[____paramKeys3_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_Length); i++)
		{
			(____paramKeys3_marshaled)[i] = il2cpp_codegen_marshal_string((___paramKeys3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling of parameter '___paramVals4' to native representation
	char** ____paramVals4_marshaled = NULL;
	if (___paramVals4 != NULL)
	{
		il2cpp_array_size_t ____paramVals4_Length = (___paramVals4)->max_length;
		____paramVals4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramVals4_Length + 1);
		(____paramVals4_marshaled)[____paramVals4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_Length); i++)
		{
			(____paramVals4_marshaled)[i] = il2cpp_codegen_marshal_string((___paramVals4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramVals4_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsLogEvent)(____logEvent0_marshaled, ___valueToSum1, ___numParams2, ____paramKeys3_marshaled, ____paramVals4_marshaled);

	// Marshaling cleanup of parameter '___logEvent0' native representation
	il2cpp_codegen_marshal_free(____logEvent0_marshaled);
	____logEvent0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramKeys3' native representation
	if (____paramKeys3_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramKeys3_marshaled_CleanupLoopCount = (___paramKeys3 != NULL) ? (___paramKeys3)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramKeys3_marshaled)[i]);
			(____paramKeys3_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramKeys3_marshaled);
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___paramVals4' native representation
	if (____paramVals4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramVals4_marshaled_CleanupLoopCount = (___paramVals4 != NULL) ? (___paramVals4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramVals4_marshaled)[i]);
			(____paramVals4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramVals4_marshaled);
		____paramVals4_marshaled = NULL;
	}

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAppEventsLogPurchase(double, char*, int32_t, char**, char**);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogPurchase_mFDEE1BBF22E213A15CC8444A28D02D4568D1865F (double ___logPurchase0, String_t* ___currency1, int32_t ___numParams2, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys3, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (double, char*, int32_t, char**, char**);

	// Marshaling of parameter '___currency1' to native representation
	char* ____currency1_marshaled = NULL;
	____currency1_marshaled = il2cpp_codegen_marshal_string(___currency1);

	// Marshaling of parameter '___paramKeys3' to native representation
	char** ____paramKeys3_marshaled = NULL;
	if (___paramKeys3 != NULL)
	{
		il2cpp_array_size_t ____paramKeys3_Length = (___paramKeys3)->max_length;
		____paramKeys3_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramKeys3_Length + 1);
		(____paramKeys3_marshaled)[____paramKeys3_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_Length); i++)
		{
			(____paramKeys3_marshaled)[i] = il2cpp_codegen_marshal_string((___paramKeys3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling of parameter '___paramVals4' to native representation
	char** ____paramVals4_marshaled = NULL;
	if (___paramVals4 != NULL)
	{
		il2cpp_array_size_t ____paramVals4_Length = (___paramVals4)->max_length;
		____paramVals4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramVals4_Length + 1);
		(____paramVals4_marshaled)[____paramVals4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_Length); i++)
		{
			(____paramVals4_marshaled)[i] = il2cpp_codegen_marshal_string((___paramVals4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramVals4_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsLogPurchase)(___logPurchase0, ____currency1_marshaled, ___numParams2, ____paramKeys3_marshaled, ____paramVals4_marshaled);

	// Marshaling cleanup of parameter '___currency1' native representation
	il2cpp_codegen_marshal_free(____currency1_marshaled);
	____currency1_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramKeys3' native representation
	if (____paramKeys3_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramKeys3_marshaled_CleanupLoopCount = (___paramKeys3 != NULL) ? (___paramKeys3)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramKeys3_marshaled)[i]);
			(____paramKeys3_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramKeys3_marshaled);
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___paramVals4' native representation
	if (____paramVals4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramVals4_marshaled_CleanupLoopCount = (___paramVals4 != NULL) ? (___paramVals4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramVals4_marshaled)[i]);
			(____paramVals4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramVals4_marshaled);
		____paramVals4_marshaled = NULL;
	}

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAppEventsSetLimitEventUsage(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mDBD018B1E0047E1D36F952235E148CC5CFF85ADC (bool ___limitEventUsage0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsSetLimitEventUsage)(static_cast<int32_t>(___limitEventUsage0));

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAutoLogAppEventsEnabled(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAutoLogAppEventsEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAutoLogAppEventsEnabled_mA56C8A8A2AD89B3CADA8C5D42E0AEE6D3D31C19B (bool ___autoLogAppEventsEnabled0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAutoLogAppEventsEnabled)(static_cast<int32_t>(___autoLogAppEventsEnabled0));

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBAdvertiserIDCollectionEnabled(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAdvertiserIDCollectionEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_mC5E07567FC71CDD6CD00951C190F0F32EDF99AD3 (bool ___advertiserIDCollectionEnabledID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAdvertiserIDCollectionEnabled)(static_cast<int32_t>(___advertiserIDCollectionEnabledID0));

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBGetAppLink(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBGetAppLink(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBGetAppLink_mB419CD847C3F97472C60A5ECC3FD951CA2C62845 (int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBGetAppLink)(___requestID0);

}
IL2CPP_EXTERN_C char* DEFAULT_CALL IOSFBSdkVersion();
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBSdkVersion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBSdkVersion_m5580A6DD80E0F840EA7A9114223FF85E02528F2D (const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(IOSFBSdkVersion)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBFetchDeferredAppLink(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFetchDeferredAppLink(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFetchDeferredAppLink_m577296A1DA96204DE40AFCCBA26662087F6D2C33 (int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBFetchDeferredAppLink)(___requestID0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBRefreshCurrentAccessToken(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBRefreshCurrentAccessToken(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBRefreshCurrentAccessToken_mE5C7DBCF9EF6EAA319499F4BD1C3157BB8FAD0DE (int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBRefreshCurrentAccessToken)(___requestID0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBSetUserID(char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetUserID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetUserID_m3F6DA7CC00BCEA167E21977412DEE2E12181D3E1 (String_t* ___userID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___userID0' to native representation
	char* ____userID0_marshaled = NULL;
	____userID0_marshaled = il2cpp_codegen_marshal_string(___userID0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetUserID)(____userID0_marshaled);

	// Marshaling cleanup of parameter '___userID0' native representation
	il2cpp_codegen_marshal_free(____userID0_marshaled);
	____userID0_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBOpenGamingServicesFriendFinder(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBOpenGamingServicesFriendFinder(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBOpenGamingServicesFriendFinder_mEEE93B70E6138C53DD77B13DAF61C58BD6AC9A2D (int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBOpenGamingServicesFriendFinder)(___requestID0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBUploadImageToMediaLibrary(int32_t, char*, char*, int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUploadImageToMediaLibrary(System.Int32,System.String,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUploadImageToMediaLibrary_m6FBB5647FE4BC4690E4596123BA4D3F2A190701E (int32_t ___requestID0, String_t* ___caption1, String_t* ___imageUri2, bool ___shouldLaunchMediaDialog3, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, int32_t);

	// Marshaling of parameter '___caption1' to native representation
	char* ____caption1_marshaled = NULL;
	____caption1_marshaled = il2cpp_codegen_marshal_string(___caption1);

	// Marshaling of parameter '___imageUri2' to native representation
	char* ____imageUri2_marshaled = NULL;
	____imageUri2_marshaled = il2cpp_codegen_marshal_string(___imageUri2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBUploadImageToMediaLibrary)(___requestID0, ____caption1_marshaled, ____imageUri2_marshaled, static_cast<int32_t>(___shouldLaunchMediaDialog3));

	// Marshaling cleanup of parameter '___caption1' native representation
	il2cpp_codegen_marshal_free(____caption1_marshaled);
	____caption1_marshaled = NULL;

	// Marshaling cleanup of parameter '___imageUri2' native representation
	il2cpp_codegen_marshal_free(____imageUri2_marshaled);
	____imageUri2_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBUploadVideoToMediaLibrary(int32_t, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUploadVideoToMediaLibrary(System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUploadVideoToMediaLibrary_m1EF1013FC33352A9041499903E67A321C416FDBC (int32_t ___requestID0, String_t* ___caption1, String_t* ___videoUri2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*);

	// Marshaling of parameter '___caption1' to native representation
	char* ____caption1_marshaled = NULL;
	____caption1_marshaled = il2cpp_codegen_marshal_string(___caption1);

	// Marshaling of parameter '___videoUri2' to native representation
	char* ____videoUri2_marshaled = NULL;
	____videoUri2_marshaled = il2cpp_codegen_marshal_string(___videoUri2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBUploadVideoToMediaLibrary)(___requestID0, ____caption1_marshaled, ____videoUri2_marshaled);

	// Marshaling cleanup of parameter '___caption1' native representation
	il2cpp_codegen_marshal_free(____caption1_marshaled);
	____caption1_marshaled = NULL;

	// Marshaling cleanup of parameter '___videoUri2' native representation
	il2cpp_codegen_marshal_free(____videoUri2_marshaled);
	____videoUri2_marshaled = NULL;

}
IL2CPP_EXTERN_C char* DEFAULT_CALL IOSFBGetUserID();
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBGetUserID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBGetUserID_m3272CC953583D0A16B55732B9A630FA6800DE8B2 (const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(IOSFBGetUserID)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBSetDataProcessingOptions(char**, int32_t, int32_t, int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetDataProcessingOptions(System.String[],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetDataProcessingOptions_mDA958C071487B6FF8DD90FE683563BF59ED73BFB (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___options0, int32_t ___numOptions1, int32_t ___country2, int32_t ___state3, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char**, int32_t, int32_t, int32_t);

	// Marshaling of parameter '___options0' to native representation
	char** ____options0_marshaled = NULL;
	if (___options0 != NULL)
	{
		il2cpp_array_size_t ____options0_Length = (___options0)->max_length;
		____options0_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____options0_Length + 1);
		(____options0_marshaled)[____options0_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____options0_Length); i++)
		{
			(____options0_marshaled)[i] = il2cpp_codegen_marshal_string((___options0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____options0_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetDataProcessingOptions)(____options0_marshaled, ___numOptions1, ___country2, ___state3);

	// Marshaling cleanup of parameter '___options0' native representation
	if (____options0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____options0_marshaled_CleanupLoopCount = (___options0 != NULL) ? (___options0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____options0_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____options0_marshaled)[i]);
			(____options0_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____options0_marshaled);
		____options0_marshaled = NULL;
	}

}
IL2CPP_EXTERN_C void DEFAULT_CALL IOSFBUpdateUserProperties(int32_t, char**, char**);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUpdateUserProperties(System.Int32,System.String[],System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUpdateUserProperties_mBDE8E194CD48792427A9B2573F7D81C68ECF0365 (int32_t ___numParams0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramKeys1, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___paramVals2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char**, char**);

	// Marshaling of parameter '___paramKeys1' to native representation
	char** ____paramKeys1_marshaled = NULL;
	if (___paramKeys1 != NULL)
	{
		il2cpp_array_size_t ____paramKeys1_Length = (___paramKeys1)->max_length;
		____paramKeys1_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramKeys1_Length + 1);
		(____paramKeys1_marshaled)[____paramKeys1_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys1_Length); i++)
		{
			(____paramKeys1_marshaled)[i] = il2cpp_codegen_marshal_string((___paramKeys1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramKeys1_marshaled = NULL;
	}

	// Marshaling of parameter '___paramVals2' to native representation
	char** ____paramVals2_marshaled = NULL;
	if (___paramVals2 != NULL)
	{
		il2cpp_array_size_t ____paramVals2_Length = (___paramVals2)->max_length;
		____paramVals2_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramVals2_Length + 1);
		(____paramVals2_marshaled)[____paramVals2_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals2_Length); i++)
		{
			(____paramVals2_marshaled)[i] = il2cpp_codegen_marshal_string((___paramVals2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramVals2_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBUpdateUserProperties)(___numParams0, ____paramKeys1_marshaled, ____paramVals2_marshaled);

	// Marshaling cleanup of parameter '___paramKeys1' native representation
	if (____paramKeys1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramKeys1_marshaled_CleanupLoopCount = (___paramKeys1 != NULL) ? (___paramKeys1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys1_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramKeys1_marshaled)[i]);
			(____paramKeys1_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramKeys1_marshaled);
		____paramKeys1_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___paramVals2' native representation
	if (____paramVals2_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramVals2_marshaled_CleanupLoopCount = (___paramVals2 != NULL) ? (___paramVals2)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals2_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramVals2_marshaled)[i]);
			(____paramVals2_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramVals2_marshaled);
		____paramVals2_marshaled = NULL;
	}

}
// System.Void Facebook.Unity.IOS.IOSWrapper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWrapper__ctor_m7422353F3B57D9F3069E82D8C7CD5257834AA437 (IOSWrapper_t29F40CD1D61D1BAC28199414A70911FF407F695C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
